﻿=======================================================================
Visual Studio 2005 (vc80) sp1 For Win7 安装前必读

copyright 2014, cheungmine, all rights reserved.
=======================================================================
**** CE 智能设备组件依赖的DLL必须部署到 Windows, 否则组件无法注册。****

---- MD5校验:
VS80sp1-KB926604-X86-CHS.exe=69f29e40f1aa83fb1c3c3c85e08a442f

---- 修改OS设置:
1~3.管理工具->本地安全策略->软件限制策略
4.如果里面没有项目列出,就右键新建一个.
5.右边 对象类型,右键 "强制"->属性.
6.对话框中下面的那个,应用用户改成"除开管理员"的选项.
7.重启机器

*** 用管理员用户安装vs2005 sp1,并且不会验证文件是否匹配.
安装完,请改回默认选项.或删除刚才新建的软件限制策略项目.

--- 安装次序:
1) VisualStudio2005_Setup.iso
用 UltraISO Extract到磁盘上安装

2) VS80sp1-KB926604-X86-CHS.exe

3) VS80sp1-KB932230-X86-CHS.exe

--- 然后安装 Windows Mobile SDK
4) Windows Mobile 6 Professional SDK Refresh.msi

5) Windows Mobile 6.5.3 Professional DTK.msi

--- 然后安装 WTL for vc80 向导:
6) WTL80_sf.exe
	$(PATH)/AppWiz/
            wscript.exe vc80.js
	$(PATH)/AppWizCE/
            wscript.exe vc80.js
	$(PATH)/AppWizMobile/
            wscript.exe vc80.js
    *** 此步失败, 不影响使用

--- 下面的hotfix解决下面的问题:
7) VS80sp1-KB949009-X86-INTL.exe
corelibc.lib(armsecgs.obj) : fatal error LNK1103: debugging information corrupt; recompile module
问题描述在:
http://social.msdn.microsoft.com/Forums/en-US/2857d97d-f8c2-4390-97e0-a1be54348b52/corelibclibarmsecgsobj-fatal-error-lnk1103-debugging-information-corrupt-recompile-module?forum=vssmartdevicesnative

在下面的地址下载:VS80sp1-KB949009-X86-INTL.exe
https://connect.microsoft.com/VisualStudio/Downloads/DownloadDetails.aspx?DownloadID=18623

-- WTL CE
8) 创建WTL For CE工程,报错: atlapp.h 找不到
工具->选项:
项目和解决方案:VC++目录
平台:
Windows Mobile 6
Windows Mobile 6.5.3
包含文件:C:\WTL80\include


=======================================================================
 打开工程

=======================================================================
Using VS2005+sp1 to open solution:

   ./MapCtl/CRMapCtl/CRMapCtl.sln



