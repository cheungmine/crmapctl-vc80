// FilePage.h
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(_FILE_PAGE_H__)
#define _FILE_PAGE_H__

#pragma once

// CFilePage: Current image file propertes 

class CFilePage :
    public CPropertyPageImpl<CFilePage>,
    public CDialogResize<CFilePage>
{
public:
    enum { IDD = IDD_PROP_FILE };

    CString m_sPath;

    CFilePage(LPCTSTR sPath) : m_sPath(sPath) { }

    BEGIN_DLGRESIZE_MAP(CMoveDlg)
        BEGIN_DLGRESIZE_GROUP()
            DLGRESIZE_CONTROL(IDC_TYPENAME_H, DLSZ_MOVE_X | DLSZ_MOVE_Y)
            DLGRESIZE_CONTROL(IDC_TYPENAME, DLSZ_MOVE_X | DLSZ_MOVE_Y)
            DLGRESIZE_CONTROL(IDC_FILEICON, DLSZ_MOVE_X | DLSZ_MOVE_Y)
            DLGRESIZE_CONTROL(IDC_FILELOCATION_H, DLSZ_MOVE_X | DLSZ_MOVE_Y)
            DLGRESIZE_CONTROL(IDC_FILELOCATION, DLSZ_MOVE_X | DLSZ_MOVE_Y)
            DLGRESIZE_CONTROL(IDC_FILENAME_H, DLSZ_MOVE_X | DLSZ_MOVE_Y)
            DLGRESIZE_CONTROL(IDC_FILENAME, DLSZ_MOVE_X | DLSZ_MOVE_Y)
            DLGRESIZE_CONTROL(IDC_FILESIZE_H, DLSZ_MOVE_X | DLSZ_MOVE_Y)
            DLGRESIZE_CONTROL(IDC_FILESIZE, DLSZ_MOVE_X | DLSZ_MOVE_Y)
            DLGRESIZE_CONTROL(IDC_FILEDATE_H, DLSZ_MOVE_X | DLSZ_MOVE_Y)
            DLGRESIZE_CONTROL(IDC_FILEDATE, DLSZ_MOVE_X | DLSZ_MOVE_Y)
            DLGRESIZE_CONTROL(IDC_FILEATTRIB_H, DLSZ_MOVE_X | DLSZ_MOVE_Y)
            DLGRESIZE_CONTROL(IDC_FILEATTRIB, DLSZ_MOVE_X | DLSZ_MOVE_Y)
        END_DLGRESIZE_GROUP()
    END_DLGRESIZE_MAP()

    BEGIN_MSG_MAP(CFilePage)
        MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
        MESSAGE_HANDLER(WM_SIZE, CDialogResize<CFilePage>::OnSize)
        CHAIN_MSG_MAP(CPropertyPageImpl<CFilePage>)
    END_MSG_MAP()

    LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
    {
        ATLASSERT(!m_sPath.IsEmpty()); 

        CString s;

        SHFILEINFO sfi;
        ::SHGetFileInfo(m_sPath, 0, &sfi, sizeof(sfi), SHGFI_ICON | SHGFI_TYPENAME);
        SendDlgItemMessage(IDC_FILEICON, STM_SETIMAGE, IMAGE_ICON, (LPARAM)sfi.hIcon);
        SetDlgItemText(IDC_TYPENAME, sfi.szTypeName);
        
        s = m_sPath.Left(m_sPath.ReverseFind(L'\\'));

        SetDlgItemText(IDC_FILELOCATION, s);
        SetDlgItemText(IDC_FILENAME, m_sPath.Mid(m_sPath.ReverseFind(L'\\') + 1));

        CFindFile ff;
        ff.FindFile(m_sPath);
        SetDlgItemInt(IDC_FILESIZE, (UINT)ff.GetFileSize());

        FILETIME ftim;
        ff.GetCreationTime(&ftim);
        FormatFileTime(&ftim, s);
        SetDlgItemText(IDC_FILEDATE, s);

        s.Empty();
        if (ff.MatchesMask(FILE_ATTRIBUTE_ENCRYPTED)) {
            s += L"Encrypted, ";
        }
        if (ff.IsCompressed()) {
            s += L"Compressed, ";
        }
        if (ff.IsArchived()) {
            s += L"Archive, ";
        }
        if (ff.IsReadOnly()) {
            s += L"Read-only";
        }
        
        if (s.Find(L", ", s.GetLength() - 2) != -1) {
            s = s.Left(s.GetLength() - 2);
        }
        SetDlgItemText(IDC_FILEATTRIB, s);

        DlgResize_Init(FALSE);
        return TRUE;
    }

// Implementation 
    void FormatFileTime(FILETIME *pftim, CString& sDateTime)
    {
        SYSTEMTIME stim;
        CString s;
        ::FileTimeToSystemTime(pftim, &stim);
        ::GetDateFormat(LOCALE_USER_DEFAULT, 0, &stim, NULL, sDateTime.GetBuffer(40), 40);
        sDateTime.ReleaseBuffer();
        ::GetTimeFormat(LOCALE_USER_DEFAULT, TIME_NOSECONDS, &stim, NULL, s.GetBuffer(40), 40);
        s.ReleaseBuffer();
        sDateTime += L"  " + s;
    }

};

#endif // _FILE_PAGE_H__
