// MoveDlg.h
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(_MOVE_DLG_H__)
#define _MOVE_DLG_H__

#pragma once

// CMoveDlg :  Called by CRegisterDlg to move MapDev.exe to \Windows folder

class CMoveDlg : public  CStdDialogResizeImpl<CMoveDlg,SHIDIF_FULLSCREENNOMENUBAR>
{
public:
    CString m_sAppPath;
    CString m_sApp;

    enum { IDD = IDD_MOVE };

    typedef CStdDialogResizeImpl<CMoveDlg,SHIDIF_FULLSCREENNOMENUBAR> baseClass;

    BEGIN_DLGRESIZE_MAP(CMoveDlg)
        BEGIN_DLGRESIZE_GROUP()
            DLGRESIZE_CONTROL(IDC_FILELOCATION_H, DLSZ_SIZE_X | DLSZ_SIZE_Y)
            DLGRESIZE_CONTROL(IDC_FILELOCATION, DLSZ_SIZE_X | DLSZ_MOVE_Y)
            DLGRESIZE_CONTROL(IDC_INFOSTATIC, DLSZ_SIZE_X | DLSZ_SIZE_Y)
            DLGRESIZE_CONTROL(IDC_SHORTCUT, DLSZ_SIZE_X | DLSZ_MOVE_Y)
            DLGRESIZE_CONTROL(IDC_MOVE_T, DLSZ_MOVE_X | DLSZ_MOVE_Y)
            DLGRESIZE_CONTROL(IDC_MOVE, DLSZ_MOVE_X | DLSZ_MOVE_Y)
            DLGRESIZE_CONTROL(IDCANCEL, DLSZ_MOVE_X | DLSZ_MOVE_Y)
        END_DLGRESIZE_GROUP()
    END_DLGRESIZE_MAP()

    BEGIN_MSG_MAP(CMoveDlg)
        COMMAND_HANDLER(IDC_MOVE, BN_CLICKED, OnMove)
        MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
        CHAIN_MSG_MAP(baseClass)
    END_MSG_MAP()
        
    LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
    {
        SHDoneButton(m_hWnd, SHDB_HIDE);

        GetModuleFileName(NULL, m_sAppPath.GetBuffer(MAX_PATH+1), MAX_PATH);
        m_sAppPath.ReleaseBuffer();

        SetDlgItemText(IDC_FILELOCATION, m_sAppPath);
        m_sApp = m_sAppPath.Mid(m_sAppPath.ReverseFind(L'\\') + 1);
        
        CheckDlgButton(IDC_SHORTCUT, TRUE);

        return bHandled=FALSE;
    }

// Move operation
    LRESULT OnMove(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
    {
        CString sDest = L"\\Windows\\" + m_sApp;
        if (::MoveFile(m_sAppPath, sDest))	{
            if (IsDlgButtonChecked(IDC_SHORTCUT)) {
                m_sAppPath.Replace(L".exe", L".lnk");
                if (!::SHCreateShortcut((LPTSTR)(LPCTSTR)m_sAppPath, (LPTSTR)(LPCTSTR)sDest))
                    AtlMessageBox(m_hWnd, L"Cannot create shortcut to MapDev.", 
                        IDR_MAINFRAME, MB_OK | MB_ICONWARNING);
            }
            EndDialog(IDOK);
        }
        else
            AtlMessageBox(m_hWnd, L"Cannot move MapDev.exe to \\Windows folder.", 
                IDR_MAINFRAME, MB_OK | MB_ICONERROR);
        return 0;
    }
};


#endif // _MOVE_DLG_H__
