// ViewPage.h
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(_VIEW_PAGE_H__)
#define _VIEW_PAGE_H__

#pragma once

// CViewPage: Current view properties 

class CViewPage :
    public CPropertyPageImpl<CViewPage>
{
public:

    CMapDevView& m_rview;
    
    CViewPage(CMapDevView& rview) : m_rview(rview){}

    enum { IDD = IDD_PROP_VIEW };

    BEGIN_MSG_MAP(CViewPage)
        MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
        CHAIN_MSG_MAP(CPropertyPageImpl<CViewPage>)
    END_MSG_MAP()

    LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
    {
        CString s;
        s.Format(_T("%.2f"), m_rview.GetZoom());
        SetDlgItemText(IDC_ZOOM, s);
        s.Format(_T("(h) %d x (v) %d"), m_rview.m_sizeAll.cx, m_rview.m_sizeAll.cy);
        SetDlgItemText(IDC_IMAGESIZE, s);

        CStatic sImg = GetDlgItem(IDC_VIEW);
        CRect rectImg;
        sImg.GetWindowRect(rectImg);
        double zoom = max((double)m_rview.GetScrollSize().cx / rectImg.Width(),
                        (double)m_rview.GetScrollSize().cy / rectImg.Height());
        CBitmapHandle hbm = AtlCopyBitmap(m_rview.m_bmp, m_rview.GetScrollSize() / zoom, true);

// Draw view rectangle in image 
        CDC dc = CreateCompatibleDC(NULL);
        dc.SelectStockBrush(HOLLOW_BRUSH);
        HBITMAP bmpOld = dc.SelectBitmap(hbm)
            ;
        CRect rect(CPoint(0, 0), m_rview.m_sizeClient);
        m_rview.WndtoTrue(rect);
        rect = CRect((CPoint)(CSize(rect.TopLeft()) / zoom), rect.Size() / zoom);
        CPen pen;
        pen.CreatePen(PS_SOLID, 2, RGB(255, 0,  0));
        HPEN penOld=dc.SelectPen(pen);
        dc.Rectangle(rect);

        dc.SelectPen(penOld);
        dc.SelectBitmap(bmpOld);
        sImg.SetBitmap(hbm);
        return TRUE;
    }

};

#endif // _VIEW_PAGE_H__
