// stdafx.h
//    include file for standard system include files,
//    or project specific include files that are used frequently,
//    but are changed infrequently.
//
/////////////////////////////////////////////////////////////////////////////

// Change these values to use different versions
#define WINVER		0x0420

#if defined(CHSINT_CE60_ARMV4I)
    #undef _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA
#else
    #define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA
#endif

#define _WIN32_WCE_AYGSHELL 1

#include <atlbase.h>
#include <atlapp.h>

/// ??atlapp.h
///
///#ifndef _WIN32_WCE /// cheungmine>>
///namespace ATL
///{
///  inline HRESULT CComModule::RegisterClassObjects(DWORD /*dwClsContext*/, DWORD /*dwFlags*/) throw()
///  { return E_NOTIMPL; }
///  inline HRESULT CComModule::RevokeClassObjects() throw()
///  { return E_NOTIMPL; }
///}; // namespace ATL
///#endif             /// <<cheungmine


extern CAppModule _Module;

#if defined(DEBUG) && !defined(_DEBUG)
    #define _DEBUG
#endif

#include <atlcom.h>
#include <atlhost.h>
#include <atlwin.h>
#include <atlctl.h>

#ifdef CHSINT_CE60_ARMV4I
    #ifdef _WIN32_IE
        #undef _WIN32_IE
        #define _WIN32_IE 0x0400
    #endif
#endif

#if defined(CHSINT_CE60_ARMV4I)
    #define TPCSHELL_H_NOT_INCLUDED
#else
    #include <tpcshell.h>
#endif

#include <aygshell.h>
#pragma comment(lib, "aygshell.lib")
