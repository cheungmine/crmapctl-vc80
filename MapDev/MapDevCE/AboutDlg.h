// AboutDlg.h
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(_ABOUT_DLG_H__)
#define _ABOUT_DLG_H__

#pragma once

// CAboutDlg 

class CAboutDlg :
    public CStdSimpleDialogResizeImpl<CAboutDlg,
        IDD_ABOUTBOX, SHIDIF_DONEBUTTON | SHIDIF_FULLSCREENNOMENUBAR>
{
public:
    BEGIN_DLGRESIZE_MAP(CAboutDlg)
        BEGIN_DLGRESIZE_GROUP()
            DLGRESIZE_CONTROL(IDC_HEAD, DLSZ_MOVE_X | DLSZ_MOVE_Y)
            DLGRESIZE_CONTROL(IDC_APPICON, DLSZ_MOVE_X | DLSZ_MOVE_Y)
            DLGRESIZE_CONTROL(IDC_INFOSTATIC, DLSZ_SIZE_X | DLSZ_SIZE_Y)
        END_DLGRESIZE_GROUP()
    END_DLGRESIZE_MAP()
};


#endif // _ABOUT_DLG_H__
