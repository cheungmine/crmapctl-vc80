// RegisterDlg.h
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(_REGISTER_DLG_H__)
#define _REGISTER_DLG_H__

#pragma once

#if defined(WIN32_PLATFORM_WFSP)
    // Smartphone2003
    #undef _WTL_CE_DRA
#endif

#if defined(CHSINT_CE60_ARMV4I)
    // CHSINT SDK CE6.0
    #undef _WTL_CE_DRA
    #define STD_SIMPLE_DIALOG
#endif

// CRegisterDlg: Register MapDev.exe as standard program for image files

#ifdef _WTL_CE_DRA
class CRegisterDlg :
    #ifdef STD_SIMPLE_DIALOG
        public  CStdSimpleDialogResizeImpl<CRegisterDlg,
            SHIDIF_DONEBUTTON | SHIDIF_FULLSCREENNOMENUBAR>
    #else
        public  CStdOrientedDialogImpl<CRegisterDlg,
            SHIDIF_DONEBUTTON | SHIDIF_FULLSCREENNOMENUBAR>
    #endif
#else
class CRegisterDlg :
    #ifdef STD_SIMPLE_DIALOG
        public  CStdSimpleDialogResizeImpl<CRegisterDlg,
            SHIDIF_DONEBUTTON | SHIDIF_FULLSCREENNOMENUBAR>
    #else
        public  CStdDialogResizeImpl<CRegisterDlg,
            SHIDIF_DONEBUTTON | SHIDIF_FULLSCREENNOMENUBAR>
    #endif
#endif
{
public:

#ifdef _WTL_CE_DRA
	typedef CStdOrientedDialogImpl<CRegisterDlg, SHIDIF_DONEBUTTON | SHIDIF_FULLSCREENNOMENUBAR> baseClass;
	enum {IDD = IDD_REGISTER, IDD_LANDSCAPE = IDD_REGISTER_L};
#else
    #ifdef STD_SIMPLE_DIALOG
        typedef CStdSimpleDialogResizeImpl<CRegisterDlg, SHIDIF_DONEBUTTON | SHIDIF_FULLSCREENNOMENUBAR> baseClass;
	        enum {IDD = IDD_REGISTER};
    #else
	    typedef CStdDialogResizeImpl<CRegisterDlg, SHIDIF_DONEBUTTON | SHIDIF_FULLSCREENNOMENUBAR> baseClass;
	        enum {IDD = IDD_REGISTER};
    #endif
#endif // _WTL_CE_DRA

	CString m_sAppPath;
	CString m_sApp;

// Image type enumeration
	enum ImageType { BMP = IDC_BMP, JPG, PNG, GIF } ;

// Implementation
// Helper class: image command registry key
	class CImageTypeKey : public CRegKey
	{
	public:
		CString m_sCmd;
		DWORD size;

		CImageTypeKey(ImageType typ) : size(MAX_PATH)
		{
			CString sKey = GetTypeString(typ);
			sKey += L"\\Shell\\Open\\Command";
			Open(HKEY_CLASSES_ROOT, sKey);
		}

		LPCTSTR GetTypeString(ImageType typ)
		{
			switch (typ) {
			case BMP:
                return L"bmpimage"; 
			case JPG:
                return L"jpegimage"; 
			case PNG:
                return L"pngimage"; 
			case GIF:
                return L"gifimage"; 
			default:
                ATLASSERT(FALSE);
                return NULL;
			}
		}

		LPCTSTR GetCmd()
		{
#if _ATL_VER <0x800
			QueryValue(m_sCmd.GetBuffer(size), L"", &size);
#else
			QueryStringValue(L"",  m_sCmd.GetBuffer(size), &size);
#endif // _ATL_VER <0x800
			m_sCmd.ReleaseBuffer();
			return m_sCmd;
		}

		void SetCmd(LPCTSTR sCmd)
		{
#if _ATL_VER <0x800
			SetValue(sCmd, L"");
#else
			SetStringValue(L"", sCmd);
#endif // _ATL_VER <0x800
		}
	};

// Image type file extension
		LPCTSTR GetExtString(ImageType typ)
		{
			switch (typ) {
			case BMP:
                return L".bmp"; ;
			case JPG:
                return L".jpg"; 
			case PNG:
                return L".png"; 
			case GIF:
                return L".gif"; 
			default:
                ATLASSERT(FALSE);
                return NULL;
			}
		}

// Image type registration status
	bool IsRegistered(ImageType typ)
	{
		CImageTypeKey key(typ);
		CString sCmd = key.GetCmd();
		return sCmd.Find(m_sApp) != -1 ;		
	}
	
// Image type registration-deregistration
	void Register(ImageType typ, BOOL bRegister)
	{
		CImageTypeKey key(typ);
		CString sOldCmd = key.GetCmd();
		CString sNewCmd = m_sAppPath;
		CAppInfoT<CMainFrame> info;

        if (bRegister) {
			sNewCmd += L" %1";
        } else {
			info.Restore(sNewCmd, key.GetTypeString(typ));
        }

		key.SetCmd(sNewCmd);
		
        if (bRegister) {
			info.Save(sOldCmd, key.GetTypeString(typ));
        } else {
			info.Delete(key.GetTypeString(typ));
        }
	}
	
#ifndef _WTL_CE_DRA
	BEGIN_DLGRESIZE_MAP(CMoveDlg)
		BEGIN_DLGRESIZE_GROUP()
			DLGRESIZE_CONTROL(IDC_INFOSTATIC, DLSZ_SIZE_X | DLSZ_SIZE_Y)
			DLGRESIZE_CONTROL(IDC_REGISTER_H, DLSZ_SIZE_X | DLSZ_SIZE_Y)
			DLGRESIZE_CONTROL(IDC_IBMP, DLSZ_MOVE_X | DLSZ_MOVE_Y)
			DLGRESIZE_CONTROL(IDC_BMP, DLSZ_MOVE_X | DLSZ_MOVE_Y)
			DLGRESIZE_CONTROL(IDC_IJPG, DLSZ_MOVE_X | DLSZ_MOVE_Y)
			DLGRESIZE_CONTROL(IDC_JPG, DLSZ_MOVE_X | DLSZ_MOVE_Y)
			DLGRESIZE_CONTROL(IDC_IPNG, DLSZ_MOVE_X | DLSZ_MOVE_Y)
			DLGRESIZE_CONTROL(IDC_PNG, DLSZ_MOVE_X | DLSZ_MOVE_Y)
			DLGRESIZE_CONTROL(IDC_IGIF, DLSZ_MOVE_X | DLSZ_MOVE_Y)
			DLGRESIZE_CONTROL(IDC_GIF, DLSZ_MOVE_X | DLSZ_MOVE_Y)
		END_DLGRESIZE_GROUP()
	END_DLGRESIZE_MAP()
#else
	void OnOrientation(DRA::DisplayMode mode)
	{
// Text controls re-initialization
		for(int iBtn = IDC_BMP ; iBtn <= IDC_GIF ; iBtn++)
		{
			SHFILEINFO sfi;
			::SHGetFileInfo(GetExtString((ImageType)iBtn), FILE_ATTRIBUTE_NORMAL, &sfi, sizeof(sfi), 
				SHGFI_USEFILEATTRIBUTES | SHGFI_TYPENAME );
			SetDlgItemText(iBtn, sfi.szTypeName);
			CheckDlgButton(iBtn, IsRegistered((ImageType)iBtn));
		}
	}
#endif // ndef _WTL_CE_DRA

	BEGIN_MSG_MAP(CRegisterDlg)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		COMMAND_RANGE_HANDLER(IDC_BMP, IDC_GIF, OnCheckType)
		CHAIN_MSG_MAP(baseClass)
	END_MSG_MAP()

// Dialog initialization
	LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
	{
		::GetModuleFileName(NULL, m_sAppPath.GetBuffer(MAX_PATH + 1), MAX_PATH);
		m_sAppPath.ReleaseBuffer();
		m_sApp = m_sAppPath.Mid(m_sAppPath.ReverseFind(L'\\') + 1);

		// Call CMoveDlg if MapDev.exe is not located in \Windows folder
		if (CString(L"\\Windows\\") + m_sApp != m_sAppPath)
		{
			CMoveDlg dlg;
            if (dlg.DoModal() == IDCANCEL) {
            #ifdef STD_SIMPLE_DIALOG
                EndDialog(m_hWnd, IDCANCEL);
            #else
				EndDialog(IDCANCEL);
            #endif
            }

			::GetModuleFileName(NULL, m_sAppPath.GetBuffer(MAX_PATH + 1), MAX_PATH);
			m_sAppPath.ReleaseBuffer();
		}

// Controls initialization: IDC_BMP, IDC_JPG etc... MUST be in sequence.
		for(int iBtn = IDC_BMP, iIcon = IDC_IBMP ; iBtn <= IDC_GIF ; iBtn++, iIcon++)
		{
			SHFILEINFO sfi;
			::SHGetFileInfo(GetExtString((ImageType)iBtn), FILE_ATTRIBUTE_NORMAL, &sfi, sizeof(sfi), 
				SHGFI_USEFILEATTRIBUTES | SHGFI_ICON | SHGFI_TYPENAME );
			SendDlgItemMessage(iIcon, STM_SETIMAGE, IMAGE_ICON, (LPARAM)sfi.hIcon);
			SetDlgItemText(iBtn, sfi.szTypeName);
			CheckDlgButton(iBtn, IsRegistered((ImageType)iBtn));
		}

		return bHandled = FALSE; // to prevent CDialogImplBaseT< TBase >::DialogProc settings
	}

// Operation
	LRESULT OnCheckType(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
	{
		Register((ImageType)wID, IsDlgButtonChecked(wID));
		return 0;
	}
};

#endif // _REGISTER_DLG_H__
