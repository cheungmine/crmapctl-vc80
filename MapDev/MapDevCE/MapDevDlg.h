// MapDevdlg.h : interface of the MapDev dialog
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(_MAP_DEV_DLG_H__)
#define _MAP_DEV_DLG_H__

#pragma once

// forward declaration
class CMainFrame;

#include "AboutDlg.h"
#include "MoveDlg.h"
#include "RegisterDlg.h"
#include "FilePage.h"
#include "ImagePage.h"
#include "ViewPage.h"
#include "ImageProperties.h"

#endif // _MAP_DEV_DLG_H__

