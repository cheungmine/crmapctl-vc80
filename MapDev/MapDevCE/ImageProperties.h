// ImageProperties.h
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(_IMAGE_PROPERTIES_H__)
#define _IMAGE_PROPERTIES_H__

#pragma once


class CImageProperties :
    public CPropertySheetImpl<CImageProperties>
{
public:
    CFilePage m_FilePage;
    CImagePage m_ImagePage;
    CViewPage m_ViewPage;

    CImageProperties(LPCTSTR sname, CMapDevView & rview) :
        m_FilePage(sname),
        m_ImagePage(rview.m_bmp),
        m_ViewPage(rview) 
    {
        SetTitle(rview.m_sImageName);

        if (*sname) {
            AddPage(m_FilePage);
        }
        AddPage(m_ImagePage);
        AddPage(m_ViewPage);
    }

    void OnSheetInitialized()
    {
        AtlCreateEmptyMenuBar(m_hWnd, false);
    }
};

#endif // _IMAGE_PROPERTIES_H__
