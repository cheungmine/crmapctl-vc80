// ImagePage.h
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(_IMAGE_PAGE_H__)
#define _IMAGE_PAGE_H__

#pragma once

// CImagePage: Current image properties 

class CImagePage :
    public CPropertyPageImpl<CImagePage>,
    public CDialogResize<CImagePage>
{
public:
    enum { IDD = IDD_PROP_IMAGE };
    CBitmapHandle m_bmp;
    
    CImagePage(HBITMAP hbmp) : m_bmp(hbmp) {}

    BEGIN_DLGRESIZE_MAP(CMoveDlg)
        BEGIN_DLGRESIZE_GROUP()
            DLGRESIZE_CONTROL(IDC_TYPENAME_H, DLSZ_MOVE_X | DLSZ_MOVE_Y)
            DLGRESIZE_CONTROL(IDC_TYPENAME, DLSZ_MOVE_X | DLSZ_MOVE_Y)
            DLGRESIZE_CONTROL(IDC_IMAGESIZE_H, DLSZ_MOVE_X | DLSZ_MOVE_Y)
            DLGRESIZE_CONTROL(IDC_IMAGESIZE, DLSZ_MOVE_X | DLSZ_MOVE_Y)
            DLGRESIZE_CONTROL(IDC_NUMCOLORS_H, DLSZ_MOVE_X | DLSZ_MOVE_Y)
            DLGRESIZE_CONTROL(IDC_NUMCOLORS, DLSZ_MOVE_X | DLSZ_MOVE_Y)
            DLGRESIZE_CONTROL(IDC_BITDEPTH_H, DLSZ_MOVE_X | DLSZ_MOVE_Y)
            DLGRESIZE_CONTROL(IDC_BITDEPTH, DLSZ_MOVE_X | DLSZ_MOVE_Y)
            DLGRESIZE_CONTROL(IDC_NUMBYTES_H, DLSZ_MOVE_X | DLSZ_MOVE_Y)
            DLGRESIZE_CONTROL(IDC_NUMBYTES, DLSZ_MOVE_X | DLSZ_MOVE_Y)
            DLGRESIZE_CONTROL(IDC_IMAGE, DLSZ_MOVE_X | DLSZ_MOVE_Y | DLSZ_REPAINT)
        END_DLGRESIZE_GROUP()
    END_DLGRESIZE_MAP()

    BEGIN_MSG_MAP(CImagePage)
        MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
        MESSAGE_HANDLER(WM_SIZE, CDialogResize<CImagePage>::OnSize)
        CHAIN_MSG_MAP(CPropertyPageImpl<CImagePage>)
    END_MSG_MAP()

    LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
    {
        ATLASSERT(!m_bmp.IsNull());

        DIBSECTION ds;
        ATLVERIFY(::GetObject(m_bmp, sizeof(DIBSECTION), &ds) == sizeof(DIBSECTION));

        CString s;
        s.Format(L"(h) %d x (v) %d", ds.dsBmih.biWidth, ds.dsBmih.biHeight);
        SetDlgItemText(IDC_IMAGESIZE, s);
        SetDlgItemInt(IDC_BITDEPTH, ds.dsBmih.biBitCount);
        SetDlgItemInt(IDC_NUMBYTES, ds.dsBmih.biSizeImage);
        SetDlgItemInt(IDC_NUMCOLORS, AtlGetDibNumColors(&ds.dsBmih));

        CStatic sImg = GetDlgItem(IDC_IMAGE);
        CRect rectImg;
        sImg.GetWindowRect(rectImg);
        CSize sizeImg(ds.dsBmih.biWidth, ds.dsBmih.biHeight);
        double fzoom = max((double)sizeImg.cx / rectImg.Width(),
                        (double)sizeImg.cy / rectImg.Height());

        CBitmapHandle hbm = AtlCopyBitmap(m_bmp, sizeImg / fzoom, true);
        sImg.SetBitmap(hbm);

        DlgResize_Init(FALSE);
        return TRUE;
    }
};

#endif // _IMAGE_PAGE_H__
