// Symbol.h : CSymbol ������

#pragma once

#include "Config.h"
#include "ItemType.h"

class CSymbols;

// CSymbol

class ATL_NO_VTABLE CSymbol :
	public CComObjectRootEx<CComSingleThreadModel>,
    public CComCoClass<CSymbol>,
	public ISupportErrorInfo,
	public IItemTypeDispatchImpl<ISymbols, ISymbol, &IID_ISymbol, &LIBID_CRMapCtlLib, 1, 0>
{
public:
	CSymbol()
	{
	}

DECLARE_NO_REGISTRY()


BEGIN_COM_MAP(CSymbol)
	COM_INTERFACE_ENTRY(ISymbol)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

};

//DEL:OBJECT_ENTRY_AUTO(__uuidof(Symbol), CSymbol)
