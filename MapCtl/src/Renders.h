// Renders.h : CRenders 的声明

#pragma once

#include "Config.h"
#include "CollType.h"
#include "Render.h"


typedef list< CAdapt< CComPtr<IRender> > > RenderCollType;

typedef CComEnumOnSTL<IEnumVARIANT, &IID_IEnumVARIANT, VARIANT, 
    _CopyVariantFromAdaptItf<IRender>,
    RenderCollType >
CComEnumVariantOnListOfRenders;

typedef ICollOnSTLListWithMapImpl<IDispatchImpl<IRenders, &IID_IRenders>,
    RenderCollType,
    IRender*,
    _CopyItfFromAdaptItf<IRender>,
    CComEnumVariantOnListOfRenders,
    std::wstring,
    CRender,
    IRender >
IRenderCollImpl;


typedef  COLL_ITER(vector, IRender)  RENDER_ITER;
typedef  COLL_RITER(vector, IRender) RENDER_RITER;

// 指定索引的指针
#define  RENDER_ITER_AT(it,index)  RENDER_ITER (##it) = m_coll.begin()+index


// CRenders

class ATL_NO_VTABLE CRenders :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CRenders>,
	public ISupportErrorInfo,
    public IRenderCollImpl
{
public:
	CRenders()
	{
	}

DECLARE_NO_REGISTRY()


BEGIN_COM_MAP(CRenders)
	COM_INTERFACE_ENTRY(IRenders)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

};

//DEL:OBJECT_ENTRY_AUTO(__uuidof(Renders), CRenders)
