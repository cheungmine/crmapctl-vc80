// PrintSettings.h : CPrintSettings ������

#pragma once

#include "Config.h"


// CPrintSettings

class ATL_NO_VTABLE CPrintSettings :
	public CComObjectRootEx<CComSingleThreadModel>,
    public CComCoClass<CPrintSettings>,
	public ISupportErrorInfo,
	public IDispatchImpl<IPrintSettings, &IID_IPrintSettings, &LIBID_CRMapCtlLib, 1, 0>
{
public:
	CPrintSettings()
	{
	}

DECLARE_NO_REGISTRY()


BEGIN_COM_MAP(CPrintSettings)
	COM_INTERFACE_ENTRY(IPrintSettings)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid)
    {
	    static const IID* arr[] = 
	    {
		    &IID_IPrintSettings
	    };

	    for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	    {
		    if (InlineIsEqualGUID(*arr[i],riid))
			    return S_OK;
	    }
	    return S_FALSE;
    }


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

};

//DEL:OBJECT_ENTRY_AUTO(__uuidof(PrintSettings), CPrintSettings)
