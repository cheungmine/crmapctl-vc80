// RegsvrCE.h
//
#ifndef _REGSVR_CE_H__
#define _REGSVR_CE_H__


#pragma comment(linker, "/nodefaultlib:libc.lib")
#pragma comment(linker, "/nodefaultlib:libcd.lib")

// 注意 - 这个值与作为目标的 Windows CE OS 版本的关联性并不强
#define WINVER _WIN32_WCE

#include <ceconfig.h>
#if defined(WIN32_PLATFORM_PSPC) || defined(WIN32_PLATFORM_WFSP)
#define SHELL_AYGSHELL
#endif

#ifdef _CE_DCOM
#define _ATL_APARTMENT_THREADED
#endif

#ifdef SHELL_AYGSHELL
#include <aygshell.h>
#pragma comment(lib, "aygshell.lib") 
#endif // SHELL_AYGSHELL

#include <stdio.h>
#include <tchar.h>

#if defined(WIN32_PLATFORM_PSPC) || defined(WIN32_PLATFORM_WFSP)
#ifndef _DEVICE_RESOLUTION_AWARE
#define _DEVICE_RESOLUTION_AWARE
#endif
#endif

#ifdef _DEVICE_RESOLUTION_AWARE
#include "DeviceResolutionAware.h"
#endif

#if _WIN32_WCE < 0x500 && ( defined(WIN32_PLATFORM_PSPC) || defined(WIN32_PLATFORM_WFSP) )
    #pragma comment(lib, "ccrtrtti.lib")
    #ifdef _X86_	
        #if defined(_DEBUG)
            #pragma comment(lib, "libcmtx86d.lib")
        #else
            #pragma comment(lib, "libcmtx86.lib")
        #endif
    #endif
#endif

#include <altcecrt.h>
#include <windows.h>
#include <commctrl.h>

#define DLLREG_PROC     L"DllRegisterServer"
#define DLLUNREG_PROC   L"DllUnregisterServer"

typedef HRESULT(*pfnDllRegisterProc)(void);
typedef HRESULT(*pfnDllUnregisterProc)(void);

#define REGCE_SUCCESS  0

#define REGCE_ERROR_LOADLIBRARY  (-100)

#define REGCE_ERROR_GET_DLLREG_PROC  (-101)
#define REGCE_ERROR_CALL_DLLREG_PROC  (-102)

#define REGCE_ERROR_GET_DLLUNREG_PROC  (-103)
#define REGCE_ERROR_CALL_DLLUNREG_PROC  (-104)


//
// liblog4c
//
#define LOG_CATEGORY_NAME "crmapctl-wce"

#include <log4c-wrapper.h>

#if defined(LOG4C_ENABLED)
    // WinCE
    #if defined(_DEBUG) || defined(DEBUG)
        #if defined(WM6_5_3)
            #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/Windows Mobile 6.5.3 Professional DTK (ARMV4I)/Debug/liblog4c.lib")
        #elif defined(WM6)
            #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/Windows Mobile 6 Professional SDK (ARMV4I)/Debug/liblog4c.lib")
        #elif defined(PPC2003)
            #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/Pocket PC 2003 (ARMV4)/Debug/liblog4c.lib")
        #elif defined(SMP2003)
            #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/Smartphone 2003 (ARMV4)/Debug/liblog4c.lib")
        #elif defined(CE500_ARMV4I)
            #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/STANDARDSDK_500 (ARMV4I)/Debug/liblog4c.lib")
        #elif defined(CE500_MIPSII)
            #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/STANDARDSDK_500 (MIPSII)/Debug/liblog4c.lib")
        #elif defined(CE500_MIPSII_FP)
            #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/STANDARDSDK_500 (MIPSII_FP)/Debug/liblog4c.lib")
        #elif defined(CE500_MIPSIV)
            #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/STANDARDSDK_500 (MIPSIV)/Debug/liblog4c.lib")
        #elif defined(CE500_MIPSIV_FP)
            #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/STANDARDSDK_500 (MIPSIV_FP)/Debug/liblog4c.lib")
        #elif defined(CE500_SH4)
            #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/STANDARDSDK_500 (SH4)/Debug/liblog4c.lib")
        #elif defined(CE500_X86)
            #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/STANDARDSDK_500 (x86)/Debug/liblog4c.lib")
        #elif defined(CE6_ARMV4I)
            #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/CHSINT SDK For WinCE 6.0 (ARMV4I)/Debug/liblog4c.lib")
        #else        
            #error "Unknown platform definition"
        #endif
    #else
        #if defined(WM6_5_3)
            #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/Windows Mobile 6.5.3 Professional DTK (ARMV4I)/Release/liblog4c.lib")
        #elif defined(WM6)
            #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/Windows Mobile 6 Professional SDK (ARMV4I)/Release/liblog4c.lib")
        #elif defined(PPC2003)
            #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/Pocket PC 2003 (ARMV4)/Release/liblog4c.lib")
        #elif defined(SMP2003)
            #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/Smartphone 2003 (ARMV4)/Release/liblog4c.lib")
        #elif defined(CE500_ARMV4I)
            #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/STANDARDSDK_500 (ARMV4I)/Release/liblog4c.lib")
        #elif defined(CE500_MIPSII)
            #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/STANDARDSDK_500 (MIPSII)/Release/liblog4c.lib")
        #elif defined(CE500_MIPSII_FP)
            #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/STANDARDSDK_500 (MIPSII_FP)/Release/liblog4c.lib")
        #elif defined(CE500_MIPSIV)
            #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/STANDARDSDK_500 (MIPSIV)/Release/liblog4c.lib")
        #elif defined(CE500_MIPSIV_FP)
            #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/STANDARDSDK_500 (MIPSIV_FP)/Release/liblog4c.lib")
        #elif defined(CE500_SH4)
            #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/STANDARDSDK_500 (SH4)/Release/liblog4c.lib")
        #elif defined(CE500_X86)
            #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/STANDARDSDK_500 (x86)/Release/liblog4c.lib")
        #elif defined(CE6_ARMV4I)
            #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/CHSINT SDK For WinCE 6.0 (ARMV4I)/Release/liblog4c.lib")
        #else        
            #error "Unknown platform definition"
        #endif
    #endif
#endif


static int RegsvrWCE (const WCHAR * WCE_DLL, WCHAR ERR_MSG[128])
{
    LOG_TRACE0();

    wchar_t wchBuf[MAX_PATH+1];
    GetModuleFileName(NULL, wchBuf, MAX_PATH);

    ::MessageBoxW(0, wchBuf, L"CurPath", MB_OK|MB_ICONSTOP);

    HMODULE hDll = ::LoadLibraryW(WCE_DLL);

    if (!hDll) {
        LOG_ERROR("LoadLibrary failed");

        ::FormatMessageW(FORMAT_MESSAGE_FROM_SYSTEM|FORMAT_MESSAGE_IGNORE_INSERTS,
            0,
            ::GetLastError(),
            0,
            ERR_MSG,
            128,
            0);
        return REGCE_ERROR_LOADLIBRARY;
    }

    pfnDllRegisterProc fnDllReg = (pfnDllRegisterProc) GetProcAddressW(hDll, DLLREG_PROC);
    if (!fnDllReg) {
        ::FreeLibrary(hDll);
        return REGCE_ERROR_GET_DLLREG_PROC;
    }

    HRESULT  hr = (*fnDllReg)();

    if (hr == S_OK) {
        ::FreeLibrary(hDll);
        return REGCE_SUCCESS;
    } else {
        ::FormatMessageW(FORMAT_MESSAGE_FROM_SYSTEM|FORMAT_MESSAGE_IGNORE_INSERTS,
                        0,
                        hr,
                        0,
                        ERR_MSG,
                        128,
                        0);
        ::FreeLibrary(hDll);
        return REGCE_ERROR_CALL_DLLREG_PROC;
    }
}


static int UnregsvrWCE (const WCHAR * WCE_DLL, WCHAR ERR_MSG[128])
{
    HMODULE hDll = ::LoadLibraryW(WCE_DLL);

    if (!hDll) {
        ::FormatMessageW(FORMAT_MESSAGE_FROM_SYSTEM|FORMAT_MESSAGE_IGNORE_INSERTS,
            0,
            ::GetLastError(),
            0,
            ERR_MSG,
            128,
            0);
        return REGCE_ERROR_LOADLIBRARY;
    }

    pfnDllUnregisterProc fnDllUnreg = (pfnDllUnregisterProc) ::GetProcAddressW(hDll, DLLUNREG_PROC);
    if (!fnDllUnreg) {
        ::FreeLibrary(hDll);
        return REGCE_ERROR_GET_DLLUNREG_PROC;
    }

    HRESULT  hr = (*fnDllUnreg)();

    if (hr == S_OK) {
        ::FreeLibrary(hDll);
        return REGCE_SUCCESS;
    } else {
        ::FormatMessageW(FORMAT_MESSAGE_FROM_SYSTEM|FORMAT_MESSAGE_IGNORE_INSERTS,
            0,
            hr,
            0,
            ERR_MSG,
            128,
            0);
        ::FreeLibrary(hDll);    
        return REGCE_ERROR_CALL_DLLUNREG_PROC;
    }
}

#endif // _REGSVR_CE_H__
