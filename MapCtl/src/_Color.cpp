// Color.cpp : _CColor ��ʵ��
//
#include "_Color.h"

#pragma warning (push)
#pragma warning (disable: 4996)

// _CColor

STDMETHODIMP _CColor::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IColor
	};

	for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}


STDMETHODIMP _CColor::get_Alpha(BYTE* pVal)
{
	*pVal = rgba.alpha;
	return S_OK;
}


STDMETHODIMP _CColor::put_Alpha(BYTE newVal)
{
	rgba.alpha = newVal;
	return S_OK;
}	


STDMETHODIMP _CColor::get_Red(BYTE* pVal)
{
	*pVal = rgba.red;
	return S_OK;
}


STDMETHODIMP _CColor::put_Red(BYTE newVal)
{
	rgba.red = newVal;
	return S_OK;
}


STDMETHODIMP _CColor::get_Green(BYTE* pVal)
{
	*pVal = rgba.green;
	return S_OK;
}


STDMETHODIMP _CColor::put_Green(BYTE newVal)
{
	rgba.green = newVal;
	return S_OK;
}


STDMETHODIMP _CColor::get_Blue(BYTE* pVal)
{
	*pVal = rgba.blue;
	return S_OK;
}


STDMETHODIMP _CColor::put_Blue(BYTE newVal)
{
	rgba.blue = newVal;
	return S_OK;
}


STDMETHODIMP _CColor::get_Hue(BYTE* pVal)
{
	winhsl_color_t  whsl;
	hsl_color_t     hsl;

    RGBtoHSL(&rgba, &hsl);
    HSLtoWinHSL(&hsl, &whsl);
    
    *pVal = whsl.hue;
    return S_OK;
}


STDMETHODIMP _CColor::put_Hue(BYTE newVal)
{
    winhsl_color_t  whsl;
	hsl_color_t     hsl;

    RGBtoHSL(&rgba, &hsl);
    HSLtoWinHSL(&hsl, &whsl);

    if (newVal > 240) {
        whsl.hue = 240;
    } else if (newVal < 0) {
        whsl.hue = 0;
    } else {
        whsl.hue = newVal;
    }

    WinHSLtoHSL(&whsl, &hsl);
    HSLtoRGB(&hsl, &rgba);

	return S_OK;
}


STDMETHODIMP _CColor::get_Sat(BYTE* pVal)
{
	winhsl_color_t  whsl;
	hsl_color_t     hsl;

    RGBtoHSL(&rgba, &hsl);
    HSLtoWinHSL(&hsl, &whsl);
    
    *pVal = whsl.sat;
    return S_OK;
}


STDMETHODIMP _CColor::put_Sat(BYTE newVal)
{
    winhsl_color_t  whsl;
	hsl_color_t     hsl;

    RGBtoHSL(&rgba, &hsl);
    HSLtoWinHSL(&hsl, &whsl);

    if (newVal > 240) {
        whsl.sat = 240;
    } else if (newVal < 0) {
        whsl.sat = 0;
    } else {
        whsl.sat = newVal;
    }

    WinHSLtoHSL(&whsl, &hsl);
    HSLtoRGB(&hsl, &rgba);

	return S_OK;
}


STDMETHODIMP _CColor::get_Lum(BYTE* pVal)
{
	winhsl_color_t  whsl;
	hsl_color_t     hsl;

    RGBtoHSL(&rgba, &hsl);
    HSLtoWinHSL(&hsl, &whsl);
    
    *pVal = whsl.lum;
    return S_OK;
}


STDMETHODIMP _CColor::put_Lum(BYTE newVal)
{
    winhsl_color_t  whsl;
	hsl_color_t     hsl;

    RGBtoHSL(&rgba, &hsl);
    HSLtoWinHSL(&hsl, &whsl);

    if (newVal > 240) {
        whsl.lum = 240;
    } else if (newVal < 0) {
        whsl.lum = 0;
    } else {
        whsl.lum = newVal;
    }
	
    WinHSLtoHSL(&whsl, &hsl);
    HSLtoRGB(&hsl, &rgba);

	return S_OK;
}


STDMETHODIMP _CColor::get_OleColor(OLE_COLOR* pVal)
{
	*pVal = RGB(rgba.red, rgba.green, rgba.blue);
	return S_OK;
}


STDMETHODIMP _CColor::put_OleColor(OLE_COLOR newVal)
{
	rgba.red = GET_RED(newVal);
    rgba.green = GET_GREEN(newVal);
    rgba.blue = GET_BLUE(newVal);
	return S_OK;
}


STDMETHODIMP _CColor::get_WebColor(BSTR* pVal)
{			
    wchar_t  clr[] = L"#000000";
    wsprintfW(clr, L"#%02x%02x%02x", rgba.red, rgba.green, rgba.blue);
    *pVal = ::SysAllocString(clr);	
	return S_OK;
}


STDMETHODIMP _CColor::put_WebColor(VARIANT newVal)
{
    HRESULT hres = E_INVALIDARG;

    WCHAR buf[3];

	if (newVal.vt == VT_BSTR && newVal.bstrVal) {
        int chLen = ::SysStringLen(newVal.bstrVal);

        if (chLen > 0 && chLen < 30) {
            const WCHAR * wsz = newVal.bstrVal;

            if (wsz[0] == L'#') {
                // #RRGGBB
                if (chLen == 4) {
                    wcsncpy(buf, wsz+1, 1);
	                buf[1]=0;
	                rgba.red = (BYTE) wcstol(buf, 0, 16);

    		        wcsncpy(buf, wsz+2, 1);
	                buf[1]=0;
	                rgba.green = (BYTE) wcstol(buf, 0, 16);

	                wcsncpy(buf, wsz+3, 1);
	                buf[1]=0;
	                rgba.blue = (BYTE) wcstol(buf, 0, 16);

                    hres = S_OK;
                } else if (chLen == 7) {
	                wcsncpy(buf, wsz+1, 2);
	                buf[2]=0;
	                rgba.red = (BYTE) wcstol(buf, 0, 16);

    		        wcsncpy(buf, wsz+3, 2);
	                buf[2]=0;
	                rgba.green = (BYTE) wcstol(buf, 0, 16);

	                wcsncpy(buf, wsz+5, 2);
	                buf[2]=0;
	                rgba.blue = (BYTE) wcstol(buf, 0, 16);

                    hres = S_OK;
                }
            } else {
                // X11 Color
                COLORREF rgb = 0;
                if (X11ColorFind(wsz, &rgb)) {
                    rgba.red = GET_RED(rgb);
                    rgba.green = GET_GREEN(rgb);
                    rgba.blue = GET_BLUE(rgb);

                    hres = S_OK;
                }
            }
        }
    } else if (newVal.vt == VT_DISPATCH && newVal.pdispVal) {
        IColor *pSource = 0;
        if (S_OK != newVal.pdispVal->QueryInterface(IID_IColor, (void**) &pSource)) {
            hres = Clone(pSource);
            pSource->Release();
        }
    }

    return hres;
}


STDMETHODIMP _CColor::get_RgbaColor(BSTR* pVal)
{
    wchar_t  clr[] = L"#00000000";
    wsprintfW(clr, L"#%02x%02x%02x%02x",
        rgba.red, rgba.green, rgba.blue, rgba.alpha);
    *pVal = ::SysAllocString(clr);
	return S_OK;
}


STDMETHODIMP _CColor::put_RgbaColor(BSTR newVal)
{
    HRESULT hres = E_INVALIDARG;

    if (!newVal) {
        return E_POINTER;
    }

    int chLen = ::SysStringLen(newVal);

    if (chLen == 4 || chLen == 5 || chLen == 7 || chLen == 9) {
        if (newVal[0] == L'#') {
            WCHAR buf[3];

            const WCHAR * wsz = newVal;

            if (chLen == 4) {
                // #RGB
                wcsncpy(buf, wsz+1, 1);
                buf[1]=0;
                rgba.red = (BYTE) wcstol(buf, 0, 16);

                wcsncpy(buf, wsz+2, 1);
                buf[1]=0;
                rgba.green = (BYTE) wcstol(buf, 0, 16);

                wcsncpy(buf, wsz+3, 1);
                buf[1]=0;
                rgba.blue = (BYTE) wcstol(buf, 0, 16);

                hres = S_OK;
            } else if (chLen == 5) {
                // #RGBA
                wcsncpy(buf, wsz+1, 1);
                buf[1]=0;
                rgba.red = (BYTE) wcstol(buf, 0, 16);

                wcsncpy(buf, wsz+2, 1);
                buf[1]=0;
                rgba.green = (BYTE) wcstol(buf, 0, 16);

                wcsncpy(buf, wsz+3, 1);
                buf[1]=0;
                rgba.blue = (BYTE) wcstol(buf, 0, 16);

                wcsncpy(buf, wsz+4, 1);
                buf[1]=0;
                rgba.alpha = (BYTE) wcstol(buf, 0, 16);

                hres = S_OK;
            } else if (chLen == 7) {
                // #RRGGBB
                wcsncpy(buf, wsz+1, 2);
                buf[2]=0;
                rgba.red = (BYTE) wcstol(buf, 0, 16);

                wcsncpy(buf, wsz+3, 2);
                buf[2]=0;
                rgba.green = (BYTE) wcstol(buf, 0, 16);

                wcsncpy(buf, wsz+5, 2);
                buf[2]=0;
                rgba.blue = (BYTE) wcstol(buf, 0, 16);

                hres = S_OK;
            } else {
                // #RRGGBBAA
                wcsncpy(buf, wsz+1, 2);
                buf[2]=0;
                rgba.red = (BYTE) wcstol(buf, 0, 16);

                wcsncpy(buf, wsz+3, 2);
                buf[2]=0;
                rgba.green = (BYTE) wcstol(buf, 0, 16);

                wcsncpy(buf, wsz+5, 2);
                buf[2]=0;
                rgba.blue = (BYTE) wcstol(buf, 0, 16);

                wcsncpy(buf, wsz+7, 2);
                buf[2]=0;
                rgba.alpha = (BYTE) wcstol(buf, 0, 16);

                hres = S_OK;
            }
        }
    }

    return hres;
}


STDMETHODIMP _CColor::Neg(VARIANT_BOOL hasAlpha)
{
    rgba.red = 0xff - rgba.red;
    rgba.green = 0xff - rgba.green;
	rgba.blue = 0xff - rgba.blue;

    if (hasAlpha) {
		rgba.alpha = 0xff - rgba.alpha;
    }

	return S_OK;
}


STDMETHODIMP _CColor::Tune(BYTE bVal)
{
	winhsl_color_t  whsl;
    hsl_color_t  hsl;

    RGBtoHSL(&rgba, &hsl);
    HSLtoWinHSL(&hsl, &whsl);

	int lum = whsl.lum + bVal;

    if (lum > 240) {
        whsl.lum = 240;
    } else if (lum < 0) {
        whsl.lum = 0;
    } else {
	    whsl.lum = (BYTE) lum;
    }

    WinHSLtoHSL(&whsl, &hsl);
    HSLtoRGB(&hsl, &rgba);
	return S_OK;
}


STDMETHODIMP _CColor::Clone(IColor* source)
{
    if (!source) {
        return E_POINTER;
    }

	rgba = ((_CColor*) source)->rgba;
	return S_OK;
}

#pragma warning (pop)