// Layers.h : CLayers 的声明
//
#pragma once

#include "Config.h"
#include "CollType.h"
#include "Layer.h"


typedef list< CAdapt< CComPtr<ILayer> > > LayerCollType;

typedef CComEnumOnSTL<IEnumVARIANT, &IID_IEnumVARIANT, VARIANT, 
    _CopyVariantFromAdaptItf<ILayer>,
    LayerCollType >
CComEnumVariantOnListOfLayers;

typedef ICollOnSTLListWithMapImpl<IDispatchImpl<ILayers, &IID_ILayers>,
    LayerCollType,
    ILayer*,
    _CopyItfFromAdaptItf<ILayer>,
    CComEnumVariantOnListOfLayers,
    std::wstring,
    CLayer,
    ILayer >
ILayerCollImpl;


typedef  COLL_ITER(list, ILayer)  LAYER_ITER;
typedef  COLL_RITER(list, ILayer) LAYER_RITER;

// 指定索引的指针
#define  LAYER_ITER_AT(it, index)   \
    LAYER_ITER (##it) = m_coll.begin(); \
    do { \
        for (long _id=0; _id<index; _id++) { \
            (##it)++; \
        } \
    } while (0)


// CLayers

class ATL_NO_VTABLE CLayers :
	public CComObjectRootEx<CComSingleThreadModel>,
    public CComCoClass<CLayers>,
    public ISupportErrorInfo,
	public ILayerCollImpl
{
public:
	CLayers()
	{
	}

DECLARE_NO_REGISTRY()

BEGIN_COM_MAP(CLayers)
	COM_INTERFACE_ENTRY(ILayers)
	COM_INTERFACE_ENTRY(IDispatch)
    COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid)
    {
	    static const IID* arr[] = 
	    {
		    &IID_ILayers
	    };

	    for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	    {
		    if (InlineIsEqualGUID(*arr[i],riid))
			    return S_OK;
	    }
	    return S_FALSE;
    }

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
        LOG_TRACE("S_OK");
		return S_OK;
	}

	void FinalRelease()
	{
        m_coll.clear();
        LOG_TRACE("");
	}


    void Draw (CDrawInfo *di)
    {
        for (LAYER_RITER iter = m_coll.rbegin(); iter != m_coll.rend(); iter++) {
            // Before Draw Layer

			((CLayer*) iter->m_T.p)->Draw(di);

            // After Draw Layer
        }
    }

public:

    STDMETHOD(Move)(LONG atIndex, LONG toIndex)
    {
        return E_NOTIMPL;
    }

    STDMETHOD(get_Bound)(IBound** ppVal)
    {
        return E_NOTIMPL;
    }
};

//DEL:OBJECT_ENTRY_AUTO(__uuidof(Layers), CLayers)
