/**
 * gdbfilter.h
 *
 *
 */

#ifndef _GDB_FILTER_H_
#define _GDB_FILTER_H_

#ifdef _MSC_VER
# pragma warning (disable : 4996)
#endif

#ifdef    __cplusplus
extern "C" {
#endif

#include <assert.h>
#include <memory.h>

#include "gdbapi.h"
#include "gdbquad.h"
#include "gdberror.h"
#include "gdbshape.h"

/*
 * GDBFilter
 */
typedef struct _GDBFilter
{
    GDBHandleOb __handle[1];

    GDB_FLTR_METHOD method;
    GDBAPI_BOOL     truth;

    /* clipping object: shape or rowid */
    GDB_SHAPE       shape;  /* shape */

    rowid_t  rowid;  /* id of shape in layer */
    GDB_LAYER       layer;  /* layer which shape is in */

    GDBAPI_BOOL     clipping;
} GDBFilter, *HGDBFilter;

#define CAST_HANDLE_FILTER(hdl, herr, hfilter) \
    CAST_HANDLE_TYPE(hdl, herr, GDB_HTYPE_FILTER, HGDBFilter, hfilter)

static void _GDBFilterClearAll(HGDBFilter fltr)
{
    if (fltr->shape) {
        GDB_HANDLE hdl = fltr->shape;
        fltr->shape = 0;
        GDBHandleFree (hdl);
    }
    if (fltr->layer) {
        GDB_HANDLE hdl = fltr->layer;
        fltr->layer = 0;
        GDBHandleFree (hdl);
    }
}


static GDBAPI_RESULT _GDBFilterSetAttr(HGDBFilter fltr,
    void *pAttrValue, int cbSizeValue, GDB_ATTR_NAME attrName, HGDBERR hError)
{
    switch (attrName) {
    case GDB_ATTR_FLTR_METHOD:
        fltr->method = VALUE_PTR(pAttrValue, GDB_FLTR_METHOD);
        break;

    case GDB_ATTR_FLTR_SHAPE:
        if (fltr->shape == pAttrValue) {
            break;
        } else {
            if (fltr->shape) {
                GDB_HANDLE hdl = fltr->shape;
                fltr->shape = 0;
                GDBHandleFree (hdl);
            }
            if (pAttrValue) {
                GDB_HANDLE hdl = VALUE_PTR(pAttrValue, GDB_HANDLE);
                if (GDBHandleGetType(hdl) == GDB_HTYPE_SHAPE) {
                    fltr->shape = hdl;
                } else {
                    GDBERROR_THROW_EHTYPE(hError);
                }
            }
        }
        break;

    case GDB_ATTR_FLTR_RECT:
        ((GDBShapeInfo *)fltr->shape)->bound._MBR = * ((GDBExtRect*) pAttrValue);
        break;

    case GDB_ATTR_FLTR_ROWID:
        fltr->rowid = VALUE_PTR(pAttrValue, rowid_t);
        break;

    case GDB_ATTR_FLTR_LAYER:
        if (fltr->layer == pAttrValue) {
            break;
        } else {
            if (fltr->layer) {
                GDB_HANDLE hdl = fltr->layer;
                fltr->layer = 0;
                GDBHandleFree (hdl);
            }
            if (pAttrValue) {
                GDB_HANDLE hdl = VALUE_PTR(pAttrValue, GDB_HANDLE);
                if (GDBHandleGetType(hdl) == GDB_HTYPE_LAYER) {
                    fltr->layer = hdl;
                } else {
                    GDBERROR_THROW_EHTYPE(hError);
                }
            }
        }
        break;

    case GDB_ATTR_FLTR_CLIPPING:
        fltr->clipping = VALUE_PTR(pAttrValue, GDBAPI_BOOL);
        break;

    case GDB_ATTR_FLTR_TRUTH:
        fltr->truth = VALUE_PTR(pAttrValue, GDBAPI_BOOL);
        break;
    }
    return 0;
}


static GDBAPI_RESULT _GDBFilterGetAttr(HGDBFilter fltr,
    void *pAttrValue, int *cbSizeValue, GDB_ATTR_NAME attrName, GDB_ERROR hError)
{
    if (!pAttrValue && !cbSizeValue) {
        GDBERROR_THROW_EATTRVAL(hError);
    }

    switch (attrName) {
    case GDB_ATTR_FLTR_METHOD:
        ATTR_GET_TYPED_VALUE(GDB_FLTR_METHOD, fltr->method,
            pAttrValue, cbSizeValue, hError);
        break;

    case GDB_ATTR_FLTR_SHAPE:
        ATTR_GET_TYPED_VALUE(GDB_HANDLE, fltr->shape,
            pAttrValue, cbSizeValue, hError);
        break;

    case GDB_ATTR_FLTR_RECT:
        ATTR_GET_TYPED_VALUE(GDBExtRect,
            ((GDBShapeInfo*) fltr->shape)->bound._MBR,
            pAttrValue, cbSizeValue, hError);
        break;

    case GDB_ATTR_FLTR_ROWID:
        ATTR_GET_TYPED_VALUE(rowid_t, fltr->rowid,
            pAttrValue, cbSizeValue, hError);
        break;

    case GDB_ATTR_FLTR_LAYER:
        ATTR_GET_TYPED_VALUE(GDB_HANDLE, fltr->layer,
            pAttrValue, cbSizeValue, hError);
        break;

    case GDB_ATTR_FLTR_CLIPPING:
        ATTR_GET_TYPED_VALUE(GDBAPI_BOOL, fltr->clipping,
            pAttrValue, cbSizeValue, hError);
        break;

    case GDB_ATTR_FLTR_TRUTH:
        ATTR_GET_TYPED_VALUE(GDBAPI_BOOL, fltr->truth,
            pAttrValue, cbSizeValue, hError);
        break;

 default:
        GDBERROR_THROW_EATTR(hError);
    }

    return GDBAPI_SUCCESS;
}

#ifdef    __cplusplus
}
#endif

#endif /* _GDB_FILTER_H_ */
