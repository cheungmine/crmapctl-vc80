/**
 * gdbquad.h
 *    quad tree index
 *
 *       Level: 1             Level: 2
 * +--------+--------+  +----+----+----+----+  
 * |  0 1   |  1 1   |  | 11 | 13 | 31 | 33 |
 * |        |        |  +----+----+----+----+
 * |   1    |   3    |  | 10 | 12 | 30 | 32 |
 * +--------+--------+  +----+----+----+----+  
 * |  0 0   |  1 0   |  | 01 | 03 | 21 | 23 |
 * |        |        |  +----+----+----+----+
 * |   0    |   2    |  | 00 | 02 | 20 | 22 |
 * +--------+--------+  +----+----+----+----+
 *     S         S
 *        
 * GSize0 = S+S
 * Level0 = 0
 *
 * Level0: 0-based
 * Level: 1-based > Level0
 *
 */

#ifndef _GDB_QUAD_H_
#define _GDB_QUAD_H_

#ifdef _MSC_VER
# pragma warning (disable : 4996)
#endif

#ifdef    __cplusplus
extern "C" {
#endif

#include "peanocode.h"

#define GDB_LEVEL_LIMIT 16

/* Is vertex p, q on the same side of line l:[1, 2]
 */
#define sameside_line(lx1, ly1, lx2, ly2, px, py, qx, qy) \
    (((lx2-lx1)*(py-ly1) - (ly2-ly1)*(px-lx1))* \
	 ((lx2-lx1)*(qy-ly2) - (ly2-ly1)*(qx-lx2)))>0? 1:0

/* Is two line-segments (s1, s2) intersect ?
 */
#define lseg_intersect(s1x1, s1y1, s1x2, s1y2, s2x1, s2y1, s2x2, s2y2) \
    (! is_sameside(s1x1, s1y1, s1x2, s1y2, s2x1, s2y1, s2x2, s2y2) && \
   ! is_sameside(s2x1, s2y1, s2x2, s2y2, s1x1, s1y1, s1x2, s1y2))

static void quad_index (double GSize0, int Level0,
    double x, double y, char index[], int Level)
{
    if (Level0 == Level) {
        return;
    } else {
        double S = GSize0/2;

        if (x >= 0 && x < S && y >= 0 && y < S) {
            index[Level0] = '0';
            quad_index (S, Level0+1, x, y, index, Level);
        } else if (x >= S && x < GSize0 && y >= 0 && y < S) {
            index[Level0] = '2';
            quad_index (S, Level0+1, x-S, y, index, Level);
        } else if (x >= S && x < GSize0 && y >= S && y < GSize0) {
            index[Level0] = '3';
            quad_index (S, Level0+1, x-S, y-S, index, Level);
        } else if (x >= 0 && x < S && y >= S && y < GSize0) {
            index[Level0] = '1';
            quad_index (S, Level0+1, x, y-S, index, Level);
        }
    }
}

static void quad_index2grid (int Level0, char index[], int Level, int *xi, int *yi)
{
    char c;
    int i = Level0;

    *xi = 0;
    *yi = 0;

    for (; i < Level; i++) {
        c = index[i];

        if (c == '0') {
            *xi = *xi*2;
            *yi = *yi*2;
        } else if (c == '1') {
            *xi = *xi*2;
            *yi = *yi*2 + 1;
        } else if (c == '2') {
            *xi = *xi*2 + 1;
            *yi = *yi*2;
        } else if (c == '3') {
            *xi = *xi*2 + 1;
            *yi = *yi*2 + 1;
        }
    }
}

static void quad_grid2index (int Level0, int xi, int yi, int Level, char index[])
{
    quad_index ((1<<(Level-Level0)), Level0, xi, yi, index, Level);
}

static void quad_grid2point (double GSize0, int Level0,
    int xi, int yi, int Level, double *x, double *y)
{
    double S = GSize0 / (1<<(Level-Level0));
    *x = xi * S;
    *y = yi *S;
}

static void quad_grid2ext (double GSize0, int Level0,
    int xi, int yi, int Level,
    double *Xmin, double *Ymin, double *Xmax, double *Ymax)
{
    quad_grid2point (GSize0, Level0, xi, yi, Level, Xmin, Ymin);
    quad_grid2point (GSize0, Level0, xi+1, yi+1, Level, Xmax, Ymax);
}

static int quad_ext2grid_lvl (double GSize0, int Level0, char index[], int level,
    double Xmin, double Ymin, double Xmax, double Ymax,
    int *x1, int *y1, int *x2, int *y2)
{
    quad_index (GSize0, Level0, Xmin, Ymin, index, level);
    quad_index2grid (Level0, index, level, x1, y1);
    quad_index (GSize0, Level0, Xmax, Ymax, index, level);
    quad_index2grid (Level0, index, level, x2, y2);
    *x2 += 1;
    *y2 += 1;
    return (*x2 - *x1) * (*y2 - *y1);
}

static int quad_ext2grid (double GSize0, int Level0,
    double Xmin, double Ymin, double Xmax, double Ymax,
    int *MinXI, int *MinYI, int *MaxXI, int *MaxYI)
{
    char index[GDB_LEVEL_LIMIT];
    int num;
    int i = sizeof(index) -1; /* max level */

    for (; i > 0; i--) {
        num = quad_ext2grid_lvl (GSize0, Level0, index, i,
            Xmin, Ymin, Xmax, Ymax, MinXI, MinYI, MaxXI, MaxYI);

        if (num <= 4) {
            if (i == 1 || num == 1) {
                return i;
            } else {
                /* try to upgrade level */
                int x1, y1, x2, y2, n, n2 = num;

                while (i > 1 && (n = quad_ext2grid_lvl (GSize0, Level0, index, i-1,
                    Xmin, Ymin, Xmax, Ymax, &x1, &y1, &x2, &y2)) < n2) {
                    n2 = n; 

                    --i;
                    *MinXI = x1;
                    *MinYI = y1;
                    *MaxXI = x2;
                    *MaxYI = y2;

                    if (n == 1) {
                        break;
                    }
                }
                break;
            }
        }
    }

    return i;
}

#ifdef    __cplusplus
}
#endif

#endif /* _GDB_QUAD_H_ */
