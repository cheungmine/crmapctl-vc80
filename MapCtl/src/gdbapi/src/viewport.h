/******************************************************************************
 * viewport.h
 *
 * cheungmine
 * 2013-5-10
 *****************************************************************************/
#ifndef VIEWPORT_H_INCLUDED
#define VIEWPORT_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

#include <assert.h>

#ifndef _ASSERT
# define _ASSERT(cond) assert (cond)
#endif

#include <float.h>
#include <memory.h>

#ifndef _USE_MATH_DEFINES
# define _USE_MATH_DEFINES
# include <math.h>
#endif

#ifndef M_2PI
# define M_2PI  (M_PI*2)
#endif

#ifndef _TANGENT
# if defined(_WIN32_WCE)
#   define  _TANGENT  tan
# elif defined(_WIN32)
#   define  _TANGENT  tanf
# else
#   define  _TANGENT  tanf
# endif
#endif

#ifndef _INT
    typedef int _INT;
#endif

#ifndef _FLOAT
    typedef float _FLOAT;
#endif

#ifndef _LFLOAT
    typedef double _LFLOAT;
#endif

#ifndef _MIN
# define _MIN(v1, v2) ((v1)<(v2)? (v1):(v2))
#endif

#ifndef _MAX
# define _MAX(v1, v2) ((v1)>(v2)? (v1):(v2))
#endif

#ifndef _ABS
# define _ABS(v) ((v)>=0?(v):(-v))
#endif

#ifndef _SWAP_Type

# define _SWAP_Type(a, b, type) \
    do { \
        type t = (a); \
        (a) = (b); \
        (b) = t; \
    } while (0)

#endif

#ifndef BOOL
    typedef int BOOL;
#endif

#ifndef TRUE
# define TRUE  1
#endif

#ifndef FALSE
# define FALSE  0
#endif


#ifndef __INLINE

#ifdef _MSC_VER
# define __INLINE static __forceinline
# define __INLINE_ALL __INLINE
#else
# define __INLINE static inline
# if (defined(__APPLE__) && defined(__ppc__))
        /* static inline __attribute__ here breaks osx ppc gcc42 build */
#   define __INLINE_ALL static __attribute__((always_inline))
# else
#   define __INLINE_ALL static inline __attribute__((always_inline))
# endif
#endif

#endif /* __INLINE */

#define VWPT_SCALE_MIN      FLT_MIN
#define VWPT_SCALE_MAX      FLT_MAX

/*===========================================================================*/
/*                                                                           */
/*                     Public Structs Definitions                            */
/*                                                                           */
/*===========================================================================*/
/* same as RECT in <windef.h> */
typedef struct _VWPT_Rect
{
    _INT Xmin,    /* left */
       Ymin,    /* top */
       Xmax,    /* right */
       Ymax;    /* bottom */
} VWPT_Rect;

typedef struct _VWPT_FRect
{
    _FLOAT Xmin,
         Ymin,
         Xmax,
         Ymax;
} VWPT_FRect;

typedef struct _VWPT_LFRect
{
    _LFLOAT Xmin, 
                    Ymin, 
                    Xmax, 
                    Ymax;
} VWPT_LFRect;


/* same as POINT in <windef.h> */
typedef struct _VWPT_Point
{
    _INT X, 
       Y;
} VWPT_Point;

typedef struct _VWPT_FPoint
{
    _FLOAT X, 
         Y;
} VWPT_FPoint;

typedef struct _VWPT_LFPoint
{
    _LFLOAT X, 
                    Y;
} VWPT_LFPoint;

/**
 * VWPT_Transform struct
 */
typedef struct _VWPT_Transform
{
    /* viewport rect in logical reference */
    VWPT_LFRect  viewRC;

    /* data rect in world coords reference */
    VWPT_LFRect  dataRC;

    /* viewport center point in logical unit */
    VWPT_LFPoint viewCP;

    /* viewed data rect center in world coords */
    VWPT_LFPoint dataCP;

    /* X_DPI/Y_DPI, default is 1.0. used when transformation */
    _LFLOAT      ratioXY;

    /* current scaleFactor: viewRC/vdata_rc  (like: pixels/meter) */
    _LFLOAT      scaleFactor;

    _LFLOAT      scaleFactorMin;
    _LFLOAT      scaleFactorMax;
} VWPT_Transform;


#define VWPT_SetRect(rect, xmin, ymin, xmax, ymax, type) \
    (rect).Xmin = (type) (xmin); (rect).Ymin = (type) (ymin); \
    (rect).Xmax = (type) (xmax); (rect).Ymax = (type) (ymax)


/**
 * test 2 rects overlapped
 */
#define VWPT_TestOverlapped(aXmin, aYmin, aXmax, aYmax, bXmin, bYmin, bXmax, bYmax) \
    ((aXmin)<(bXmax) && (aYmin)<(bYmax) && (bXmin)<(aXmax) && (bYmin)<(aYmax))

#define VWPT_RectOverlapped(a, b) \
    VWPT_TestOverlapped((a).Xmin, (a).Ymin, (a).Xmax, (a).Ymax, \
        (b).Xmin, (b).Ymin, (b).Xmax, (b).Ymax)

#define VWPT_WidthRect(rc) ((rc).Xmax - (rc).Xmin)
#define VWPT_HeightRect(rc) ((rc).Ymax - (rc).Ymin)

#define VWPT_CenterRect(rc, cp) \
    (cp).X = ((rc).Xmin + (rc).Xmax)/2; (cp).Y = ((rc).Ymin + (rc).Ymax)/2

#define VWPT_OffsetRect(rc, dx, dy) \
    (rc).Xmin += (dx); (rc).Xmax += (dx); (rc).Ymin += (dy); (rc).Ymax += (dy)

static void VWPT_ValidateRect (VWPT_LFRect *rect)
{
    if (rect->Xmin > rect->Xmax) {
        _SWAP_Type(rect->Xmin, rect->Xmax, _LFLOAT);
    }

    if (rect->Ymin > rect->Ymax) {
        _SWAP_Type(rect->Ymin, rect->Ymax, _LFLOAT);
    }
}

static void VWPT_ShrinkRect (VWPT_LFRect *rect, _LFLOAT dx, _LFLOAT dy)
{
    rect->Xmin += dx;
    rect->Xmax -= dx;
    rect->Ymin += dy;
    rect->Ymax -= dy;
}

#define VWPT_ZeroRect(rect) \
    (rect).Xmin = (rect).Xmax = (rect).Ymin = (rect).Ymax = 0

#define VWPT_IsZeroRect(rect) \
    ((rect).Xmin == 0 && (rect).Xmax == 0 && (rect).Ymin == 0 && (rect).Ymax == 0)


static BOOL VWPT_IntersectRect (VWPT_LFRect *subject, VWPT_LFRect *clip, VWPT_LFRect *result)
{
    if (subject == clip) {
        if (result != subject) {
            *result = *subject;
        }
        return TRUE;
    } else if (VWPT_IsZeroRect(*subject)) {
        return FALSE;
    } else if (VWPT_IsZeroRect(*clip)) {
        return FALSE;
    } else {
        if (VWPT_RectOverlapped(*subject, *clip)) {
            result->Xmin = _MAX(subject->Xmin, clip->Xmin);
            result->Ymin = _MAX(subject->Ymin, clip->Ymin);
            result->Xmax = _MIN(subject->Xmax, clip->Xmax);
            result->Ymax = _MIN(subject->Ymax, clip->Ymax);
            _ASSERT(result->Xmax > result->Xmin && result->Ymax > result->Ymin);
            return TRUE;
        } else {
            return FALSE;
        }
    }
}

static void VWPT_UnionRect (VWPT_LFRect *subject, VWPT_LFRect *clip, VWPT_LFRect *result)
{
    if (subject == clip) {
        if (result != subject) {
            *result = *subject;
        }
    } else if (VWPT_IsZeroRect(*subject)) {
        if (result != clip) {
            *result = *clip;
        }
    } else if (VWPT_IsZeroRect(*clip)) {
        if (result != subject) {
            *result = *subject;
        }
    } else {
        result->Xmin = _MIN(subject->Xmin, clip->Xmin);
        result->Ymin = _MIN(subject->Ymin, clip->Ymin);
        result->Xmax = _MAX(subject->Xmax, clip->Xmax);
        result->Ymax = _MAX(subject->Ymax, clip->Ymax);
    }
}

__INLINE BOOL VWPT_IsInViewRect (const VWPT_Transform *vwpt, const VWPT_LFRect *Vrc)
{
    return VWPT_RectOverlapped(vwpt->viewRC, *Vrc);
}

/**
 * Set new scale factor
 */
__INLINE void VWPT_SetScale (VWPT_Transform *vwpt, _LFLOAT newScaleFactor)
{
    _LFLOAT scaleValue = _ABS(newScaleFactor);
    vwpt->scaleFactor = (scaleValue < vwpt->scaleFactorMin) ?
        vwpt->scaleFactorMin :
        (scaleValue > vwpt->scaleFactorMax? vwpt->scaleFactorMax : scaleValue);
}

/**
 * Update scale and returns it
 */
__INLINE _LFLOAT VWPT_CalcScale (VWPT_Transform *vwpt, BOOL updateVWPT)
{
    _LFLOAT w = VWPT_WidthRect(vwpt->viewRC) / VWPT_WidthRect(vwpt->dataRC);
    _LFLOAT h = VWPT_HeightRect(vwpt->viewRC) / VWPT_HeightRect(vwpt->dataRC);
    _LFLOAT s = _MIN(w, h);
    if (updateVWPT) {
        VWPT_SetScale(vwpt, s);
    }
    return s;
}

/**
 * initialize only view
 */
static void VWPT_TransformInitView (VWPT_Transform *vwpt,
    _LFLOAT viewWidth, _LFLOAT viewHeight)
{
    vwpt->ratioXY = 1.0; 

    vwpt->scaleFactorMin = VWPT_SCALE_MIN; 
    vwpt->scaleFactorMax = VWPT_SCALE_MAX;

    vwpt->viewRC.Xmin = 0;
    vwpt->viewRC.Ymin = 0;
    vwpt->viewRC.Xmax = viewWidth;
    vwpt->viewRC.Ymax = viewHeight;

    vwpt->dataRC = vwpt->viewRC;

    VWPT_CenterRect(vwpt->viewRC, vwpt->viewCP);
    VWPT_CenterRect(vwpt->dataRC, vwpt->dataCP);

    VWPT_CalcScale(vwpt, TRUE);
}

/**
 * initialize all
 */
static void VWPT_TransformInitAll (VWPT_Transform *vwpt,
    const VWPT_LFRect *view, const VWPT_LFRect *data, double ratioXY /* 1.0 */)
{
    vwpt->viewRC = *view; 
    vwpt->dataRC = *data; 
    vwpt->ratioXY = ratioXY; 

    vwpt->scaleFactorMin = VWPT_SCALE_MIN; 
    vwpt->scaleFactorMax = VWPT_SCALE_MAX;

    VWPT_CenterRect(vwpt->viewRC, vwpt->viewCP);
    VWPT_CenterRect(vwpt->dataRC, vwpt->dataCP);

    VWPT_CalcScale(vwpt, TRUE);
}

static void VWPT_CopyData(VWPT_Transform *dstVWPT, const VWPT_Transform *srcVWPT)
{
    *dstVWPT = *srcVWPT;
}

/**
 * Set the view port when view's size changed, such as: WM_ONSIZE
 *   (vrcXmin, vrcYmin, vrcXmax, vrcYmax) are coords of view rect
 */
static void VWPT_UpdateViewRect(VWPT_Transform *vwpt,
    _LFLOAT vrcXmin, _LFLOAT vrcYmin, _LFLOAT vrcXmax, _LFLOAT vrcYmax)
{
    vwpt->viewRC.Xmin = vrcXmin;
    vwpt->viewRC.Ymin = vrcYmin;
    vwpt->viewRC.Xmax = vrcXmax;
    vwpt->viewRC.Ymax = vrcYmax;
    VWPT_CenterRect(vwpt->viewRC, vwpt->viewCP);
}

/**
 * Set the data rect when data changed:
 *   (drcXmin, drcYmin, drcXmax, drcYmax) are coords of data rect
 */
static void VWPT_UpdateDataRect(VWPT_Transform *vwpt,
    _LFLOAT drcXmin, _LFLOAT drcYmin, _LFLOAT drcXmax, _LFLOAT drcYmax)
{
    vwpt->dataRC.Xmin = drcXmin;
    vwpt->dataRC.Ymin = drcYmin;
    vwpt->dataRC.Xmax = drcXmax;
    vwpt->dataRC.Ymax = drcYmax;
    VWPT_CenterRect(vwpt->dataRC, vwpt->dataCP);
    VWPT_CalcScale(vwpt, TRUE);
}

/* get reference to ratioXY */
#define VWPT_GetRatioXY(vwpt) \
    (vwpt->ratioXY)

#define VWPT_SetRatioXY(vwpt, ratioXY) \
    (vwpt)->ratioXY = (ratioXY)

/* get reference to current scaleFactor(=1/percentage) */
#define VWPT_GetScaleFactor(vwpt) \
    (vwpt->scaleFactor)

/* get reference to viewport rect */
#define VWPT_GetViewRect(vwpt) \
    (vwpt->viewRC)

/* get reference to data rect */
#define VWPT_GetDataRect(vwpt) \
    (vwpt->dataRC)

/* get reference to view data center */
#define VWPT_GetViewedDataCenter(vwpt) \
    (vwpt->dataCP)

/* get reference to view port center */
#define VWPT_GetViewCenter(vwpt) \
    (vwpt->viewCP)

/* get current view data rect */
static void VWPT_GetViewedDataRect(const VWPT_Transform *vwpt, VWPT_LFRect *viewedDataRC)
{
    _LFLOAT val;

    val = VWPT_WidthRect(vwpt->viewRC) / vwpt->scaleFactor / 2;    
    viewedDataRC->Xmin = vwpt->dataCP.X - val;
    viewedDataRC->Xmax = vwpt->dataCP.X + val;

    val = VWPT_HeightRect(vwpt->viewRC) / vwpt->scaleFactor / 2;
    viewedDataRC->Ymin = vwpt->dataCP.Y - val;
    viewedDataRC->Ymax = vwpt->dataCP.Y + val;
}

/* current display scale: DataRect / ViewedDataRect */
static _LFLOAT VWPT_GetViewedDataScale(const VWPT_Transform *vwpt)
{
    _LFLOAT sw, sh;
    VWPT_LFRect dataRCViewed;

    VWPT_GetViewedDataRect(vwpt, &dataRCViewed);

    sw = VWPT_WidthRect(vwpt->dataRC) / VWPT_WidthRect(dataRCViewed);
    sh = VWPT_HeightRect(vwpt->dataRC) / VWPT_HeightRect(dataRCViewed);

    return _MAX(sw, sh);
}

/*===========================================================================*/
/*                                                                           */
/*                     View to Data Functions                                */
/*                                                                           */
/*===========================================================================*/
#define VWPT_Vp2Dp_Type(vwpt, Vx, Vy, Dx, Dy, type) \
    (Dx) = (type) (vwpt->dataCP.X + ((Vx) - vwpt->viewCP.X) / vwpt->scaleFactor); \
    (Dy) = (type) (vwpt->dataCP.Y + (vwpt->viewCP.Y -(Vy)*vwpt->ratioXY) / vwpt->scaleFactor)

#define VWPT_Vp2Dp_Type_Multi(vwpt, VpArray, DpArray, type, ArraySize) \
    do { \
        int np = (ArraySize); \
        while (np-- > 0) { \
            VWPT_Vp2Dp_Type(vwpt, VpArray[np].X, VpArray[np].Y, DpArray[np].X, DpArray[np].Y, type); \
        } \
    } while (0)

__INLINE void VWPT_Vxy2Dxy(const VWPT_Transform *vwpt,
    _LFLOAT Vx, _LFLOAT Vy, _LFLOAT *Dx, _LFLOAT *Dy)
{
    VWPT_Vp2Dp_Type(vwpt, Vx, Vy, *Dx, *Dy, _LFLOAT);
}

__INLINE void VWPT_Vp2Dp(const VWPT_Transform *vwpt,
    const VWPT_LFPoint* Vp, VWPT_LFPoint* Dp)
{
    Dp->X = vwpt->dataCP.X + (Vp->X - vwpt->viewCP.X) / vwpt->scaleFactor;
    Dp->Y = vwpt->dataCP.Y + (vwpt->viewCP.Y - Vp->Y*vwpt->ratioXY) / vwpt->scaleFactor;
}

__INLINE void VWPT_Vrc2Drc(const VWPT_Transform *vwpt,
    const VWPT_Rect* Vrc, VWPT_LFRect* Drc)
{
    VWPT_Vp2Dp_Type(vwpt, Vrc->Xmin, Vrc->Ymax, Drc->Xmin, Drc->Ymin, _LFLOAT);
    VWPT_Vp2Dp_Type(vwpt, Vrc->Xmax, Vrc->Ymin, Drc->Xmax, Drc->Ymax, _LFLOAT);
    _ASSERT(Drc->Xmax >= Drc->Xmin && Drc->Ymax >= Drc->Ymin);
}

/**
 * view length(vl) to data length(dl)
 */
#define VWPT_VLength2DLength(vwpt, viewLength) ((viewLength)/(vwpt)->scaleFactor)

/*===========================================================================*/
/*                                                                           */
/*                     Data to View Functions                                */
/*                                                                           */
/*===========================================================================*/
__INLINE void VWPT_Dxy2Vxy(const VWPT_Transform *vwpt,
    _LFLOAT Dx, _LFLOAT Dy, _INT *Vx, _INT *Vy)
{
    *Vx = (_INT) (vwpt->viewCP.X + vwpt->scaleFactor*(Dx - vwpt->dataCP.X) + 0.5);
    *Vy = (_INT) ((vwpt->viewCP.Y - vwpt->scaleFactor*(Dy - vwpt->dataCP.Y))/vwpt->ratioXY + 0.5);
}

__INLINE void VWPT_Dxy2Vxy_LF(const VWPT_Transform *vwpt,
    _LFLOAT Dx, _LFLOAT Dy, _LFLOAT *Vx, _LFLOAT *Vy)
{
    *Vx = vwpt->viewCP.X + vwpt->scaleFactor*(Dx - vwpt->dataCP.X);
    *Vy = (vwpt->viewCP.Y - vwpt->scaleFactor*(Dy - vwpt->dataCP.Y))/vwpt->ratioXY;
}

__INLINE void VWPT_Dp2Vp(const VWPT_Transform *vwpt,
    const VWPT_LFPoint* Dp, VWPT_Point* Vp)
{
    Vp->X = (_INT) (vwpt->viewCP.X+vwpt->scaleFactor*(Dp->X - vwpt->dataCP.X) + 0.5);
    Vp->Y = (_INT) ((vwpt->viewCP.Y-vwpt->scaleFactor*(Dp->Y - vwpt->dataCP.Y))/vwpt->ratioXY + 0.5);
}

__INLINE void VWPT_Dp2Vp_F(const VWPT_Transform *vwpt,
    const VWPT_LFPoint* Dp, VWPT_FPoint* Vp)
{
    Vp->X = (_FLOAT) (vwpt->viewCP.X+vwpt->scaleFactor*(Dp->X - vwpt->dataCP.X));
    Vp->Y = (_FLOAT) ((vwpt->viewCP.Y-vwpt->scaleFactor*(Dp->Y - vwpt->dataCP.Y))/vwpt->ratioXY);
}

__INLINE void VWPT_Dp2Vp_LF(const VWPT_Transform *vwpt,
    const VWPT_LFPoint* Dp, VWPT_LFPoint* Vp)
{
    Vp->X = vwpt->viewCP.X+vwpt->scaleFactor*(Dp->X - vwpt->dataCP.X);
    Vp->Y = (vwpt->viewCP.Y-vwpt->scaleFactor*(Dp->Y - vwpt->dataCP.Y))/vwpt->ratioXY;    
}

__INLINE void VWPT_Dp2Vp_LF_Multi(const VWPT_Transform *vwpt,
    const VWPT_LFPoint *DpArray, VWPT_LFPoint *VpArray, int ArraySize)
{
    const VWPT_LFPoint *Dp = DpArray;
    VWPT_LFPoint *Vp = VpArray;

    while (ArraySize-- > 0) {
        VWPT_Dp2Vp_LF(vwpt, Dp++, Vp++);
    }  
}

__INLINE void VWPT_Dp2Vp_Multi(const VWPT_Transform *vwpt,
    const VWPT_LFPoint *DpArray, VWPT_Point *VpArray, int ArraySize)
{
    const VWPT_LFPoint *Dp = DpArray;
    VWPT_Point *Vp = VpArray;

    while (ArraySize-- > 0) {
        VWPT_Dp2Vp(vwpt, Dp++, Vp++);
    }
}

__INLINE void VWPT_Dp2Vp_F_Multi(const VWPT_Transform *vwpt,
    const VWPT_LFPoint *DpArray, VWPT_FPoint *VpArray, int ArraySize)
{
    const VWPT_LFPoint *Dp = DpArray;
    VWPT_FPoint *Vp = VpArray;

    while (ArraySize-- > 0) {
        VWPT_Dp2Vp_F(vwpt, Dp++, Vp++);
    }
}

/**
 * ^         max         o------------->
 * |  --------+          |  min 
 * |  |       |          |   +--------|
 * |  |  Drc  |    ==>   |   |   Vrc  |
 * |  +--------          |   |        |
 * | min                 |   ---------+
 *-o------------->       V           max
 */
__INLINE void VWPT_Drc2Vrc(const VWPT_Transform *vwpt,
    const VWPT_LFRect *Drc, VWPT_Rect *Vrc)
{
    VWPT_Dxy2Vxy(vwpt, Drc->Xmin, Drc->Ymax, &Vrc->Xmin, &Vrc->Ymin);
    VWPT_Dxy2Vxy(vwpt, Drc->Xmax, Drc->Ymin, &Vrc->Xmax, &Vrc->Ymax);
}

__INLINE void VWPT_Drc2Vrc_LF(const VWPT_Transform *vwpt,
    const VWPT_LFRect *Drc, VWPT_LFRect *Vrc)
{
    VWPT_Dxy2Vxy_LF(vwpt, Drc->Xmin, Drc->Ymax, &Vrc->Xmin, &Vrc->Ymin);
    VWPT_Dxy2Vxy_LF(vwpt, Drc->Xmax, Drc->Ymin, &Vrc->Xmax, &Vrc->Ymax);
}

#define VWPT_Drcxy2Vrc_LF(vwpt, DrcXmin, DrcYmin, DrcXmax, DrcYmax, Vrc) \
    VWPT_Dxy2Vxy_LF(&(vwpt), DrcXmin, DrcYmax, &(Vrc).Xmin, &(Vrc).Ymin); \
    VWPT_Dxy2Vxy_LF(&(vwpt), DrcXmax, DrcYmin, &(Vrc).Xmax, &(Vrc).Ymax)

/**
 * data length(dl) to view length(vl)
 */
#define VWPT_DLength2VLength(vwpt, dataLength) ((dataLength)*(vwpt)->scaleFactor)

/*===========================================================================*/
/*                                                                           */
/*                     View  Manipulation                                    */
/*                                                                           */
/*===========================================================================*/

/**
 * move viewport's center to position
 */
static void VWPT_CenterAt (VWPT_Transform *vwpt, _LFLOAT vpX, _LFLOAT vpY)
{
    VWPT_Vxy2Dxy(vwpt, vpX, vpY, &vwpt->dataCP.X, &vwpt->dataCP.Y);
}

/**
 * Zoom in or out view. times is scaleFactor, must be > 0
 */
static void VWPT_ZoomTimes (VWPT_Transform *vwpt, _LFLOAT times)
{
    VWPT_SetScale(vwpt, vwpt->scaleFactor*times);
}

/**
 * Just like the above. the difference is the below reset view data's center
 */
static void VWPT_ZoomCenter (VWPT_Transform *vwpt, _FLOAT times)
{
    VWPT_SetScale(vwpt, vwpt->scaleFactor*times);
    VWPT_CenterRect(vwpt->dataRC, vwpt->dataCP);
}

/**
 * display all destination with times of given parameter
 */
static void VWPT_ZoomAll (VWPT_Transform *vwpt, _FLOAT times)
{
    VWPT_CenterRect(vwpt->dataRC, vwpt->dataCP);
    VWPT_SetScale(vwpt, times*VWPT_CalcScale(vwpt, FALSE));
}
 
/**
 * move view, only the center of view data center changed
 * VpOffsetX and VpOffsetY are offsets of Vp
 */
static void VWPT_PanView(VWPT_Transform *vwpt,
    _LFLOAT VpOffsetX, _LFLOAT VpOffsetY)
{
    vwpt->dataCP.X -= (VpOffsetX / vwpt->scaleFactor);
    vwpt->dataCP.Y += (VpOffsetY / vwpt->scaleFactor);
}

/**
 * Zoom at given viewport position Vp (X, Y)
 */
static void VWPT_ZoomAt(VWPT_Transform *vwpt,
    _INT vpX, _INT vpY, _FLOAT times /* 1.0 */)
{
    _LFLOAT dpX, dpY, vpXo, vpYo, vpX2, vpY2;
    VWPT_Vxy2Dxy(vwpt, vpX, vpY, &dpX, &dpY);
    VWPT_Dxy2Vxy_LF(vwpt, dpX, dpY, &vpXo, &vpYo);
    VWPT_ZoomTimes(vwpt, times);
    VWPT_Dxy2Vxy_LF(vwpt, dpX, dpY, &vpX2, &vpY2);
    VWPT_PanView(vwpt, vpXo-vpX2, vpYo-vpY2);
}

#define VWPT_PanView2(vwpt, fromVx, fromVy, toVx, toVy) \
    VWPT_PanView(vwpt, toVx-fromVx, toVy-fromVy)

/**
 * flip view by direction and overlapped percent
 *       ^Y
 *       |  1  2  3
 *       +  4 (5) 6
 *       |  7  8  9
 *       o-----+---->X
 */
static void VWPT_FlipView(VWPT_Transform *vwpt,
    _INT direction, _FLOAT overlappingPercent)
{
    _LFLOAT  dx = VWPT_WidthRect(vwpt->viewRC) / vwpt->scaleFactor;
    _LFLOAT  dy = VWPT_HeightRect(vwpt->viewRC) / vwpt->scaleFactor;
    _FLOAT   fv = (_FLOAT) (1.0 - overlappingPercent / 100);

    switch (direction) {
    case 2:
        vwpt->dataCP.Y += dy*fv;
        break;
    case 8:
        vwpt->dataCP.Y -= dy*fv;
        break;
    case 4:
        vwpt->dataCP.X -= dx*fv;
        break;
    case 6:
        vwpt->dataCP.X += dx*fv;
        break;
    case 1:
        vwpt->dataCP.X -= dx*fv;
        vwpt->dataCP.Y += dy*fv;
        break;
    case 3:
        vwpt->dataCP.X += dx*fv;
        vwpt->dataCP.Y += dy*fv;
        break;
    case 7:
        vwpt->dataCP.X -= dx*fv;
        vwpt->dataCP.Y -= dy*fv;
        break;
    case 9:
        vwpt->dataCP.X += dx*fv;
        vwpt->dataCP.Y -= dy*fv;
        break;
    case 5:
        VWPT_ZoomTimes(vwpt, overlappingPercent / 100);
        break;
    }
}

/**
 * Zoom in(from left to right) or zoom out(from right to left), in viewport
 */

static void VWPT_ZoomBox2(VWPT_Transform *vwpt,
    _LFLOAT Vx, _LFLOAT Vy, _LFLOAT Vx2, _LFLOAT Vy2, _LFLOAT percentage)
{  
    _LFLOAT NTIMES = _ABS(percentage);

    _INT dW = (_INT) ((Vx - Vx2) * NTIMES + 0.5);  /* dW as sign flag */
    _INT dH = (_INT) ((Vy - Vy2) * NTIMES + 0.5);  /* dH > 0 */
    dH = _ABS(dH);

    if (dW != 0 && dH > 0) {
        VWPT_CenterAt(vwpt, (Vx + Vx2)/2, (Vy + Vy2)/2);

        if (dW > 0) {
            /* zoom out */
            VWPT_SetScale(vwpt, vwpt->scaleFactor *
                _MIN(dW/VWPT_WidthRect(vwpt->viewRC)/NTIMES, dH/VWPT_HeightRect(vwpt->viewRC)/NTIMES));
        } else {
            /* zoom in */
            VWPT_SetScale(vwpt, vwpt->scaleFactor *
                _MIN(-VWPT_WidthRect(vwpt->viewRC)*NTIMES/dW, VWPT_HeightRect(vwpt->viewRC)*NTIMES/dH));
        }
    }
}

static void VWPT_ZoomBox(VWPT_Transform *vwpt, VWPT_LFPoint *Vp, VWPT_LFPoint *Vp2, _FLOAT percentage)
{
    VWPT_ZoomBox2(vwpt, Vp->X, Vp->Y, Vp2->X, Vp2->Y, percentage);
}

static void VWPT_ZoomExtent2(VWPT_Transform *vwpt,
    _LFLOAT Dx, _LFLOAT Dy, _LFLOAT Dx2, _LFLOAT Dy2, _FLOAT percentage)
{
    _LFLOAT Vx, Vy, Vx2, Vy2;
    VWPT_Dxy2Vxy_LF(vwpt, Dx, Dy, &Vx, &Vy);
    VWPT_Dxy2Vxy_LF(vwpt, Dx2, Dy2, &Vx2, &Vy2);
    VWPT_ZoomBox2(vwpt, Vx, Vy, Vx2, Vy2, percentage);
}

static void VWPT_ZoomExtent(VWPT_Transform *vwpt, VWPT_LFPoint *Dp, VWPT_LFPoint *Dp2, _FLOAT percentage)
{
    VWPT_ZoomExtent2(vwpt, Dp->X, Dp->Y, Dp2->X, Dp2->Y, percentage);
}

#ifdef __cplusplus
}
#endif

#endif  /* VIEWPORT_H_INCLUDED */
