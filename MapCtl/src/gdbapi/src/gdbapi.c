/**
 * gdbapi.c
 * @brief
 *    GDB_HANDLE implementation
 * @author
 *    ZhangLiang
 * @since
 *    2013-4-25
 * @date
 *    2013-5-31
 */

#include "gdbapi.h"

#include "gdberror.h"
#include "gdbcoldef.h"
#include "gdblayer.h"
#include "gdbshape.h"
#include "gdbfilter.h"
#include "gdbcontext.h"

//# pragma comment (lib, "../../libs/sqlite3/dist/lib/libsqlite3.lib")
//# pragma comment (lib, "../../libs/libtiff/lib/libtiff-5.lib")

/**
 * GDBHandleAlloc
 */
GDBAPI_RESULT GDBHandleAlloc (HGDBHandleOb *pHandle, GDB_HANDLE_TYPE hType,
    void *pvExtraData, int cbSizeData)
{
    HGDBHandleOb hdl = 0;

    switch (hType) {
    case GDB_RAW_HANDLE:
        ALLOC_HANDLE(GDBHandleOb, pvExtraData, cbSizeData, hType, hdl);
        break;
    case GDB_HTYPE_SHAPE:
        ALLOC_HANDLE(GDBShapeInfo, pvExtraData, cbSizeData, hType, hdl);
        break;
    case GDB_HTYPE_CONTEXT:
        ALLOC_HANDLE(GDBContext, pvExtraData, cbSizeData, hType, hdl);
        break;
    case GDB_HTYPE_ERROR:
        ALLOC_HANDLE(GDBError, pvExtraData, cbSizeData, hType, hdl);
        break;
    case GDB_HTYPE_STMT:
        ALLOC_HANDLE(GDBStmt, pvExtraData, cbSizeData, hType, hdl);
        break;
    case GDB_HTYPE_COLDEF:
        ALLOC_HANDLE(GDBColDef, pvExtraData, cbSizeData, hType, hdl);
        break;
    case GDB_HTYPE_COLVAL:
        ALLOC_HANDLE(GDBColVal, pvExtraData, cbSizeData, hType, hdl);
        break;
    case GDB_HTYPE_LAYER:
        ALLOC_HANDLE(GDBLayerInfo, pvExtraData, cbSizeData, hType, hdl);
        break;
    case GDB_HTYPE_FILTER:
        ALLOC_HANDLE(GDBFilter, pvExtraData, cbSizeData, hType, hdl);
        break;
    default:
        return GDBAPI_EHTYPE;
    }

    *pHandle = hdl;
    return GDBAPI_SUCCESS;
}

/**
 * GDBHandleFree
 */
void GDBHandleFree (GDB_HANDLE handle)
{
    if (handle) {
        switch (handle->type) {
        case GDB_HTYPE_SHAPE:
            _GDBShapeClearAll((HGDBShape) handle);
            break;

        case GDB_HTYPE_CONTEXT:
            GDBContextClose (handle, 0);
            break;

        case GDB_HTYPE_STMT:
            GDBStmtFinish(handle, GDBAPI_FALSE, 0);
            break;

        case GDB_HTYPE_FILTER:
            _GDBFilterClearAll((HGDBFilter) handle);
            break;
        }

        FREE_S(handle);
    }
}

/**
 * GDBHandleGetData
 */
int GDBHandleGetData (HGDBHandleOb handle, void ** ppData)
{
    if (!handle) {
        return GDBAPI_EHANDLE;
    }

    if (ppData) {
        *ppData = handle->__exdata;
    }
 
    return handle->size;
}

/**
 * GDBHandleGetType
 */
GDB_HANDLE_TYPE GDBHandleGetType (GDB_HANDLE handle)
{
    if (!handle) {
        return GDB_INVALID_HANDLE;
    } else {
        return ((HGDBHandleOb) handle)->type;
    }
}

/**
 * GDBAttrSet
 */
GDBAPI_RESULT GDBAttrSet (
    GDB_HANDLE handle,
    void *pAttrValue,
    int   cbSizeValue,
    GDB_ATTR_NAME attrName,
    GDB_ERROR herr)
{
    if (cbSizeValue == -1) {
        cbSizeValue = (pAttrValue? strlen((char*)pAttrValue)+1 : 0);
    } else if (cbSizeValue < 0) {
        return
            _GDBSetErrorInfo (herr, GDBAPI_EPARAM, "Invalid cbSizeValue",
                0, 0, __FILE__, __LINE__, __FUNCTION__);
    }

    switch (GET_HTYPE(handle)) {
    case GDB_HTYPE_CONTEXT:
        {
            HGDBContext hctx;
            CAST_HANDLE_CONTEXT(handle, herr, hctx);
            return _GDBCtxSetAttr(hctx, pAttrValue, cbSizeValue, attrName, herr);
        }
    case GDB_HTYPE_COLDEF:
        {
            HGDBColDef hcol;
            CAST_HANDLE_COLDEF(handle, herr, hcol);
            return
                _GDBColDefSetAttr(hcol, pAttrValue, cbSizeValue, attrName, herr);
        }
    case GDB_HTYPE_LAYER:
        {
            HGDBLayer hlayer;
            CAST_HANDLE_LAYER(handle, herr, hlayer);
            return
                _GDBLayerInfoSetAttr(hlayer, pAttrValue, cbSizeValue, attrName, herr);
        }
    case GDB_HTYPE_SHAPE:
        {
            HGDBShape hshape;
            CAST_HANDLE_SHAPE(handle, herr, hshape);
            return
                _GDBShapeInfoSetAttr(hshape, pAttrValue, cbSizeValue, attrName, herr);
        }
    case GDB_HTYPE_COLVAL:
        {
            HGDBColVal hcolval;
            CAST_HANDLE_COLVAL(handle, herr, hcolval);
            return
                _GDBColValSetAttr(hcolval, pAttrValue, cbSizeValue, attrName, herr);
        }
    case GDB_HTYPE_FILTER:
        {
            HGDBFilter hfilter;
            CAST_HANDLE_FILTER(handle, herr, hfilter);
            return
                _GDBFilterSetAttr (hfilter, pAttrValue, cbSizeValue, attrName, herr);
        }
    }

    return
        _GDBSetErrorInfo (herr, GDBAPI_EHANDLE, "Handle Type Not Supported",
            0, 0, __FILE__, __LINE__, __FUNCTION__);
}

/**
 * GDBAttrGet
 */
GDBAPI_RESULT GDBAttrGet (
    GDB_HANDLE handle,
    void *pAttrValue,
    int  *cbSizeValue,
    GDB_ATTR_NAME attrName,
    GDB_ERROR herr)
{
    switch (GET_HTYPE(handle)) {
    case GDB_HTYPE_CONTEXT:
        {
            HGDBContext hctx;
            CAST_HANDLE_CONTEXT(handle, herr, hctx);
            return _GDBCtxGetAttr(hctx, pAttrValue, cbSizeValue, attrName, herr);
        }
    case GDB_HTYPE_LAYER:
        {
            HGDBLayer hlayer;
            CAST_HANDLE_LAYER(handle, herr, hlayer);
            return _GDBLayerInfoGetAttr (hlayer, pAttrValue, cbSizeValue, attrName, herr);
        }

    case GDB_HTYPE_COLDEF:
        {
            HGDBColDef hcoldef;
            CAST_HANDLE_COLDEF(handle, herr, hcoldef);
            return _GDBColDefGetAttr (hcoldef, pAttrValue, cbSizeValue, attrName, herr);
        }

    case GDB_HTYPE_COLVAL:
        {
            HGDBColVal hcolval;
            CAST_HANDLE_COLVAL(handle, herr, hcolval);
            return _GDBColValGetAttr (hcolval, pAttrValue, cbSizeValue, attrName, herr);
        }

    case GDB_HTYPE_SHAPE:
        {
            HGDBShape hshape;
            CAST_HANDLE_SHAPE(handle, herr, hshape);
            return _GDBShapeInfoGetAttr (hshape, pAttrValue, cbSizeValue, attrName, herr);
        }

    case GDB_HTYPE_FILTER:
        {
            HGDBFilter hfilter;
            CAST_HANDLE_FILTER(handle, herr, hfilter);
            return _GDBFilterGetAttr (hfilter, pAttrValue, cbSizeValue, attrName, herr);
        }
    }

    return _GDBSetErrorInfo(herr, GDBAPI_EHTYPE,
        "Handle Type Not Supported",
        0, 0, __FILE__, __LINE__, __FUNCTION__);
}
