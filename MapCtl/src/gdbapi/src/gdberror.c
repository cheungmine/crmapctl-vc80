/**
 * gdberror.c
 *
 *
 */

#include "gdberror.h"

/* sqlite3 api */
#include "./sqlite3/src/sqlite3.h"

const char* GDBErrorGetMsg (HGDBERR hError)
{
    HGDBError herr = (HGDBError) hError;

    *herr->__errmsg = 0;

    if (herr->gdbapi_errcode != GDBAPI_SUCCESS) {
        if (herr->gdbapi_errcode == GDBAPI_SQLITE3_ERROR) {
            if (herr->sqlite3_errcode != SQLITE_OK) {
                snprintf (herr->__errmsg, sizeof(herr->__errmsg), "%s: %s",
                    herr->sqlite3_errmsg, herr->error_source);
            }
        } else {
            snprintf (herr->__errmsg, sizeof(herr->__errmsg), "%s: %s",
                herr->gdbapi_errmsg, herr->error_source);
        }
    }

    herr->__errmsg[sizeof(herr->__errmsg)-1] = 0;
    return herr->__errmsg;
}
