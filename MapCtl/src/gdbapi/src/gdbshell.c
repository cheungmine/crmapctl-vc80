/**
 * @file
 *    gdbshell.c
 * @brief
 *    This file contains code to implement the "gdbshell" command line
 * utility for accessing GDB (GeoDatabase) databases.
 *
 * @author
 *    Zhang Liang \n
 *    350137278@qq.com \n
 *    http://blog.csdn.net/cheungmine
 * @since
 *    2013/4/25
 * @date
 *    2013/6/3
 * @version
 *    0.0.1
 * @note
 *    $ gdbshell [COMMAND] <--options>
 *  COMMAND:
 *  - shape2gdb
 *    shape2gdb --shapefile="C:/~tmp/shpfiles/上海建筑物.shp" --gdb=C:/~tmp/test4.gdb --layertable=SUBJECT
 *    shape2gdb --shapefile="C:/~tmp/shpfiles/上海建筑物.shp" --gdb=C:/~tmp/test4.gdb --layertable=CLIP
 *
 *  - tiff2gdb --tiff="c:/mapdata/img/earth_1km.tif" --gdb="C:/~tmp/test.gdb" --imagetable="img_layer"
 *
 *  - layerovly
 *    layerovly --mode=ai --gdb="C:/~tmp/test4.gdb" --subjectlayer="SUBJECT" --cliplayer="CLIP" --resultlayer="RES1"
 *
 *  - drawshape
 *    drawshp --source=C:/~tmp/shpfiles/DLTB.shp --output="C:/~tmp/DLTB.png" --shapeid=* --width=1280 --length=800
 *    drawshp --source=C:/~tmp/shpfiles/DLTB.shp --output="C:/~tmp/DLTB.png" --shapeid=5,6,7 --format=png --width=1280 --length=800
 *
 *  -drawlayer
 *    drawgdb --gdb="C:/~tmp/test.gdb" --source="SUBJECT" --output="C:/~tmp/subject.png" --format=png --width=1280 --length=800
 *    drawgdb --gdb="C:/~tmp/test.gdb" --source="SUBJECT" --output="C:/~tmp/subject.png" --format=png --width=1280 --length=800 --grid=enabled
 *    drawgdb --gdb="C:/~tmp/test.gdb" --source="SUBJECT" --output="C:/~tmp/subject.png" --format=png --width=1280 --length=800 --wherecond="AREA>100.0" --grid=enabled
 *    drawgdb --gdb="C:/~tmp/test.gdb" --source="SUBJECT" --output="C:/~tmp/subject.png" --format=png --width=1280 --length=800 --grid=enabled --wherecond="SHAPEID in (123,124)"
 *    drawgdb --gdb="C:/~tmp/test4.gdb" --source="RES1" --output="C:/~tmp/RES1.png"
 */

#define _CRTDBG_MAP_ALLOC       
#include<stdlib.h>    
#include<crtdbg.h>  

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef _MSC_VER
# include "getoptw.h"
#else
# include <getopt.h>
#endif

#include "./matalg/matalg.h"

#include "gdbshell.h"


#define GDB_CMD_INVALID      0
#define GDB_CMD_SHAPE2GDB    100
#define GDB_CMD_TIFF2GDB     110
#define GDB_CMD_DRAWSHAPE    200
#define GDB_CMD_DRAWLAYER    210
#define GDB_CMD_LAYEROVLY    400

int exec_cmd_shape2gdb (int argc, char *argv[]);
int exec_cmd_drawshape (int argc, char *argv[]);
int exec_cmd_drawlayer (int argc, char *argv[]);
int exec_cmd_tiff2gdb (int argc, char *argv[]);
int exec_cmd_layerovly (int argc, char *argv[]);

void usage_shape2gdb ()
{
    fprintf (stdout,
        "\nCOMMAND: shape2gdb [options]\n"
        "Import a shape file (*.shp) into GDB\n"
        "\nOptions:\n"
        "  --shapefile=SHPFILE  Full path filename of a shape file (.shp)\n"
        "  --gdb=GDBFILE        Full path filename of a GDB file (.gdb)\n"
        "  --layertable=<TABLENAME>  Optional table name of GDB for shape file\n"
        "\nExamples:\n"
        "  $ gdbshell shape2gdb --shapefile=\"C:/~tmp/shpfiles/river.shp\""
        " --gdb=\"C:/~tmp/test.gdb\" --layertable=\"RIVER\"\n\n"
        "  $ gdbshell shape2gdb --shapefile=C:/~tmp/shpfiles/landarea.shp"
        " --gdb=C:/~tmp/test.gdb\n\n");
    fflush (stdout);
}

void usage_tiff2gdb ()
{
    fprintf (stdout, 
        "\nCOMMAND: tiff2gdb [options]\n"
        "TODO: Import a tif file (*.tiff|*.tif) into GDB\n");
    fflush (stdout);
}

void usage_drawshape ()
{
    fprintf (stdout, 
        "\nCOMMAND: drawshp [options]\n"
        "Draw a shape file onto PNG file\n"
        "\nOptions:\n"
        "  --source=SHPFILE    Full path filename of a shape file (.shp) to be drawn\n"
        "  --output=PNGFILE    Full path filename of a PNG file (.png) generated\n"
        "  --shapeid=IDLIST    A list of shapes id or * for all shapes in shape file to be drawn\n"
        "  --width=PIXELS      Width size of PNG in pixels\n"
        "  --length=PIXELS     Length size of PNG in pixels\n"
        "  --format=IMAGEFMT   Optional image format of output to be created\n"
        "\nExamples:\n"
        "  $ gdbshell drawshape --source=C:/~tmp/shpfiles/DLTB.shp "
        "--output=C:/~tmp/DLTB.png --shapeid=* --format=png --width=1280 --length=800\n\n"
        "  $ gdbshell drawshape --source=C:/~tmp/shpfiles/DLTB.shp "
        "--output=C:/~tmp/DLTB.png --width=1280 --length=800\n\n"
        "  $ gdbshell drawshape --source=C:/~tmp/shpfiles/DLTB.shp "
        "--output=C:/~tmp/DLTB.png --shapeid=\"5,6,7\" --format=png --width=1280 --length=800\n\n"
        "  $ gdbshell drawshape --source=C:/~tmp/shpfiles/DLTB.shp "
        "--output=C:/~tmp/DLTB.png --shapeid=5,6,7\n\n");
    fflush (stdout);
}

void usage_drawlayer ()
{
    fprintf (stdout, 
        "\nCOMMAND: drawlayer [options]\n"
        "Draw a GDB layer table onto PNG file\n"
        "\nOptions:\n"
        "  --gdb=GDBFILE       Full path filename of a GDB file\n"
        "  --source=LAYER      GDB layer table name to be drawn\n"
        "  --output=PNGFILE    Full path filename of a PNG file (.png) generated\n"
        "  --width=PIXELS      Width size of PNG in pixels\n"
        "  --length=PIXELS     Length size of PNG in pixels\n"
        "  --format=IMAGEFMT   Optional image format of output to be created\n"
        "  --wherecond=<WHERE...>  Optional where clause for layer table query\n"
        "  --grid=<enabled>        Optional argument to control grid drawing\n"
        "\nExamples:\n"
        "  $ gdbshell drawlayer --gdb=C:/~tmp/test.gdb --source=RIVER "
        "--output=C:/~tmp/RIVER.png --format=png --width=1280 --length=800 --grid=enabled\n\n"
        "  $ gdbshell drawlayer --gdb=C:/~tmp/test.gdb --source=RIVER "
        "--output=C:/~tmp/RIVER.png --wherecond=\"AREA > 1000\"\n\n"
        "  $ gdbshell drawlayer --gdb=C:/~tmp/test.gdb --source=RIVER "
        "--output=C:/~tmp/RIVER.png --wherecond=\"SHAPEID in (101,102,201,302)\"\n\n");
    fflush (stdout);
}

void usage_layerovly ()
{
    fprintf (stdout, 
        "\nCOMMAND: layerovly [options]\n"
        "Do GDB layers overlay\n"
        "\nOptions:\n"
        "  --gdb=GDBFILE           Full path filename of a GDB file\n"
        "  --mode=areaint          Overlay mode: areaint\n"
        "  --subjectlayer=SUBJECT  Subject layer table name in GDB\n"
        "  --cliplayer=CLIP        Clip layer table name in GDB\n"
        "  --resultlayer=RESULT    Result layer table to be created in GDB\n"
        "  --offset=<INDEX>        Optional start row index in clip layer\n"
        "  --rowcount=<COUNT>      Optional count of rows in clip layer\n"
        "\nExamples:\n"
        "  $ gdbshell layerovly --mode=ai --gdb=C:/~tmp/test.gdb "
        "--subjectlayer=RIVER --cliplayer=BOUND --resultlayer=RIVBD1 --offset=0 --rowcount=-1\n\n"
        "  $ gdbshell layerovly --mode=areaint --gdb=C:/~tmp/test.gdb "
        "--subjectlayer=RIVER --cliplayer=BOUND --resultlayer=RIVBD2\n\n"
        "  $ gdbshell layerovly --mode=areaintersect --gdb=C:/~tmp/test.gdb "
        "--subjectlayer=RIVER --cliplayer=BOUND --resultlayer=RIVBD3 --offset=100 --rowcount=10000\n\n");
    fflush (stdout);
}


static char * SafeCopyString (char *dest, const char *source, size_t cbSizeDest)
{
    if (!dest) {
        return 0;
    }
    if (source) {
        strncpy(dest, source, cbSizeDest - 1);
        dest[cbSizeDest - 1] = 0;
    } else {
        *dest = 0;
    }
    return dest;
}

static int parse_bool(const char *arg)
{
    if (! arg) {
        return 0;
    } else if (! stricmp(arg, "true") ||
        ! stricmp(arg, "yes") ||
        ! stricmp(arg, "enable") || ! stricmp(arg, "enabled") ||
        ! stricmp(arg, "on") ||
        ! stricmp(arg, "1")) {
        return 1;
    } else {
        return 0;
    }
}

static int parse_gdbcmd (const char *arg)
{
    if (!arg) {
        return GDB_CMD_INVALID;
    } else if (!strcmp(arg, "shape2gdb") || !strcmp(arg, "shp2gdb")) {
        return GDB_CMD_SHAPE2GDB;
    } else if (!strcmp(arg, "drawshape") || !strcmp(arg, "drawshp")) {
        return GDB_CMD_DRAWSHAPE;
    } else if (!strcmp(arg, "drawlayer") || !strcmp(arg, "drawgdb")) {
        return GDB_CMD_DRAWLAYER;
    } else if (!strcmp(arg, "tiff2gdb") || !strcmp(arg, "tif2gdb") ||
        !strcmp(arg, "tifffile2gdb") || !strcmp(arg, "tiffile2gdb")) {
        return GDB_CMD_TIFF2GDB;
    } else if (!strcmp(arg, "layeroverlay") || !strcmp(arg, "layerovly")) {
        return GDB_CMD_LAYEROVLY;
    } else {
        return GDB_CMD_INVALID;
    }
}

void print_usage ()
{
    fprintf (stdout, "Usage: gdbshell COMMAND [options]\n");
    fprintf (stdout, "COMMAND:\n");
    fprintf (stdout, "  shape2gdb    Import ESRI shape files (*.shp) into GDB\n");
    fprintf (stdout, "  tiff2gdb     TODO: Import tif images (*.tif|*.tiff) into GDB\n");
    fprintf (stdout, "  layerovly    Overlay of 2 GDB layers\n");
    fprintf (stdout, "  drawshape    Draw shape file\n");
    fprintf (stdout, "  drawlayer    Draw GDB layer\n");
    fflush (stdout);
}


int main (int argc, char *argv[])
{
    int gdbcmd;

    fprintf (stdout,
        "\n------------------------------------------------------------------\n"
        "gdbshell 1.0.9 pre (GDBAPI shell commands) for windows 32/64 bits.\n"
        "Copyright (c) 2013 Cloud Map Spatial Infomation Tech. Inc."
        "\n------------------------------------------------------------------\n");
    fflush (stdout);

    if (argc == 1) {
        print_usage ();
        fprintf (stdout, "type 'gdbshell COMMAND --help' for more information. For example:\n");
        fprintf (stdout, "  $ gdbshell shape2gdb --help\n");
        fprintf (stdout, "gdbshell exits without COMMAND to execute!\n");
        fflush (stdout);
        exit(-1);
    }

    gdbcmd = parse_gdbcmd(argv[1]);

    if (gdbcmd == GDB_CMD_SHAPE2GDB) {
        exec_cmd_shape2gdb(argc, argv);
    } else if (gdbcmd == GDB_CMD_DRAWSHAPE) {
        exec_cmd_drawshape(argc, argv);
    } else if (gdbcmd == GDB_CMD_DRAWLAYER) {
        exec_cmd_drawlayer(argc, argv);
    } else if (gdbcmd == GDB_CMD_TIFF2GDB) {
        exec_cmd_tiff2gdb(argc, argv);
    } else if (gdbcmd == GDB_CMD_LAYEROVLY) {
        exec_cmd_layerovly(argc, argv);
    }

    fflush (stdout);
    _CrtDumpMemoryLeaks();
    return (0);
}


int exec_cmd_shape2gdb (int argc, char *argv[])
{
    int opt, loptsId;

    char shapefile[MAX_FILENAME_LEN+1];

    char gdb[MAX_FILENAME_LEN+1];
    char layertable[MAX_TABLENAME_LEN+1];

    /* parse arguments */
    const struct option lopts[] = {
        {"help", no_argument, 0, 'h'},
        {"shapefile", required_argument, 0, 's'},
        {"gdb", required_argument, 0, 'g'},
        {"layertable", optional_argument, 0, 't'},
        {0, 0, 0, 0}
    };

    static const char *optString = "hs:g:t:";

    shapefile[0] = 0;
    gdb[0] = 0;
    layertable[0] = 0;

    while ((opt = getopt_long(argc, argv, optString, lopts, &loptsId)) != EOF) {
        switch (opt) {
        case 'h':
            usage_shape2gdb ();
            return 0;

        case 's':
            SafeCopyString(shapefile, optarg, sizeof(shapefile));
            break;
        case 'g':
            SafeCopyString(gdb, optarg, sizeof(gdb));
            break;
        case 't':
            SafeCopyString(layertable, optarg, sizeof(layertable));
            break;
        case 0:
            fprintf(stdout, "unknown argument: %s\n", lopts[loptsId].name);
            break;
        default:
            fprintf(stdout, "no argument found.\n");
            break;
        }
    }

    if (!shapefile[0]) {
        fprintf(stdout, "ERROR: command argument: --shapefile SHAPEFILE not found.\n");
        return -1;
    }

    if (!gdb[0]) {
        fprintf(stdout, "ERROR: command argument: --gdb GDB not found.\n");
        return -1;
    }

    if (!layertable[0]) {
        const char *p0, *p1;
        if ((p0 = strrchr(shapefile, '/')) != 0 || (p0 = strrchr(shapefile, '\\')) != 0) {
            p0++;
            p1 = strrchr(shapefile, '.');
            if (p1 > p0 && p1 - p0 < MAX_TABLENAME_LEN) {
                SafeCopyString(layertable, p0, p1-p0+1);
            } else {
                fprintf(stdout, "ERROR: invalid shapefile.\n");
                return -1;
            }
        } else {
            p0 = shapefile;
            p1 = strrchr(shapefile, '.');
            if (p1 > p0 && p1 - p0 <= MAX_TABLENAME_LEN) {
                SafeCopyString(layertable, shapefile, p1-p0+1);
            } else {
                fprintf(stdout, "ERROR: invalid shapefile.\n");
                return -1;
            }
        }
    }

    shape2gdb(shapefile, gdb, layertable, CREMOD_CREATE_NEW);

    return 0;
}


int exec_cmd_drawshape (int argc, char *argv[])
{
    int opt, loptsId;

    char shapefile[MAX_FILENAME_LEN+1];
    char shapeid[256];

    char output[MAX_FILENAME_LEN+1];
    char format[MAX_FILEFMT_LEN+1];
    int width = 1600;
    int length = 900;

    gt_surface_fmt surface_fmt = SURFACE_FMT_PNG;

    /* parse arguments */
    const struct option lopts[] = {
        {"help", no_argument, 0, 'h'},
        {"source", required_argument, 0, 's'},
        {"output", required_argument, 0, 'o'},
        {"shapeid", optional_argument, 0, 'i'},
        {"width", optional_argument, 0, 'w'},
        {"length", optional_argument, 0, 'l'},
        {"format", optional_argument, 0, 'f'},
        {0, 0, 0, 0}
    };

    static const char *optString = "hs:o:i:w:l:f:";

    shapefile[0] = 0;
    output[0] = 0;
    shapeid[0] = 0;
    format[0] = 0;

    while ((opt = getopt_long(argc, argv, optString, lopts, &loptsId)) != EOF) {
        switch (opt) {
        case 'h':
            usage_drawshape ();
            return 0;

        case 's':
            SafeCopyString(shapefile, optarg, sizeof(shapefile));
            break;
        case 'o':
            SafeCopyString(output, optarg, sizeof(output));
            break;
        case 'i':
            SafeCopyString(shapeid, optarg, sizeof(shapeid));
            break;
        case 'f':
            SafeCopyString(format, optarg, sizeof(format));
            break;
        case 'w':
            if (optarg) {
                width = atoi(optarg);
                if (width < 1 || width > 2048) {
                    fprintf(stdout, "invalid surface width.\n");
                    return -1;
                }
            }
            break;
        case 'l':
            if (optarg) {
                length = atoi(optarg);
                if (length < 1 || length > 2048) {
                    fprintf(stdout, "invalid surface length.\n");
                    return -1;
                }
            }
            break;
        case 0:
            fprintf(stdout, "unknown argument: %s\n", lopts[loptsId].name);
            break;
        default:
            fprintf(stdout, "no argument found.\n");
            break;
        }
    }

    if (!shapefile[0] || !output[0]) {
        fprintf(stdout, "ERROR: command argument: --shapefile SHAPEFILE not found.\n");
        return -1;
    }

    if (format[0]) {
    
    }

    if (!shapeid[0]) {
        shapeid[0] = '*';
        shapeid[1] = 0;
    }

    drawshapefile(shapefile, shapeid, output, width, length, surface_fmt);

    return 0;
}


int exec_cmd_drawlayer (int argc, char *argv[])
{
    int opt, loptsId;

    char gdb[MAX_FILENAME_LEN+1];
    char layer[MAX_LAYERTABLE_LEN+1];
    char wherecond[MAX_FILENAME_LEN+1];
    char output[MAX_FILENAME_LEN+1];
    char format[MAX_FILEFMT_LEN+1];

    GDBAPI_BOOL grid = GDBAPI_FALSE;

    int width = 1600;
    int length = 900;

    gt_surface_fmt surface_fmt = SURFACE_FMT_PNG;

    /* parse arguments */
    const struct option lopts[] = {
        {"help", no_argument, 0, 'h'},
        {"gdb", required_argument, 0, 'g'},
        {"source", required_argument, 0, 's'},
        {"output", required_argument, 0, 'o'},
        {"wherecond", optional_argument, 0, 'c'},
        {"width", optional_argument, 0, 'w'},
        {"length", optional_argument, 0, 'l'},
        {"format", optional_argument, 0, 'f'},
        {"grid", optional_argument, 0, 'd'},
        {0, 0, 0, 0}
    };

    static const char *optString = "hg:s:o:c:w:l:f:d:";

    gdb[0] = 0;
    wherecond[0] = 0;
    output[0] = 0;
    format[0] = 0;

    while ((opt = getopt_long(argc, argv, optString, lopts, &loptsId)) != EOF) {
        switch (opt) {
        case 'h':
            usage_drawlayer ();
            return 0;

        case 'g':
            SafeCopyString(gdb, optarg, sizeof(gdb));
            break;
        case 's':
            SafeCopyString(layer, optarg, sizeof(layer));
            break;
        case 'o':
            SafeCopyString(output, optarg, sizeof(output));
            break;
        case 'c':
            SafeCopyString(wherecond, optarg, sizeof(wherecond));
            break;
        case 'f':
            SafeCopyString(format, optarg, sizeof(format));
            break;
        case 'd':
            grid = parse_bool (optarg);
            break;
        case 'w':
            if (optarg) {
                width = atoi(optarg);
                if (width < 1 || width > 2048) {
                    fprintf(stdout, "invalid surface width.\n");
                    return -1;
                }
            }
            break;
        case 'l':
            if (optarg) {
                length = atoi(optarg);
                if (length < 1 || length > 2048) {
                    fprintf(stdout, "invalid surface length.\n");
                    return -1;
                }
            }
            break;
        case 0:
            fprintf(stdout, "unknown argument: %s\n", lopts[loptsId].name);
            break;
        default:
            fprintf(stdout, "no argument found.\n");
            break;
        }
    }

    if (!layer[0] || !output[0]) {
        fprintf(stdout, "ERROR: command argument: --source=LAYERNAME not found.\n");
        return -1;
    }

    if (format[0]) {
    
    }

    return drawgdblayer (gdb, layer, wherecond,
        output, width, length, grid, surface_fmt);
}


int exec_cmd_tiff2gdb (int argc, char *argv[])
{
    int opt, loptsId;

    char tiff[MAX_FILENAME_LEN+1];
    char tfw[MAX_FILENAME_LEN+1];
    char gdb[MAX_FILENAME_LEN+1];
    char imagetable[MAX_TABLENAME_LEN+1];

    /* parse arguments */
    const struct option lopts[] = {
        {"help", no_argument, 0, 'h'},
        {"tiff", required_argument, 0, 's'},
        {"tfw", optional_argument, 0, 'w'},
        {"gdb", required_argument, 0, 'g'},
        {"imagetable", optional_argument, 0, 't'},
        {0, 0, 0, 0}
    };

    static const char *optString = "hs:w:g:t:";

    *tiff = 0;
    *gdb = 0;
    *tfw = 0;
    *imagetable = 0;

    while ((opt = getopt_long(argc, argv, optString, lopts, &loptsId)) != EOF) {
        switch (opt) {
        case 'h':
            usage_tiff2gdb ();
            return 0;

        case 's':
            SafeCopyString(tiff, optarg, sizeof(tiff));
            break;
        case 'd':
            SafeCopyString(gdb, optarg, sizeof(gdb));
            break;
        case 'w':
            SafeCopyString(tfw, optarg, sizeof(tfw));
            break;
        case 't':
            SafeCopyString(imagetable, optarg, sizeof(imagetable));
            break;
        case 0:
            fprintf(stdout, "unknown argument: %s\n", lopts[loptsId].name);
            break;
        default:
            fprintf(stdout, "no argument found.\n");
            break;
        }
    }

    if (!tiff[0]) {
        fprintf(stdout, "ERROR: command argument: --tiff SHAPEFILE not found.\n");
        return -1;
    }

    if (!gdb[0]) {
        fprintf(stdout, "ERROR: command argument: --gdb SQLITEDB not found.\n");
        return -1;
    }

    if (!imagetable[0]) {
        const char *p0, *p1;
        if ((p0 = strrchr(tiff, '/')) != 0 || (p0 = strrchr(tiff, '\\')) != 0) {
            p0++;
            p1 = strrchr(tiff, '.');
            if (p1 > p0 && p1 - p0 < MAX_TABLENAME_LEN) {
                SafeCopyString(imagetable, p0, p1-p0+1);
            } else {
                fprintf(stdout, "ERROR: invalid tiff.\n");
                return -1;
            }
        } else {
            p0 = tiff;
            p1 = strrchr(tiff, '.');
            if (p1 > p0 && p1 - p0 <= MAX_TABLENAME_LEN) {
                SafeCopyString(imagetable, tiff, p1-p0+1);
            } else {
                fprintf(stdout, "ERROR: invalid tiff.\n");
                return -1;
            }
        }
    }

    tiff2gdb(tiff, tfw, gdb, imagetable, CREMOD_CREATE_NEW);

    return 0;
}


int exec_cmd_layerovly (int argc, char *argv[])
{
    int opt, loptsId;

    char gdb[MAX_FILENAME_LEN+1];
    char subjectlayer[MAX_FILENAME_LEN+1];
    char cliplayer[MAX_FILENAME_LEN+1];
    char resultlayer[MAX_FILENAME_LEN+1];
    GDB_OVERLAY_MODE ovlymode = GDB_OVLY_AREA_INTERSECT;

    rowid_t offset = 0;
    rowid_t rowcount = -1;

    /* parse arguments */
    const struct option lopts[] = {
        {"help", no_argument, 0, 'h'},
        {"gdb", required_argument, 0, 'g'},
        {"mode", required_argument, 0, 'm'},
        {"subjectlayer", required_argument, 0, 's'},
        {"cliplayer", required_argument, 0, 'c'},
        {"resultlayer", optional_argument, 0, 'r'},
        {"offset", optional_argument, 0, 'o'},
        {"rowcount", optional_argument, 0, 'n'},
        {0, 0, 0, 0}
    };

    static const char *optString = "hg:m:s:c:r:o:n:";

    *gdb = 0;
    *subjectlayer = 0;
    *cliplayer = 0;
    *resultlayer = 0;

    while ((opt = getopt_long(argc, argv, optString, lopts, &loptsId)) != EOF) {
        switch (opt) {
        case 'h':
            usage_layerovly ();
            return 0;

        case 'g':
            SafeCopyString(gdb, optarg, sizeof(gdb));
            break;
        case 'm':
            if (! stricmp(optarg, "ai") || 
                ! stricmp(optarg, "areaint") ||
                ! stricmp(optarg, "areaovly")) {
                ovlymode = GDB_OVLY_AREA_INTERSECT;
            } else if (! stricmp(optarg, "ad") || 
                ! stricmp(optarg, "areadif") ||
                ! stricmp(optarg, "areadiff") ||
                ! stricmp(optarg, "areadiffer") ||
                ! stricmp(optarg, "areadifference")) {
                ovlymode = GDB_OVLY_AREA_DIFFER;
            } else if (! stricmp(optarg, "au") || 
                    ! stricmp(optarg, "areaunion")) {
                ovlymode = GDB_OVLY_AREA_UNION;
            } else if (! stricmp(optarg, "axor")) {
                ovlymode = GDB_OVLY_AREA_XOR;
            }
            break;
        case 's':
            SafeCopyString(subjectlayer, optarg, sizeof(subjectlayer));
            break;
        case 'c':
            SafeCopyString(cliplayer, optarg, sizeof(cliplayer));
            break;
        case 'r':
            SafeCopyString(resultlayer, optarg, sizeof(resultlayer));
            break;
        case 'o':
            if (optarg) {
                offset = atoi(optarg);
            }
            break;
        case 'n':
            if (optarg) {
                rowcount = atoi(optarg);
            }
            break;

        case 0:
            fprintf(stdout, "unknown argument: %s\n", lopts[loptsId].name);
            break;
        default:
            fprintf(stdout, "no argument found.\n");
            break;
        }
    }

    if (ovlymode == GDB_OVLY_AREA_INTERSECT ||
            ovlymode == GDB_OVLY_AREA_DIFFER) {
        areaovly (ovlymode, gdb, subjectlayer, cliplayer, resultlayer, offset, rowcount);
    } else {
        fprintf(stdout, "ERROR: unsupported layeroverlay mode.\n");
    }

    return 0;
}


int gdbopen (GDB_CONTEXT hctx, char *gdb, char *username, char *passwd, GDBAPI_BOOL nocreate, GDB_ERROR herr)
{
        int ret;

        /* set attrs for a context */
        ret = GDBAttrSet (hctx, gdb, -1, GDB_ATTR_CTX_DBNAME, herr);
        if (ret != GDBAPI_SUCCESS) {
                return ret;
        }

        ret = GDBAttrSet (hctx, username, -1, GDB_ATTR_CTX_USERNAME, herr);
        if (ret != GDBAPI_SUCCESS) {
                return ret;
        }

        ret = GDBAttrSet (hctx, passwd, -1, GDB_ATTR_CTX_PASSWORD, herr);
        if (ret != GDBAPI_SUCCESS) {
                return ret;
        }

        ret = GDBAttrSet (hctx, &nocreate, 0, GDB_ATTR_CTX_NOCREATE, herr);
        if (ret != GDBAPI_SUCCESS) {
                return ret;
        }

        /* open a gdb */
        ret = GDBContextOpen (hctx, herr);
        if (ret == GDBAPI_SUCCESS) {
                fprintf(stdout, "open GDB (%s) successfully.\n", gdb);
        } else {
                fprintf(stdout, "open GDB (%s) error.\n", gdb);
        }

        return ret;
}
