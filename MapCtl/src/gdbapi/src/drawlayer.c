/*
 * drawshape.c
 *
 * cheungmine
 * 2013-5-5
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifdef _MSC_VER
# include <windows.h>
# pragma warning (disable : 4996)
#endif

#include "gdbshell.h"

#include "gdbshape.h"

#include "viewport.h"

/*
 * cairo api
 */
#include "../libs/cairo/include/cairo.h"
#include "../libs/cairo/include/cairo-features.h"
#include "../libs/cairo/include/cairo-svg.h"
#include "../libs/cairo/include/cairo-pdf.h"
#include "../libs/cairo/include/cairo-ps.h"
#include "../libs/cairo/include/cairo-version.h"
#include "../libs/cairo/include/cairo-win32.h"

# pragma comment (lib, "../../libs/cairo/lib/cairo.lib")

#define CHECK_ERROR_RESULT(ret, herror) do { \
    if (ret != GDBAPI_SUCCESS) { \
      if (herror) { \
        fprintf(stdout, "ERROR at %d: %s\n", \
          __LINE__, GDBErrorGetMsg(herror)); \
      } \
      goto ERR_EXIT; \
    } \
  } while (0)


typedef struct GDBDrawInfo
{
  gt_color3 color;
  gt_color3 color2;

  cairo_surface_t *surface;
  cairo_t *cr;

  rowid_t rowcount;

  GDBExtRect dataRect;

  VWPT_Transform vwpt;
  HPOLYGON  plg;
} GDBDrawInfo;


static void fill_rectangle (cairo_t *cr, const VWPT_LFRect *rect,
  gt_color3 *content, gt_color3 *contour)
{
  if (content) {
    cairo_set_source_rgb(cr, content->red, content->green, content->blue);

    cairo_new_path(cr);
    cairo_move_to(cr, rect->Xmin, rect->Ymin);
    cairo_line_to(cr, rect->Xmin, rect->Ymax);
    cairo_line_to(cr, rect->Xmax, rect->Ymax);
    cairo_line_to(cr, rect->Xmax, rect->Ymin);
    cairo_line_to(cr, rect->Xmin, rect->Ymin);
    cairo_close_path(cr);

    if (contour) {
      cairo_fill_preserve(cr);

      cairo_set_source_rgb(cr, contour->red, contour->green, contour->blue);
      cairo_stroke(cr);
    } else {
      cairo_fill(cr);
    }
  } else if (contour) {
    cairo_set_source_rgb(cr, contour->red, contour->green, contour->blue);
    cairo_move_to(cr, rect->Xmin, rect->Ymin);
    cairo_line_to(cr, rect->Xmin, rect->Ymax);
    cairo_line_to(cr, rect->Xmax, rect->Ymax);
    cairo_line_to(cr, rect->Xmax, rect->Ymin);
    cairo_line_to(cr, rect->Xmin, rect->Ymin);
    cairo_stroke(cr);
  }
}

static void build_polygon_path (cairo_t *cr, VWPT_Transform *vwpt, const VWPT_LFPoint *pPoints, int numPoints)
{
  VWPT_LFPoint Vp;
  int i = 0;

  for (; i < numPoints; i++) {
    VWPT_Dp2Vp_LF(vwpt, &pPoints[i], &Vp);

    if (i == 0) {
      cairo_move_to(cr, Vp.X, Vp.Y);
    } else {
      cairo_line_to(cr, Vp.X, Vp.Y);
    }
  }

  VWPT_Dp2Vp_LF(vwpt, &pPoints[0], &Vp);
  cairo_line_to(cr, Vp.X, Vp.Y);
}

static void draw_polygon (cairo_t *cr, VWPT_Transform *vwpt,
  HPOLYGON plg,
  gt_color3 *content,
  gt_color3 *contour)
{
  int ret = 0;
  int partid = 0;
  
  int nPoints;
  VertexType *points;
  int nParts;
  PLGPART_TYPE ptype;

  nParts = PolygonGetPart (plg, -1, 0, 0);

  for (; partid < nParts; partid++) {
    nPoints = PolygonGetPart (plg, partid, &points, &ptype);

    if (ptype == PLGPART_CONTOUR) {
      /* contour path */
      cairo_new_path(cr);
    } else {
      /* hole path */
      cairo_new_sub_path(cr);
    }

    build_polygon_path (cr, vwpt, (const VWPT_LFPoint*) points, nPoints);

    cairo_close_path(cr);
  }

  if (partid) {
    /* success returns 0; */
    cairo_set_source_rgb(cr, content->red, content->green, content->blue);
    cairo_fill_preserve(cr);

    cairo_set_source_rgb(cr, contour->red, contour->green, contour->blue);
    cairo_stroke(cr);
  }
}


static int LayerOnQueryRow (
  GDB_CONTEXT hCtx,
  rowid_t     rowNo,
  int         numCols,
  const GDB_COLVAL *hColValues,
  const GDB_SHAPE hShape,
  void        *inParam,
  GDB_ERROR   hError)
{
  GDBDrawInfo *gdi = (GDBDrawInfo*) inParam;

  if (! gdi->plg) {
    const GDBExtBound *pBound;
    GDBAttrGet (hShape, (void**) &pBound, 0, GDB_ATTR_SHAPE_BOUNDADDR, hError);

    if (gdi->rowcount == 0) {
      gdi->dataRect = pBound->_MBR;
    } else {
      if (gdi->dataRect.Xmin > pBound->Xmin) {
        gdi->dataRect.Xmin = pBound->Xmin;
      }
      if (gdi->dataRect.Ymin > pBound->Ymin) {
        gdi->dataRect.Ymin = pBound->Ymin;
      }
      if (gdi->dataRect.Xmax < pBound->Xmax) {
        gdi->dataRect.Xmax = pBound->Xmax;
      }
      if (gdi->dataRect.Ymax < pBound->Ymax) {
        gdi->dataRect.Ymax = pBound->Ymax;
      }
    }
  } else {
    /* TODO: draw only polygon shape */
    void *wkb;
    int size = 0;

    GDBAttrGet (hShape, (void**) &wkb, &size, GDB_ATTR_SHAPE_WKBADDR, hError);

    if (size) {
      BuildPolygonFromWKB (gdi->plg, (const unsigned char*) wkb, size, 3);
      draw_polygon (gdi->cr, &gdi->vwpt, gdi->plg, &gdi->color, &gdi->color2);
    }
  }

  gdi->rowcount++;

  return 1;
}


int drawgdblayer (char *gdb, char *layertable, char *wherecond,
  char *output, int width, int length,
  GDBAPI_BOOL gridEnabled, gt_surface_fmt format)
{
  int ret;

  clock_t t0, t1;

  GDB_CONTEXT    hctx = 0;
  GDB_ERROR      herr = 0;
  GDB_LAYER      hlayer = 0;

  GDBDrawInfo    gdi;

  GDBExtRect  layerRect;

  VWPT_LFRect dataRC;
  VWPT_LFRect viewRC;

  GDB_TABLE_TYPE tabletype;

  memset(&gdi, 0, sizeof(GDBDrawInfo));

  gdi.surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, width, length);
  if (!gdi.surface) {
    printf("ERROR: cairo_image_surface_create ()\n");
    goto ERR_EXIT;
  }

  gdi.cr = cairo_create(gdi.surface);
  if (!gdi.cr) {
    cairo_surface_destroy(gdi.surface);
    gdi.surface = 0;
    printf("ERROR: cairo_create ()\n");
    goto ERR_EXIT;
  }

  gdi.color.red = 0.0;
  gdi.color.green = 1.0;
  gdi.color.blue = 0.0;

  gdi.color2.red = 0.0;
  gdi.color2.green = 0.0;
  gdi.color2.blue = 1.0;

  cairo_set_source_rgb(gdi.cr, gdi.color.red, gdi.color.green, gdi.color.blue);
  cairo_set_line_width(gdi.cr, 0.5);
  cairo_set_line_cap (gdi.cr, CAIRO_LINE_CAP_BUTT);

  cairo_set_antialias (gdi.cr, CAIRO_ANTIALIAS_SUBPIXEL);

  t0 = clock();

  ret = GDBHandleAlloc (&hctx, GDB_HTYPE_CONTEXT, 0, 0);
  CHECK_ERROR_RESULT(ret, 0);

  ret = GDBHandleAlloc (&herr, GDB_HTYPE_ERROR, 0, 0);
  CHECK_ERROR_RESULT(ret, 0);

  ret = gdbopen (hctx, gdb, "gdbshell", "gdbshell", 1, herr);
  CHECK_ERROR_RESULT(ret, herr);

  ret = GDBHandleAlloc (&hlayer, GDB_HTYPE_LAYER, 0, 0);
  CHECK_ERROR_RESULT(ret, 0);

  tabletype = GDB_TABLE_LAYER;
  ret = GDBTableGetInfo (hctx, layertable, &tabletype, 0, 0, 0, hlayer, herr);
  CHECK_ERROR_RESULT(ret, 0);

  GDBAttrGet (hlayer, &layerRect, 0, GDB_ATTR_LAYER_RECT, herr);

  /* get rect extent of layer */
  ret = GDBLayerQuery (hctx, hlayer, 0, 0, 0, 0, 0, wherecond,
    LayerOnQueryRow, (void*) &gdi, herr);
  CHECK_ERROR_RESULT(ret, 0);

  gdi.rowcount = 0;

  PolygonAlloc (0, &gdi.plg, 1);

  VWPT_SetRect(viewRC, 0, 0, width, length, _LFLOAT);
  VWPT_SetRect(dataRC, gdi.dataRect.Xmin, gdi.dataRect.Ymin,
    gdi.dataRect.Xmax, gdi.dataRect.Ymax, _LFLOAT);
  VWPT_TransformInitAll(&gdi.vwpt, &viewRC, &dataRC, 1);
  VWPT_ZoomAll(&gdi.vwpt, 1.0);

  VWPT_ZoomExtent2(&gdi.vwpt, dataRC.Xmin, dataRC.Ymin, dataRC.Xmax, dataRC.Ymax, 100);

  VWPT_ZoomTimes(&gdi.vwpt, 0.9);

  VWPT_Drc2Vrc_LF(&gdi.vwpt, &dataRC, &viewRC);

  if (gridEnabled) {

  }

  /* draw shapes of layer */
  ret = GDBLayerQuery (hctx, hlayer, 0, 0, 0, 0, 0, wherecond,
    LayerOnQueryRow, (void*) &gdi, herr);
  CHECK_ERROR_RESULT(ret, 0);

  fill_rectangle (gdi.cr, &viewRC, 0, &gdi.color2);

  /* clear draw info */
  cairo_destroy (gdi.cr);
  cairo_surface_write_to_png (gdi.surface, output);
  cairo_surface_destroy (gdi.surface);

  GDBHandleFree (hlayer);
  GDBHandleFree (hctx);
  GDBHandleFree (herr);
  PolygonFree (&gdi.plg, 1);

  t1 = clock();
  fprintf(stdout, "\ndrawlayer to (%s) successfully.\n", output);
  fprintf(stdout, "\tTotal %d shapes drawn.\n", (int) gdi.rowcount);
  fprintf(stdout, "\tElapsed %d milli-seconds.\n", t1 - t0);

  return (0);

ERR_EXIT:
  fprintf(stdout, "\ndrawlayer error!\n");

  cairo_destroy (gdi.cr);
  cairo_surface_destroy (gdi.surface);

  GDBHandleFree (hlayer);
  GDBHandleFree (hctx);
  GDBHandleFree (herr);
  PolygonFree (&gdi.plg, 1);

  return (-1);
}
�