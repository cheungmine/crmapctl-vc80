/**
 * @file
 *    gdbapi.h
 * @brief
 *    GDBAPI public header defines the interface that the GDBAPI library
 *    presents to client programs.
 * @author
 *    Zhang Liang \n
 *    350137278@qq.com \n
 *    http://blog.csdn.net/cheungmine
 * @since
 *    2013/4/24
 * @date
 *    2014/3/29
 * @version
 *    0.0.1
 * @note
 *
 * This header file defines the interface that the GDBAPI library presents to
 * client programs.  If a C-function, structure, datatype, or constant
 * definition does not appear in this file, then it is not a published API of
 * GDBAPI, is subject to change without notice, and should not be referenced
 * by programs that use GDBAPI.
 *
 * Some of the definitions that are in this file are marked as "experimental".
 * Experimental interfaces are normally new features recently added to GDBAPI.
 * We do not anticipate changes to experimental interfaces but reserve the
 * right to make minor changes if experience from use "in the wild" suggest
 * such changes are prudent.
 *
 * The official C-language API documentation for GDBAPI is derived from
 * comments in this file.  This file is the authoritative source on how GDBAPI
 * interfaces are suppose to operate.
 *
 * The name of this file under configuration management is "gdbapi.h.in". The
 * makefile makes minor changes to this file (such as inserting the version
 * number) and changes its name to "gdbapi.h" as part of the build process.
 */

#ifndef _GDBAPI_H
#define _GDBAPI_H

#ifdef    __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#if defined (_SVR4) || defined (SVR4) || defined (__OpenBSD__) || defined (_sgi) || defined (__sun) || defined (sun) || defined (__digital__) || defined (__HP_cc)
    #include <inttypes.h>

    #elif defined (_MSC_VER) && _MSC_VER < 1600
        /* VS 2010 (_MSC_VER 1600) has stdint.h */
        typedef __int8 int8_t;
        typedef unsigned __int8 uint8_t;
        typedef __int16 int16_t;
        typedef unsigned __int16 uint16_t;
        typedef __int32 int32_t;
        typedef unsigned __int32 uint32_t;
        typedef __int64 int64_t;
        typedef unsigned __int64 uint64_t;
    #elif defined (_AIX)
        # include <sys/inttypes.h>
    #else
        # include <stdint.h>
#endif


/**
 * GDBAPI_RESULT
 * @brief
 *    GDB Result Code Definitions
 */
typedef int GDBAPI_RESULT;

#define GDBAPI_SUCCESS     0
#define GDBAPI_ERROR     (-1)
#define GDBAPI_EINDEX    (-1)
#define GDBAPI_EHANDLE   (-2)
#define GDBAPI_EHTYPE    (-3)
#define GDBAPI_EOUTMEM   (-4)
#define GDBAPI_EDBINST   (-5)
#define GDBAPI_EPARAM    (-6)
#define GDBAPI_ENOTIMPL  (-7)
#define GDBAPI_EAPPERR   (-8)

#define GDBAPI_EATTR     (-20)
#define GDBAPI_EATTRVAL  (-21)
#define GDBAPI_ERDONLY   (-22)
#define GDBAPI_EATTRWO   (-23)

#define GDBAPI_ECOLDEF   (-30)

#define GDBAPI_SQLITE3_ERROR (-100)

#define GDBAPI_DB_ERROR  (1)
#define GDBAPI_DB_BUSY   (5)
#define GDBAPI_DB_ROW    (100)
#define GDBAPI_DB_DONE   (101)

typedef size_t rowid_t;


/**
 * GDBAPI_BOOL
 * @brief
 *    GDB BOOL
 */
typedef int GDBAPI_BOOL;

#define GDBAPI_TRUE    1
#define GDBAPI_FALSE   0


/**
 * @brief
 *    GDB const definitions
 */
#define GDB_FILENAME_MAX_LEN    260
#define GDB_TABLENAME_MAX_LEN   30
#define GDB_TABLESPACE_LEN      30
#define GDB_OWNER_LEN           30
#define GDB_SPATIAL_NUMCOLS     8
#define GDB_TABLE_MAX_NUMCOLS   800

#define GDBAPI_STOP_END  0
#define GDBAPI_STOP_ERR  (0x80000001)
#define GDBAPI_NEXT_ROW  1
#define GDBAPI_SKIP_ROW  (-1)


/**
 * GDB_HANDLE
 * @brief
 *    GDB_HANDLE Types
 */
typedef struct GDBHandleObject* GDB_HANDLE;

typedef GDB_HANDLE GDB_CONTEXT;
typedef GDB_HANDLE GDB_STMT;
typedef GDB_HANDLE GDB_ERROR;
typedef GDB_HANDLE GDB_COLDEF;
typedef GDB_HANDLE GDB_COLVAL;
typedef GDB_HANDLE GDB_LAYER;
typedef GDB_HANDLE GDB_FILTER;
typedef GDB_HANDLE GDB_RASTER;
typedef GDB_HANDLE GDB_SHAPE;
typedef GDB_HANDLE GDB_IMAGE;
typedef GDB_HANDLE GDB_SPATREF;


/**
 * GDB_HANDLE_TYPE
 * @brief
 *    GDB Handle Type Enumerations
 *
 */
typedef enum GDB_HANDLE_TYPE
{
    GDB_INVALID_HANDLE = -1,
    GDB_RAW_HANDLE = 0,
    GDB_HTYPE_CONTEXT,
    GDB_HTYPE_STMT,
    GDB_HTYPE_COLDEF,
    GDB_HTYPE_COLVAL,
    GDB_HTYPE_LAYER,
    GDB_HTYPE_IMAGE,
    GDB_HTYPE_SHAPE,
    GDB_HTYPE_IMGTILE,
    GDB_HTYPE_FILTER,
    GDB_HTYPE_SPATREF,
    GDB_HTYPE_ERROR
} GDB_HANDLE_TYPE;


/**
 * @brief GDB Param Type for GDBStmtBind() Enumerations
 */
typedef enum
{
    GDB_PARAM_NULL = 0x00,
    GDB_PARAM_INT = 0x10,
    GDB_PARAM_DOUBLE = 0x20,
    GDB_PARAM_TEXT = 0x30,
    GDB_PARAM_BLOB = 0x40,
    GDB_PARAM_INT64 = 0x50
} GDB_PARAM_TYPE;


/**
 * GDB_COLDEF_TYPE
 * @brief
 *    GDB Column data Type Enumerations
 */
typedef enum
{
    GDB_CTYPE_INVALID = GDB_PARAM_NULL,
    GDB_CTYPE_INTEGER = GDB_PARAM_INT,
    GDB_CTYPE_INTEGER_AUTOINC = GDB_PARAM_INT+0x01,
    GDB_CTYPE_BIGINTEGER = GDB_PARAM_INT64,
    GDB_CTYPE_NUMERIC = GDB_PARAM_DOUBLE,
    GDB_CTYPE_STRING = GDB_PARAM_TEXT,
    GDB_CTYPE_DATETIME = GDB_PARAM_TEXT+0x01,
    GDB_CTYPE_DATETIME_DEFAULT = GDB_PARAM_TEXT+0x02,
    GDB_CTYPE_BLOB = GDB_PARAM_BLOB,
    GDB_CTYPE_SHAPE = GDB_PARAM_BLOB+0x01,
    GDB_CTYPE_IMAGE = GDB_PARAM_BLOB+0x02
} GDB_COLDEF_TYPE;

#define GDB_COLTYPE_TO_PARAMTYPE(ctype)  \
    ((GDB_PARAM_TYPE) ((((int)(ctype))>>4)<<4))


/**
 * GDB_TABLE_TYPE 
 * @brief
 *    GDB Table Type Enumerations
 */
typedef enum
{
    GDB_TABLE_GENERIC,
    GDB_TABLE_LAYER,
    GDB_TABLE_IMAGE
} GDB_TABLE_TYPE;


/**
 * GDB_ATTR_NAME
 * @brief
 *    GDB Handle Attributes Enumerations
 */
typedef enum
{
    GDB_ATTR_CTX_DBNAME,
    GDB_ATTR_CTX_USERNAME,
    GDB_ATTR_CTX_PASSWORD,
    GDB_ATTR_CTX_SINGLETHREAD,
    GDB_ATTR_CTX_READONLY,
    GDB_ATTR_CTX_NOCREATE,
    GDB_ATTR_CTX_NOSHARECACHE,
    GDB_ATTR_CTX_TABLESPACE,
    GDB_ATTR_CTX_OWNER,

    GDB_ATTR_COLDEF_NAME,
    GDB_ATTR_COLDEF_TYPE,
    GDB_ATTR_COLDEF_PRECISION,
    GDB_ATTR_COLDEF_LENGTH,
    GDB_ATTR_COLDEF_SCALE,
    GDB_ATTR_COLDEF_PRIKEY,
    GDB_ATTR_COLDEF_UNIQUE,
    GDB_ATTR_COLDEF_NOTNULL,

    GDB_ATTR_COLVAL_INDEX,
    GDB_ATTR_COLVAL_TYPE,
    GDB_ATTR_COLVAL_PARAMTYPE,
    GDB_ATTR_COLVAL_ADDR,
    GDB_ATTR_COLVAL_COLNAME,

    GDB_ATTR_LAYER_LAYERID,
    GDB_ATTR_LAYER_TABLESPACE,
    GDB_ATTR_LAYER_OWNER,
    GDB_ATTR_LAYER_TABLENAME,
    GDB_ATTR_LAYER_SPATIALCOL,
    GDB_ATTR_LAYER_ROWIDCOL,
    GDB_ATTR_LAYER_XMINCOL,
    GDB_ATTR_LAYER_YMINCOL,
    GDB_ATTR_LAYER_XMAXCOL,
    GDB_ATTR_LAYER_YMAXCOL,
    GDB_ATTR_LAYER_LENGTHCOL,
    GDB_ATTR_LAYER_AREACOL,
    GDB_ATTR_LAYER_LAYERTYPE,
    GDB_ATTR_LAYER_LAYERMASK,
    GDB_ATTR_LAYER_LEVELMIN,
    GDB_ATTR_LAYER_LEVELMAX,
    GDB_ATTR_LAYER_BOUNDS,
    GDB_ATTR_LAYER_RECT,
    GDB_ATTR_LAYER_XMIN,
    GDB_ATTR_LAYER_YMIN,
    GDB_ATTR_LAYER_XMAX,
    GDB_ATTR_LAYER_YMAX,
    GDB_ATTR_LAYER_ZMIN,
    GDB_ATTR_LAYER_ZMAX,
    GDB_ATTR_LAYER_MMIN,
    GDB_ATTR_LAYER_MMAX,
    GDB_ATTR_LAYER_REFID,
    GDB_ATTR_LAYER_MINIMUMID,
    GDB_ATTR_LAYER_DESCRIPT,
    GDB_ATTR_LAYER_CREDATE,

    GDB_ATTR_SHAPE_ROWID,
    GDB_ATTR_SHAPE_TYPE,
    GDB_ATTR_SHAPE_TYPENAME,
    GDB_ATTR_SHAPE_BOUND,
    GDB_ATTR_SHAPE_BOUNDADDR,
    GDB_ATTR_SHAPE_RECT,
    GDB_ATTR_SHAPE_XMIN,
    GDB_ATTR_SHAPE_YMIN,
    GDB_ATTR_SHAPE_XMAX,
    GDB_ATTR_SHAPE_YMAX,
    GDB_ATTR_SHAPE_ZMIN,
    GDB_ATTR_SHAPE_ZMAX,
    GDB_ATTR_SHAPE_MMIN,
    GDB_ATTR_SHAPE_MMAX,
    GDB_ATTR_SHAPE_WKBCOPY,
    GDB_ATTR_SHAPE_WKBADDR,
    GDB_ATTR_SHAPE_LENGTH,
    GDB_ATTR_SHAPE_AREA,
    GDB_ATTR_SHAPE_ISNULL,

    GDB_ATTR_FLTR_METHOD,
    GDB_ATTR_FLTR_SHAPE, /* Set Attached */
    GDB_ATTR_FLTR_RECT,
    GDB_ATTR_FLTR_ROWID,
    GDB_ATTR_FLTR_LAYER, /* Set Attached */
    GDB_ATTR_FLTR_CLIPPING,
    GDB_ATTR_FLTR_TRUTH,

    /* TODO: */
    GDB_ATTR_SPREF_ID,
    GDB_ATTR_SPREF_FALSEX,
    GDB_ATTR_SPREF_FALSEY,

    /* READ ONLY ATTRIBUTES FOR SCHEMA */
    GDB_ATTR_CTX_RO_TABLESPACES,
    GDB_ATTR_CTX_RO_LAYERIDS
} GDB_ATTR_NAME;


/**
 * GDB_FLTR_METHOD
 * @brief
 *    GDB Layer Query filter
 */
typedef enum
{
    GDB_FLTR_RECT_OVERLAP,
    GDB_FLTR_AREA_INTERSECT

  /* TODO */
} GDB_FLTR_METHOD;


/**
 * GDB_CREATE_MODE
 * @brief
 *    GDB Layer Create Mode
 */
typedef enum
{
    GDB_ALWAYS_CREATE_NEW,
    GDB_APPEND_EXISTS_ONLY,
    GDB_CREATE_IF_NOT_EXISTS
} GDB_CREATE_MODE;


/**
 * GDB_OVERLAY_MODE
 * @brief
 *    GDB Layer Overlay Mode
 */
typedef enum
{
    GDB_OVLY_ENV_OVERLAP,
    GDB_OVLY_AREA_INTERSECT,
    GDB_OVLY_AREA_DIFFER,
    GDB_OVLY_AREA_UNION,
    GDB_OVLY_AREA_XOR
} GDB_OVERLAY_MODE;


/**
 * GDBPoint
 * @brief
 *    Point type with X,Y dimensions
 */
typedef struct GDBPoint
{
    double X;
    double Y;
} GDBPoint;


/**
 * GDBPointZ
 * @brief
 *    Point type with X,Y,Z dimensions
 */
typedef struct GDBPointZ
{
    double X;
    double Y;
    double Z;
} GDBPointZ;


/**
 * GDBPointZM
 * @brief
 *    Point type with X,Y,Z,M dimensions
 */
typedef struct GDBPointZM
{
    double X;
    double Y;
    double Z;
    double M;
} GDBPointZM;


/**
 * GDBExtRect
 * @brief
 *    XY-Bounds of geo-data including X,Y dimensions
 */
typedef struct GDBExtRect
{
    double Xmin;
    double Ymin;
    double Xmax;
    double Ymax;
} GDBExtRect;


/**
 * GDBExtBound
 * @brief
 *    Bounds of geo-data including X,Y,Z,M dimensions
 */
typedef struct GDBExtBound
{
    union {
        struct {
            double Xmin;
            double Ymin;
            double Xmax;
            double Ymax;
        };
        GDBExtRect  _MBR;
    };

    double Zmin;
    double Zmax;
    double Mmin;
    double Mmax;
} GDBExtBound;


/**
 * GDBExtGrid
 * @brief
 *    Integer XY-Bounds of including X,Y dimensions
 */
typedef struct GDBExtGrid
{
    int MinXI;
    int MinYI;
    int MaxXI;
    int MaxYI;
} GDBExtGrid;


/**
 * GDBCALLBACK_ExecuteInsertRow
 * @brief
 *    Callback function declaration used by GDBExecuteInsert.
 *    The implementation of this callback is the caller's duty.
 *    Typically use GDBCALLBACK_ExecuteInsertRow function to set values of
 *    attribute columns and saptial.
 *
 * @return
 *    1 for next row or 0 to stop.
 */
typedef int (* GDBCALLBACK_ExecuteInsertRow) (
    GDB_CONTEXT hCtx,
    rowid_t     rowNo,
    int         numCols,
    GDB_COLVAL  *hColValues,
    GDB_HANDLE  hSpatial,
    void        *inParam,
    GDB_ERROR   hError);


typedef GDBCALLBACK_ExecuteInsertRow GDBCALLBACK_ExecuteInsertShape;


typedef int (* GDBCALLBACK_OnFetchRow) (
    GDB_CONTEXT hCtx,
    rowid_t     rowNo,
    int         numCols,
    const GDB_COLVAL *hColValues,
    const GDB_HANDLE hSpatial,
    void        *inParam,
    GDB_ERROR   hError);


/**
 * GDBCALLBACK_ImportSHPFileOnPrepare
 * @brief
 *    Prototype of callback invoked by GDBImportSHPFile.
 * @note
 *    This function will be invoked once after GDBImportSHPFile has
 *    load the profile of shpFile.
 *
 *    General infomation about a shapefile and layertable can be obtained
 *    here and now.
 *
 *    It is the caller's responsibility to implement this function.    
 *    If you don't care anything, just return 1 to continue.
 * 
 */
typedef int (* GDBCALLBACK_ImportSHPFileOnPrepare) (
    GDB_CONTEXT hCtx,
    const char *shpTypeName,
    int totalShapes,
    GDB_COLDEF *hColdefs,
    int *pColinds,
    int numColdefs,
    GDB_LAYER layerinfo,
    void *inParam,
    GDB_ERROR hError);


/**
 * GDBCALLBACK_ImportSHPFileOnExecute
 * @brief
 *    Prototype of callback invoked by GDBImportSHPFile.
 * @note
 *    This function will be tiggered as many as the amount of shapes.
 *
 *    Every time after a new shape together attributes has been read over
 *    and prior to inserting into layer table this callback function is
 *    invoked.
 *
 *    All information about a shape being inserted can be obtained herein.
 *
 *    Any more information can be obtained by using GDBAttrGet with
 *    shapeinfo.
 *
 *    It is the caller's responsibility to implement this function.
 */
typedef int (* GDBCALLBACK_ImportSHPFileOnExecute) (
    GDB_CONTEXT hCtx,
    int totalShapes,
    int shapeIndex,
    int nSHPType,
    int numparts,
    int numpoints,
    int numCols,
    GDB_COLVAL *Colvals,
    GDB_SHAPE shapeinfo,
    void *inParam,
    GDB_ERROR hError);


/**
 * GDBCALLBACK_ImportSHPFileOnFinish
 * @brief
 *    Prototype of callback invoked by GDBImportSHPFile.
 * @note
 *    This function will be invoked once after GDBImportSHPFile has
 *    imported all shapes from shpFile.
 *
 *    Any more information can be obtained by using GDBAttrGet with
 *    layerinfo.
 *
 *    It is the caller's responsibility to implement this function.
 */
typedef int (* GDBCALLBACK_ImportSHPFileOnFinish) (
    GDB_CONTEXT hCtx,
    int numShapesImported,
    int numShapesIgnored,
    const GDB_LAYER layerinfo,
    void *inParam,
    GDB_ERROR hError);


/**
 * GDBCALLBACK_LayerOverlayOnPrepare
 * @brief
 *    Prototype of callback invoked by GDBLayerOverlay.
 */
typedef int (* GDBCALLBACK_LayerOverlayOnPrepare) (
    GDB_CONTEXT hCtx,
    rowid_t     clipRowcount,
    GDB_LAYER   resultLayer,
    GDB_COLDEF  *resultColdefs,
    int         numColdefs,
    void        *inParam,
    GDB_ERROR   hError);


/**
 * GDBCallback_LayerOverlayOnExecute
 * @brief
 *    Prototype of callback invoked by GDBLayerOverlay.
 */
typedef int (* GDBCallback_LayerOverlayOnExecute) (
    GDB_CONTEXT hCtx,
    rowid_t     clipRowno,
    GDB_SHAPE   subjectShape,
    GDB_SHAPE   clipShape,
    GDB_SHAPE   resultShape,
    void        *inParam,
    GDB_ERROR   hError);


/**
 * GDBCallback_LayerOverlayOnFinish
 * @brief
 *    Prototype of callback invoked by GDBLayerOverlay.
 */
typedef int (* GDBCallback_LayerOverlayOnFinish) (
    GDB_CONTEXT hCtx,
    rowid_t     resultRowcount,
    GDB_LAYER   resultLayer,
    void        *inParam,
    GDB_ERROR   hError);


/**
 * GDBHandleAlloc
 * @brief
 *    Create a GDB_HANDLE by specified hType.
 *
 * @param pHandle
 *    pointer an handle recieve output.
 * @param hType
 *    specify a handle type with GDB_HANDLE_TYPE.
 * @param pvExtraData
 *    pointer to extra data. can be NULL if no input data required.
 * @param cbSizeData
 *    size of extra data in bytes.
 * @return
 *    GDBAPI_SUCCESS for success,
 *    other GDBAPI_RESULT code if error.
 * @retval
 *    GDBAPI_RESULT
 * @note
 *  <pre>
 *    1) Specifing handle type with GDB_RAW_HANDLE to get a memory buffer.
 *    2) Specifing handle type other than listed in GDB_HANDLE_TYPE causes
 *       GDBAPI_EHTYPE error.
 *    3) Never to specify handle type with GDB_INVALID_HANDLE.
 *    Below code snippet shows how to create a GDB_CONTEXT without extra
 *    data and open a GDB:
      @verbatim
      GDBAPI_RESULT ret;
      GDB_CONTEXT hctx;
      GDB_ERROR   hError;
      const char gdbfile[] = "./test.gdb";

      ret = GDBHandleAlloc (&hError, GDB_HTYPE_ERR, 0, 0);
      assert (ret == GDBAPI_SUCCESS);

      ret = GDBHandleAlloc (&hctx, GDB_HTYPE_CONTEXT, 0, 0);
      assert (ret == GDBAPI_SUCCESS);

      GDBAttrSet (hctx, gdbfile, strlen(gdbfile), GDB_ATTR_CTX_DBNAME, hError);
      GDBAttrSet (hctx, "gdbshell", strlen("gdbshell"), GDB_ATTR_CTX_USERNAME, hError);
      GDBAttrSet (hctx, "gdbshell", strlen("gdbshell"), GDB_ATTR_CTX_PASSWORD, hError);

      ret = GDBContextOpen (hctx, hError);
      if (ret != GDBAPI_SUCCESS) {
        fprintf(stdout, "**** Open GDB '%s' failed:\n%s\n", gdbfile,
          GDBErrorGetMessage(hError));
      } else {
        fprintf(stdout, "**** Open GDB '%s' successfully.\n", gdbfile);
      }
      ...
      // free all handles after using
      GDBContextClose (hctx, hError); // this line can be removed
      GDBHandleFree (hError);
      GDBHandleFree (hctx);
      @endverbatim
 *  </pre>
 * 
 * @see
 *    GDBHandleFree
 */
extern
GDBAPI_RESULT GDBHandleAlloc (
    GDB_HANDLE *pHandle,
    GDB_HANDLE_TYPE hType,
    void *pvExtraData,
    int cbSizeData);


/**
 * GDBHandleFree
 * @brief
 *    Destroy a GDB_HANDLE and all memories associated with it.
 *
 * @param handle
 *    A valid GDB_HANDLE created successfully from GDBHandleAlloc.
 * @retval
 *    N/A
 * @note
 *    Although it is harmless to apply GDBHandleFree on a NULL handle,
 *    we suggest strongly only apply GDBHandleFree on a valid GDB_HANDLE.
 *    GDBHandleFree also do all the cleaning work intellectively.
 *    As a result GDBStmtFinish or GDBContextClose may be passed over 
 *    prior to GDBHandleFree.
 * @see
 *    GDBHandleAlloc for how to use GDBHandleFree.
 */
extern
void GDBHandleFree (
    GDB_HANDLE handle);


/**
 * GDBHandleGetData
 * @brief
 *    Get extra data from a valid GDB_HANDLE.
 *
 * @param handle
 *    A valid GDB_HANDLE created successfully from GDBHandleAlloc.
 * @param ppData
 *    Provided address of a pointer to extra data.
 * @return
 *    Size of extra data in bytes if succeeded, otherwise an error occurred.
 * @retval int
 * @note
 *    DO NOT call any free function on ppData.
 *    ppData it is only an reference of pointer to extra data.
 *
 *  <pre>
 *    Below code snippet shows how to use GDBHandleGetData:
      @verbatim
      GDB_HANDLE hstmt;
      char *sql;
      int ret;
  
      ret = GDBHandleAlloc (&hstmt, GDB_HTYPE_STMT, 0, 300);
      if (ret == GDBAPI_SUCCESS) {
        ret = GDBHandleGetData ((GDB_HANDLE)hstmt, (void**) &sql);
        assert (ret == 300);
  
        sprintf (sql, "CREATE TABLE IF NOT EXISTS test_tbl("
          "id INTEGER PRIMARY KEY NOT NULL,"
          "name VARCHAR2(30));");
        ...
  
        /// DO NOT call: free(sql);
        GDBHandleFree (hstmt);
      }
      @endverbatim
 *  </pre>
 * @see
 *    GDBStmtPrepare
 */
extern
int GDBHandleGetData (
    GDB_HANDLE handle,
    void ** ppData);


/**
 * GDBHandleGetType
 * @brief
 *    Get handle type from a valid GDB_HANDLE.
 *
 * @param handle
 *    A valid GDB_HANDLE created successfully from GDBHandleAlloc.
 * @return
 *    Handle type in GDB_HANDLE_TYPE enumeration.
 */
extern
GDB_HANDLE_TYPE GDBHandleGetType (
    GDB_HANDLE handle);


extern
GDBAPI_RESULT GDBAttrSet (
    GDB_HANDLE handle,
    void *pAttrValue,
    int   cbSizeValue,
    GDB_ATTR_NAME attrName,
    GDB_ERROR hError);


extern
GDBAPI_RESULT GDBAttrGet (
    GDB_HANDLE handle,
    void *pAttrValue,
    int  *cbSizeValue,
    GDB_ATTR_NAME attrName,
    GDB_ERROR hError);


extern
const char* GDBErrorGetMsg (
    GDB_ERROR hError);


/**
 * GDB_CONTEXT
 */
extern
GDBAPI_RESULT GDBContextOpen (
    GDB_CONTEXT hCtx,
    GDB_ERROR hError);


extern
GDBAPI_RESULT GDBContextClose (
    GDB_CONTEXT hCtx,
    GDB_ERROR hError);


extern
GDBAPI_RESULT GDBContextGetDbInst (
    GDB_CONTEXT hCtx,
    void **dbInst);


extern
GDBAPI_RESULT GDBExecuteSQL (
    GDB_CONTEXT hCtx,
    const char *sql,
    GDB_ERROR hError);


extern
GDBAPI_RESULT GDBBeginTransaction (
    GDB_CONTEXT hCtx,
    GDB_ERROR hError);


extern
GDBAPI_RESULT GDBCommitTransaction (
    GDB_CONTEXT hCtx,
    GDB_ERROR hError);


extern
GDBAPI_RESULT GDBRollbackTransaction (
    GDB_CONTEXT hCtx,
    GDB_ERROR hError);


/**
 * GDBStmtPrepare
 */
extern
GDBAPI_RESULT GDBStmtPrepare (
    GDB_CONTEXT hCtx,
    const char *sql,
    int size,
    GDB_STMT hstmt,
    GDB_ERROR hError);


extern
int GDBStmtGetParamIndex (
    GDB_STMT stmt,
    const char *param);


extern
GDBAPI_RESULT GDBStmtBindParam (
    GDB_STMT stmt,
    int paramIndex,
    void *paramValue,
    int cbParamSize,  
    GDB_PARAM_TYPE paramType,
    GDB_ERROR hError);


extern
GDBAPI_RESULT GDBStmtFinish (
    GDB_STMT stmt,
    GDBAPI_BOOL bFreeStmt,
    GDB_HANDLE hError);


extern
GDBAPI_RESULT GDBStmtExecute (
    GDB_STMT stmt,
    GDB_ERROR hError);


extern
GDBAPI_RESULT GDBStmtReset (
    GDB_STMT stmt,
    GDB_ERROR hError);


extern
GDBAPI_RESULT GDBStmtGetStmtInst (
    GDB_STMT stmt,
    void **stmtInst);


extern
GDBAPI_RESULT GDBStmtGetCol (
    GDB_STMT stmt,
    int colIndex,
    void *colValue,
    int *colValueSize,
    char **colName,
    GDB_PARAM_TYPE *colType,
    GDB_ERROR hError);


/**
 * GDBTableCreate
 * @brief
 *    Create a table in GDB database.
 *
 * @param hCtx
 *    Specify a GDB_CONTEXT handle which has suffered GDBContextOpen hitherto.
 * @param tabletype
 *    Specify the type of table to be created among:
 *      GDB_TABLE_GENERIC, GDB_TABLE_LAYER, GDB_TABLE_IMAGE.
 * @param tablename
 *    The name of table be created. Such that table must not already exist.
 * @param coldefs
 *    A list of GDB_COLDEF that defines the table columns.
 * @param numcols
 *    Number of GDB_COLDEFs (column definitions) in list of GDB_COLDEF.
 * @param hspatial
 *    A valid GDB_LAYER or GDB_RASTER handle specified by tabletype.
 * @param hError
 *    Optional GDB_ERROR handle to recieve error info.
 * @return
 *    GDBAPI_SUCCESS for success, other GDBAPI_RESULT code if error.
 * @retval
 *    GDBAPI_RESULT
 * @note
 *    GDBTableCreate can be used to create generic table, spatial table (layer),
 *    and image table.
 */
extern
GDBAPI_RESULT GDBTableCreate (
    GDB_CONTEXT hCtx,
    GDB_TABLE_TYPE tabletype,
    const char *tablename,
    const GDB_COLDEF *coldefs,
    int numcols,
    GDB_HANDLE hspatial,
    GDB_ERROR hError);


/* note?? */
extern
GDBAPI_RESULT GDBTableGetInfo (
    GDB_CONTEXT hCtx,
    const char *tablename,
    GDB_TABLE_TYPE *tabletype,
    rowid_t *rowcount,
    int *numcols,
    GDB_COLDEF *coldefs,
    GDB_HANDLE hspatial,
    GDB_ERROR hError);


extern
GDBAPI_RESULT GDBPrepareInsert (
    GDB_CONTEXT hCtx,
    const char *tablename,
    GDB_TABLE_TYPE tabletype,
    const GDB_COLDEF *coldefs,
    int *insColinds,
    int numColinds,
    GDB_HANDLE hspatial,
    GDB_ERROR hError);


extern
GDBAPI_RESULT GDBExecuteInsert (
    GDB_CONTEXT hCtx,
    rowid_t startRowno,
    GDBCALLBACK_ExecuteInsertRow pfnInsertRow,
    void *inParam,
    GDB_ERROR hError);


extern
GDBAPI_RESULT GDBColValSetAddr (
    GDB_COLVAL hcolval,
    void *colValue,
    int cbSizeValue,
    GDB_ERROR hError);


extern
GDBAPI_RESULT GDBColValSetCopy (
    GDB_COLVAL hcolval,
    void *colValue,
    int cbSizeValue,
    GDB_PARAM_TYPE type,
    GDB_ERROR hError);


extern
GDBAPI_RESULT GDBFinishInsert (
    GDB_CONTEXT hCtx,
    GDB_ERROR hError);


extern
GDBAPI_RESULT GDBLayerCreateIndex (
    GDB_CONTEXT hCtx,
    GDB_LAYER   hLayer,
    GDB_ERROR   hError);


extern
GDBAPI_RESULT GDBLayerDropIndex (
    GDB_CONTEXT hCtx,
    GDB_LAYER layerinfo,
    GDB_ERROR hError);


/**
 * GDBLayerQuery
 * @brief
 *    Prepare a spatial and attributes query on a subject layer.
 *
 * @param hCtx
 *    Specify a valid GDB_CONTEXT handle. A valid GDB_CONTEXT handle means
 *    a GDB_CONTEXT handle which has suffered GDBContextOpen hitherto
 *    successfully.
 * @param subjectLayer
 *    A valid handle to layer to be filtered.
 * @param filters
 *    - If numfilters is 1, the address of a pointer to filter handle.
 *    - If numfilters > 1, the address of array of filter handles.
 *    - NULL for no filters.
 * @param numfilters
 *    The number of filters in array of filters. Can be 0 for no filters.
 * @param wherecond
 *    A SQL where clause for attributes querying combinated with spatial
 *    filters.
 * @param hError
 *    Optional GDB_ERROR handle to recieve error info.
 *
 * @return
 *    - GDBAPI_SUCCESS for success.
 *    - other GDBAPI_RESULT code if error.
 *
 * @note
 *    - GDBLayerPrepareQuery applies spatial query (filters) together with
 *      SQL-style query (wherecond) on a subject layer.
 *    - Next to calling GDBLayerPrepareQuery successfully, GDBLayerExecuteQuery
 *      should be invoked subsequently to fetch result rows.
 */
extern
GDBAPI_RESULT GDBLayerQuery (
    GDB_CONTEXT hCtx,
    GDB_LAYER   subjectLayer,
    GDB_COLDEF  *coldefs,
    int         *colinds,
    int         numcols,
    GDB_FILTER  *filters,
    int         numfilters,
    const char  *wherecond,
    GDBCALLBACK_OnFetchRow pfnOnFetchRow,
    void        *inParam,
    GDB_ERROR   hError);


/**
 * GDBLayerOverlay
 * @brief
 *    
 *
 * @param hCtx
 *    Specify a valid GDB_CONTEXT handle. A valid GDB_CONTEXT handle means
 *    a GDB_CONTEXT handle which has suffered GDBContextOpen hitherto
 *    successfully.
 */
extern
GDBAPI_RESULT GDBLayerOverlay (
    GDB_CONTEXT hCtx,
    GDB_OVERLAY_MODE
                overlaymode,
    GDB_LAYER   subjectLayer,
    GDB_LAYER   clipLayer,
    rowid_t     clipRowOffset,
    rowid_t     *clipRowCount,
    GDB_LAYER   resultLayer,
    GDBCALLBACK_LayerOverlayOnPrepare
                pfnOnPrepare,
    GDBCallback_LayerOverlayOnExecute
                pfnOnExecute,
    GDBCallback_LayerOverlayOnFinish
                pfnOnFinish,
    void        *inParam,
    GDB_ERROR   hError);


/**
 * GDBImportSHPFile
 * @brief
 *    Import a shape file into GDB as a new created layer table
 *
 * @param hCtx
 *    Specify a valid GDB_CONTEXT handle. A valid GDB_CONTEXT handle means
 *    a GDB_CONTEXT handle which has suffered GDBContextOpen hitherto
 *    successfully.
 * @param mode
 *    Specify if we create a new layer table or reuse an existing one to
 *    recieve shapes from shp file:
 *    - GDB_ALWAYS_CREATE_NEW \n
 *      Always create a new layer table with given table name. An error will
 *      occur if that layer table exists.
 *    - GDB_APPEND_EXISTS_ONLY \n
 *      Append shapes to the existing layer table with given table name. An
 *      error will occur if that layer table does NOT exist.
 *    - GDB_CREATE_IF_NOT_EXISTS \n
 *      This option can be regarded as the combination of above two. Fistly
 *      GDBAPI will try to append shapes to an existing layer table. If that
 *      table does not exist, GDBAPI will create a new layer table and import
 *      shapes into it.
 * @param shpFile
 *    Specify the full path filename of shape file to be imported which must
 *    exist and valid.
 * @param layertable
 *    Specify name of layer table to recieve shapes. NULL value for layertable
 *    indicates to use shpFile title (shpFile stripped away path and ext) as 
 *    layer table name.
 * @param rowidcol
 *    Each shape in a layer table has an unique identifier called as rowid.
 *    This rowidcol specifies a field name to store rowid in layer table.
 *    If a rowidcol existing in DBF of shape file is specified, GDBAPI will
 *    try to reuse it as rowid field. If failed, GDBAPI will create a suiatble
 *    field automatically as rowid field for layer table. You can ensure if
 *    that is what you want in callback pfnOnPrepare by using GDBAttrGet()
 *    with GDB_ATTR_LAYER_ROWIDCOL.
 * @param spref
 *    In version 1.0, spatial reference is not supported. Leave it be NULL
 *    and let GDBAPI selects one for you.
 *
 * @param pfnOnPrepare
 *    An address of callback function which will be invoked once after GDBAPI
 *    has loaded profile of shpFile. It is the caller's duty to implement this
 *    callback function. Null value for pfnOnPrepare is allowable.
 * @param pfnOnExecute
 *    An address of callback function which will be tiggered as many as the
 *    amount of shapes in shpFile. Every time after a shape together with its
 *    attribute fields has been read over and prior to inserting into layer
 *    table, this callback function is invoked. All details of a shape to be
 *    imported can be obtained herein.
 *    Leave it be NULL if we don't care about anything of details.
 * @param pfnOnFinish
 *    An address of callback function invoked once after GDBAPI has imported
 *    all shapes from shpFile. It can be NULL.
 * @param inParam
 *    Pointer to an user defined parameter used by above three callback
 *    functions.
 * @param hError
 *    Optional GDB_ERROR handle to recieve error info.
 *
 * @return GDBAPI_RESULT code:
 *    - GDBAPI_SUCCESS for success.
 *    - other GDBAPI_RESULT code if error.
 * @note
 *    - Don't call GDBImportSHPFile in any transaction scenario since
 *      GDBAPI does that for you.
 *    - GDBImportSHPFile does not create a spatial index after importing
 *      shape file to layer table.
 *    - We strongly suggest you create index on layer table immediately once
 *      any spatial data in layer has been changed.
 *  <pre>
 *    Below code snippet shows a simplest sample of importing shpfile to GDB:
      @verbatim
      GDB_CONTEXT    hctx;
      GDB_ERROR      hError;

      GDBHandleAlloc (&hctx, GDB_HTYPE_CONTEXT, 0, 0);
      GDBHandleAlloc (&hError, GDB_HTYPE_ERROR, 0, 0);

      GDBAttrSet (hctx, "/path/to/test.gdb", -1, GDB_ATTR_CTX_DBNAME, hError);

      GDBAttrSet (hctx, "gdbsample", -1, GDB_ATTR_CTX_USERNAME, hError);
      GDBAttrSet (hctx, "gdbsample", -1, GDB_ATTR_CTX_PASSWORD, hError);

      GDBContextOpen (hctx, hError);
      GDBImportSHPFile (hctx, GDB_ALWAYS_CREATE_NEW,
        "/path/to/BLDG.shp", "BLDG", "SHAPEID", NULL,
        NULL, NULL, NULL, NULL, hError);

      GDBHandleFree (hctx);
      GDBHandleFree (hError);
      @endverbatim
 *  </pre>
 * @see
 *    - GDBCALLBACK_ImportSHPFileOnPrepare
 *    - GDBCALLBACK_ImportSHPFileOnExecute
 *    - GDBCALLBACK_ImportSHPFileOnFinish
 *    - GDBLayerCreateIndex
 */
extern
GDBAPI_RESULT GDBImportSHPFile (
    GDB_CONTEXT hCtx,
    GDB_CREATE_MODE mode,
    const char  *shpFile,
    const char  *layertable,
    const char  *rowidcol,
    GDB_SPATREF spref,
    GDBCALLBACK_ImportSHPFileOnPrepare pfnOnPrepare,
    GDBCALLBACK_ImportSHPFileOnExecute pfnOnExecute,
    GDBCALLBACK_ImportSHPFileOnFinish pfnOnFinish,
    void *inParam,
    GDB_ERROR   hError);


/**
 * GDBExportSHPFile
 * @brief
 *    Export a layer table to a shape file.
 *
 * @param hCtx
 *    Specify a valid GDB_CONTEXT handle. A valid GDB_CONTEXT handle means
 *    a GDB_CONTEXT handle which has suffered GDBContextOpen hitherto
 *    successfully.
 */
extern
GDBAPI_RESULT GDBExportSHPFile (
    GDB_CONTEXT hCtx,
    GDB_CREATE_MODE mode,
    GDB_LAYER   hLayer,
    rowid_t     offset,
    rowid_t     rowcount,
    const char  *shpFile,
    const char  *whereCondition,
    GDBCALLBACK_ExecuteInsertShape pfnInsertShape,
    void        *inParam,
    GDB_ERROR   hError);


/******************************************************************************/
/*                                                                            */
/*                             GDBPoint APIs                                  */
/*                                                                            */
/******************************************************************************/
extern
GDBAPI_RESULT GDBPointGetDistance (
    const GDBPointZ * start,
    const GDBPointZ * end,
    const GDB_SPATREF spref,
    double *val);


extern
GDBAPI_RESULT GDBPointGetAzimuth (
    const GDBPointZ * start,
    const GDBPointZ * end,
    const GDB_SPATREF spref,
    double *val);


/******************************************************************************/
/*                                                                            */
/*                             GDBExtBound APIs                               */
/*                                                                            */
/******************************************************************************/

/**
 * GDBExtBoundIsValid
 * @brief
 *
 */
extern
GDBAPI_BOOL GDBExtBoundIsValid (
    const GDBExtBound * bound);


extern
void GDBExtBoundUnion (
    const GDBExtBound * source,
    const GDBExtBound * clip,
    GDBExtBound * result);


extern
GDBAPI_BOOL GDBExtBoundIntersect (
    const GDBExtBound * source,
    const GDBExtBound * clip,
    GDBExtBound * result);


#ifdef    __cplusplus
}
#endif

#endif /* _GDBAPI_H */
