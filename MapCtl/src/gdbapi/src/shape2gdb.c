/**
 * shape2gdb.c
 * @brief
 *    import shapefile into GDB.
 *
 * @author
 *    cheungmine
 * @since 
 *    2013-4-25
 * @date
 *    2013-5-26
 * @note
 */
#define _CRTDBG_MAP_ALLOC       
#include<stdlib.h>    
#include<crtdbg.h>

#include <assert.h>
#include <stdio.h>
#include <time.h>

#include "gdbshell.h"

# pragma comment (lib, "../../libs/shapefile/dist/lib/libshapefile.lib")

#ifdef _MSC_VER
 #pragma warning (disable : 4996)
#endif

#define START_ROWID  0

#define CHECK_ERROR_RESULT(ret, herror) do { \
    if (ret != GDBAPI_SUCCESS) { \
        if (herror) { \
            fprintf(stdout, "ERROR at %d: %s\n", \
                __LINE__, GDBErrorGetMsg(herror)); \
        } \
        goto ERR_EXIT; \
    } \
} while (0)

static const char *ColDefTypeName (GDB_COLDEF_TYPE ctype, char colTypeName[])
{
    switch (ctype) {
    case GDB_CTYPE_INVALID:
        strcpy(colTypeName, "INVALID");
        break;
    case GDB_CTYPE_INTEGER:
        strcpy(colTypeName, "INTEGER");
        break;
    case GDB_CTYPE_INTEGER_AUTOINC:
        strcpy(colTypeName, "INTEGER AUTOINCREMENT");
        break;
    case GDB_CTYPE_NUMERIC:
        strcpy(colTypeName, "NUMERIC");
        break;
    case GDB_CTYPE_STRING:
        strcpy(colTypeName, "STRING");
        break;
    case GDB_CTYPE_DATETIME:
        strcpy(colTypeName, "DATETIME");
        break;
    case GDB_CTYPE_DATETIME_DEFAULT:
        strcpy(colTypeName, "DATETIME DEFAULT");
        break;
    case GDB_CTYPE_BLOB:
        strcpy(colTypeName, "BLOB");
        break;
    case GDB_CTYPE_SHAPE:
        strcpy(colTypeName, "SHAPE");
        break;
    case GDB_CTYPE_IMAGE:
        strcpy(colTypeName, "IMAGE");
        break;
    default:
        strcpy(colTypeName, "UNKNOWN COLTYPE");
        break;
    }
    return colTypeName;
}


static int ImportSHPFileOnPrepare (
    GDB_CONTEXT ctx,
    const char *shpTypeName,
    int totalShapes,
    GDB_COLDEF *hColdefs,
    int *pColinds,
    int numColdefs,
    GDB_LAYER layerinfo,
    void *inParam,
    GDB_ERROR hError)
{
    int wkbtype;
    double Xmin, Ymin, Xmax, Ymax, Zmin, Zmax, Mmin, Mmax;
 
    fprintf(stdout, "\n---------------------------------------------\n");

    GDBAttrGet (layerinfo, &wkbtype, 0, GDB_ATTR_LAYER_LAYERTYPE, hError);

    fprintf(stdout, "Shape Type: %d (%s)\n", wkbtype, shpTypeName);
	  fprintf(stdout, "Total Shapes: %ld\n", totalShapes);

    GDBAttrGet(layerinfo, (void*) &Xmin, 0, GDB_ATTR_LAYER_XMIN, hError);
    GDBAttrGet(layerinfo, (void*) &Ymin, 0, GDB_ATTR_LAYER_YMIN, hError);
    GDBAttrGet(layerinfo, (void*) &Xmax, 0, GDB_ATTR_LAYER_XMAX, hError);
    GDBAttrGet(layerinfo, (void*) &Ymax, 0, GDB_ATTR_LAYER_YMAX, hError);
    GDBAttrGet(layerinfo, (void*) &Zmin, 0, GDB_ATTR_LAYER_ZMIN, hError);
    GDBAttrGet(layerinfo, (void*) &Zmax, 0, GDB_ATTR_LAYER_ZMAX, hError);
    GDBAttrGet(layerinfo, (void*) &Mmin, 0, GDB_ATTR_LAYER_MMIN, hError);
    GDBAttrGet(layerinfo, (void*) &Mmax, 0, GDB_ATTR_LAYER_MMAX, hError);

    fprintf(stdout, "\tXmin = %.4lf\n", Xmin);
	  fprintf(stdout, "\tYmin = %.4lf\n", Ymin);
	  fprintf(stdout, "\tXmax = %.4lf\n", Xmax);
	  fprintf(stdout, "\tYmax = %.4lf\n", Ymax);
    fprintf(stdout, "\tZmin = %.4lf\n", Zmin);
	  fprintf(stdout, "\tZmax = %.4lf\n", Zmax);
    fprintf(stdout, "\tMmin = %.4lf\n", Mmin);
	  fprintf(stdout, "\tMmax = %.4lf\n", Mmax);

    fprintf(stdout, "DBF Fields: %d\n", numColdefs);
    if (numColdefs > 0) {
        int i, col, colWidth, colScale;
        char *colName;
        GDB_COLDEF_TYPE colType;
        char colTypeName[30];

        for (i = 0; i < numColdefs; i++) {
            col = pColinds[i];

            GDBAttrGet(hColdefs[col], (void*) &colName, 0, GDB_ATTR_COLDEF_NAME, hError);
            GDBAttrGet(hColdefs[col], (void*) &colType, 0, GDB_ATTR_COLDEF_TYPE, hError);
            GDBAttrGet(hColdefs[col], (void*) &colWidth, 0, GDB_ATTR_COLDEF_LENGTH, hError);
            GDBAttrGet(hColdefs[col], (void*) &colScale, 0, GDB_ATTR_COLDEF_SCALE, hError);

            fprintf(stdout, "\t%d: %s %s (%d, %d)\n",
                col + 1,
                colName,
                ColDefTypeName(colType, colTypeName),
                colWidth,
                colScale);
        }
    }
    fprintf(stdout, "---------------------------------------------\n\n");
  
    /* 1: goto next; 0: break */
    return 1;
}


static int ImportSHPFileOnFinish (
    GDB_CONTEXT ctx,
    int numShapesImported,
    int numShapesIgnored,
    const GDB_LAYER layerinfo,
    void *inParam,
    GDB_ERROR hError)
{
    int ret;
    clock_t t0, t2;
    int sum;

    t0 = *((clock_t*) inParam);
    t2 = clock();

    sum = numShapesImported;
    if (sum == 0) {
      sum = 1;
    }

    fprintf(stdout, "\nINFO:\n");

    fprintf(stdout,
        "Total %d shapes imported. %d shapes ignored.\n"   
        "\tElapsed time %.3f seconds.\n",
          numShapesImported, numShapesIgnored,
          (float) (t2 - t0) * 0.001);

    fprintf(stdout, "Create layer spatial index...\n");

    t0 = clock();
    //ret = GDBLayerCreateIndex (ctx, layerinfo, gsize1, 0, hError);
    ret = GDBLayerCreateIndex (ctx, layerinfo, hError);
    if (ret != GDBAPI_SUCCESS) {
        fprintf(stdout, "ERROR: %s\n", GDBErrorGetMsg(hError)); 
        return GDBAPI_ERROR;
    }
    t2 = clock();

    fprintf(stdout, "\tElapsed time %.3f seconds.\n", (float) (t2 - t0) * 0.001);
    return GDBAPI_SUCCESS;
}


static int ImportSHPFileOnExecute (
    GDB_CONTEXT ctx,
    int totalShapes,
    int shapeIndex,
    int nSHPType,
    int numparts,
    int numpoints,
    int numCols,
    GDB_COLVAL *Colvals,
    GDB_HANDLE hshape,
    void *inParam,
    GDB_ERROR hError)
{
    char typeName[31];
    int  sizeName = sizeof(typeName);

    int step = totalShapes / 100 + 1;
    if (shapeIndex%step == 0) {
        fprintf(stdout, ".");
    }

    GDBAttrGet (hshape, typeName, &sizeName, GDB_ATTR_SHAPE_TYPENAME, hError);

    return 1;
}

typedef struct ImportParam
{
    clock_t start;

    int     totalShapes;

    double  totalAreas;
    double  totalLengths;

    double  Xspans;
    double  Yspans;
} ImportParam;


int shape2gdb (char *shapefile, char *gdb, char *layertable, gt_table_cremod mode)
{
    int ret;

    clock_t t0;

    GDB_CONTEXT    hctx = 0;
    GDB_ERROR      herr = 0;

    /* create a context handle which represents a gdb instance */
    ret = GDBHandleAlloc (&hctx, GDB_HTYPE_CONTEXT, 0, 0);
    CHECK_ERROR_RESULT(ret, 0);

    GDBHandleAlloc (&herr, GDB_HTYPE_ERROR, 0, 0);
    CHECK_ERROR_RESULT(ret, 0);

    GDBAttrSet (hctx, "GDBAPI", -1, GDB_ATTR_CTX_TABLESPACE, herr);
    GDBAttrSet (hctx, "GDBAPI", -1, GDB_ATTR_CTX_OWNER, herr);

    ret = gdbopen (hctx, gdb, "gdbshell", "gdbshell", 0, herr);
    CHECK_ERROR_RESULT(ret, herr);

    fprintf(stdout, "Import Shape File: (%s)\n", shapefile);

    t0 = clock();

    ret = GDBImportSHPFile (hctx,
        GDB_ALWAYS_CREATE_NEW,
        shapefile,
        layertable,
        "SHAPEID", /* specify rowid col if we have */
        0,         /* default spatial reference */
        &ImportSHPFileOnPrepare,
        &ImportSHPFileOnExecute,
        &ImportSHPFileOnFinish,
        &t0,
        herr);
    CHECK_ERROR_RESULT(ret, herr);

    fprintf(stdout, "\nImport successfully into GDB: %s\n", gdb);

    GDBHandleFree (hctx);
    GDBHandleFree (herr);

    return (0);

ERR_EXIT:
    fprintf(stdout, "\n******** Failed to import Shape File! ******** \n");

    GDBHandleFree (hctx);
    GDBHandleFree (herr);

    return (-1);
}
