/**
 * gdbcoldef.h
 *
 *
 */

#ifndef _GDB_COLDEF_H_
#define _GDB_COLDEF_H_

#ifdef    __cplusplus
extern "C" {
#endif

#include "gdberror.h"

#ifndef sqlite3_column_isnull
# define sqlite3_column_isnull(pSqlite3Stmt, columnIndex) \
    (sqlite3_column_bytes(pSqlite3Stmt, columnIndex)==0)
#endif

#ifndef sqlite3_column_notnull
# define sqlite3_column_notnull(pSqlite3Stmt, columnIndex) \
    (sqlite3_column_bytes(pSqlite3Stmt, columnIndex) > 0)
#endif

static int sqlite3_coltype_2_coldef_type (int coltype)
{
    switch (coltype) {
    case SQLITE_INTEGER:
        return GDB_CTYPE_INTEGER;
    case SQLITE_FLOAT:
        return GDB_CTYPE_NUMERIC;
    case SQLITE_TEXT:
        return GDB_CTYPE_STRING;
    case SQLITE_BLOB:
        return GDB_CTYPE_BLOB;
    case SQLITE_NULL:
    default:
        return GDB_CTYPE_INVALID;
    }
}


#define GDB_COLNAME_LEN 30
#define GDB_DFLTVALUE_LEN 255

#define COLINDEX_CID  0
#define COLINDEX_NAME 1
#define COLINDEX_TYPE 2
#define COLINDEX_NOTNULL 3
#define COLINDEX_DFLT_VALUE 4
#define COLINDEX_PK 5

/*
 * GDBColVal
 */
typedef struct _GDBColVal
{
    GDBHandleOb __handle[1];

    union {
        /* GDBColVal */
        struct {
            int              colind;
            GDB_COLDEF_TYPE  coltype;

            int              colsize;
            void            *coladdr;

            union {
                double dblVal;
                sqlite_int64 int64Val;
                int    intVal;
                char   *pszVal;
            };
            char             colname[GDB_COLNAME_LEN+1];
        };

        /* GDBColValHead */
        struct {
            int              _attrcols;
            GDB_TABLE_TYPE   _tbltype;
            int              _spatcols;
            union {
                HGDBSHP        _hshape; /* GDB_TABLE_LAYER */
                HGDBTILE       _htile;  /* GDB_TABLE_IMAGE */
            };
        };
    };
} GDBColVal, GDBColValHead, *HGDBColVal;

#define CAST_HANDLE_COLVAL(hdl, herr, hcolval) \
    CAST_HANDLE_TYPE(hdl, herr, GDB_HTYPE_COLVAL, HGDBColVal, hcolval)

/*
 * GDBColDef
 */
typedef struct _GDBColDef
{
    GDBHandleOb __handle[1];

    char colname[GDB_COLNAME_LEN+1];
    GDB_COLDEF_TYPE coltype;

    /* 3.14159 */
    int precision; /* =6 */
    int scale; /* =5 */

    GDBAPI_BOOL notnull;
    GDBAPI_BOOL prikey;
    GDBAPI_BOOL unique;

    char dfltvalue[GDB_DFLTVALUE_LEN+1];
} GDBColDef, *HGDBColDef;

#define CAST_HANDLE_COLDEF(hdl, herr, hcoldef) \
    CAST_HANDLE_TYPE(hdl, herr, GDB_HTYPE_COLDEF, HGDBColDef, hcoldef)


static int _GDBColDef2SQL (HGDBColDef hcol, char cd[200])
{
    int cb = 0;
    cd[0] = 0;

    switch (hcol->coltype) {
    case GDB_CTYPE_INTEGER:
        if (hcol->prikey) {
            if (hcol->notnull) {
                cb = sprintf (cd, "%s INTEGER PRIMARY KEY NOT NULL", hcol->colname);
            } else {
                cb = sprintf (cd, "%s INTEGER PRIMARY KEY", hcol->colname);
            }
        } else {
            if (hcol->unique) {
                if (hcol->notnull) {
                    cb = sprintf (cd, "%s INTEGER UNIQUE NOT NULL", hcol->colname);
                } else {
                    cb = sprintf (cd, "%s INTEGER UNIQUE", hcol->colname);
                }
            } else {
                if (hcol->notnull) {
                    cb = sprintf (cd, "%s INTEGER NOT NULL", hcol->colname);
                } else {
                    cb = sprintf (cd, "%s INTEGER", hcol->colname);
                }
            }
        }
        break;

    case GDB_CTYPE_INTEGER_AUTOINC:
        if (hcol->prikey) {
            cb = sprintf (cd, "%s INTEGER PRIMARY KEY AUTOINCREMENT", hcol->colname);
        } else {
            cb = sprintf (cd, "%s INTEGER AUTOINCREMENT", hcol->colname);
        }
        break;

    case GDB_CTYPE_NUMERIC:
        if (hcol->precision) {
            if (hcol->notnull) {
                cb = sprintf (cd, "%s NUMERIC(%d,%d) NOT NULL", hcol->colname, hcol->precision, hcol->scale);
            } else {
                cb = sprintf (cd, "%s NUMERIC(%d,%d)", hcol->colname, hcol->precision, hcol->scale);
            }
        } else if (hcol->scale) {
            if (hcol->notnull) {
                cb = sprintf (cd, "%s NUMERIC(%d,%d) NOT NULL", hcol->colname, hcol->scale, hcol->scale);
            } else {
                cb = sprintf (cd, "%s NUMERIC(%d,%d)", hcol->colname, hcol->scale, hcol->scale);
            }
        } else {
            if (hcol->notnull) {
                cb = sprintf (cd, "%s DOUBLE NOT NULL", hcol->colname);
            } else {
                cb = sprintf (cd, "%s DOUBLE", hcol->colname);
            }
        } 
        break;

    case GDB_CTYPE_STRING:
        if (!hcol->precision) {
            return GDBAPI_ECOLDEF;
        }

        if (hcol->prikey) {
            if (hcol->notnull) {
                cb = sprintf (cd, "%s VARCHAR2(%d) PRIMARY KEY NOT NULL", hcol->colname, hcol->precision);
            } else {
                cb = sprintf (cd, "%s VARCHAR2(%d) PRIMARY KEY", hcol->colname, hcol->precision);
            }
        } else {
            if (hcol->unique) {
                if (hcol->notnull) {
                    cb = sprintf (cd, "%s VARCHAR2(%d) UNIQUE KEY NOT NULL", hcol->colname, hcol->precision);
                } else {
                    cb = sprintf (cd, "%s VARCHAR2(%d) UNIQUE KEY", hcol->colname, hcol->precision);
                }
            } else {
                if (hcol->notnull) {
                    cb = sprintf (cd, "%s VARCHAR2(%d) NOT NULL", hcol->colname, hcol->precision);
                } else {
                    cb = sprintf (cd, "%s VARCHAR2(%d)", hcol->colname, hcol->precision);
                }
            }
        }
        break;

    case GDB_CTYPE_DATETIME:
        if (hcol->prikey) {
            if (hcol->notnull) {
                cb = sprintf (cd, "%s VARCHAR2(20) PRIMARY KEY NOT NULL", hcol->colname);
            } else {
                cb = sprintf (cd, "%s VARCHAR2(20) PRIMARY KEY", hcol->colname);
            }
        } else {
            if (hcol->unique) {
                if (hcol->notnull) {
                    cb = sprintf (cd, "%s VARCHAR2(20) UNIQUE NOT NULL", hcol->colname);
                } else {
                    cb = sprintf (cd, "%s VARCHAR2(20) UNIQUE", hcol->colname);
                }
            } else {
                if (hcol->notnull) {
                    cb = sprintf (cd, "%s VARCHAR2(20) NOT NULL", hcol->colname);
                } else {
                    cb = sprintf (cd, "%s VARCHAR2(20)", hcol->colname);
                }
            }
        }
        break;

    case GDB_CTYPE_DATETIME_DEFAULT:
        if (hcol->prikey) {
            if (hcol->notnull) {
                cb = sprintf (cd, "%s VARCHAR2(20) PRIMARY KEY NOT NULL DEFAULT (datetime('now', 'localtime'))", hcol->colname);
            } else {
                cb = sprintf (cd, "%s VARCHAR2(20) PRIMARY KEY DEFAULT (datetime('now', 'localtime'))", hcol->colname);
            }
        } else {
            if (hcol->unique) {
                if (hcol->notnull) {
                    cb = sprintf (cd, "%s VARCHAR2(20) UNIQUE NOT NULL DEFAULT (datetime('now', 'localtime'))", hcol->colname);
                } else {
                    cb = sprintf (cd, "%s VARCHAR2(20) UNIQUE DEFAULT (datetime('now', 'localtime'))", hcol->colname);
                }
            } else {
                if (hcol->notnull) {
                    cb = sprintf (cd, "%s VARCHAR2(20) NOT NULL DEFAULT (datetime('now', 'localtime'))", hcol->colname);
                } else {
                    cb = sprintf (cd, "%s VARCHAR2(20) DEFAULT (datetime('now', 'localtime'))", hcol->colname);
                }
            }
        }
        break;

    case GDB_CTYPE_INVALID:
        if (hcol->prikey) {
            if (hcol->notnull) {
                cb = sprintf (cd, "%s PRIMARY KEY NOT NULL", hcol->colname);
            } else {
                cb = sprintf (cd, "%s PRIMARY KEY", hcol->colname);
            }
        } else {
            if (hcol->notnull) {
                cb = sprintf (cd, "%s NOT NULL", hcol->colname);
            } else {
                cb = sprintf (cd, "%s", hcol->colname);
            }
        }
        break;

    case GDB_CTYPE_BLOB:
    case GDB_CTYPE_SHAPE:
    case GDB_CTYPE_IMAGE:
        if (hcol->notnull) {
            cb = sprintf (cd, "%s BLOB NOT NULL", hcol->colname);
        } else {
            cb = sprintf (cd, "%s BLOB", hcol->colname);
        }
        break;
    }

    return cb+4;
}

static int CType2Int(char *ctype, char chrStart)
{
    char *p = strchr(ctype, chrStart);
    if (p) {
        p++;
    }

    if (p && *p) {
        return atoi(p);
    } else {
        return 0;
    }
}

static void _GDBColDefSetType (HGDBColDef hcol, const char *coltype)
{
    char ctype[101];

    SafeCopyString(ctype, coltype, sizeof(ctype));

    if (strstr(ctype, "VARCHAR2")) {
        hcol->coltype = GDB_CTYPE_STRING;
        hcol->precision = CType2Int(ctype, '(');
    } else if (strstr(ctype, "DOUBLE")) {
        hcol->coltype = GDB_CTYPE_NUMERIC;
    } else if (strstr(ctype, "NUMERIC")) {
        hcol->coltype = GDB_CTYPE_NUMERIC;
        hcol->precision = CType2Int(ctype, '(');
        hcol->scale = CType2Int(ctype, ',');
    } else if (strstr(ctype, "DATETIME")) {
        hcol->coltype = GDB_CTYPE_DATETIME;
    } else if (strstr(ctype, "INTEGER")) {
        hcol->coltype = GDB_CTYPE_INTEGER;
    } else if (strstr(ctype, "BLOB")) {
        hcol->coltype = GDB_CTYPE_BLOB;
    } else {
        hcol->coltype = GDB_CTYPE_INVALID;
    }
}


static GDBAPI_RESULT _GDBColDefGetAttr (HGDBColDef hcol, void *pAttrValue,
    int *cbSizeValue, GDB_ATTR_NAME attrName, GDB_ERROR hError)
{
    if (!pAttrValue && !cbSizeValue) {
        GDBERROR_THROW_EATTRVAL(hError);
    }

    switch (attrName) {
    case GDB_ATTR_COLDEF_NAME:
        ATTR_GET_STRING_ADDR(hcol->colname, pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_COLDEF_TYPE:
        ATTR_GET_TYPED_VALUE(GDB_COLDEF_TYPE, hcol->coltype,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_COLDEF_PRECISION:
    case GDB_ATTR_COLDEF_LENGTH:
        ATTR_GET_TYPED_VALUE(int, hcol->precision,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_COLDEF_SCALE:
        ATTR_GET_TYPED_VALUE(int, hcol->scale,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_COLDEF_PRIKEY:
        ATTR_GET_TYPED_VALUE(int, hcol->prikey,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_COLDEF_UNIQUE:
        ATTR_GET_TYPED_VALUE(int, hcol->unique,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_COLDEF_NOTNULL:
        ATTR_GET_TYPED_VALUE(int, hcol->notnull,
            pAttrValue, cbSizeValue, hError);
        break;

    default:
        GDBERROR_THROW_EATTR(hError);
    }

    return GDBAPI_SUCCESS;
}

static GDBAPI_RESULT _GDBColDefSetAttr(HGDBColDef hcol,
    void *pAttrValue, int cbSizeValue, GDB_ATTR_NAME attrName, HGDBERR hError)
{
    switch (attrName) {
    case GDB_ATTR_COLDEF_NAME:
        if (cbSizeValue <= sizeof(hcol->colname)) {
            memcpy(hcol->colname, pAttrValue, cbSizeValue);
            if (cbSizeValue < sizeof(hcol->colname)) {
                hcol->colname[cbSizeValue] = 0;
            }
        } else {
            assert (0);
        }
        hcol->colname[sizeof(hcol->colname) - 1] = 0;
        break;

    case GDB_ATTR_COLDEF_TYPE:
        hcol->coltype = (GDB_COLDEF_TYPE) VALUE_PTR(pAttrValue, int);
        break;

    case GDB_ATTR_COLDEF_PRECISION:
    case GDB_ATTR_COLDEF_LENGTH:
        hcol->precision = VALUE_PTR(pAttrValue, int);
        break;

    case GDB_ATTR_COLDEF_SCALE:
        hcol->scale = VALUE_PTR(pAttrValue, int);
        break;

    case GDB_ATTR_COLDEF_PRIKEY:
        hcol->prikey = VALUE_PTR(pAttrValue, GDBAPI_BOOL);
        break;

    case GDB_ATTR_COLDEF_UNIQUE:
        hcol->unique = VALUE_PTR(pAttrValue, GDBAPI_BOOL);
        break;

    case GDB_ATTR_COLDEF_NOTNULL:
        hcol->notnull = VALUE_PTR(pAttrValue, GDBAPI_BOOL);
        break;

    default:
        GDBERROR_THROW_EATTR(hError);
        break;
    }

    return GDBAPI_SUCCESS;
}

/*
 * GDBColVal
 */
typedef struct _GDBColValList
{
    int num_hcolvals;
    GDBColValHead *hcolhead;
    HGDBColVal hcolvals[1];  
} GDBColValList;

static GDBColValList * _GDBColValListAlloc (
    const HGDBColDef *coldefs,
    int *insColinds,
    int numColinds)
{
    HGDBColDef hcol;
    HGDBColVal hval;
    int at, i = 0;

    GDBColValList * colList = (GDBColValList *)
        calloc(1,
            sizeof(GDBColValList) + 
            sizeof(HGDBColVal) * numColinds + 
            sizeof(GDBColVal)*(numColinds+1));

    if (colList) {
        colList->num_hcolvals = numColinds;
        colList->hcolhead = (GDBColValHead *) (colList->hcolvals + numColinds);

        for (; i < numColinds; i++) {
            hval = (colList->hcolhead + i + 1);
            hval->__handle->type = GDB_HTYPE_COLVAL;

            at = (insColinds? insColinds[i] : i);
            hcol = (HGDBColDef) coldefs[at];
      
            hval->colind = at;
            hval->coltype = hcol->coltype;
            hval->colsize = hcol->precision;

            colList->hcolvals[i] = hval;
        }
    }

    return colList;
}

static GDBAPI_RESULT _GDBColValGetAttr (HGDBColVal hcolval, void *pAttrValue,
    int *cbSizeValue, GDB_ATTR_NAME attrName, GDB_ERROR hError)
{
    if (!pAttrValue && !cbSizeValue) {
        GDBERROR_THROW_EATTRVAL(hError);
    }

    switch (attrName) {
    case GDB_ATTR_COLVAL_COLNAME:
        ATTR_GET_STRING_ADDR(hcolval->colname, pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_COLVAL_INDEX:
        if (pAttrValue) {
            ATTR_CHECK_SET_SIZE(cbSizeValue, sizeof(int), hError);
            *((int*) pAttrValue) = (int) hcolval->colind;
        } else {
            *cbSizeValue = sizeof(int);
        }
        break;
    case GDB_ATTR_COLVAL_TYPE:
        if (pAttrValue) {
            ATTR_CHECK_SET_SIZE(cbSizeValue, sizeof(int), hError);
            *((int*) pAttrValue) = (int) hcolval->coltype;
        } else {
            *cbSizeValue = sizeof(int);
        }
        break;
    case GDB_ATTR_COLVAL_PARAMTYPE:
        if (pAttrValue) {
            ATTR_CHECK_SET_SIZE(cbSizeValue, sizeof(int), hError);
            *((int*) pAttrValue) = (int) GDB_COLTYPE_TO_PARAMTYPE(hcolval->coltype);
        } else {
            *cbSizeValue = sizeof(int);
        }
        break;
    case GDB_ATTR_COLVAL_ADDR:
        if (pAttrValue) {
            *((void**) pAttrValue) = hcolval->coladdr;
            if (cbSizeValue) {
                *cbSizeValue = hcolval->colsize;
            }
        } else {
            *cbSizeValue = hcolval->colsize;
        }
        break;
  
    default:
        GDBERROR_THROW_EATTR(hError);
    }

    return GDBAPI_SUCCESS;
}

static GDBAPI_RESULT _GDBColValSetAttr(HGDBColVal hcolval,
    void *pAttrValue, int cbSizeValue, GDB_ATTR_NAME attrName, HGDBERR hError)
{
    switch (attrName) {
    case GDB_ATTR_COLVAL_COLNAME:
        if (cbSizeValue <= sizeof(hcolval->colname)) {
            memcpy(hcolval->colname, pAttrValue, cbSizeValue);
            if (cbSizeValue < sizeof(hcolval->colname)) {
                hcolval->colname[cbSizeValue] = 0;
            }
        } else {
            assert (0);
        }
        hcolval->colname[sizeof(hcolval->colname) - 1] = 0;
        break;
    case GDB_ATTR_COLVAL_INDEX:
        GDBERROR_THROW_ERDONLY(hError);
        break;

    case GDB_ATTR_COLVAL_TYPE:
    case GDB_ATTR_COLVAL_PARAMTYPE:
        GDBERROR_THROW_ERDONLY(hError);
        break;

    case GDB_ATTR_COLVAL_ADDR:
        hcolval->coladdr = pAttrValue;
        hcolval->colsize = cbSizeValue;
        break;
  
    default:
        GDBERROR_THROW_EATTR(hError);
    }

    return GDBAPI_SUCCESS;
}

#ifdef    __cplusplus
}
#endif

#endif /* _GDB_COLDEF_H_ */
