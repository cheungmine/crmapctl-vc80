/**
 * @file
 *    gdbcontext.h
 * @brief
 *    private header defines the interface of GDBContext.
 * @author
 *    Zhang Liang \n
 *    350137278@qq.com \n
 *    http://blog.csdn.net/cheungmine
 * @since
 *    2013/12/26
 * @date
 *    2013/12/26
 * @version
 *    0.0.1
 * @note
 *
 */

#ifndef _GDB_CONTEXT_H_
#define _GDB_CONTEXT_H_

#ifdef    __cplusplus
extern "C" {
#endif

/* sqlite3 api */
#include "./sqlite3/src/sqlite3.h"

#include "gdberror.h"

#define GDB_FILENAME_LEN 200
#define GDB_USERNAME_LEN 30
#define GDB_PASSWORD_LEN 40


typedef struct _GDBContext
{
    GDBHandleOb __handle[1];

    sqlite3 *gdb;

    GDBAPI_BOOL   singlethread;
    GDBAPI_BOOL   readonly;
    GDBAPI_BOOL   nocreate;
    GDBAPI_BOOL   nosharecache;

    char tablespace[GDB_TABLESPACE_LEN+1];  /* GDB_ATTR_CTX_TABLESPACE: default GDBAPI */
    char owner[GDB_OWNER_LEN+1];            /* GDB_ATTR_CTX_OWNER: default GDBAPI */

    GDB_STMT hstmt_INSERT_GDBLAYERS;
    GDB_STMT hstmt_INSERT_COLUMNS;
    GDB_STMT hstmt_QUERY_LAYER;
  
    char gdbfile[GDB_FILENAME_LEN + 1];  /* GDB_ATTR_DBNAME */
    char username[GDB_USERNAME_LEN + 1]; /* GDB_ATTR_USERNAME */
    char password[GDB_PASSWORD_LEN + 1]; /* GDB_ATTR_PASSWORD */
} GDBContext, *HGDBContext;


typedef struct _GDBStmt
{
    GDBHandleOb __handle[1];

    GDBContext *hctx;

    sqlite3_stmt *stmt;

    void *attachment;
} GDBStmt, *HGDBStmt;


#define CAST_HANDLE_CONTEXT(hdl, herr, hctx) \
    CAST_HANDLE_TYPE(hdl, herr, GDB_HTYPE_CONTEXT, HGDBContext, hctx)

#define CAST_HANDLE_STMT(hdl, herr, hstmt) \
    CAST_HANDLE_TYPE(hdl, herr, GDB_HTYPE_STMT, HGDBStmt, hstmt)


static void _ContextSet_hstmt_INSERT_GDBLAYERS (
    HGDBContext ctx, HGDBSTMT hstmt, HGDBERR herr)
{
    HGDBSTMT tmp = ctx->hstmt_INSERT_GDBLAYERS;
    ctx->hstmt_INSERT_GDBLAYERS = hstmt;
    if (tmp) {
        GDBStmtFinish(tmp, GDBAPI_TRUE, herr);
    }
}


static void _ContextSet_hstmt_INSERT_COLUMNS (
    HGDBContext ctx, HGDBSTMT hstmt, HGDBERR herr)
{
    HGDBSTMT tmp = ctx->hstmt_INSERT_COLUMNS;
    ctx->hstmt_INSERT_COLUMNS = hstmt;
    if (tmp) {
        GDBStmtFinish(tmp, GDBAPI_TRUE, herr);
    }
}


static void _ContextSet_hstmt_QUERY_LAYER (
    HGDBContext ctx, HGDBSTMT hstmt, HGDBERR herr)
{
    HGDBSTMT tmp = ctx->hstmt_QUERY_LAYER;
    ctx->hstmt_QUERY_LAYER = hstmt;
    if (tmp) {
        GDBStmtFinish(tmp, GDBAPI_TRUE, herr);
    }
}


static GDBAPI_RESULT _GDBCtxSetAttr (
    HGDBContext   hctx,
    void*         pAttrValue,
    int           cbSizeValue,
    GDB_ATTR_NAME attrName,
    GDB_ERROR     hError)
{
    switch (attrName) {
    case GDB_ATTR_CTX_DBNAME:
      if (cbSizeValue <= sizeof(hctx->gdbfile)) {
          memcpy(hctx->gdbfile, pAttrValue, cbSizeValue);
          if (cbSizeValue < sizeof(hctx->gdbfile)) {
              hctx->gdbfile[cbSizeValue] = 0;
          }
      } else {
          assert (0);
      }
      hctx->gdbfile[sizeof(hctx->gdbfile) - 1] = 0;
      break;

    case GDB_ATTR_CTX_USERNAME:
      if (cbSizeValue <= sizeof(hctx->username)) {
          memcpy(hctx->username, pAttrValue, cbSizeValue);
          if (cbSizeValue < sizeof(hctx->username)) {
              hctx->username[cbSizeValue] = 0;
          }
      } else {
          assert (0);
      }
      hctx->username[sizeof(hctx->username) - 1] = 0;
      break;

    case GDB_ATTR_CTX_PASSWORD:
        if (cbSizeValue <= sizeof(hctx->password)) {
            memcpy(hctx->password, pAttrValue, cbSizeValue);
            if (cbSizeValue < sizeof(hctx->password)) {
                hctx->password[cbSizeValue] = 0;
            }
        } else {
            assert (0);
        }
        hctx->password[sizeof(hctx->password) - 1] = 0;
        break;

    case GDB_ATTR_CTX_SINGLETHREAD:
        hctx->singlethread = Value2Bool(pAttrValue);
        break;

    case GDB_ATTR_CTX_READONLY:
        hctx->readonly = Value2Bool(pAttrValue);
        break;

    case GDB_ATTR_CTX_NOCREATE:
        hctx->nocreate = Value2Bool(pAttrValue);
        break;

    case GDB_ATTR_CTX_NOSHARECACHE:
        hctx->nosharecache = Value2Bool(pAttrValue);
        break;

    case GDB_ATTR_CTX_TABLESPACE:
        if (cbSizeValue <= sizeof(hctx->tablespace)) {
            memcpy(hctx->tablespace, pAttrValue, cbSizeValue);
            if (cbSizeValue < sizeof(hctx->tablespace)) {
                hctx->tablespace[cbSizeValue] = 0;
            }
        } else {
            assert (0);
        }
        hctx->tablespace[sizeof(hctx->tablespace) - 1] = 0;
        break;

    case GDB_ATTR_CTX_OWNER:
        if (cbSizeValue <= sizeof(hctx->owner)) {
            memcpy(hctx->owner, pAttrValue, cbSizeValue);
            if (cbSizeValue < sizeof(hctx->owner)) {
                hctx->owner[cbSizeValue] = 0;
            }
        } else {
            assert (0);
        }
        hctx->owner[sizeof(hctx->owner) - 1] = 0;
        break;

    default:
        assert (0);
        break;
    }

    return GDBAPI_SUCCESS;
}


static GDBAPI_RESULT _GDBCtxGetAttr (
    HGDBContext   hctx,
    void*         pAttrValue,
    int*          cbSizeValue,
    GDB_ATTR_NAME attrName,
    GDB_ERROR     hError)
{
    if (!pAttrValue && !cbSizeValue) {
        GDBERROR_THROW_EATTRVAL(hError);
    }

    switch (attrName) {
    case GDB_ATTR_CTX_DBNAME:
        ATTR_GET_STRING_ADDR(hctx->gdbfile, pAttrValue, cbSizeValue, hError);
        break;

    case GDB_ATTR_CTX_USERNAME:
        ATTR_GET_STRING_ADDR(hctx->username, pAttrValue, cbSizeValue, hError);
        break;

    case GDB_ATTR_CTX_PASSWORD:
        ATTR_GET_STRING_ADDR(hctx->password, pAttrValue, cbSizeValue, hError);
        break;
    
    case GDB_ATTR_CTX_SINGLETHREAD:
        ATTR_GET_TYPED_VALUE(GDBAPI_BOOL, hctx->singlethread,
            pAttrValue, cbSizeValue, hError);
        break;
    
    case GDB_ATTR_CTX_READONLY:
        ATTR_GET_TYPED_VALUE(GDBAPI_BOOL, hctx->readonly,
            pAttrValue, cbSizeValue, hError);
        break;
    
    case GDB_ATTR_CTX_NOCREATE:
        ATTR_GET_TYPED_VALUE(GDBAPI_BOOL, hctx->nocreate,
            pAttrValue, cbSizeValue, hError);
        break;
    
    case GDB_ATTR_CTX_NOSHARECACHE:
        ATTR_GET_TYPED_VALUE(GDBAPI_BOOL, hctx->nosharecache,
            pAttrValue, cbSizeValue, hError);
        break;

    case GDB_ATTR_CTX_TABLESPACE:
        ATTR_GET_STRING_ADDR(hctx->tablespace, pAttrValue, cbSizeValue, hError);
        break;

    case GDB_ATTR_CTX_OWNER:
        ATTR_GET_STRING_ADDR(hctx->owner, pAttrValue, cbSizeValue, hError);
        break;

    //??TODO:
    case GDB_ATTR_CTX_RO_TABLESPACES:
        break;

    case GDB_ATTR_CTX_RO_LAYERIDS:
        break;

    default:
        GDBERROR_THROW_EATTR(hError);
    }

    return GDBAPI_SUCCESS;
}

#ifdef    __cplusplus
}
#endif

#endif /* _GDB_CONTEXT_H_ */
