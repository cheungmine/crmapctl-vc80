/**
 * @file
 *    matalg_base.h
 *
 * @brief
 *    Matrix Algebra Lib
 *
 * @since
 *    2013-6-1
 * @date
 *
 */

/*****************************************************************************/
/*                             Matrix Algebra Lib                            */
/*                                                                           */
/* Copyright (c) 2013, ZhangLiang (cheungmine@gmail.com; 350137278@qq.com)   */
/* All Rights Reserved.                                                      */
/*                                                                           */
/* Permission is hereby granted, free of charge, to any person obtaining a   */
/* copy of this software and associated documentation files (the "Software"),*/
/* to deal in the Software without restriction, including without limitation */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,  */
/* and/or sell copies of the Software, and to permit persons to whom the     */
/* Software is furnished to do so, subject to the following conditions:      */
/*                                                                           */
/* The above Copyright notice and this permission notice shall be included   */
/* in all copies or substantial portions of the Software.                    */
/*                                                                           */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.    */
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY      */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT */
/* OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR  */
/* THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                */
/*****************************************************************************/

#ifndef _MATALG_BASE_H_
#define _MATALG_BASE_H_

#ifdef    __cplusplus
extern "C" {
#endif

#ifdef WIN32
# define _USE_MATH_DEFINES
#endif

#include <math.h>

#include "matalg_inl.h"

#ifdef __sun__
# include <ieeefp.h> /* for finite() */
#endif

#ifndef M_PI
# define M_PI        3.14159265358979323846
#endif

#ifndef M_PI2
# define M_PI2       6.28318530717958647692
#endif

#ifndef M_PI_2
# define M_PI_2      1.57079632679489661923
#endif

#ifndef M_PI_4
# define M_PI_4     0.785398163397448309616
#endif

#ifndef M_SQRT2
# define M_SQRT2     1.41421356237309504880
#endif

#ifndef M_SQRT1_2
# define M_SQRT1_2   0.70710678118654752440
#endif

#ifndef M_1_PI
# define M_1_PI      0.318309886183790671538
#endif

#ifndef M_1_PI
# define M_2_PI     0.636619772367581343076
#endif

#ifndef M_E
# define M_E         2.7182818284590452354
#endif

#ifndef M_LOG2E
# define M_LOG2E     1.4426950408889634074
#endif

#ifndef M_LOG10E
# define M_LOG10E    0.43429448190325182765
#endif

#ifndef M_LN2
# define M_LN2       0.69314718055994530942
#endif

#ifndef M_LN10
# define M_LN10      2.30258509299404568402
#endif

#ifndef MAX
# define MAX(a, b) ((a) > (b) ? (a) : (b))
#endif

#ifndef MIN
# define MIN(a, b) ((a) < (b) ? (a) : (b))
#endif

#ifndef ABS
# define ABS(a) ((a) > 0? (a) : ((a) < 0? (-(a)) : 0))
#endif

#ifndef SGN
# define SGN(a) ((a) > 0? 1 : ((a) < 0? (-1) : 0))
#endif

/* non-standard defines, used in some places */
#ifndef MAXFLOAT
# define MAXFLOAT  ((float) 3.40282347e+38)
#endif

#ifndef MINFLOAT
# define MINFLOAT  ((float) 1.0e-35f)
#endif

#ifndef sqrtf
# define sqrtf(a) ((float) sqrt(a))
#endif

#ifndef powf
# define powf(a, b) ((float) pow(a, b))
#endif

#ifndef cosf
# define cosf(a) ((float) cos(a))
#endif

#ifndef sinf
# define sinf(a) ((float) sin(a))
#endif

#ifndef acosf
# define acosf(a) ((float) acos(a))
#endif

#ifndef asinf
# define asinf(a) ((float) asin(a))
#endif

#ifndef atan2f
# define atan2f(a, b) ((float) atan2(a, b))
#endif

#ifndef tanf
# define tanf(a) ((float) tan(a))
#endif

#ifndef atanf
# define atanf(a) ((float) atan(a))
#endif

#ifndef floorf
# define floorf(a) ((float) floor(a))
#endif

#ifndef ceilf
# define ceilf(a) ((float) ceil(a))
#endif

#ifndef fabsf
# define fabsf(a) ((float) fabs(a))
#endif

#ifndef logf
# define logf(a) ((float)log(a))
#endif

#ifndef expf
# define expf(a) ((float)exp(a))
#endif

#ifndef fmodf
# define fmodf(a, b) ((float)fmod(a, b))
#endif

#ifndef hypotf
# define hypotf(a, b) ((float)hypot(a, b))
#endif

#ifdef _WIN32
# ifndef FREE_WINDOWS
#   define isnan(n) _isnan(n)
#   define finite _finite
#   define hypot _hypot
# endif
#endif

#ifndef SWAP_TYPE
# define SWAP_TYPE(a, b, _type)  do { \
    _type t; \
    t = (a); \
    (a) = (b); \
    (b) = t; \
  } while (0)
#endif

#include "matalg_base_inl.c"

__INLINE float sqrt3f (float f);
__INLINE double sqrt3d (double d);

__INLINE double sqrt_s (double d);
__INLINE float sqrtf_s (float f);

__INLINE double acos_s (double d);
__INLINE double asin_s (double d);

__INLINE float acosf_s (float f);
__INLINE float asinf_s (float f);

__INLINE float interpf (float org, float tgt, float ratio);

__INLINE float minf (float a, float b);
__INLINE float maxf (float a, float b);

__INLINE int sign (double d);
__INLINE float signf (float f);

#if (defined(_WIN32) || defined(_WIN64)) && !defined(FREE_WINDOWS)
extern double copysign (double x, double y);
extern double round (double x);
#endif

#ifdef    __cplusplus
}
#endif

#endif /* _MATALG_BASE_H_ */
