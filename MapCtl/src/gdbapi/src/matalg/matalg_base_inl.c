/**
 * @file
 *    matalg_base_inl.c
 *
 * @brief
 *    Matrix Algebra Lib
 *
 * @since
 *    2013-6-1
 * @date
 *
 */

/*****************************************************************************/
/*                             Matrix Algebra Lib                            */
/*                                                                           */
/* Copyright (c) 2013, ZhangLiang (cheungmine@gmail.com; 350137278@qq.com)   */
/* All Rights Reserved.                                                      */
/*                                                                           */
/* Permission is hereby granted, free of charge, to any person obtaining a   */
/* copy of this software and associated documentation files (the "Software"),*/
/* to deal in the Software without restriction, including without limitation */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,  */
/* and/or sell copies of the Software, and to permit persons to whom the     */
/* Software is furnished to do so, subject to the following conditions:      */
/*                                                                           */
/* The above Copyright notice and this permission notice shall be included   */
/* in all copies or substantial portions of the Software.                    */
/*                                                                           */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.    */
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY      */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT */
/* OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR  */
/* THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                */
/*****************************************************************************/

#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef _MATAL_BASE_INL_C_
#define _MATAL_BASE_INL_C_

#include "matalg_base.h"

#ifndef FLT_EPSILON
# define FLT_EPSILON  1.192092896e-07F
#endif

__INLINE float sqrt3f (float f)
{
    return (float)
        ((f == 0.0f)? 0.0f: (f < 0? (-exp(log(-f)/3)) : (exp(log(f)/3))));
}

__INLINE double sqrt3d (double d)
{
    return (double)
        ((d == 0.0)? 0: (d < 0? (-exp(log(-d)/3)) : (exp(log(d)/3))));
}

__INLINE double sqrt_s (double d)
{
    return (d <= 0.0)? 0.0 : sqrt(d);
}

__INLINE float sqrtf_s (float f)
{
    return (f <= 0.0f)? 0.0f : sqrtf(f);
}

__INLINE double acos_s (double d)
{
    return ((d <= -1.0)? M_PI : (d >= 1.0? 0. : acos(d)));
}

__INLINE double asin_s(double d)
{
    return (d <= -1.0? -M_PI_2 : (d>=1.0? M_PI_2 : asin(d)));
}

__INLINE float acosf_s(float f)
{
    return (f <= -1.0f)? (float) M_PI : (f >= 1.0f? 0.0f: acosf(f));
}

__INLINE float asinf_s(float f)
{
    return (float)((f <= -1.0f)? -M_PI_2 : ((f >= 1.0f)? M_PI_2 : asinf(f)));
}

__INLINE float interpf (float org, float tgt, float ratio)
{
    return (ratio * tgt) + (1.0f - ratio) * org;
}

__INLINE float minf (float a, float b)
{
    return (a < b ? a : b);
}

__INLINE float maxf (float a, float b)
{
    return (a > b ? a : b);
}

__INLINE int sign (double d)
{
    return (d < 0.0 ? (-1): (d > 0.0 ? 1 : 0));
}

__INLINE float signf (float f)
{
    return (f < 0.f ? -1.f : 1.f);
}

#endif /* _MATAL_BASE_INL_C_ */
