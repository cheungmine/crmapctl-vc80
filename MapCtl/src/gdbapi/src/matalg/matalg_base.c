/**
 * @file
 *    matalg_base.c
 *
 * @brief
 *    Matrix Algebra Lib
 *
 * @since
 *    2013-6-1
 * @date
 *
 */

/*****************************************************************************/
/*                             Matrix Algebra Lib                            */
/*                                                                           */
/* Copyright (c) 2013, ZhangLiang (cheungmine@gmail.com; 350137278@qq.com)   */
/* All Rights Reserved.                                                      */
/*                                                                           */
/* Permission is hereby granted, free of charge, to any person obtaining a   */
/* copy of this software and associated documentation files (the "Software"),*/
/* to deal in the Software without restriction, including without limitation */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,  */
/* and/or sell copies of the Software, and to permit persons to whom the     */
/* Software is furnished to do so, subject to the following conditions:      */
/*                                                                           */
/* The above Copyright notice and this permission notice shall be included   */
/* in all copies or substantial portions of the Software.                    */
/*                                                                           */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.    */
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY      */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT */
/* OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR  */
/* THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                */
/*****************************************************************************/

#include "matalg_base.h"

/* WARNING: MSVC compiling hack for double_round() */
#if (defined(_WIN32) || defined(_WIN64)) && !(defined(FREE_WINDOWS))

/* from python 3.1 pymath.c */
double copysign (double x, double y)
{
    /* use atan2 to distinguish -0. from 0. */
    return (y > 0. || (y == 0. && atan2 (y, -1.) > 0.)) ? fabs (x) : -fabs (x);
}

/* from python 3.1 pymath.c */
double round (double x)
{
    double vx, y;
    vx = fabs (x);
    y = floor (vx);
    if (vx - y >= 0.5) {
        y += 1.0;
    }
    return copysign (y, x);
}

#else /* OpenSuse 11.1 seems to need this. */
double round (double x);
#endif

/* from python 3.1 floatobject.c, 
 * digits must be between 0 and 21 */
static double double_round (double x, int digits)
{
    double pow1, y, z;

    if (digits >= 0) {
        pow1 = digits==0? 1.0 : pow (10.0, (double) digits);
        y = (x*pow1);
        /* if y overflows, then rounded value is exactly x */
        if (!finite(y)) {
            return x;
        }

        z = round(y);
        if (fabs(y-z) == 0.5) {
            /* halfway between two integers; use round-half-even */
            z = 2.0 * round (y/2.0);
        }
        return z / pow1;
    } else {
        pow1 = pow (10.0, (double) -digits);
        y = x / pow1;

        z = round(y);
        if (fabs(y-z) == 0.5) {
            /* halfway between two integers; use round-half-even */
            z = 2.0 * round (y/2.0);
        }
        return z*pow1;
    }
}
