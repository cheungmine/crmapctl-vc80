/**
 * gdbshell.h
 * @brief
 *    GDB command line tool shell
 * @author http://blog.csdn.net/cheungmine
 * @since 2013/4/18
 * @date 2013/6/3
 * @version 0.0.1
 */

#ifndef _GDBSHELL_H
#define _GDBSHELL_H

#ifdef _MSC_VER
# pragma warning(disable : 4996) 
#endif

#include "gdbapi.h"

#include <time.h>

#ifdef    __cplusplus
extern "C" {
#endif
 
#define MAX_FILENAME_LEN   260
#define MAX_LAYERTABLE_LEN 30
#define MAX_FILEFMT_LEN    5
#define MAX_TABLENAME_LEN  30

typedef enum
{
    CREMOD_CREATE_NEW,
    CREMOD_REPLACE_EXIST,
    CREMOD_APPEND_EXIST
} gt_table_cremod;

typedef struct gt_color3
{
    double red;
    double green;
    double blue;
} gt_color3;

extern 
int shape2gdb (char *shapefile, char *gdbfile, char *layertable,
    gt_table_cremod mode);

/*
 * drawshape.c
 */
typedef enum
{
    SURFACE_FMT_PNG,
    SURFACE_FMT_PDF
} gt_surface_fmt;

extern
int drawshapefile (char *shapefile, char *shapeid,
    char *output, int width, int length, gt_surface_fmt format);

extern
int drawgdblayer (char *gdb, char *layer, char *wherecond,
    char *output, int width, int length, int grid, gt_surface_fmt format);

/*
 * tiff2gdb.c
 */
extern
int tiff2gdb (char *tifffile, char *tfwfile, char *gdb,
    char *imagetable, gt_table_cremod mode);


/* layerovly.c
 */
extern
int areaovly (GDB_OVERLAY_MODE mode, char *gdb, char *subjectlayer, char *cliplayer,
    char *resultlayer, rowid_t offset, rowid_t rowcount);

extern
int gdbopen (GDB_CONTEXT hctx, char *gdb, char *username, char *passwd,
    GDBAPI_BOOL nocreate, GDB_ERROR herr);

static void print_time (const char *prefix)
{
    time_t t;
    struct tm *ti;
    time (&t);
    ti = localtime (&t);
    fprintf(stdout, "%s%s\n", prefix, asctime (ti));
}

#ifdef    __cplusplus
}
#endif

#endif /* _GDBSHELL_H */
