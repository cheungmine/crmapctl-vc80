/**
 * gdblayer.h
 * @brief
 *    GDB_LAYER definition
 * @author
 *    ZhangLiang
 * @since
 *    2013-4-25
 * @date
 *    2013-6-03
 */

#ifndef _GDB_LAYER_H_
#define _GDB_LAYER_H_

#ifdef _MSC_VER
 #pragma warning (disable : 4996)
#endif

#ifdef    __cplusplus
extern "C" {
#endif

#include "gdbtype.h"
#include "gdberror.h"
#include "gdbquad.h"

/* shape file api */
#include "./shapefile/src/shapefile.h"

/* sqlite3 api */
#include "./sqlite3/src/sqlite3.h"


#define PARAMID_LAYERID     (0)
#define PARAMID_TABLESPACE  (PARAMID_LAYERID+1)
#define PARAMID_OWNER       (PARAMID_LAYERID+2)
#define PARAMID_TABLENAME   (PARAMID_LAYERID+3)
#define PARAMID_SPATIALCOL  (PARAMID_LAYERID+4)
#define PARAMID_ROWIDCOL    (PARAMID_LAYERID+5)
#define PARAMID_XMINCOL     (PARAMID_LAYERID+6)
#define PARAMID_YMINCOL     (PARAMID_LAYERID+7)
#define PARAMID_XMAXCOL     (PARAMID_LAYERID+8)
#define PARAMID_YMAXCOL     (PARAMID_LAYERID+9)
#define PARAMID_LENGTHCOL   (PARAMID_LAYERID+10)
#define PARAMID_AREACOL     (PARAMID_LAYERID+11)
#define PARAMID_LAYERTYPE   (PARAMID_LAYERID+12)
#define PARAMID_LAYERMASK   (PARAMID_LAYERID+13)
#define PARAMID_LEVELMIN    (PARAMID_LAYERID+14)
#define PARAMID_LEVELMAX    (PARAMID_LAYERID+15)
#define PARAMID_XMIN        (PARAMID_LAYERID+16)
#define PARAMID_YMIN        (PARAMID_LAYERID+17)
#define PARAMID_XMAX        (PARAMID_LAYERID+18)
#define PARAMID_YMAX        (PARAMID_LAYERID+19)
#define PARAMID_ZMIN        (PARAMID_LAYERID+20)
#define PARAMID_ZMAX        (PARAMID_LAYERID+21)
#define PARAMID_MMIN        (PARAMID_LAYERID+22)
#define PARAMID_MMAX        (PARAMID_LAYERID+23)
#define PARAMID_REFID       (PARAMID_LAYERID+24)
#define PARAMID_MINIMUMID   (PARAMID_LAYERID+25)
#define PARAMID_DESCRIPT    (PARAMID_LAYERID+26)
#define PARAMID_CREDATE     (PARAMID_LAYERID+27)
#define PARAMID_COUNT       (PARAMID_LAYERID+28)

#define CAST_HANDLE_LAYER(hdl, herr, hlayer) \
    CAST_HANDLE_TYPE(hdl, herr, GDB_HTYPE_LAYER, HGDBLayer, hlayer)

#define LAYER_COLNAME_LEN  30
#define LAYER_DESCRIPT_LEN  255

typedef struct _GDBLayerInfo
{
    GDBHandleOb __handle[1];

    char colflags[PARAMID_COUNT];

    int  LAYERID;

    char TABLESPACE[LAYER_COLNAME_LEN+1];
    char OWNER[LAYER_COLNAME_LEN+1];
    char TABLENAME[LAYER_COLNAME_LEN+1];
    char SPATIALCOL[LAYER_COLNAME_LEN+1];
    char ROWIDCOL[LAYER_COLNAME_LEN+1];

    char XMINCOL[LAYER_COLNAME_LEN+1];
    char YMINCOL[LAYER_COLNAME_LEN+1];
    char XMAXCOL[LAYER_COLNAME_LEN+1];
    char YMAXCOL[LAYER_COLNAME_LEN+1];
    char LENGTHCOL[LAYER_COLNAME_LEN+1];
    char AREACOL[LAYER_COLNAME_LEN+1];

    int  LAYERTYPE;
    int  LAYERMASK;

    int  LEVELMIN;
    int  LEVELMAX;

    union {
        struct {
            union {
                struct {
                    double XMIN;
                    double YMIN;
                    double XMAX;
                    double YMAX;
                };
                GDBExtRect _MBR;
            };

            double ZMIN;
            double ZMAX;
            double MMIN;
            double MMAX;
        };
        GDBExtBound _BOUNDS;
    };

    int REFID;

    rowid_t MINIMUMID;

    char DESCRIPT[LAYER_DESCRIPT_LEN+1];
    char CREDATE[LAYER_COLNAME_LEN+1];
} GDBLayerInfo, *HGDBLayer;

#define CAST_HANDLE_LAYER(hdl, herr, hlayer) \
    CAST_HANDLE_TYPE(hdl, herr, GDB_HTYPE_LAYER, HGDBLayer, hlayer)

#define LAYER_SET_VALUE_ATTR(layerInfo, pid, layerCol, ColType, pAttrVal) do {\
        if (pAttrVal) { \
            layerInfo->colflags[pid] = (char) 1; \
            layerCol = VALUE_PTR(pAttrValue, ColType); \
        } else { \
            layerInfo->colflags[pid] = (char) 0; \
        } \
    } while (0)

#define LAYER_SET_STRING_ATTR(layerInfo, pid, layerCol, pAttrVal, SizeVal) \
    do { \
        layerInfo->colflags[pid] = (char) 0; \
        if (pAttrVal) { \
            if (SizeVal <= sizeof(layerCol)) { \
                layerInfo->colflags[pid] = (char) 1; \
                memcpy(layerCol, pAttrVal, SizeVal); \
                if (SizeVal < sizeof(layerCol)) { \
                    layerCol[SizeVal] = 0; \
                } \
            } else { \
                GDBERROR_THROW_EATTRVAL(hError); \
            } \
            layerCol[sizeof(layerCol)-1] = 0; \
        } \
    } while (0)

static const char * _GDBLayerGetSITblName(int layerid, int gridno,
    char *sitable)
{
    sprintf (sitable, "GDB_SI_L%d_G%d", layerid, gridno);
    return sitable;
}

static const char * _GDBLayerGetIndexName(const char *sitable, char *gridindex)
{
    sprintf (gridindex, "%s_ID", sitable);
    return gridindex;
}

static GDBAPI_RESULT _GDBLayerInfoGetAttr (HGDBLayer hlayer,
    void *pAttrValue, int *cbSizeValue, GDB_ATTR_NAME attrName, GDB_ERROR hError)
{
    if (!pAttrValue && !cbSizeValue) {
        GDBERROR_THROW_EATTRVAL(hError);
    }

    switch (attrName) {
    case GDB_ATTR_LAYER_LAYERID:
        ATTR_GET_TYPED_VALUE(int, hlayer->LAYERID,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_LAYER_TABLESPACE:
        ATTR_GET_STRING_ADDR(hlayer->TABLESPACE, pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_LAYER_OWNER:
        ATTR_GET_STRING_ADDR(hlayer->OWNER, pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_LAYER_TABLENAME:
        ATTR_GET_STRING_ADDR(hlayer->TABLENAME, pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_LAYER_SPATIALCOL:
        ATTR_GET_STRING_ADDR(hlayer->SPATIALCOL, pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_LAYER_ROWIDCOL:
        ATTR_GET_STRING_ADDR(hlayer->ROWIDCOL, pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_LAYER_XMINCOL:
        ATTR_GET_STRING_ADDR(hlayer->XMINCOL, pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_LAYER_YMINCOL:
        ATTR_GET_STRING_ADDR(hlayer->YMINCOL, pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_LAYER_XMAXCOL:
        ATTR_GET_STRING_ADDR(hlayer->XMAXCOL, pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_LAYER_YMAXCOL:
        ATTR_GET_STRING_ADDR(hlayer->YMAXCOL, pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_LAYER_LENGTHCOL:
        ATTR_GET_STRING_ADDR(hlayer->LENGTHCOL, pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_LAYER_AREACOL:
        ATTR_GET_STRING_ADDR(hlayer->AREACOL, pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_LAYER_LAYERTYPE:
        ATTR_GET_TYPED_VALUE(int, hlayer->LAYERTYPE,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_LAYER_LAYERMASK:
        ATTR_GET_TYPED_VALUE(int, hlayer->LAYERMASK,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_LAYER_LEVELMIN:
        ATTR_GET_TYPED_VALUE(int, hlayer->LEVELMIN,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_LAYER_LEVELMAX:
        ATTR_GET_TYPED_VALUE(int, hlayer->LEVELMAX,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_LAYER_BOUNDS:
        ATTR_GET_TYPED_VALUE(GDBExtBound, hlayer->_BOUNDS,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_LAYER_RECT:
        ATTR_GET_TYPED_VALUE(GDBExtRect, hlayer->_MBR,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_LAYER_XMIN:
        ATTR_GET_TYPED_VALUE(double, hlayer->XMIN,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_LAYER_YMIN:
        ATTR_GET_TYPED_VALUE(double, hlayer->YMIN,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_LAYER_XMAX:
        ATTR_GET_TYPED_VALUE(double, hlayer->XMAX,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_LAYER_YMAX:
        ATTR_GET_TYPED_VALUE(double, hlayer->YMAX,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_LAYER_ZMIN:
        ATTR_GET_TYPED_VALUE(double, hlayer->ZMIN,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_LAYER_ZMAX:
        ATTR_GET_TYPED_VALUE(double, hlayer->ZMAX,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_LAYER_MMIN:
        ATTR_GET_TYPED_VALUE(double, hlayer->MMIN,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_LAYER_MMAX:
        ATTR_GET_TYPED_VALUE(double, hlayer->MMAX,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_LAYER_REFID:
        ATTR_GET_TYPED_VALUE(int, hlayer->REFID,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_LAYER_MINIMUMID:
        ATTR_GET_TYPED_VALUE(rowid_t, hlayer->MINIMUMID,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_LAYER_CREDATE:
        ATTR_GET_STRING_ADDR(hlayer->CREDATE, pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_LAYER_DESCRIPT:
        ATTR_GET_STRING_ADDR(hlayer->DESCRIPT, pAttrValue, cbSizeValue, hError);
        break;
    default:
        GDBERROR_THROW_EATTR(hError);
    }

    return GDBAPI_SUCCESS;
}

static GDBAPI_RESULT _GDBLayerInfoSetAttr(HGDBLayer hlayer,
    void *pAttrValue, int cbSizeValue, GDB_ATTR_NAME attrName, GDB_ERROR hError)
{
    switch (attrName) {
    case GDB_ATTR_LAYER_LAYERID:
        GDBERROR_THROW_ERDONLY(hError);
        break;
    case GDB_ATTR_LAYER_TABLESPACE:
        LAYER_SET_STRING_ATTR(hlayer, PARAMID_TABLESPACE, hlayer->TABLESPACE, pAttrValue, cbSizeValue);
        break;
    case GDB_ATTR_LAYER_OWNER:
        LAYER_SET_STRING_ATTR(hlayer, PARAMID_OWNER, hlayer->OWNER, pAttrValue, cbSizeValue);
        break;
    case GDB_ATTR_LAYER_TABLENAME:
        LAYER_SET_STRING_ATTR(hlayer, PARAMID_TABLENAME, hlayer->TABLENAME, pAttrValue, cbSizeValue);
        break;
    case GDB_ATTR_LAYER_SPATIALCOL:
        LAYER_SET_STRING_ATTR(hlayer, PARAMID_SPATIALCOL, hlayer->SPATIALCOL, pAttrValue, cbSizeValue);
        break;
    case GDB_ATTR_LAYER_ROWIDCOL:
        LAYER_SET_STRING_ATTR(hlayer, PARAMID_ROWIDCOL, hlayer->ROWIDCOL, pAttrValue, cbSizeValue);
        break;
    case GDB_ATTR_LAYER_XMINCOL:
        LAYER_SET_STRING_ATTR(hlayer, PARAMID_XMINCOL, hlayer->XMINCOL, pAttrValue, cbSizeValue);
        break;
    case GDB_ATTR_LAYER_YMINCOL:
        LAYER_SET_STRING_ATTR(hlayer, PARAMID_YMINCOL, hlayer->YMINCOL, pAttrValue, cbSizeValue);
        break;
    case GDB_ATTR_LAYER_XMAXCOL:
        LAYER_SET_STRING_ATTR(hlayer, PARAMID_XMAXCOL, hlayer->XMAXCOL, pAttrValue, cbSizeValue);
        break;
    case GDB_ATTR_LAYER_YMAXCOL:
        LAYER_SET_STRING_ATTR(hlayer, PARAMID_YMAXCOL, hlayer->YMAXCOL, pAttrValue, cbSizeValue);
        break;
    case GDB_ATTR_LAYER_LENGTHCOL:
        LAYER_SET_STRING_ATTR(hlayer, PARAMID_LENGTHCOL, hlayer->LENGTHCOL, pAttrValue, cbSizeValue);
        break;
    case GDB_ATTR_LAYER_AREACOL:
        LAYER_SET_STRING_ATTR(hlayer, PARAMID_AREACOL, hlayer->AREACOL, pAttrValue, cbSizeValue);
        break;
    case GDB_ATTR_LAYER_LAYERTYPE:
        LAYER_SET_VALUE_ATTR(hlayer, PARAMID_LAYERTYPE, hlayer->LAYERTYPE, int, pAttrValue);
        break;
    case GDB_ATTR_LAYER_LAYERMASK:
        LAYER_SET_VALUE_ATTR(hlayer, PARAMID_LAYERMASK, hlayer->LAYERMASK, int, pAttrValue);
        break;
    case GDB_ATTR_LAYER_LEVELMIN:
        LAYER_SET_VALUE_ATTR(hlayer, PARAMID_LEVELMIN, hlayer->LEVELMIN, int, pAttrValue);
        break;
    case GDB_ATTR_LAYER_LEVELMAX:
        LAYER_SET_VALUE_ATTR(hlayer, PARAMID_LEVELMAX, hlayer->LEVELMAX, int, pAttrValue);
        break;
    case GDB_ATTR_LAYER_XMIN:
        LAYER_SET_VALUE_ATTR(hlayer, PARAMID_XMIN, hlayer->XMIN, double, pAttrValue);
        break;
    case GDB_ATTR_LAYER_YMIN:
        LAYER_SET_VALUE_ATTR(hlayer, PARAMID_YMIN, hlayer->YMIN, double, pAttrValue);
        break;
    case GDB_ATTR_LAYER_XMAX:
        LAYER_SET_VALUE_ATTR(hlayer, PARAMID_XMAX, hlayer->XMAX, double, pAttrValue);
        break;
    case GDB_ATTR_LAYER_YMAX:
        LAYER_SET_VALUE_ATTR(hlayer, PARAMID_YMAX, hlayer->YMAX, double, pAttrValue);
        break;
    case GDB_ATTR_LAYER_ZMIN:
        LAYER_SET_VALUE_ATTR(hlayer, PARAMID_ZMIN, hlayer->ZMIN, double, pAttrValue);
        break;
    case GDB_ATTR_LAYER_ZMAX:
        LAYER_SET_VALUE_ATTR(hlayer, PARAMID_ZMAX, hlayer->ZMAX, double, pAttrValue);
        break;
    case GDB_ATTR_LAYER_MMIN:
        LAYER_SET_VALUE_ATTR(hlayer, PARAMID_MMIN, hlayer->MMIN, double, pAttrValue);
        break;
    case GDB_ATTR_LAYER_MMAX:
        LAYER_SET_VALUE_ATTR(hlayer, PARAMID_MMAX, hlayer->MMAX, double, pAttrValue);
        break;
    case GDB_ATTR_LAYER_REFID:
        LAYER_SET_VALUE_ATTR(hlayer, PARAMID_REFID, hlayer->REFID, int, pAttrValue);
        break;
    case GDB_ATTR_LAYER_MINIMUMID:
        LAYER_SET_VALUE_ATTR(hlayer, PARAMID_MINIMUMID, hlayer->MINIMUMID, rowid_t, pAttrValue);
        break;
    case GDB_ATTR_LAYER_CREDATE:
        GDBERROR_THROW_ERDONLY(hError);
        break;
    case GDB_ATTR_LAYER_DESCRIPT:
        LAYER_SET_STRING_ATTR(hlayer, PARAMID_DESCRIPT, hlayer->DESCRIPT, pAttrValue, cbSizeValue);
        break;
    case GDB_ATTR_LAYER_RECT:
        hlayer->colflags[PARAMID_ZMIN] = HANDLE_ATTR_OFF;
        hlayer->colflags[PARAMID_ZMAX] = HANDLE_ATTR_OFF;
        hlayer->colflags[PARAMID_MMIN] = HANDLE_ATTR_OFF;
        hlayer->colflags[PARAMID_MMAX] = HANDLE_ATTR_OFF;

        if (pAttrValue) {
            hlayer->colflags[PARAMID_XMIN] = HANDLE_ATTR_ON;
            hlayer->colflags[PARAMID_YMIN] = HANDLE_ATTR_ON;
            hlayer->colflags[PARAMID_XMAX] = HANDLE_ATTR_ON;
            hlayer->colflags[PARAMID_YMAX] = HANDLE_ATTR_ON;
            memcpy(&hlayer->_MBR, pAttrValue, sizeof(hlayer->_MBR));
        } else {
            hlayer->colflags[PARAMID_XMIN] = HANDLE_ATTR_OFF;
            hlayer->colflags[PARAMID_YMIN] = HANDLE_ATTR_OFF;
            hlayer->colflags[PARAMID_XMAX] = HANDLE_ATTR_OFF;
            hlayer->colflags[PARAMID_YMAX] = HANDLE_ATTR_OFF;
        }
        break;
    case GDB_ATTR_LAYER_BOUNDS:
        if (pAttrValue) {
            if (cbSizeValue == 0 || cbSizeValue == sizeof(hlayer->_BOUNDS)) {
                hlayer->colflags[PARAMID_XMIN] = HANDLE_ATTR_ON;
                hlayer->colflags[PARAMID_YMIN] = HANDLE_ATTR_ON;
                hlayer->colflags[PARAMID_XMAX] = HANDLE_ATTR_ON;
                hlayer->colflags[PARAMID_YMAX] = HANDLE_ATTR_ON;
                hlayer->colflags[PARAMID_ZMIN] = HANDLE_ATTR_ON;
                hlayer->colflags[PARAMID_ZMAX] = HANDLE_ATTR_ON;
                hlayer->colflags[PARAMID_MMIN] = HANDLE_ATTR_ON;
                hlayer->colflags[PARAMID_MMAX] = HANDLE_ATTR_ON;
                memcpy(&hlayer->_BOUNDS, pAttrValue, sizeof(hlayer->_BOUNDS));
            } else if (cbSizeValue == sizeof(hlayer->_MBR)) {
                hlayer->colflags[PARAMID_XMIN] = HANDLE_ATTR_ON;
                hlayer->colflags[PARAMID_YMIN] = HANDLE_ATTR_ON;
                hlayer->colflags[PARAMID_XMAX] = HANDLE_ATTR_ON;
                hlayer->colflags[PARAMID_YMAX] = HANDLE_ATTR_ON;

                hlayer->colflags[PARAMID_ZMIN] = HANDLE_ATTR_OFF;
                hlayer->colflags[PARAMID_ZMAX] = HANDLE_ATTR_OFF;
                hlayer->colflags[PARAMID_MMIN] = HANDLE_ATTR_OFF;
                hlayer->colflags[PARAMID_MMAX] = HANDLE_ATTR_OFF;
                memcpy(&hlayer->_MBR, pAttrValue, sizeof(hlayer->_MBR));
            } else {
                return
                    _GDBSetErrorInfo (hError, GDBAPI_EPARAM,
                        "Invalid cbSizeValue for GDB_ATTR_LAYER_BOUNDS",
                        0, 0, __FILE__, __LINE__, __FUNCTION__);
            }
        } else {
            hlayer->colflags[PARAMID_XMIN] = HANDLE_ATTR_OFF;
            hlayer->colflags[PARAMID_YMIN] = HANDLE_ATTR_OFF;
            hlayer->colflags[PARAMID_XMAX] = HANDLE_ATTR_OFF;
            hlayer->colflags[PARAMID_YMAX] = HANDLE_ATTR_OFF;
            hlayer->colflags[PARAMID_ZMIN] = HANDLE_ATTR_OFF;
            hlayer->colflags[PARAMID_ZMAX] = HANDLE_ATTR_OFF;
            hlayer->colflags[PARAMID_MMIN] = HANDLE_ATTR_OFF;
            hlayer->colflags[PARAMID_MMAX] = HANDLE_ATTR_OFF;
        }
        break;

    default:
        GDBERROR_THROW_EATTR(hError);
    }

    return GDBAPI_SUCCESS;
}

GDBAPI_RESULT LayerBindInsert(GDB_CONTEXT ctx, GDB_ERROR herr, HGDBLayer hlayer);

static int _GDBLayerFormatSpatCols(HGDBLayer hlayer, char *colnames, char spatstr[200], int *spatcols)
{
    int ret = 0, cb2 = 0, nSpatCols = 0;
    *spatcols = 0;

    if (hlayer->ROWIDCOL[0]) {
        if (nSpatCols) {
            ret += sprintf (colnames+ret, ",");
            cb2 += sprintf (spatstr+cb2, ",");
        }
        ret += sprintf (colnames+ret, "%s", hlayer->ROWIDCOL);
        cb2 += sprintf (spatstr+cb2, ":ROWIDCOL");
        nSpatCols++;
    }
    if (hlayer->SPATIALCOL[0]) {
        if (nSpatCols) {
            ret += sprintf (colnames+ret, ",");
            cb2 += sprintf (spatstr+cb2, ",");
        }
        ret += sprintf (colnames+ret, "%s", hlayer->SPATIALCOL);
        cb2 += sprintf (spatstr+cb2, ":SPATIALCOL");
        nSpatCols++;
    }
    if (hlayer->XMINCOL[0]) {
        if (nSpatCols) {
            ret += sprintf (colnames+ret, ",");
            cb2 += sprintf (spatstr+cb2, ",");
        }
        ret += sprintf (colnames+ret, "%s", hlayer->XMINCOL);
        cb2 += sprintf (spatstr+cb2, ":XMINCOL");
        nSpatCols++;
    }
    if (hlayer->YMINCOL[0]) {
        if (nSpatCols) {
            ret += sprintf (colnames+ret, ",");
            cb2 += sprintf (spatstr+cb2, ",");
        }
        ret += sprintf (colnames+ret, "%s", hlayer->YMINCOL);
        cb2 += sprintf (spatstr+cb2, ":YMINCOL");
        nSpatCols++;
    }
    if (hlayer->XMAXCOL[0]) {
        if (nSpatCols) {
            ret += sprintf (colnames+ret, ",");
            cb2 += sprintf (spatstr+cb2, ",");
        }
        ret += sprintf (colnames+ret, "%s", hlayer->XMAXCOL);
        cb2 += sprintf (spatstr+cb2, ":XMAXCOL");
        nSpatCols++;
    }
    if (hlayer->YMAXCOL[0]) {
        if (nSpatCols) {
            ret += sprintf (colnames+ret, ",");
            cb2 += sprintf (spatstr+cb2, ",");
        }
        ret += sprintf (colnames+ret, "%s", hlayer->YMAXCOL);
        cb2 += sprintf (spatstr+cb2, ":YMAXCOL");
        nSpatCols++;
    }
    if (hlayer->LENGTHCOL[0]) {
        if (nSpatCols) {
            ret += sprintf (colnames+ret, ",");
            cb2 += sprintf (spatstr+cb2, ",");
        }
        ret += sprintf (colnames+ret, "%s", hlayer->LENGTHCOL);
        cb2 += sprintf (spatstr+cb2, ":LENGTHCOL");
        nSpatCols++;
    }
    if (hlayer->AREACOL[0]) {
        if (nSpatCols) {
            ret += sprintf (colnames+ret, ",");
            cb2 += sprintf (spatstr+cb2, ",");
        }
        ret += sprintf (colnames+ret, "%s", hlayer->AREACOL);
        cb2 += sprintf (spatstr+cb2, ":AREACOL");
        nSpatCols++;
    }
    *spatcols = nSpatCols;
    return ret;
}


static int _GetLayerIndexCreSql (const char *sitable, char sql[300])
{
    return
        sprintf (sql, "DROP TABLE IF EXISTS %s;\n"
            "CREATE TABLE %s (\n"
            "SI INTEGER,\n"
            "XI INTEGER NOT NULL,\n"
            "YI INTEGER NOT NULL,\n"
            "SHAPEID INTEGER NOT NULL,\n"
            "MINXI INTEGER NOT NULL,\n"
            "MINYI INTEGER NOT NULL,\n"
            "MAXXI INTEGER NOT NULL,\n"
            "MAXYI INTEGER NOT NULL);",
            sitable, sitable);
}


static int _GetLayerSITableCresql (int layerid,
    char sitable[], char siindex[], char quadid[], char cresql[])
{
    sprintf (sitable, "GDBLAYER_SI_%d", layerid);
    sprintf (siindex, "%s_INDEX", sitable);
    sprintf (quadid, "%s_ID", sitable);

    return
        sprintf (cresql, "DROP TABLE IF EXISTS %s;\n"
            "CREATE TABLE %s (\n"
            "ID VARCHAR2(%d) NOT NULL,\n"
            "XI INTEGER NOT NULL,\n"
            "YI INTEGER NOT NULL,\n"
            "SHAPEID INTEGER NOT NULL,\n"
            "MINXI INTEGER NOT NULL,\n"
            "MINYI INTEGER NOT NULL,\n"
            "MAXXI INTEGER NOT NULL,\n"
            "MAXYI INTEGER NOT NULL);",
            sitable, sitable, GDB_LEVEL_LIMIT);
}


static int SHPType2WKBType (int nShapeType)
{
    switch (nShapeType) {
    case SHPT_POLYGON:
        return WKB_Polygon;
    case SHPT_POLYGONZ:
        return WKB_PolygonZ;
    case SHPT_POLYGONM:
        return WKB_PolygonM;
 
    case SHPT_ARC:
        return WKB_LineString;
    case SHPT_ARCZ:
        return WKB_LineStringZ;
    case SHPT_ARCM:
        return WKB_LineStringM;

    case SHPT_POINT:
        return WKB_Point;
    case SHPT_POINTZ:
        return WKB_PointZ;
    case SHPT_POINTM:
        return WKB_PointM;

    case SHPT_MULTIPOINT:
        return WKB_MultiPoint;
    case SHPT_MULTIPOINTZ:
        return WKB_MultiPointZ;
    case SHPT_MULTIPOINTM:
        return WKB_MultiPointM;

    case SHPT_NULL:
        return WKB_Geometry;
    }

    return (-1);
}


#define ROWSHAPE_COLID_ROWID   0
#define ROWSHAPE_COLID_XMIN    1
#define ROWSHAPE_COLID_YMIN    2
#define ROWSHAPE_COLID_XMAX    3
#define ROWSHAPE_COLID_YMAX    4
#define ROWSHAPE_COLID_SPATIAL 5

#define ROWSHAPE_COLID_AREA    6
#define ROWSHAPE_COLID_LENGTH  7

typedef struct _GDBLayerRowShape
{
    sqlite3_stmt *stmt;

    rowid_t  rowid;
    int      wkbsize;
    void     *wkbdata;

    union {
        struct {
            double Xmin;
            double Ymin;
            double Xmax;
            double Ymax;
        };
        GDBExtRect _MBR;
    };
} GDBLayerRowShape;

static int _GDBLayerRowShapeRead (GDBLayerRowShape *shape)
{
    shape->rowid = sqlite3_column_rowid (shape->stmt, ROWSHAPE_COLID_ROWID);
    shape->Xmin = sqlite3_column_double (shape->stmt, ROWSHAPE_COLID_XMIN);
    shape->Ymin = sqlite3_column_double (shape->stmt, ROWSHAPE_COLID_YMIN);
    shape->Xmax = sqlite3_column_double (shape->stmt, ROWSHAPE_COLID_XMAX);
    shape->Ymax = sqlite3_column_double (shape->stmt, ROWSHAPE_COLID_YMAX);
    shape->wkbsize = sqlite3_column_bytes (shape->stmt, ROWSHAPE_COLID_SPATIAL);
    shape->wkbdata = (void*) sqlite3_column_blob (shape->stmt, ROWSHAPE_COLID_SPATIAL);

    return shape->wkbsize;
}

static int _GDBLayerFindSpatCol (HGDBLayer layer, const char *colName)
{
    if (! strcmp (layer->ROWIDCOL, colName)) {
        return ROWSHAPE_COLID_ROWID;
    } else if (! strcmp (layer->SPATIALCOL, colName)) {
        return ROWSHAPE_COLID_SPATIAL;
    } else if (! strcmp (layer->XMINCOL, colName)) {
        return ROWSHAPE_COLID_XMIN;
    } else if (! strcmp (layer->YMINCOL, colName)) {
        return ROWSHAPE_COLID_YMIN;
    } else if (! strcmp (layer->XMAXCOL, colName)) {
        return ROWSHAPE_COLID_XMAX;
    } else if (! strcmp (layer->YMAXCOL, colName)) {
        return ROWSHAPE_COLID_YMAX;
    } else if (! strcmp (layer->AREACOL, colName)) {
        return ROWSHAPE_COLID_AREA;
    } else if (! strcmp (layer->LENGTHCOL, colName)) {
        return ROWSHAPE_COLID_LENGTH;
    } else {
        return GDBAPI_EINDEX;
    }
}

#ifdef    __cplusplus
}
#endif

#endif /* _GDB_LAYER_H_ */
