/**
 * gdbtype.h
 *
 *
 */

#ifndef _GDB_TYPE_H_
#define _GDB_TYPE_H_

#ifdef _MSC_VER
# pragma warning (disable : 4996)
#endif

#ifdef    __cplusplus
extern "C" {
#endif

#include <assert.h>
#include <memory.h>

#include "gdbapi.h"

#include "polygon.h"

#include "wkbtype.h"

/* sqlite3 api */
#include "./sqlite3/src/sqlite3.h"

typedef GDB_CONTEXT HGDBCTX;
typedef GDB_ERROR   HGDBERR;
typedef GDB_COLDEF  HGDBCOLDEF;
typedef GDB_COLVAL  HGDBCOLVAL;
typedef GDB_STMT    HGDBSTMT;
typedef GDB_LAYER   HGDBLAYER;
typedef GDB_RASTER   HGDBIMAGE;
typedef GDB_SHAPE   HGDBSHP;
typedef GDB_IMAGE HGDBTILE;

#define HANDLE_ATTR_ON     ((char)1)
#define HANDLE_ATTR_OFF    ((char)0)

typedef struct GDBHandleObject
{
    int    type;
    int    size;
    void  *__exdata;
} GDBHandleOb, *HGDBHandleOb;

#define VALUE_PTR(pv, type)  ((type)(*((type*) (pv))))

#define ALLOC_HANDLE(ObType, pvExdata, sizeExData, htype, handle)  do { \
        char *p = (char*) calloc(1, sizeof(ObType)+(sizeExData)); \
        if (! p) { \
            return GDBAPI_EOUTMEM; \
        } \
        handle = (HGDBHandleOb) p; \
        handle->type = htype; \
        handle->size = sizeExData; \
        if (sizeExData > 0) {\
            handle->__exdata = (void*) (p + sizeof(ObType)); \
            if (pvExdata) { \
                memcpy(handle->__exdata, (pvExdata), (size_t)(sizeExData)); \
            } \
        } \
    } while (0)

#define CHECK_HTYPE(hdl, htype)  \
    ((hdl) && ((HGDBHandleOb)(hdl))->type == (htype))

#define GET_HTYPE(hdl)  \
    (((HGDBHandleOb)(hdl))->type)

#define CAST_HANDLE_TYPE(hdl, herr, htype, ObHandleType, ObVar) do { \
        ObVar = 0; \
        if (!hdl) { \
            return _GDBSetErrorInfo (herr, GDBAPI_EHANDLE, \
                "Null handle", \
                0, 0, __FILE__, __LINE__, "CAST_HANDLE_TYPE"); \
        } \
        if (((HGDBHandleOb) (hdl))->type != htype) { \
            return _GDBSetErrorInfo (herr, GDBAPI_EHTYPE, \
                "Handle types mismatched", \
                0, 0, __FILE__, __LINE__, "CAST_HANDLE_TYPE"); \
        } \
        ObVar = (ObHandleType) (hdl); \
    } while (0)

#define FREE_S(ptr) \
    do { \
        if (ptr) { \
            void *p = (ptr); \
            (ptr) = 0; \
            free(p); \
        } \
    } while (0)


#ifndef snprintf
# define snprintf _snprintf
#endif

#define sqlite3_column_rowid(stmt, icol) \
    (sizeof(rowid_t) == sizeof(sqlite_int64) ?  \
        (rowid_t) sqlite3_column_int64(stmt, icol) : \
        (rowid_t) sqlite3_column_int(stmt, icol))

static char * SafeCopyString (char *dest, const char *source, size_t cbSizeDest)
{
    if (!dest) {
        return 0;
    }
    if (source) {
        strncpy(dest, source, cbSizeDest - 1);
        dest[cbSizeDest - 1] = 0;
    } else {
        *dest = 0;
    }
    return dest;
}

static GDBAPI_BOOL Value2Bool (void *pValue)
{
    if (! pValue) {
        return GDBAPI_FALSE;
    } else {
        return (*((int *) pValue)) == GDBAPI_TRUE? GDBAPI_TRUE : GDBAPI_FALSE;
    }
}

#ifdef    __cplusplus
}
#endif

#endif /* _GDB_TYPE_H_ */
