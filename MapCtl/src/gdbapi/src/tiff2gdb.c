/**
 * tiff2gdb.c
 * @brief
 *    import shapefile into GDB.
 *
 * @author
 *    cheungmine
 * @since 
 *    2013-4-25
 * @date
 *    2013-5-26
 * @note
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "gdbshell.h"

#ifdef _MSC_VER
 #pragma warning (disable : 4996)
#endif

#define WKB_SIZEBLOB  8192

/* tiff file api */
#include "../libs/libtiff/include/tiffio.h"


int tiff2gdb(char *tifffile, char *tfwfile, char *gdb, char *imagetable, gt_table_cremod mode)
{
    TIFF *tif;
    int   fdindex, len, w, l;

    uint32 x, y, readSize, numTiles = 0, numBmps = 0;

    const char *pathtile = "c:/mapdata/img/earth_1km.0";
    const char *tileindex = "c:/mapdata/img/earth_1km.0/index";
    const char *pathtiff = "c:/mapdata/img/earth_1km.tif";

    char   bmpname[240];
    char   strline[100];

    double tspan;

    // Create index file
    tif = TIFFOpen(pathtiff, "r");

    if (!tif) {
        fprintf(stderr, "Could not open tiff file: %s\n", pathtiff);
        exit(-1);
    }

    TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &w);
    TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &l);

    TIFFClose(tif);

    return 0;
}
