/**
 * gdbshape.h
 *
 *
 */

#ifndef _GDB_SHAPE_H_
#define _GDB_SHAPE_H_

#ifdef _MSC_VER
 #pragma warning (disable : 4996)
#endif

#ifdef    __cplusplus
extern "C" {
#endif

#include "gdberror.h"
#include "byteorder.h"
#include "polygon.h"

#include "./matalg/matalg.h"

/* shape file api */
#include "./shapefile/src/shapefile.h"

#define SHAPE_TYPENAME_LEN  30

#define SHPWKB_BLKSIZE(cbNewSize)  (((abs(cbNewSize)+8191)/8192)*8192)

#define SHPATTR_ROWID     (0)
#define SHPATTR_TYPE      (SHPATTR_ROWID + 1)
#define SHPATTR_TYPENAME  (SHPATTR_ROWID + 2)
#define SHPATTR_XMIN      (SHPATTR_ROWID + 3)
#define SHPATTR_YMIN      (SHPATTR_ROWID + 4)
#define SHPATTR_XMAX      (SHPATTR_ROWID + 5)
#define SHPATTR_YMAX      (SHPATTR_ROWID + 6)
#define SHPATTR_ZMIN      (SHPATTR_ROWID + 7)
#define SHPATTR_ZMAX      (SHPATTR_ROWID + 8)
#define SHPATTR_MMIN      (SHPATTR_ROWID + 9)
#define SHPATTR_MMAX      (SHPATTR_ROWID + 10)
#define SHPATTR_WKB       (SHPATTR_ROWID + 11)
#define SHPATTR_WKT       (SHPATTR_ROWID + 12)
#define SHPATTR_LENGTH    (SHPATTR_ROWID + 13)
#define SHPATTR_AREA      (SHPATTR_ROWID + 14)
#define SHPATTR_ISNULL    (SHPATTR_ROWID + 15)
#define SHPATTR_COUNT     (SHPATTR_ROWID + 16)

typedef struct _GDBShapeInfo
{
    GDBHandleOb __handle[1];

    char colflags[SHPATTR_COUNT];

    rowid_t   rowid;
    GDBExtBound bound;

    double    length;
    double    area;
    int       isnull;

    int       wkbBlkSize; /* total blocks size: wkbBlkSize >= wkbSize,
                           if is 0, wkb is a reference to data address. */

    int       wkbSize;    /* WKBSIZE */
    void     *wkb;        /* WKB buffer with wkbBlkSize */
} GDBShapeInfo, *HGDBShape;

#define CAST_HANDLE_SHAPE(hdl, herr, hshape) \
    CAST_HANDLE_TYPE(hdl, herr, GDB_HTYPE_SHAPE, HGDBShape, hshape)

static void _GDBShapeSetWKBAddr (HGDBShape hShp, void *wkbAddr, int wkbSize)
{
    if (hShp->wkbBlkSize) {
        hShp->wkbBlkSize = 0;
        FREE_S(hShp->wkb);
    }
    hShp->wkb = wkbAddr;
    hShp->wkbSize = wkbSize;
}

static void _GDBShapeClearAll (HGDBShape hShp)
{
    _GDBShapeSetWKBAddr (hShp, 0, 0);
    memset(hShp, 0, sizeof(*hShp));
}

#define CASE_WKB_TYPE(type, str) \
    case type: \
        strcpy(typeName, str); \
        _strupr(typeName); \
        break

static int _GDBShapeIsValid (HGDBShape hshape)
{
    if (hshape->wkb && hshape->wkbSize >= sizeof(char)+sizeof(uint32_t)) {
        return 1;
    } else {
        return 0;
    }
}

static int _GDBShapeGetType (HGDBShape hshape, char typeName[SHAPE_TYPENAME_LEN+1])
{
    int wkbtype = 0;

    if (_GDBShapeIsValid(hshape)) {
        char *wkb = (char*) hshape->wkb;
        wkbtype = ((wkb[0] == WKB_BYTEORDER_XDR)?
            int32_betoh(*((int32_t*) (wkb+1))) :
            int32_letoh(*((int32_t*) (wkb+1))));
    }

    if (typeName) {
        *typeName = 0;
  
        switch (wkbtype) {
        CASE_WKB_TYPE(WKB_Geometry, "WKB_Geometry");
        CASE_WKB_TYPE(WKB_GeometryZ, "WKB_GeometryZ");
        CASE_WKB_TYPE(WKB_GeometryM, "WKB_GeometryM");
        CASE_WKB_TYPE(WKB_GeometryZM, "WKB_GeometryZM");

        CASE_WKB_TYPE(WKB_Point, "WKB_Point");
        CASE_WKB_TYPE(WKB_PointZ, "WKB_PointZ");
        CASE_WKB_TYPE(WKB_PointM, "WKB_PointM");
        CASE_WKB_TYPE(WKB_PointZM, "WKB_PointZM");

        CASE_WKB_TYPE(WKB_LineString, "WKB_LineString");
        CASE_WKB_TYPE(WKB_LineStringZ, "WKB_LineStringZ");
        CASE_WKB_TYPE(WKB_LineStringM, "WKB_LineStringM");
        CASE_WKB_TYPE(WKB_LineStringZM, "WKB_LineStringZM");

        CASE_WKB_TYPE(WKB_Polygon, "WKB_Polygon");
        CASE_WKB_TYPE(WKB_PolygonZ, "WKB_PolygonZ");
        CASE_WKB_TYPE(WKB_PolygonM, "WKB_PolygonM");
        CASE_WKB_TYPE(WKB_PolygonZM, "WKB_PolygonZM");

        CASE_WKB_TYPE(WKB_MultiPoint, "WKB_MultiPoint");
        CASE_WKB_TYPE(WKB_MultiPointZ, "WKB_MultiPointZ");
        CASE_WKB_TYPE(WKB_MultiPointM, "WKB_MultiPointM");
        CASE_WKB_TYPE(WKB_MultiPointZM, "WKB_MultiPointZM");

        CASE_WKB_TYPE(WKB_MultiLineString, "WKB_MultiLineString");
        CASE_WKB_TYPE(WKB_MultiLineStringZ, "WKB_MultiLineStringZ");
        CASE_WKB_TYPE(WKB_MultiLineStringM, "WKB_MultiLineStringM");
        CASE_WKB_TYPE(WKB_MultiLineStringZM, "WKB_MultiLineStringZM");

        CASE_WKB_TYPE(WKB_MultiPolygon, "WKB_MultiPolygon");
        CASE_WKB_TYPE(WKB_MultiPolygonZ, "WKB_MultiPolygonZ");
        CASE_WKB_TYPE(WKB_MultiPolygonM, "WKB_MultiPolygonM");
        CASE_WKB_TYPE(WKB_MultiPolygonZM, "WKB_MultiPolygonZM");

        CASE_WKB_TYPE(WKB_GeometryCollection, "WKB_GeometryCollection");
        CASE_WKB_TYPE(WKB_GeometryCollectionZ, "WKB_GeometryCollectionZ");
        CASE_WKB_TYPE(WKB_GeometryCollectionM, "WKB_GeometryCollectionM");
        CASE_WKB_TYPE(WKB_GeometryCollectionZM, "WKB_GeometryCollectionZM");

        CASE_WKB_TYPE(WKB_CircularString, "WKB_CircularString");
        CASE_WKB_TYPE(WKB_CircularStringZ, "WKB_CircularStringZ");
        CASE_WKB_TYPE(WKB_CircularStringM, "WKB_CircularStringM");
        CASE_WKB_TYPE(WKB_CircularStringZM, "WKB_CircularStringZM");

        CASE_WKB_TYPE(WKB_CompoundCurve, "WKB_CompoundCurve");
        CASE_WKB_TYPE(WKB_CompoundCurveZ, "WKB_CompoundCurveZ");
        CASE_WKB_TYPE(WKB_CompoundCurveM, "WKB_CompoundCurveM");
        CASE_WKB_TYPE(WKB_CompoundCurveZM, "WKB_CompoundCurveZM");

        CASE_WKB_TYPE(WKB_CurvePolygon, "WKB_CurvePolygon");
        CASE_WKB_TYPE(WKB_CurvePolygonZ, "WKB_CurvePolygonZ");
        CASE_WKB_TYPE(WKB_CurvePolygonM, "WKB_CurvePolygonM");
        CASE_WKB_TYPE(WKB_CurvePolygonZM, "WKB_CurvePolygonZM");

        CASE_WKB_TYPE(WKB_MultiCurve, "WKB_MultiCurve");
        CASE_WKB_TYPE(WKB_MultiCurveZ, "WKB_MultiCurveZ");
        CASE_WKB_TYPE(WKB_MultiCurveM, "WKB_MultiCurveM");
        CASE_WKB_TYPE(WKB_MultiCurveZM, "WKB_MultiCurveZM");

        CASE_WKB_TYPE(WKB_MultiSurface, "WKB_MultiSurface");
        CASE_WKB_TYPE(WKB_MultiSurfaceZ, "WKB_MultiSurfaceZ");
        CASE_WKB_TYPE(WKB_MultiSurfaceM, "WKB_MultiSurfaceM");
        CASE_WKB_TYPE(WKB_MultiSurfaceZM, "WKB_MultiSurfaceZM");

        CASE_WKB_TYPE(WKB_Curve, "WKB_Curve");
        CASE_WKB_TYPE(WKB_CurveZ, "WKB_CurveZ");
        CASE_WKB_TYPE(WKB_CurveM, "WKB_CurveM");
        CASE_WKB_TYPE(WKB_CurveZM, "WKB_CurveZM");

        CASE_WKB_TYPE(WKB_Surface, "WKB_Surface");
        CASE_WKB_TYPE(WKB_SurfaceZ, "WKB_SurfaceZ");
        CASE_WKB_TYPE(WKB_SurfaceM, "WKB_SurfaceM");
        CASE_WKB_TYPE(WKB_SurfaceZM, "WKB_SurfaceZM");

        CASE_WKB_TYPE(WKB_PolyhedralSurface, "WKB_PolyhedralSurface");
        CASE_WKB_TYPE(WKB_PolyhedralSurfaceZ, "WKB_PolyhedralSurfaceZ");
        CASE_WKB_TYPE(WKB_PolyhedralSurfaceM, "WKB_PolyhedralSurfaceM");
        CASE_WKB_TYPE(WKB_PolyhedralSurfaceZM, "WKB_PolyhedralSurfaceZM");

        CASE_WKB_TYPE(WKB_TIN, "WKB_TIN");
        CASE_WKB_TYPE(WKB_TINZ, "WKB_TINZ");
        CASE_WKB_TYPE(WKB_TINM, "WKB_TINM");
        CASE_WKB_TYPE(WKB_TINZM, "WKB_TINZM");

        CASE_WKB_TYPE(WKB_Triangle, "WKB_Triangle");
        CASE_WKB_TYPE(WKB_TriangleZ, "WKB_TriangleZ");
        CASE_WKB_TYPE(WKB_TriangleM, "WKB_TriangleM");
        CASE_WKB_TYPE(WKB_TriangleZM, "WKB_TriangleZM");
        }
    }

    return wkbtype;
}


static GDBAPI_RESULT _GDBShapeInfoSetAttr(HGDBShape hshape,
    void *pAttrValue, int cbSizeValue, GDB_ATTR_NAME attrName, GDB_ERROR hError)
{
    switch (attrName) {
    case GDB_ATTR_SHAPE_ROWID:
        if (pAttrValue) {
            hshape->colflags[SHPATTR_ROWID] = HANDLE_ATTR_ON;
            if (cbSizeValue == 8) {
                hshape->rowid = VALUE_PTR(pAttrValue, rowid_t);
            } else {
                hshape->rowid = (rowid_t) VALUE_PTR(pAttrValue, int);
            }
        } else {
            hshape->colflags[SHPATTR_ROWID] = HANDLE_ATTR_OFF;
        }
        break;

    case GDB_ATTR_SHAPE_TYPE:
        GDBERROR_THROW_ERDONLY(hError);
        break;

    case GDB_ATTR_SHAPE_TYPENAME:
        GDBERROR_THROW_ERDONLY(hError);
        break;

    case GDB_ATTR_SHAPE_BOUNDADDR:
        GDBERROR_THROW_ERDONLY(hError);
        break;

    case GDB_ATTR_SHAPE_BOUND:
        if (pAttrValue) {
            if (cbSizeValue == 0 || cbSizeValue == sizeof(GDBExtBound)) {
                hshape->colflags[SHPATTR_XMIN] = HANDLE_ATTR_ON;
                hshape->colflags[SHPATTR_YMIN] = HANDLE_ATTR_ON;
                hshape->colflags[SHPATTR_XMAX] = HANDLE_ATTR_ON;
                hshape->colflags[SHPATTR_YMAX] = HANDLE_ATTR_ON;
                hshape->colflags[SHPATTR_ZMIN] = HANDLE_ATTR_ON;
                hshape->colflags[SHPATTR_ZMAX] = HANDLE_ATTR_ON;
                hshape->colflags[SHPATTR_MMIN] = HANDLE_ATTR_ON;
                hshape->colflags[SHPATTR_MMAX] = HANDLE_ATTR_ON;
                memcpy(&hshape->bound, pAttrValue, sizeof(GDBExtBound));
            } else if (cbSizeValue == sizeof(GDBExtRect)) {
                hshape->colflags[SHPATTR_XMIN] = HANDLE_ATTR_ON;
                hshape->colflags[SHPATTR_YMIN] = HANDLE_ATTR_ON;
                hshape->colflags[SHPATTR_XMAX] = HANDLE_ATTR_ON;
                hshape->colflags[SHPATTR_YMAX] = HANDLE_ATTR_ON;

                hshape->colflags[SHPATTR_ZMIN] = HANDLE_ATTR_OFF;
                hshape->colflags[SHPATTR_ZMAX] = HANDLE_ATTR_OFF;
                hshape->colflags[SHPATTR_MMIN] = HANDLE_ATTR_OFF;
                hshape->colflags[SHPATTR_MMAX] = HANDLE_ATTR_OFF;

                memcpy(&hshape->bound, pAttrValue, sizeof(GDBExtRect));
            } else {
                GDBERROR_THROW_EATTRVAL(hError);
            }
        } else {
            hshape->colflags[SHPATTR_XMIN] = HANDLE_ATTR_OFF;
            hshape->colflags[SHPATTR_YMIN] = HANDLE_ATTR_OFF;
            hshape->colflags[SHPATTR_XMAX] = HANDLE_ATTR_OFF;
            hshape->colflags[SHPATTR_YMAX] = HANDLE_ATTR_OFF;
            hshape->colflags[SHPATTR_ZMIN] = HANDLE_ATTR_OFF;
            hshape->colflags[SHPATTR_ZMAX] = HANDLE_ATTR_OFF;
            hshape->colflags[SHPATTR_MMIN] = HANDLE_ATTR_OFF;
            hshape->colflags[SHPATTR_MMAX] = HANDLE_ATTR_OFF;
        }
        break;

    case GDB_ATTR_SHAPE_RECT:
        hshape->colflags[SHPATTR_ZMIN] = HANDLE_ATTR_OFF;
        hshape->colflags[SHPATTR_ZMAX] = HANDLE_ATTR_OFF;
        hshape->colflags[SHPATTR_MMIN] = HANDLE_ATTR_OFF;
        hshape->colflags[SHPATTR_MMAX] = HANDLE_ATTR_OFF;

        if (pAttrValue) {
            hshape->colflags[SHPATTR_XMIN] = HANDLE_ATTR_ON;
            hshape->colflags[SHPATTR_YMIN] = HANDLE_ATTR_ON;
            hshape->colflags[SHPATTR_XMAX] = HANDLE_ATTR_ON;
            hshape->colflags[SHPATTR_YMAX] = HANDLE_ATTR_ON;
            memcpy(&hshape->bound, pAttrValue, sizeof(GDBExtRect));
        } else {
            hshape->colflags[SHPATTR_XMIN] = HANDLE_ATTR_OFF;
            hshape->colflags[SHPATTR_YMIN] = HANDLE_ATTR_OFF;
            hshape->colflags[SHPATTR_XMAX] = HANDLE_ATTR_OFF;
            hshape->colflags[SHPATTR_YMAX] = HANDLE_ATTR_OFF;
        }
        break;

    case GDB_ATTR_SHAPE_XMIN:
        if (pAttrValue) {
            hshape->colflags[SHPATTR_XMIN] = HANDLE_ATTR_ON;
            hshape->bound.Xmin = VALUE_PTR(pAttrValue, double);
        } else {
            hshape->colflags[SHPATTR_XMIN] = HANDLE_ATTR_OFF;
        }
        break;

    case GDB_ATTR_SHAPE_YMIN:
        if (pAttrValue) {
            hshape->colflags[SHPATTR_YMIN] = HANDLE_ATTR_ON;
            hshape->bound.Ymin = VALUE_PTR(pAttrValue, double);
        } else {
            hshape->colflags[SHPATTR_YMIN] = HANDLE_ATTR_OFF;
        }
        break;

    case GDB_ATTR_SHAPE_XMAX:
        if (pAttrValue) {
            hshape->colflags[SHPATTR_XMAX] = HANDLE_ATTR_ON;
            hshape->bound.Xmax = VALUE_PTR(pAttrValue, double);
        } else {
            hshape->colflags[SHPATTR_XMAX] = HANDLE_ATTR_OFF;
        }
        break;

    case GDB_ATTR_SHAPE_YMAX:
        if (pAttrValue) {
            hshape->colflags[SHPATTR_YMAX] = HANDLE_ATTR_ON;
            hshape->bound.Ymax = VALUE_PTR(pAttrValue, double);
        } else {
            hshape->colflags[SHPATTR_YMAX] = HANDLE_ATTR_OFF;
        }
        break;

    case GDB_ATTR_SHAPE_ZMIN:
        if (pAttrValue) {
            hshape->colflags[SHPATTR_ZMIN] = HANDLE_ATTR_ON;
            hshape->bound.Zmin = VALUE_PTR(pAttrValue, double);
        } else {
            hshape->colflags[SHPATTR_ZMIN] = HANDLE_ATTR_OFF;
        }
        break;

    case GDB_ATTR_SHAPE_ZMAX:
        if (pAttrValue) {
            hshape->colflags[SHPATTR_ZMAX] = HANDLE_ATTR_ON;
            hshape->bound.Zmax = VALUE_PTR(pAttrValue, double);
        } else {
            hshape->colflags[SHPATTR_ZMAX] = HANDLE_ATTR_OFF;
        }
        break;

    case GDB_ATTR_SHAPE_MMIN:
        if (pAttrValue) {
            hshape->colflags[SHPATTR_MMIN] = HANDLE_ATTR_ON;
            hshape->bound.Mmin = VALUE_PTR(pAttrValue, double);
        } else {
            hshape->colflags[SHPATTR_MMIN] = HANDLE_ATTR_OFF;
        }
        break;

    case GDB_ATTR_SHAPE_MMAX:
        if (pAttrValue) {
            hshape->colflags[SHPATTR_MMAX] = HANDLE_ATTR_ON;
            hshape->bound.Mmax = VALUE_PTR(pAttrValue, double);
        } else {
            hshape->colflags[SHPATTR_MMAX] = HANDLE_ATTR_OFF;
        }
        break;
    case GDB_ATTR_SHAPE_WKBADDR:
        if (pAttrValue) {
            hshape->colflags[SHPATTR_WKB] = HANDLE_ATTR_ON;
            _GDBShapeSetWKBAddr (hshape, pAttrValue, cbSizeValue);
        } else {
            hshape->colflags[SHPATTR_WKB] = HANDLE_ATTR_OFF;
            _GDBShapeSetWKBAddr (hshape, 0, 0);
        }
        break;
    case GDB_ATTR_SHAPE_WKBCOPY:
        hshape->colflags[SHPATTR_WKB] = HANDLE_ATTR_ON;
        if (cbSizeValue > 0) {
            /* realloc memory by increment */
            int newBlkSize = SHPWKB_BLKSIZE(cbSizeValue);
            if (newBlkSize > hshape->wkbBlkSize) {
                hshape->wkb = realloc(hshape->wkb, newBlkSize);
                hshape->wkbBlkSize = newBlkSize;
            }
            if (! hshape->wkb) {
                hshape->wkbBlkSize = 0;
                hshape->wkbSize = 0;
                return
                    _GDBSetErrorInfo (hError, GDBAPI_EOUTMEM,
                        "recalloc for GDB_ATTR_SHAPE_WKB failed",
                        0, 0, __FILE__, __LINE__, __FUNCTION__);
            }
            hshape->wkbSize = cbSizeValue;
            if (pAttrValue) {
                memcpy(hshape->wkb, pAttrValue, hshape->wkbSize);
            }
        } else if (cbSizeValue < 0) {
            /* always realloc memory */
            int newBlkSize = SHPWKB_BLKSIZE(cbSizeValue);
            hshape->wkb = realloc(hshape->wkb, newBlkSize);
            if (! hshape->wkb) {
                hshape->wkbBlkSize = 0;
                hshape->wkbSize = 0;
                return
                    _GDBSetErrorInfo (hError, GDBAPI_EOUTMEM,
                        "recalloc for GDB_ATTR_SHAPE_WKB failed",
                        0, 0, __FILE__, __LINE__, __FUNCTION__);
            }
            hshape->wkbBlkSize = newBlkSize;
            hshape->wkbSize = abs(cbSizeValue);
            if (pAttrValue) {
                memcpy(hshape->wkb, pAttrValue, hshape->wkbSize);
            }
        } else {
            assert (cbSizeValue == 0);
            hshape->colflags[SHPATTR_WKB] = HANDLE_ATTR_OFF;

            /* off SHPATTR_WKB flag and set cbSizeValue with 0 */
            hshape->wkbSize = 0;

            if (!pAttrValue) {
                /* free wkb buffer and turn off SHPATTR_WKB flag */
                hshape->wkbBlkSize = 0;
                FREE_S(hshape->wkb);
            }
        }
        break;

    case GDB_ATTR_SHAPE_LENGTH:
        if (pAttrValue) {
            hshape->colflags[SHPATTR_LENGTH] = HANDLE_ATTR_ON;
            hshape->length = VALUE_PTR(pAttrValue, double);
        } else {
            hshape->colflags[SHPATTR_LENGTH] = HANDLE_ATTR_OFF;
        }
        break;

    case GDB_ATTR_SHAPE_AREA:
        if (pAttrValue) {
            hshape->colflags[SHPATTR_AREA] = HANDLE_ATTR_ON;
            hshape->area = VALUE_PTR(pAttrValue, double);
        } else {
            hshape->colflags[SHPATTR_AREA] = HANDLE_ATTR_OFF;
        }
        break;

    case GDB_ATTR_SHAPE_ISNULL:
        if (pAttrValue) {
            hshape->colflags[SHPATTR_ISNULL] = HANDLE_ATTR_ON;
            hshape->isnull = VALUE_PTR(pAttrValue, int);
        } else {
            hshape->colflags[SHPATTR_ISNULL] = HANDLE_ATTR_OFF;
        }
        break;

    default:
        GDBERROR_THROW_EATTR(hError);
    }

    return GDBAPI_SUCCESS;
}

static GDBAPI_RESULT _GDBShapeInfoGetAttr (HGDBShape hshape, void *pAttrValue,
    int *cbSizeValue, GDB_ATTR_NAME attrName, GDB_ERROR hError)
{
    if (!pAttrValue && !cbSizeValue) {
        GDBERROR_THROW_EATTRVAL(hError);
    }

    switch (attrName) {
    case GDB_ATTR_SHAPE_ROWID:
        ATTR_GET_TYPED_VALUE(rowid_t, hshape->rowid,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_SHAPE_WKBADDR:
        if (pAttrValue) {
            *((void**) pAttrValue) = hshape->wkb;
        }
        if (cbSizeValue) {
            *cbSizeValue = hshape->wkbSize;
        }
        break;
    case GDB_ATTR_SHAPE_WKBCOPY:
        if (pAttrValue) {
            /* get wkb copy */
            if (cbSizeValue && *cbSizeValue >= hshape->wkbSize) {
                memcpy(pAttrValue, hshape->wkb, hshape->wkbSize);
                *cbSizeValue = hshape->wkbSize;
            } else {
                return
                    _GDBSetErrorInfo (hError, GDBAPI_EPARAM,
                        "Invalid cbSizeValue to recieve GDB_ATTR_SHAPE_WKBCOPY",
                        0, 0, __FILE__, __LINE__, __FUNCTION__);
            }
        } else {
            *cbSizeValue = hshape->wkbSize;
        }
        break;
    case GDB_ATTR_SHAPE_TYPE:
        if (pAttrValue) {
            ATTR_CHECK_SET_SIZE(cbSizeValue, sizeof(int), hError);
            *((int*) pAttrValue) = (int) _GDBShapeGetType (hshape, 0);
        } else {
            *cbSizeValue = sizeof(int);
        }
        break;
    case GDB_ATTR_SHAPE_TYPENAME:
        if (pAttrValue) {
            if (!cbSizeValue) {
                return
                    _GDBSetErrorInfo (hError, GDBAPI_EPARAM,
                        "Invalid cbSizeValue to recieve GDB_ATTR_SHAPE_TYPENAME",
                        0, 0, __FILE__, __LINE__, __FUNCTION__);
            } else {
                int cb;
                char typeName[SHAPE_TYPENAME_LEN+1];
                _GDBShapeGetType (hshape, typeName);
                cb = strlen(typeName) + 1;
                ATTR_CHECK_SET_SIZE(cbSizeValue, cb, hError);
                memcpy(pAttrValue, typeName, cb);
            }
        } else {
            char typeName[SHAPE_TYPENAME_LEN+1];
            _GDBShapeGetType (hshape, typeName);
            *cbSizeValue = strlen(typeName) + 1;
        }
        break;

        break;
    case GDB_ATTR_SHAPE_BOUND:
        ATTR_GET_TYPED_VALUE(GDBExtBound, hshape->bound,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_SHAPE_BOUNDADDR:
        if (pAttrValue) {
            *((void**) pAttrValue) = (void*) &hshape->bound;
        }
        break;
    case GDB_ATTR_SHAPE_RECT:
        ATTR_GET_TYPED_VALUE(GDBExtRect, hshape->bound._MBR,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_SHAPE_XMIN:
        ATTR_GET_TYPED_VALUE(double, hshape->bound.Xmin,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_SHAPE_YMIN:
        ATTR_GET_TYPED_VALUE(double, hshape->bound.Ymin,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_SHAPE_XMAX:
        ATTR_GET_TYPED_VALUE(double, hshape->bound.Xmax,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_SHAPE_YMAX:
        ATTR_GET_TYPED_VALUE(double, hshape->bound.Ymax,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_SHAPE_ZMIN:
        ATTR_GET_TYPED_VALUE(double, hshape->bound.Zmin,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_SHAPE_ZMAX:
        ATTR_GET_TYPED_VALUE(double, hshape->bound.Zmax,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_SHAPE_MMIN:
        ATTR_GET_TYPED_VALUE(double, hshape->bound.Mmin,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_SHAPE_MMAX:
        ATTR_GET_TYPED_VALUE(double, hshape->bound.Mmax,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_SHAPE_LENGTH:
        ATTR_GET_TYPED_VALUE(double, hshape->length,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_SHAPE_AREA:
        ATTR_GET_TYPED_VALUE(double, hshape->area,
            pAttrValue, cbSizeValue, hError);
        break;
    case GDB_ATTR_SHAPE_ISNULL:
        ATTR_GET_TYPED_VALUE(int, hshape->isnull,
            pAttrValue, cbSizeValue, hError);
        break;

    default:
        GDBERROR_THROW_EATTR(hError);
        break;
    }

    return GDBAPI_SUCCESS;
}

/*
 * Polygon Build
 */
static void WriteValueToBytes (void *val, byte_t *inb, int sizeVal)
{
    memcpy(inb, val, sizeVal);
}

#define READ_BYTE_BE(wkb, c, out)  out = wkb[c]; c++

#define READ_UINT32_BE(wkb, c, out)  \
    out = int32_betoh(*(int32_t*)(wkb+c)); \
    c += sizeof(int32_t)

#define READ_FLOAT64_BE(wkb, c, out) \
    out = float64_betoh(*(double*)(wkb+c)); \
    c += sizeof(double)

#define WRITE_UINT32_BE(wkb, c, in, tmp) \
    tmp = int32_htobe(in); \
    WriteValueToBytes(&tmp, (wkb+c), sizeof(int32_t)); \
    c += sizeof(int32_t)

#define WRITE_FLOAT64_BE(wkb, c, in, tmp) \
    tmp = float64_htobe(in); \
    WriteValueToBytes(&tmp, (wkb+c), sizeof(double)); \
    c += sizeof(double)

#define POLYGON_WKB_MINSIZE  61
#define POLYGON_WKB_MASK     1000

static GDBAPI_BOOL BuildPolygonFromWKB (HPOLYGON p,
    const unsigned char *wkb, int size,
    int digits)
{
    byte_t   byteOrder;

    uint32_t wkbType, parts, ring, poly, rings, points, i, c = 0;

    VertexType  *vl;

    if (! wkb || size < POLYGON_WKB_MINSIZE ) {
        return GDBAPI_FALSE;
    }

    READ_BYTE_BE(wkb, c, byteOrder);
    if (byteOrder == WKB_BYTEORDER_NDR) {
        assert(0);
        return GDBAPI_FALSE;
    }

    READ_UINT32_BE(wkb, c, wkbType);
    READ_UINT32_BE(wkb, c, rings);

    if (rings == 0) {
        return GDBAPI_FALSE;
    }

    if (wkbType == WKB_Polygon) {
        PolygonClear(p);

        for (ring = 0; ring < rings; ring++) {
            READ_UINT32_BE(wkb, c, points);

            vl = (VertexType *) calloc(points, sizeof(VertexType));
            if (! vl) {
                return GDBAPI_FALSE;
            }

            for (i = 0; i < points; i++) {
                READ_FLOAT64_BE(wkb, c, vl[i].x);
                READ_FLOAT64_BE(wkb, c, vl[i].y);
            }

            PolygonAddPart(p, points-1, vl, (ring==0? PLGPART_NOTHOLE : PLGPART_HOLE), PLGC_TRUE);
        }

        return PolygonGetPart (p, -1, 0, 0) > 0;
    } else if (wkbType == WKB_MultiPolygon) {
        PolygonClear(p);

        parts = rings;

        for (poly = 0; poly < parts; poly++) {
            READ_UINT32_BE(wkb, c, wkbType);

            if (wkbType != WKB_Polygon) {
                return GDBAPI_FALSE;
            }

            READ_UINT32_BE(wkb, c, rings);

            for (ring = 0; ring < rings; ring++) {
                READ_UINT32_BE(wkb, c, points);

                vl = (VertexType *) calloc(points, sizeof(VertexType));
                if (! vl) {
                    return GDBAPI_FALSE;
                }

                for (i = 0; i < points; i++) {
                    READ_FLOAT64_BE(wkb, c, vl[i].x);
                    READ_FLOAT64_BE(wkb, c, vl[i].y);
                }

                PolygonAddPart(p, points-1, vl, (ring==0? PLGPART_NOTHOLE : PLGPART_HOLE), PLGC_TRUE);
            }
        }

        return PolygonGetPart (p, -1, 0, 0) > 0;
    } else {
        return GDBAPI_FALSE;
    }
}

static GDBAPI_BOOL PolygonToShapeWKB (HPOLYGON plg, HGDBSHP shp, HGDBERR herr)
{
    byte_t   byteOrder = WKB_BYTEORDER_XDR;
    byte_t   *wkbaddr;
    uint32_t wkbtype,
             wkbsize,
             rings,
             partid,
             parts,
             nverts,
             tmp32,
             i,
             c;

    double   tmp64;

    VertexType *vertlist;

    PLGPART_TYPE parttype;

    c = 0;
    wkbsize = 0;

    parts = PolygonGetContour (plg, -1, 0);

    if (parts == 1) {
        wkbtype = WKB_Polygon;
    
        nverts = PolygonGetContour (plg, 0, &vertlist);
        if (nverts >= 3) {
            rings = PolygonGetPart (plg, -1, 0, 0);

            wkbsize += sizeof(byteOrder) + sizeof(wkbtype) + sizeof(rings);
            wkbsize += sizeof(nverts) + sizeof(double)*2*(nverts+1);

            /* add contour points here */
            for (partid = 0; partid < rings; partid++) {
                nverts = PolygonGetPart (plg, partid, &vertlist, &parttype);
                if (nverts >= 3 && parttype == PLGPART_HOLE) {
                    /* add hole points here */
                    wkbsize += sizeof(nverts) + sizeof(double)*2*(nverts+1);
                }
            }
        }

        if (wkbsize == 0) {
            return GDBAPI_FALSE;
        }

        if (GDBAPI_SUCCESS != GDBAttrSet (shp, 0, wkbsize, GDB_ATTR_SHAPE_WKBCOPY, herr)) {
            return GDBAPI_FALSE;
        }
        GDBAttrGet (shp, (void*) &wkbaddr, 0, GDB_ATTR_SHAPE_WKBADDR, herr);

        nverts = PolygonGetContour (plg, 0, &vertlist);
        rings = PolygonGetPart (plg, -1, 0, 0);

        wkbaddr[c++] = byteOrder;
        WRITE_UINT32_BE(wkbaddr, c, wkbtype, tmp32);
        WRITE_UINT32_BE(wkbaddr, c, rings, tmp32);

        WRITE_UINT32_BE(wkbaddr, c, nverts+1, tmp32);
        for (i = 0; i < nverts; i++) {
            WRITE_FLOAT64_BE(wkbaddr, c, vertlist[i].x, tmp64);
            WRITE_FLOAT64_BE(wkbaddr, c, vertlist[i].y, tmp64);
        }
        WRITE_FLOAT64_BE(wkbaddr, c, vertlist[0].x, tmp64);
        WRITE_FLOAT64_BE(wkbaddr, c, vertlist[0].y, tmp64);

        /* add contour points here */
        for (partid = 0; partid < rings; partid++) {
            nverts = PolygonGetPart (plg, partid, &vertlist, &parttype);
            if (nverts >= 3 && parttype == PLGPART_HOLE) {
                /* add hole points here */
                WRITE_UINT32_BE(wkbaddr, c, nverts+1, tmp32);
                for (i = 0; i < nverts; i++) {
                    WRITE_FLOAT64_BE(wkbaddr, c, vertlist[i].x, tmp64);
                    WRITE_FLOAT64_BE(wkbaddr, c, vertlist[i].y, tmp64);
                }
                WRITE_FLOAT64_BE(wkbaddr, c, vertlist[0].x, tmp64);
                WRITE_FLOAT64_BE(wkbaddr, c, vertlist[0].y, tmp64);
            }
        }
        assert(c == wkbsize);
        return GDBAPI_TRUE;
    } else if (parts > 1) {
        wkbtype = WKB_MultiPolygon;
        /* TODO: */

        return GDBAPI_FALSE;
    } else {
        return GDBAPI_FALSE;
    }
}

#ifdef    __cplusplus
}
#endif

#endif /* _GDB_SHAPE_H_ */
