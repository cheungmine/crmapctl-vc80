/**
 * extbound.c
 *
 * @since
 *    2014-3-29
 */

#include "gdbapi.h"

#include "./matalg/matalg_base.h"


GDBAPI_BOOL GDBExtBoundIsValid (const GDBExtBound * bound)
{
    return (
        bound->Xmax >= bound->Xmin &&
        bound->Ymax >= bound->Ymin &&
        bound->Zmax >= bound->Zmin &&
        bound->Mmax >= bound->Mmin)? GDBAPI_TRUE : GDBAPI_FALSE;
}


void GDBExtBoundUnion (const GDBExtBound * source, const GDBExtBound * clip,
    GDBExtBound * result)
{
    result->Xmin = MIN(source->Xmin, clip->Xmin);
    result->Ymin = MIN(source->Ymin, clip->Ymin);
    result->Zmin = MIN(source->Zmin, clip->Zmin);
    result->Mmin = MIN(source->Mmin, clip->Mmin);

    result->Xmax = MAX(source->Xmax, clip->Xmax);
    result->Ymax = MAX(source->Ymax, clip->Ymax);
    result->Zmax = MAX(source->Zmax, clip->Zmax);
    result->Mmax = MAX(source->Mmax, clip->Mmax);
}


GDBAPI_BOOL GDBExtBoundIntersect (const GDBExtBound * source, const GDBExtBound * clip,
    GDBExtBound * result)
{
    if (source->Xmin > clip->Xmax || source->Xmax < clip->Xmin) {
        return GDBAPI_FALSE;
    }

    if (source->Ymin > clip->Ymax || source->Ymax < clip->Ymin) {
        return GDBAPI_FALSE;
    }

    if (source->Zmin > clip->Zmax || source->Zmax < clip->Zmin) {
        return GDBAPI_FALSE;
    }

    if (result) {
        result->Xmin = MAX(source->Xmin, clip->Xmin);
        result->Ymin = MAX(source->Ymin, clip->Ymin);
        result->Zmin = MAX(source->Zmin, clip->Zmin);

        result->Xmax = MIN(source->Xmax, clip->Xmax);
        result->Ymax = MIN(source->Ymax, clip->Ymax);
        result->Zmax = MIN(source->Zmax, clip->Zmax);
    }

    return GDBAPI_TRUE;
}
