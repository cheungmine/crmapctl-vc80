/**
 * polygon.c
 *
 */
#include <assert.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <string.h>

#include "polygon.h"

typedef VertexType vertex_t;
typedef RectType   rect_t;

/**
 * Increase PLGC_EPSILON to encourage merging of near coincident edges
 */
static double PLGC_EPSILON = PLGC_TOLERANCE;

#define LEFT               0
#define RIGHT              1

#define ABOVE              0
#define BELOW              1

#define CLIP               0
#define SUBJ               1

#define INVERT_TRISTRIPS   PLGC_FALSE

#define EQUAL(a, b)  (fabs((a) - (b)) < PLGC_EPSILON)

#define PREV_INDEX(i, n)   ((i - 1 + n) % n)
#define NEXT_INDEX(i, n)   ((i + 1) % n)

#define OPTIMAL(v, i, n)   ((v[PREV_INDEX(i, n)].y != v[i].y) || \
                                                        (v[NEXT_INDEX(i, n)].y != v[i].y))

#define FWD_MIN(v, i, n)   ((v[PREV_INDEX(i, n)].vertex.y >= v[i].vertex.y) \
                         && (v[NEXT_INDEX(i, n)].vertex.y > v[i].vertex.y))

#define NOT_FMAX(v, i, n)   (v[NEXT_INDEX(i, n)].vertex.y > v[i].vertex.y)

#define REV_MIN(v, i, n)   ((v[PREV_INDEX(i, n)].vertex.y > v[i].vertex.y) \
                         && (v[NEXT_INDEX(i, n)].vertex.y >= v[i].vertex.y))

#define NOT_RMAX(v, i, n)   (v[PREV_INDEX(i, n)].vertex.y > v[i].vertex.y)

#define ADD_VERT(e,p,s,x,y)  do { \
        add_vertex(&((e)->outp[(p)]->v[(s)]), x, y); \
        (e)->outp[(p)]->active++; \
    } while (0)

#define P_EDGE(d,e,p,i,j)  do { \
        (d)= (e); \
        do { \
            (d)= (d)->prev; \
        } while (!(d)->outp[(p)]); \
        (i)= (d)->bot.x + (d)->dx * ((j)-(d)->bot.y); \
    } while (0)

#define N_EDGE(d,e,p,i,j)  do { \
        (d)= (e); \
        do { \
            (d)= (d)->next; \
        } while (!(d)->outp[(p)]); \
        (i)= (d)->bot.x + (d)->dx * ((j)-(d)->bot.y); \
    } while (0)

#define MALLOC_S(ptr, type, count, func, lineno)  do { \
        ptr = 0; \
        if ((count) > 0) { \
            (ptr) = (type *) malloc(sizeof(type) * (count)); \
            if (! (ptr)) { \
                fprintf (stderr, "malloc error at: %s(), line:%d.\n", func, lineno); \
                exit (-1); \
            } \
        } \
    } while (0)

#define FREE_S(ptr) do { \
        if (ptr) { \
            free (ptr); \
            (ptr) = 0; \
        } \
    } while (0)


/**
 * Vertex list structure
 */
typedef struct                 
{
    int    numverts;  /* Number of vertices in array */
    vertex_t *vertex;    /* Vertex array */
} vertexlist_t;

/**
 * Polygon structure
 */
typedef struct _polygon_t
{
    int         numparts;
    int        *holes;        /* PLGPART_TYPE */
    vertexlist_t *parts;
} polygon_t;

/**
 * Tristrip set structure
 */
typedef struct _tristrip_t
{
    int   num_strips;    /* Number of tristrips */
    vertexlist_t *strip; /* Tristrip array pointer */
} tristrip_t;

/**
 * Tristrip set structure
 */
typedef enum                        /* Edge intersection classes         */
{
    NIL,                              /* Empty non-intersection            */
    EMX,                              /* External maximum                  */
    ELI,                              /* External left intermediate        */
    TED,                              /* Top edge                          */
    ERI,                              /* External right intermediate       */
    RED,                              /* Right edge                        */
    IMM,                              /* Internal maximum and minimum      */
    IMN,                              /* Internal minimum                  */
    EMN,                              /* External minimum                  */
    EMM,                              /* External maximum and minimum      */
    LED,                              /* Left edge                         */
    ILI,                              /* Internal left intermediate        */
    BED,                              /* Bottom edge                       */
    IRI,                              /* Internal right intermediate       */
    IMX,                              /* Internal maximum                  */
    FUL                               /* Full non-intersection             */
} vertex_class;

typedef enum                        /* Horizontal edge states            */
{
    NH,                               /* No horizontal edge                */
    BH,                               /* Bottom horizontal edge            */
    TH                                /* Top horizontal edge               */
} hedge_state;

typedef enum                        /* Edge bundle state                 */
{
    UNBUNDLED,                        /* Isolated edge not within a bundle */
    BUNDLE_HEAD,                      /* Bundle head node                  */
    BUNDLE_TAIL                       /* Passive bundle tail node          */
} bundle_state;

typedef struct v_shape              /* Internal vertex list datatype     */
{
    double              x;            /* X coordinate component            */
    double              y;            /* Y coordinate component            */
    struct v_shape     *next;         /* Pointer to next vertex in list    */
} vertex_node;

typedef struct p_shape              /* Internal contour / tristrip type  */
{
    int                 active;       /* Active flag or vertex count       */
    int                 hole;         /* Hole / external contour flag      */
    vertex_node        *v[2];         /* Left and right vertex list ptrs   */
    struct p_shape     *next;         /* Pointer to next polygon contour   */
    struct p_shape     *actual;       /* Pointer to actual structure used  */
} polygon_node;

typedef struct edge_shape
{
    vertex_t            vertex;       /* Piggy-backed contour vertex data  */
    vertex_t            bot;          /* Edge lower (x, y) coordinate      */
    vertex_t            top;          /* Edge upper (x, y) coordinate      */
    double              xb;           /* Scanbeam bottom x coordinate      */
    double              xt;           /* Scanbeam top x coordinate         */
    double              dx;           /* Change in x for a unit y increase */
    int                 type;         /* Clip / subject edge flag          */
    int                 bundle[2][2]; /* Bundle edge flags                 */
    int                 bside[2];     /* Bundle left / right indicators    */
    bundle_state        bstate[2];    /* Edge bundle state                 */
    polygon_node       *outp[2];      /* Output polygon / tristrip pointer */
    struct edge_shape  *prev;         /* Previous edge in the AET          */
    struct edge_shape  *next;         /* Next edge in the AET              */
    struct edge_shape  *pred;         /* Edge connected at the lower end   */
    struct edge_shape  *succ;         /* Edge connected at the upper end   */
    struct edge_shape  *next_bound;   /* Pointer to next bound in LMT      */
} edge_node;

typedef struct lmt_shape            /* Local minima table                */
{
    double              y;            /* Y coordinate at local minimum     */
    edge_node          *first_bound;  /* Pointer to bound list             */
    struct lmt_shape   *next;         /* Pointer to next local minimum     */
} lmt_node;

typedef struct sbt_t_shape          /* Scanbeam tree                     */
{
    double              y;            /* Scanbeam node y value             */
    struct sbt_t_shape *less;         /* Pointer to nodes with lower y     */
    struct sbt_t_shape *more;         /* Pointer to nodes with higher y    */
} sb_tree;

typedef struct it_shape             /* Intersection table                */
{
    edge_node          *ie[2];        /* Intersecting edge (bundle) pair   */
    vertex_t          point;          /* Point of intersection             */
    struct it_shape    *next;         /* The next intersection table node  */
} it_node;

typedef struct st_shape             /* Sorted edge table                 */
{
    edge_node          *edge;         /* Pointer to AET edge               */
    double              xb;           /* Scanbeam bottom x coordinate      */
    double              xt;           /* Scanbeam top x coordinate         */
    double              dx;           /* Change in x for a unit y increase */
    struct st_shape    *prev;         /* Previous edge in sorted list      */
} st_node;

typedef struct bbox_shape           /* Contour axis-aligned bounding box */
{
    double             xmin;          /* Minimum x coordinate              */
    double             ymin;          /* Minimum y coordinate              */
    double             xmax;          /* Maximum x coordinate              */
    double             ymax;          /* Maximum y coordinate              */
} bbox;


/**
 * Is vertex p, q on the same side of line l:[start, end]
 */
#define is_sameside(start, end, p, q) \
    (((end.x-start.x)*(p.y-start.y) - (end.y-start.y)*(p.x-start.x))* \
   ((end.x-start.x)*(q.y-end.y) - (end.y-start.y)*(q.x-end.x)))>0 ? 1:0

/**
 * Is two line-segments (s1, s2) intersect ?
 */
#define is_intersect(s1_start, s1_end, s2_start, s2_end) \
    (! is_sameside(s1_start, s1_end, s2_start, s2_end) && \
   ! is_sameside(s2_start, s2_end, s1_start, s1_end))

/**
 * get rect of vertex list
 */
static int vertexlist_rect (const vertex_t *vl, int np, rect_t *rc)
{
    if (np == 0) {
        return 0;
    } else {
        rc->xmin = rc->xmax = vl[0].x;
        rc->ymin = rc->ymax = vl[0].y;

        if (np == 1) {
            return 1;
        } else {
            int i = 1;
            for (; i < np; i++) {
                if (vl[i].x < rc->xmin) {
                    rc->xmin = vl[i].x;
                }
                if (vl[i].y < rc->ymin) {
                    rc->ymin = vl[i].y;
                }
                if (vl[i].x > rc->xmax) {
                    rc->xmax = vl[i].x;
                }
                if (vl[i].y > rc->ymax) {
                    rc->ymax = vl[i].y;
                }
            }
            return 1;
        }
    }
}

/**
 * test if vertex(x, y) is in polygon vl
 */
static int vertex_in_poly (double x, double y,
    const vertex_t *vl, int np, const rect_t *prc)
{
    if (np < 3) {
        return 0;
    } else {
        rect_t  rc;

        if (prc) {
            rc = *prc;
        } else {
            vertexlist_rect(vl, np, &rc);
        }

        if (x < rc.xmin || x >= rc.xmax || y < rc.ymin || y >= rc.ymax) {
            return 0;
        } else {
            int j, k1, k2, i = 0, c = 0;

            /* set a horizontal beam l(v, w) from v to the ultra right */
            vertex_t  v = {x, y};
            vertex_t  w = {rc.xmax + rc.xmax - rc.xmin, y};

            /* intersect points counter */
            for (; i < np; i++) {
                j = (i+1) % np;

                if (is_intersect(vl[i], vl[j], v, w)) {
                    c++;
                } else if (vl[i].y == w.y) {
                    k1 = (np + i - 1) % np;

                    while (k1!=i && vl[k1].y==w.y) {
                        k1 = (np+k1-1) % np;
                    }
 
                    k2 = (i+1) % np;

                    while (k2 != i && vl[k2].y == w.y) {
                        k2 = (k2 + 1) % np;
                    }
             
                    if (k1 != k2 && ! is_sameside(v, w, vl[k1], vl[k2])) {
                        c++;
                    }
 
                    if (k2 <= i) {
                        break;
                    }
 
                    i = k2;
                }
            }
 
            return   c%2;
        }
    }
}

static double vertexlist_area (vertex_t *vl, int numverts, int *CCW)
{
    int i;
    double sum = 0;

    for (i = 1; i < numverts-1; i++) {
        sum += (vl[i].x - vl[0].x) * (vl[i+1].y - vl[i-1].y);
    }
    sum += (vl[i].x - vl[0].x) * (vl[0].y - vl[i-1].y);

    i = (sum > 0? 1 : (sum < 0? -1 : 0));

    if (CCW) {
        *CCW = i;
    }

    return i*sum/2;
}

static double vertex_dist (double dx, double dy)
{
    return sqrt (dx*dx + dy*dy);
}


/**
 * Horizontal edge state transitions within scanbeam boundary
 */
const hedge_state next_h_state[3][6]=
{
    /*        ABOVE     BELOW     CROSS */
    /*        L   R     L   R     L   R */  
    /* NH */ {BH, TH,   TH, BH,   NH, NH},
    /* BH */ {NH, NH,   NH, NH,   TH, TH},
    /* TH */ {NH, NH,   NH, NH,   BH, BH}
};

static void reset_it(it_node **it)
{
    it_node *itn;

    while (*it) {
        itn= (*it)->next;
        FREE_S(*it);
        *it= itn;
    }
}

static void reset_lmt(lmt_node **lmt)
{
    lmt_node *lmtn;

    while (*lmt) {
        lmtn= (*lmt)->next;
        FREE_S(*lmt);
        *lmt= lmtn;
    }
}

static void insert_bound(edge_node **b, edge_node *e)
{
    edge_node *existing_bound;

    if (!*b)
    {
        /* Link node e to the tail of the list */
        *b= e;
    }
    else
    {
        /* Do primary sort on the x field */
        if (e[0].bot.x < (*b)[0].bot.x)
        {
            /* Insert a new node mid-list */
            existing_bound= *b;
            *b= e;
            (*b)->next_bound= existing_bound;
        }
        else
        {
            if (e[0].bot.x == (*b)[0].bot.x)
            {
                /* Do secondary sort on the dx field */
                if (e[0].dx < (*b)[0].dx)
                {
                    /* Insert a new node mid-list */
                    existing_bound= *b;
                    *b= e;
                    (*b)->next_bound= existing_bound;
                }
                else
                {
                    /* Head further down the list */
                    insert_bound(&((*b)->next_bound), e);
                }
            }
            else
            {
                /* Head further down the list */
                insert_bound(&((*b)->next_bound), e);
            }
        }
    }
}


static edge_node **bound_list(lmt_node **lmt, double y)
{
    lmt_node *existing_node;

    if (!*lmt)
    {
        /* Add node onto the tail end of the LMT */
        MALLOC_S(*lmt, lmt_node, 1, __FUNCTION__, __LINE__);
        (*lmt)->y= y;
        (*lmt)->first_bound= 0;
        (*lmt)->next= 0;
        return &((*lmt)->first_bound);
    }
    else
        if (y < (*lmt)->y)
        {
            /* Insert a new LMT node before the current node */
            existing_node= *lmt;
            MALLOC_S(*lmt, lmt_node, 1, __FUNCTION__, __LINE__);
            (*lmt)->y= y;
            (*lmt)->first_bound= 0;
            (*lmt)->next= existing_node;
            return &((*lmt)->first_bound);
        }
        else
            if (y > (*lmt)->y)
                /* Head further up the LMT */
                return bound_list(&((*lmt)->next), y);
            else
                /* Use this existing LMT node */
                return &((*lmt)->first_bound);
}


static void add_to_sbtree(int *entries, sb_tree **sbtree, double y)
{
    if (!*sbtree)
    {
        /* Add a new tree node here */
        MALLOC_S(*sbtree, sb_tree, 1, __FUNCTION__, __LINE__);
        (*sbtree)->y= y;
        (*sbtree)->less= 0;
        (*sbtree)->more= 0;
        (*entries)++;
    }
    else
    {
        if ((*sbtree)->y > y)
        {
        /* Head into the 'less' sub-tree */
            add_to_sbtree(entries, &((*sbtree)->less), y);
        }
        else
        {
            if ((*sbtree)->y < y)
            {
                /* Head into the 'more' sub-tree */
                add_to_sbtree(entries, &((*sbtree)->more), y);
            }
        }
    }
}


static void build_sbt(int *entries, double *sbt, sb_tree *sbtree)
{
    if (sbtree->less)
        build_sbt(entries, sbt, sbtree->less);
    sbt[*entries]= sbtree->y;
    (*entries)++;
    if (sbtree->more)
        build_sbt(entries, sbt, sbtree->more);
}


static void free_sbtree(sb_tree **sbtree)
{
    if (*sbtree)
    {
        free_sbtree(&((*sbtree)->less));
        free_sbtree(&((*sbtree)->more));
        FREE_S(*sbtree);
    }
}


static int count_optimal_vertices(vertexlist_t c)
{
    int result= 0, i;

    /* Ignore non-contributing contours */
    if (c.numverts > 0)
    {
        for (i= 0; i < c.numverts; i++)
            /* Ignore superfluous vertices embedded in horizontal edges */
            if (OPTIMAL(c.vertex, i, c.numverts))
                result++;
    }
    return result;
}


static edge_node *build_lmt(lmt_node **lmt, sb_tree **sbtree,
                                                        int *sbt_entries, polygon_t *p, int type,
                                                        int op)
{
    int          c, i, min, max, num_edges, v, numverts;
    int          total_vertices= 0, e_index=0;
    edge_node   *e, *edge_table;

    for (c= 0; c < p->numparts; c++)
        total_vertices+= count_optimal_vertices(p->parts[c]);

    /* Create the entire input polygon edge table in one go */
    MALLOC_S(edge_table, edge_node, total_vertices, __FUNCTION__, __LINE__);

    for (c= 0; c < p->numparts; c++)
    {
        if (p->parts[c].numverts < 0)
        {
            /* Ignore the non-contributing contour and repair the vertex count */
            p->parts[c].numverts= -p->parts[c].numverts;
        }
        else
        {
            /* Perform contour optimisation */
            numverts= 0;
            for (i= 0; i < p->parts[c].numverts; i++)
                if (OPTIMAL(p->parts[c].vertex, i, p->parts[c].numverts))
                {
                    edge_table[numverts].vertex.x= p->parts[c].vertex[i].x;
                    edge_table[numverts].vertex.y= p->parts[c].vertex[i].y;

                    /* Record vertex in the scanbeam table */
                    add_to_sbtree(sbt_entries, sbtree,
                                                edge_table[numverts].vertex.y);

                    numverts++;
                }

            /* Do the contour forward pass */
            for (min= 0; min < numverts; min++)
            {
                /* If a forward local minimum... */
                if (FWD_MIN(edge_table, min, numverts))
                {
                    /* Search for the next local maximum... */
                    num_edges= 1;
                    max= NEXT_INDEX(min, numverts);
                    while (NOT_FMAX(edge_table, max, numverts))
                    {
                        num_edges++;
                        max= NEXT_INDEX(max, numverts);
                    }

                    /* Build the next edge list */
                    e= &edge_table[e_index];
                    e_index+= num_edges;
                    v= min;
                    e[0].bstate[BELOW]= UNBUNDLED;
                    e[0].bundle[BELOW][CLIP]= PLGC_FALSE;
                    e[0].bundle[BELOW][SUBJ]= PLGC_FALSE;
                    for (i= 0; i < num_edges; i++)
                    {
                        e[i].xb= edge_table[v].vertex.x;
                        e[i].bot.x= edge_table[v].vertex.x;
                        e[i].bot.y= edge_table[v].vertex.y;

                        v= NEXT_INDEX(v, numverts);

                        e[i].top.x= edge_table[v].vertex.x;
                        e[i].top.y= edge_table[v].vertex.y;
                        e[i].dx= (edge_table[v].vertex.x - e[i].bot.x) /
                       (e[i].top.y - e[i].bot.y);
                        e[i].type= type;
                        e[i].outp[ABOVE]= 0;
                        e[i].outp[BELOW]= 0;
                        e[i].next= 0;
                        e[i].prev= 0;
                        e[i].succ= ((num_edges > 1) && (i < (num_edges - 1))) ?
                       &(e[i + 1]) : 0;
                        e[i].pred= ((num_edges > 1) && (i > 0)) ? &(e[i - 1]) : 0;
                        e[i].next_bound= 0;
                        e[i].bside[CLIP]= (op == PLGCLIP_DIFFER) ? RIGHT : LEFT;
                        e[i].bside[SUBJ]= LEFT;
                    }
                    insert_bound(bound_list(lmt, edge_table[min].vertex.y), e);
                }
            }

            /* Do the contour reverse pass */
            for (min= 0; min < numverts; min++)
            {
            /* If a reverse local minimum... */
                if (REV_MIN(edge_table, min, numverts))
                {
                    /* Search for the previous local maximum... */
                    num_edges= 1;
                    max= PREV_INDEX(min, numverts);
                    while (NOT_RMAX(edge_table, max, numverts))
                    {
                        num_edges++;
                        max= PREV_INDEX(max, numverts);
                    }

                    /* Build the previous edge list */
                    e= &edge_table[e_index];
                    e_index+= num_edges;
                    v= min;
                    e[0].bstate[BELOW]= UNBUNDLED;
                    e[0].bundle[BELOW][CLIP]= PLGC_FALSE;
                    e[0].bundle[BELOW][SUBJ]= PLGC_FALSE;
                    for (i= 0; i < num_edges; i++)
                    {
                        e[i].xb= edge_table[v].vertex.x;
                        e[i].bot.x= edge_table[v].vertex.x;
                        e[i].bot.y= edge_table[v].vertex.y;

                        v= PREV_INDEX(v, numverts);

                        e[i].top.x= edge_table[v].vertex.x;
                        e[i].top.y= edge_table[v].vertex.y;
                        e[i].dx= (edge_table[v].vertex.x - e[i].bot.x) /
                       (e[i].top.y - e[i].bot.y);
                        e[i].type= type;
                        e[i].outp[ABOVE]= 0;
                        e[i].outp[BELOW]= 0;
                        e[i].next= 0;
                        e[i].prev= 0;
                        e[i].succ= ((num_edges > 1) && (i < (num_edges - 1))) ?
                       &(e[i + 1]) : 0;
                        e[i].pred= ((num_edges > 1) && (i > 0)) ? &(e[i - 1]) : 0;
                        e[i].next_bound= 0;
                        e[i].bside[CLIP]= (op == PLGCLIP_DIFFER) ? RIGHT : LEFT;
                        e[i].bside[SUBJ]= LEFT;
                    }
                    insert_bound(bound_list(lmt, edge_table[min].vertex.y), e);
                }
            }
        }
    }
    return edge_table;
}


static void add_edge_to_aet(edge_node **aet, edge_node *edge, edge_node *prev)
{
    if (!*aet)
    {
        /* Append edge onto the tail end of the AET */
        *aet= edge;
        edge->prev= prev;
        edge->next= 0;
    }
    else
    {
        /* Do primary sort on the xb field */
        if (edge->xb < (*aet)->xb)
        {
            /* Insert edge here (before the AET edge) */
            edge->prev= prev;
            edge->next= *aet;
            (*aet)->prev= edge;
            *aet= edge;
        }
        else
        {
            if (edge->xb == (*aet)->xb)
            {
                /* Do secondary sort on the dx field */
                if (edge->dx < (*aet)->dx)
                {
                    /* Insert edge here (before the AET edge) */
                    edge->prev= prev;
                    edge->next= *aet;
                    (*aet)->prev= edge;
                    *aet= edge;
                }
                else
                {
                    /* Head further into the AET */
                    add_edge_to_aet(&((*aet)->next), edge, *aet);
                }
            }
            else
            {
                /* Head further into the AET */
                add_edge_to_aet(&((*aet)->next), edge, *aet);
            }
        }
    }
}


static void add_intersection(it_node **it, edge_node *edge0, edge_node *edge1,
                             double x, double y)
{
    it_node *existing_node;

    if (!*it)
    {
        /* Append a new node to the tail of the list */
        MALLOC_S(*it, it_node, 1, __FUNCTION__, __LINE__);
        (*it)->ie[0]= edge0;
        (*it)->ie[1]= edge1;
        (*it)->point.x= x;
        (*it)->point.y= y;
        (*it)->next= 0;
    }
    else
    {
        if ((*it)->point.y > y)
        {
            /* Insert a new node mid-list */
            existing_node= *it;
            MALLOC_S(*it, it_node, 1, __FUNCTION__, __LINE__);
            (*it)->ie[0]= edge0;
            (*it)->ie[1]= edge1;
            (*it)->point.x= x;
            (*it)->point.y= y;
            (*it)->next= existing_node;
        }
        else
            /* Head further down the list */
            add_intersection(&((*it)->next), edge0, edge1, x, y);
    }
}


static void add_st_edge(st_node **st, it_node **it, edge_node *edge,
                                                double dy)
{
    st_node *existing_node;
    double   den, r, x, y;

    if (!*st)
    {
        /* Append edge onto the tail end of the ST */
        MALLOC_S(*st, st_node, 1, __FUNCTION__, __LINE__);
        (*st)->edge= edge;
        (*st)->xb= edge->xb;
        (*st)->xt= edge->xt;
        (*st)->dx= edge->dx;
        (*st)->prev= 0;
    }
    else
    {
        den= ((*st)->xt - (*st)->xb) - (edge->xt - edge->xb);

        /* If new edge and ST edge don't cross */
        if ((edge->xt >= (*st)->xt) || (edge->dx == (*st)->dx) || 
                (fabs(den) <= DBL_EPSILON))
        {
            /* No intersection - insert edge here (before the ST edge) */
            existing_node= *st;
            MALLOC_S(*st, st_node, 1, __FUNCTION__, __LINE__);
            (*st)->edge= edge;
            (*st)->xb= edge->xb;
            (*st)->xt= edge->xt;
            (*st)->dx= edge->dx;
            (*st)->prev= existing_node;
        }
        else
        {
            /* Compute intersection between new edge and ST edge */
            r= (edge->xb - (*st)->xb) / den;
            x= (*st)->xb + r * ((*st)->xt - (*st)->xb);
            y= r * dy;

            /* Insert the edge pointers and the intersection point in the IT */
            add_intersection(it, (*st)->edge, edge, x, y);

            /* Head further into the ST */
            add_st_edge(&((*st)->prev), it, edge, dy);
        }
    }
}


static void build_intersection_table(it_node **it, edge_node *aet, double dy)
{
    st_node   *st, *stp;
    edge_node *edge;

    /* Build intersection table for the current scanbeam */
    reset_it(it);
    st= 0;

    /* Process each AET edge */
    for (edge= aet; edge; edge= edge->next)
    {
        if ((edge->bstate[ABOVE] == BUNDLE_HEAD) ||
         edge->bundle[ABOVE][CLIP] || edge->bundle[ABOVE][SUBJ])
            add_st_edge(&st, it, edge, dy);
    }

    /* Free the sorted edge table */
    while (st)
    {
        stp= st->prev;
        FREE_S(st);
        st= stp;
    }
}

static int count_contours(polygon_node *polygon)
{
    int          nc, nv;
    vertex_node *v, *nextv;

    for (nc = 0; polygon; polygon = polygon->next) {
        if (polygon->active) {
            /* Count the vertices in the current contour */
            nv= 0;

            for (v = polygon->actual->v[LEFT]; v; v = v->next) {
                nv++;
            }

            /* Record valid vertex counts in the active field */
            if (nv > 2) {
                polygon->active= nv;
                nc++;
            } else {
                /* Invalid contour: just free the heap */
                for (v = polygon->actual->v[LEFT]; v; v = nextv) {
                    nextv= v->next;
                    FREE_S(v);
                }
                polygon->active= 0;
            }
        }
    }

    return nc;
}


static void add_left(polygon_node *p, double x, double y)
{
    vertex_node *nv;

    /* Create a new vertex node and set its fields */
    MALLOC_S(nv, vertex_node, 1, __FUNCTION__, __LINE__);
    nv->x= x;
    nv->y= y;

    /* Add vertex nv to the left end of the polygon's vertex list */
    nv->next= p->actual->v[LEFT];

    /* Update actual->[LEFT] to point to nv */
    p->actual->v[LEFT]= nv;
}


static void merge_left(polygon_node *p, polygon_node *q, polygon_node *list)
{
    polygon_node *target;

    /* Label contour as a hole */
    q->actual->hole= PLGPART_HOLE;

    if (p->actual != q->actual)
    {
        /* Assign p's vertex list to the left end of q's list */
        p->actual->v[RIGHT]->next= q->actual->v[LEFT];
        q->actual->v[LEFT]= p->actual->v[LEFT];

        /* Redirect any p->actual references to q->actual */
    
        for (target= p->actual; list; list= list->next)
        {
            if (list->actual == target)
            {
                list->active= PLGC_FALSE;
                list->actual= q->actual;
            }
        }
    }
}


static void add_right(polygon_node *p, double x, double y)
{
    vertex_node *nv;

    /* Create a new vertex node and set its fields */
    MALLOC_S(nv, vertex_node, 1, __FUNCTION__, __LINE__);
    nv->x= x;
    nv->y= y;
    nv->next= 0;

    /* Add vertex nv to the right end of the polygon's vertex list */
    p->actual->v[RIGHT]->next= nv;

    /* Update actual->v[RIGHT] to point to nv */
    p->actual->v[RIGHT]= nv;
}


static void merge_right (polygon_node *p, polygon_node *q, polygon_node *list)
{
    polygon_node *target;

    /* Label contour as external */
    q->actual->hole= PLGPART_NOTHOLE;

    if (p->actual != q->actual) {
        /* Assign p's vertex list to the right end of q's list */
        q->actual->v[RIGHT]->next= p->actual->v[LEFT];
        q->actual->v[RIGHT]= p->actual->v[RIGHT];

        /* Redirect any p->actual references to q->actual */
        for (target= p->actual; list; list= list->next) {
            if (list->actual == target) {
                list->active= PLGC_FALSE;
                list->actual= q->actual;
            }
        }
    }
}


static void add_local_min(polygon_node **p, edge_node *edge,
                                                    double x, double y)
{
    polygon_node *existing_min;
    vertex_node  *nv;

    existing_min= *p;

    MALLOC_S(*p, polygon_node, 1, __FUNCTION__, __LINE__);

    /* Create a new vertex node and set its fields */
    MALLOC_S(nv, vertex_node, 1, __FUNCTION__, __LINE__);
    nv->x= x;
    nv->y= y;
    nv->next= 0;

    /* Initialise actual to point to p itself */
    (*p)->actual= (*p);
    (*p)->active = PLGC_TRUE;
    (*p)->next= existing_min;

    /* Make v[LEFT] and v[RIGHT] point to new vertex nv */
    (*p)->v[LEFT]= nv;
    (*p)->v[RIGHT]= nv;

    /* Assign polygon p to the edge */
    edge->outp[ABOVE]= *p;
}


static int count_tristrips(polygon_node *tn)
{
    int total;

    for (total= 0; tn; tn= tn->next)
        if (tn->active > 2)
            total++;
    return total;
}


static void add_vertex(vertex_node **t, double x, double y)
{
    if (!(*t))
    {
        MALLOC_S(*t, vertex_node, 1, __FUNCTION__, __LINE__);
        (*t)->x= x;
        (*t)->y= y;
        (*t)->next= 0;
    }
    else
        /* Head further down the list */
        add_vertex(&((*t)->next), x, y);
}


static void new_tristrip(polygon_node **tn, edge_node *edge,
                         double x, double y)
{
    if (!(*tn))
    {
        MALLOC_S(*tn, polygon_node, 1, __FUNCTION__, __LINE__);
        (*tn)->next= 0;
        (*tn)->v[LEFT]= 0;
        (*tn)->v[RIGHT]= 0;
        (*tn)->active= 1;
        add_vertex(&((*tn)->v[LEFT]), x, y); 
        edge->outp[ABOVE]= *tn;
    }
    else
        /* Head further down the list */
        new_tristrip(&((*tn)->next), edge, x, y);
}


static bbox *create_contour_bboxes(polygon_t *p)
{
    bbox *box;
    int   c, v;

    MALLOC_S(box, bbox, p->numparts, __FUNCTION__, __LINE__);

    /* Construct contour bounding boxes */
    for (c= 0; c < p->numparts; c++)
    {
        /* Initialise bounding box extent */
        box[c].xmin= DBL_MAX;
        box[c].ymin= DBL_MAX;
        box[c].xmax= -DBL_MAX;
        box[c].ymax= -DBL_MAX;

        for (v= 0; v < p->parts[c].numverts; v++)
        {
            /* Adjust bounding box */
            if (p->parts[c].vertex[v].x < box[c].xmin)
                box[c].xmin= p->parts[c].vertex[v].x;
            if (p->parts[c].vertex[v].y < box[c].ymin)
                box[c].ymin= p->parts[c].vertex[v].y;
            if (p->parts[c].vertex[v].x > box[c].xmax)
                box[c].xmax= p->parts[c].vertex[v].x;
            if (p->parts[c].vertex[v].y > box[c].ymax)
                    box[c].ymax= p->parts[c].vertex[v].y;
        }
    }
    return box;  
}


static void minimax_test(polygon_t *subj, polygon_t *clip, int op)
{
    bbox *s_bbox, *c_bbox;
    int   s, c, *o_table, overlap;

    s_bbox= create_contour_bboxes(subj);
    c_bbox= create_contour_bboxes(clip);

    MALLOC_S(o_table, int, subj->numparts * clip->numparts,
        __FUNCTION__, __LINE__);

    /* Check all subject contour bounding boxes against clip boxes */
    for (s= 0; s < subj->numparts; s++)
        for (c= 0; c < clip->numparts; c++)
            o_table[c * subj->numparts + s]=
             (!((s_bbox[s].xmax < c_bbox[c].xmin) ||
                                (s_bbox[s].xmin > c_bbox[c].xmax))) &&
             (!((s_bbox[s].ymax < c_bbox[c].ymin) ||
                                (s_bbox[s].ymin > c_bbox[c].ymax)));

    /* For each clip contour, search for any subject contour overlaps */
    for (c= 0; c < clip->numparts; c++)
    {
        overlap= 0;
        for (s= 0; (!overlap) && (s < subj->numparts); s++)
            overlap= o_table[c * subj->numparts + s];

        if (!overlap)
            /* Flag non contributing status by negating vertex count */
            clip->parts[c].numverts = -clip->parts[c].numverts;
    }  

    if (op == PLGCLIP_INTERSECT)
    {  
        /* For each subject contour, search for any clip contour overlaps */
        for (s= 0; s < subj->numparts; s++)
        {
            overlap= 0;
            for (c= 0; (!overlap) && (c < clip->numparts); c++)
                overlap= o_table[c * subj->numparts + s];

            if (!overlap)
                /* Flag non contributing status by negating vertex count */
                subj->parts[s].numverts = -subj->parts[s].numverts;
        }  
    }

    FREE_S(s_bbox);
    FREE_S(c_bbox);
    FREE_S(o_table);
}


static void gpc_free_tristrip(tristrip_t *t)
{
    int s;

    for (s= 0; s < t->num_strips; s++)
        FREE_S(t->strip[s].vertex);
    FREE_S(t->strip);
    t->num_strips= 0;
}


static void gpc_tristrip_clip(int op, polygon_t *subj, polygon_t *clip,
                       tristrip_t *result)
{
    sb_tree       *sbtree= 0;
    it_node       *it= 0, *intersect;
    edge_node     *edge, *prev_edge, *next_edge, *succ_edge, *e0, *e1;
    edge_node     *aet= 0, *c_heap= 0, *s_heap= 0, *cf;
    lmt_node      *lmt= 0, *local_min;
    polygon_node  *tlist= 0, *tn, *tnn, *p, *q;
    vertex_node   *lt, *ltn, *rt, *rtn;
    hedge_state        horiz[2];
    vertex_class    cft;
    int            in[2], exists[2], parity[2]= {LEFT, LEFT};
    int            s, v, contributing, search, scanbeam= 0, sbt_entries= 0;
    int            vclass, bl, br, tl, tr;
    double        *sbt= 0, xb, px, nx, yb, yt, dy, ix, iy;

    /* Test for trivial 0 result cases */
    if (((subj->numparts == 0) && (clip->numparts == 0))
   || ((subj->numparts == 0) && ((op == PLGCLIP_INTERSECT) || (op == PLGCLIP_DIFFER)))
   || ((clip->numparts == 0) &&  (op == PLGCLIP_INTERSECT)))
    {
        result->num_strips= 0;
        result->strip= 0;
        return;
    }

    /* Identify potentialy contributing contours */
    if (((op == PLGCLIP_INTERSECT) || (op == PLGCLIP_DIFFER))
   && (subj->numparts > 0) && (clip->numparts > 0))
        minimax_test(subj, clip, op);

    /* Build LMT */
    if (subj->numparts > 0)
        s_heap= build_lmt(&lmt, &sbtree, &sbt_entries, subj, SUBJ, op);
    if (clip->numparts > 0)
        c_heap= build_lmt(&lmt, &sbtree, &sbt_entries, clip, CLIP, op);

    /* Return a 0 result if no contours contribute */
    if (lmt == 0)
    {
        result->num_strips= 0;
        result->strip= 0;
        reset_lmt(&lmt);
        FREE_S(s_heap);
        FREE_S(c_heap);
        return;
    }

    /* Build scanbeam table from scanbeam tree */
    MALLOC_S(sbt, double, sbt_entries, __FUNCTION__, __LINE__);
    build_sbt(&scanbeam, sbt, sbtree);
    scanbeam= 0;
    free_sbtree(&sbtree);

    /* Invert clip polygon for difference operation */
    if (op == PLGCLIP_DIFFER)
        parity[CLIP]= RIGHT;

    local_min= lmt;

    /* Process each scanbeam */
    while (scanbeam < sbt_entries)
    {
        /* Set yb and yt to the bottom and top of the scanbeam */
        yb= sbt[scanbeam++];
        if (scanbeam < sbt_entries)
        {
            yt= sbt[scanbeam];
            dy= yt - yb;
        }

        /* === SCANBEAM BOUNDARY PROCESSING ================================ */

        /* If LMT node corresponding to yb exists */
        if (local_min)
        {
            if (local_min->y == yb)
            {
                /* Add edges starting at this local minimum to the AET */
                for (edge= local_min->first_bound; edge; edge= edge->next_bound)
                    add_edge_to_aet(&aet, edge, 0);

                local_min= local_min->next;
            }
        }

        /* Set dummy previous x value */
        px= -DBL_MAX;

        /* Create bundles within AET */
        e0= aet;
        e1= aet;

        /* Set up bundle fields of first edge */
        aet->bundle[ABOVE][ aet->type]= (aet->top.y != yb);
        aet->bundle[ABOVE][!aet->type]= PLGC_FALSE;
        aet->bstate[ABOVE]= UNBUNDLED;

        for (next_edge= aet->next; next_edge; next_edge= next_edge->next)
        {
            /* Set up bundle fields of next edge */
            next_edge->bundle[ABOVE][ next_edge->type]= (next_edge->top.y != yb);
            next_edge->bundle[ABOVE][!next_edge->type]= PLGC_FALSE;
            next_edge->bstate[ABOVE]= UNBUNDLED;

            /* Bundle edges above the scanbeam boundary if they coincide */
            if (next_edge->bundle[ABOVE][next_edge->type])
            {
                if (EQUAL(e0->xb, next_edge->xb) && EQUAL(e0->dx, next_edge->dx)
   && (e0->top.y != yb))
                {
                    next_edge->bundle[ABOVE][ next_edge->type]^= 
                        e0->bundle[ABOVE][ next_edge->type];
                    next_edge->bundle[ABOVE][!next_edge->type]= 
                        e0->bundle[ABOVE][!next_edge->type]; 
                    next_edge->bstate[ABOVE]= BUNDLE_HEAD;
                    e0->bundle[ABOVE][CLIP]= PLGC_FALSE;
                    e0->bundle[ABOVE][SUBJ]= PLGC_FALSE;
                    e0->bstate[ABOVE]= BUNDLE_TAIL;
                }
                e0= next_edge;
            }
        }

        horiz[CLIP]= NH;
        horiz[SUBJ]= NH;

        /* Process each edge at this scanbeam boundary */
        for (edge= aet; edge; edge= edge->next)
        {
            exists[CLIP]= edge->bundle[ABOVE][CLIP] + 
                   (edge->bundle[BELOW][CLIP] << 1);
            exists[SUBJ]= edge->bundle[ABOVE][SUBJ] + 
                   (edge->bundle[BELOW][SUBJ] << 1);

            if (exists[CLIP] || exists[SUBJ])
            {
                /* Set bundle side */
                edge->bside[CLIP]= parity[CLIP];
                edge->bside[SUBJ]= parity[SUBJ];

                /* Determine contributing status and quadrant occupancies */
                switch (op)
                {
                case PLGCLIP_DIFFER:
                case PLGCLIP_INTERSECT:
                    contributing= (exists[CLIP] && (parity[SUBJ] || horiz[SUBJ]))
                     || (exists[SUBJ] && (parity[CLIP] || horiz[CLIP]))
                     || (exists[CLIP] && exists[SUBJ]
                     && (parity[CLIP] == parity[SUBJ]));
                    br= (parity[CLIP])
           && (parity[SUBJ]);
                    bl= (parity[CLIP] ^ edge->bundle[ABOVE][CLIP])
           && (parity[SUBJ] ^ edge->bundle[ABOVE][SUBJ]);
                    tr= (parity[CLIP] ^ (horiz[CLIP]!=NH))
           && (parity[SUBJ] ^ (horiz[SUBJ]!=NH));
                    tl= (parity[CLIP] ^ (horiz[CLIP]!=NH) ^ edge->bundle[BELOW][CLIP]) 
           && (parity[SUBJ] ^ (horiz[SUBJ]!=NH) ^ edge->bundle[BELOW][SUBJ]);
                    break;
                case PLGCLIP_XOR:
                    contributing= exists[CLIP] || exists[SUBJ];
                    br= (parity[CLIP])
                        ^ (parity[SUBJ]);
                    bl= (parity[CLIP] ^ edge->bundle[ABOVE][CLIP])
                        ^ (parity[SUBJ] ^ edge->bundle[ABOVE][SUBJ]);
                    tr= (parity[CLIP] ^ (horiz[CLIP]!=NH))
                        ^ (parity[SUBJ] ^ (horiz[SUBJ]!=NH));
                    tl= (parity[CLIP] ^ (horiz[CLIP]!=NH) ^ edge->bundle[BELOW][CLIP])
                        ^ (parity[SUBJ] ^ (horiz[SUBJ]!=NH) ^ edge->bundle[BELOW][SUBJ]);
                    break;
                case PLGCLIP_UNION:
                    contributing= (exists[CLIP] && (!parity[SUBJ] || horiz[SUBJ]))
                     || (exists[SUBJ] && (!parity[CLIP] || horiz[CLIP]))
                     || (exists[CLIP] && exists[SUBJ]
                     && (parity[CLIP] == parity[SUBJ]));
                    br= (parity[CLIP])
           || (parity[SUBJ]);
                    bl= (parity[CLIP] ^ edge->bundle[ABOVE][CLIP])
           || (parity[SUBJ] ^ edge->bundle[ABOVE][SUBJ]);
                    tr= (parity[CLIP] ^ (horiz[CLIP]!=NH))
           || (parity[SUBJ] ^ (horiz[SUBJ]!=NH));
                    tl= (parity[CLIP] ^ (horiz[CLIP]!=NH) ^ edge->bundle[BELOW][CLIP])
           || (parity[SUBJ] ^ (horiz[SUBJ]!=NH) ^ edge->bundle[BELOW][SUBJ]);
                    break;
                }

                /* Update parity */
                parity[CLIP]^= edge->bundle[ABOVE][CLIP];
                parity[SUBJ]^= edge->bundle[ABOVE][SUBJ];

                /* Update horizontal state */
                if (exists[CLIP])         
                    horiz[CLIP]=
                        next_h_state[horiz[CLIP]]
                                                [((exists[CLIP] - 1) << 1) + parity[CLIP]];
                if (exists[SUBJ])         
                    horiz[SUBJ]=
                        next_h_state[horiz[SUBJ]]
                                                [((exists[SUBJ] - 1) << 1) + parity[SUBJ]];
        
                vclass= tr + (tl << 1) + (br << 2) + (bl << 3);

                if (contributing)
                {
                    xb= edge->xb;

                    switch (vclass)
                    {
                    case EMN:
                        new_tristrip(&tlist, edge, xb, yb);
                        cf= edge;
                        break;
                    case ERI:
                        edge->outp[ABOVE]= cf->outp[ABOVE];
                        if (xb != cf->xb)
                            ADD_VERT(edge, ABOVE, RIGHT, xb, yb);
                        cf= 0;
                        break;
                    case ELI:
                        ADD_VERT(edge, BELOW, LEFT, xb, yb);
                        edge->outp[ABOVE]= 0;
                        cf= edge;
                        break;
                    case EMX:
                        if (xb != cf->xb)
                            ADD_VERT(edge, BELOW, RIGHT, xb, yb);
                        edge->outp[ABOVE]= 0;
                        cf= 0;
                        break;
                    case IMN:
                        if (cft == LED)
            {
                            if (cf->bot.y != yb)
                                ADD_VERT(cf, BELOW, LEFT, cf->xb, yb);
                            new_tristrip(&tlist, cf, cf->xb, yb);
            }
                        edge->outp[ABOVE]= cf->outp[ABOVE];
                        ADD_VERT(edge, ABOVE, RIGHT, xb, yb);
                        break;
                    case ILI:
                        new_tristrip(&tlist, edge, xb, yb);
                        cf= edge;
                        cft= ILI;
                        break;
                    case IRI:
                        if (cft == LED)
            {
                            if (cf->bot.y != yb)
                                ADD_VERT(cf, BELOW, LEFT, cf->xb, yb);
                            new_tristrip(&tlist, cf, cf->xb, yb);
            }
                        ADD_VERT(edge, BELOW, RIGHT, xb, yb);
                        edge->outp[ABOVE]= 0;
                        break;
                    case IMX:
                        ADD_VERT(edge, BELOW, LEFT, xb, yb);
                        edge->outp[ABOVE]= 0;
                        cft= IMX;
                        break;
        case IMM:
                        ADD_VERT(edge, BELOW, LEFT, xb, yb);
                        edge->outp[ABOVE]= cf->outp[ABOVE];
                        if (xb != cf->xb)
                            ADD_VERT(cf, ABOVE, RIGHT, xb, yb);
                        cf= edge;
                        break;
                    case EMM:
                        ADD_VERT(edge, BELOW, RIGHT, xb, yb);
                        edge->outp[ABOVE]= 0;
                        new_tristrip(&tlist, edge, xb, yb);
                        cf= edge;
                        break;
                    case LED:
                        if (edge->bot.y == yb)
                            ADD_VERT(edge, BELOW, LEFT, xb, yb);
                        edge->outp[ABOVE]= edge->outp[BELOW];
                        cf= edge;
                        cft= LED;
                        break;
                    case RED:
                        edge->outp[ABOVE]= cf->outp[ABOVE];
                        if (cft == LED)
            {
                            if (cf->bot.y == yb)
                {
                                ADD_VERT(edge, BELOW, RIGHT, xb, yb);
                }
                            else
                {
                                if (edge->bot.y == yb)
        {
                                    ADD_VERT(cf, BELOW, LEFT, cf->xb, yb);
                                    ADD_VERT(edge, BELOW, RIGHT, xb, yb);
        }
                }
            }
                        else
            {
                            ADD_VERT(edge, BELOW, RIGHT, xb, yb);
                            ADD_VERT(edge, ABOVE, RIGHT, xb, yb);
            }
                        cf= 0;
                        break;
                    default:
                        break;
                    } /* End of switch */
                } /* End of contributing conditional */
            } /* End of edge exists conditional */
        } /* End of AET loop */

        /* Delete terminating edges from the AET, otherwise compute xt */
        for (edge= aet; edge; edge= edge->next)
        {
            if (edge->top.y == yb)
            {
                prev_edge= edge->prev;
                next_edge= edge->next;
                if (prev_edge)
                    prev_edge->next= next_edge;
                else
                    aet= next_edge;
                if (next_edge)
                    next_edge->prev= prev_edge;

                /* Copy bundle head state to the adjacent tail edge if required */
                if ((edge->bstate[BELOW] == BUNDLE_HEAD) && prev_edge)
    {
                    if (prev_edge->bstate[BELOW] == BUNDLE_TAIL)
                    {
                        prev_edge->outp[BELOW]= edge->outp[BELOW];
                        prev_edge->bstate[BELOW]= UNBUNDLED;
                        if (prev_edge->prev)
                            if (prev_edge->prev->bstate[BELOW] == BUNDLE_TAIL)
                                prev_edge->bstate[BELOW]= BUNDLE_HEAD;
        }
    }
            }
            else
            {
                if (edge->top.y == yt)
                    edge->xt= edge->top.x;
                else
                    edge->xt= edge->bot.x + edge->dx * (yt - edge->bot.y);
            }
        }

        if (scanbeam < sbt_entries)
        {
            /* === SCANBEAM INTERIOR PROCESSING ============================== */
  
            build_intersection_table(&it, aet, dy);

            /* Process each node in the intersection table */
            for (intersect= it; intersect; intersect= intersect->next)
            {
                e0= intersect->ie[0];
                e1= intersect->ie[1];

                /* Only generate output for contributing intersections */
                if ((e0->bundle[ABOVE][CLIP] || e0->bundle[ABOVE][SUBJ])
         && (e1->bundle[ABOVE][CLIP] || e1->bundle[ABOVE][SUBJ]))
    {
                    p= e0->outp[ABOVE];
                    q= e1->outp[ABOVE];
                    ix= intersect->point.x;
                    iy= intersect->point.y + yb;

                    in[CLIP]= ( e0->bundle[ABOVE][CLIP] && !e0->bside[CLIP])
                 || ( e1->bundle[ABOVE][CLIP] &&  e1->bside[CLIP])
                 || (!e0->bundle[ABOVE][CLIP] && !e1->bundle[ABOVE][CLIP]
                     && e0->bside[CLIP] && e1->bside[CLIP]);
                    in[SUBJ]= ( e0->bundle[ABOVE][SUBJ] && !e0->bside[SUBJ])
                 || ( e1->bundle[ABOVE][SUBJ] &&  e1->bside[SUBJ])
                 || (!e0->bundle[ABOVE][SUBJ] && !e1->bundle[ABOVE][SUBJ]
                     && e0->bside[SUBJ] && e1->bside[SUBJ]);

                    /* Determine quadrant occupancies */
                    switch (op)
                    {
                    case PLGCLIP_DIFFER:
                    case PLGCLIP_INTERSECT:
                        tr= (in[CLIP])
             && (in[SUBJ]);
                        tl= (in[CLIP] ^ e1->bundle[ABOVE][CLIP])
             && (in[SUBJ] ^ e1->bundle[ABOVE][SUBJ]);
                        br= (in[CLIP] ^ e0->bundle[ABOVE][CLIP])
             && (in[SUBJ] ^ e0->bundle[ABOVE][SUBJ]);
                        bl= (in[CLIP] ^ e1->bundle[ABOVE][CLIP] ^ e0->bundle[ABOVE][CLIP])
             && (in[SUBJ] ^ e1->bundle[ABOVE][SUBJ] ^ e0->bundle[ABOVE][SUBJ]);
                        break;
                    case PLGCLIP_XOR:
                        tr= (in[CLIP])
                            ^ (in[SUBJ]);
                        tl= (in[CLIP] ^ e1->bundle[ABOVE][CLIP])
                            ^ (in[SUBJ] ^ e1->bundle[ABOVE][SUBJ]);
                        br= (in[CLIP] ^ e0->bundle[ABOVE][CLIP])
                            ^ (in[SUBJ] ^ e0->bundle[ABOVE][SUBJ]);
                        bl= (in[CLIP] ^ e1->bundle[ABOVE][CLIP] ^ e0->bundle[ABOVE][CLIP])
                            ^ (in[SUBJ] ^ e1->bundle[ABOVE][SUBJ] ^ e0->bundle[ABOVE][SUBJ]);
                        break;
                    case PLGCLIP_UNION:
                        tr= (in[CLIP])
             || (in[SUBJ]);
                        tl= (in[CLIP] ^ e1->bundle[ABOVE][CLIP])
             || (in[SUBJ] ^ e1->bundle[ABOVE][SUBJ]);
                        br= (in[CLIP] ^ e0->bundle[ABOVE][CLIP])
             || (in[SUBJ] ^ e0->bundle[ABOVE][SUBJ]);
                        bl= (in[CLIP] ^ e1->bundle[ABOVE][CLIP] ^ e0->bundle[ABOVE][CLIP])
             || (in[SUBJ] ^ e1->bundle[ABOVE][SUBJ] ^ e0->bundle[ABOVE][SUBJ]);
                        break;
                    }

                    vclass= tr + (tl << 1) + (br << 2) + (bl << 3);

                    switch (vclass)
                    {
                    case EMN:
                        new_tristrip(&tlist, e1, ix, iy);
                        e0->outp[ABOVE]= e1->outp[ABOVE];
                        break;
                    case ERI:
                        if (p)
                        {
                            P_EDGE(prev_edge, e0, ABOVE, px, iy);
                            ADD_VERT(prev_edge, ABOVE, LEFT, px, iy);
                            ADD_VERT(e0, ABOVE, RIGHT, ix, iy);
                            e1->outp[ABOVE]= e0->outp[ABOVE];
                            e0->outp[ABOVE]= 0;
                        }
                        break;
                    case ELI:
                        if (q)
                        {
                            N_EDGE(next_edge, e1, ABOVE, nx, iy);
                            ADD_VERT(e1, ABOVE, LEFT, ix, iy);
                            ADD_VERT(next_edge, ABOVE, RIGHT, nx, iy);
                            e0->outp[ABOVE]= e1->outp[ABOVE];
                            e1->outp[ABOVE]= 0;
                        }
                        break;
                    case EMX:
                        if (p && q)
                        {
                            ADD_VERT(e0, ABOVE, LEFT, ix, iy);
                            e0->outp[ABOVE]= 0;
                            e1->outp[ABOVE]= 0;
                        }
                        break;
                    case IMN:
                        P_EDGE(prev_edge, e0, ABOVE, px, iy);
                        ADD_VERT(prev_edge, ABOVE, LEFT, px, iy);
                        N_EDGE(next_edge, e1, ABOVE, nx, iy);
                        ADD_VERT(next_edge, ABOVE, RIGHT, nx, iy);
                        new_tristrip(&tlist, prev_edge, px, iy); 
                        e1->outp[ABOVE]= prev_edge->outp[ABOVE];
                        ADD_VERT(e1, ABOVE, RIGHT, ix, iy);
                        new_tristrip(&tlist, e0, ix, iy);
                        next_edge->outp[ABOVE]= e0->outp[ABOVE];
                        ADD_VERT(next_edge, ABOVE, RIGHT, nx, iy);
                        break;
                    case ILI:
                        if (p)
                        {
                            ADD_VERT(e0, ABOVE, LEFT, ix, iy);
                            N_EDGE(next_edge, e1, ABOVE, nx, iy);
                            ADD_VERT(next_edge, ABOVE, RIGHT, nx, iy);
                            e1->outp[ABOVE]= e0->outp[ABOVE];
                            e0->outp[ABOVE]= 0;
                        }
                        break;
                    case IRI:
                        if (q)
                        {
                            ADD_VERT(e1, ABOVE, RIGHT, ix, iy);
                            P_EDGE(prev_edge, e0, ABOVE, px, iy);
                            ADD_VERT(prev_edge, ABOVE, LEFT, px, iy);
                            e0->outp[ABOVE]= e1->outp[ABOVE];
                            e1->outp[ABOVE]= 0;
                        }
                        break;
                    case IMX:
                        if (p && q)
                        {
                            ADD_VERT(e0, ABOVE, RIGHT, ix, iy);
                            ADD_VERT(e1, ABOVE, LEFT, ix, iy);
                            e0->outp[ABOVE]= 0;
                            e1->outp[ABOVE]= 0;
                            P_EDGE(prev_edge, e0, ABOVE, px, iy);
                            ADD_VERT(prev_edge, ABOVE, LEFT, px, iy);
                            new_tristrip(&tlist, prev_edge, px, iy);
                            N_EDGE(next_edge, e1, ABOVE, nx, iy);
                            ADD_VERT(next_edge, ABOVE, RIGHT, nx, iy);
                            next_edge->outp[ABOVE]= prev_edge->outp[ABOVE];
                            ADD_VERT(next_edge, ABOVE, RIGHT, nx, iy);
                        }
                        break;
                    case IMM:
                        if (p && q)
                        {
                            ADD_VERT(e0, ABOVE, RIGHT, ix, iy);
                            ADD_VERT(e1, ABOVE, LEFT, ix, iy);
                            P_EDGE(prev_edge, e0, ABOVE, px, iy);
                            ADD_VERT(prev_edge, ABOVE, LEFT, px, iy);
                            new_tristrip(&tlist, prev_edge, px, iy);
                            N_EDGE(next_edge, e1, ABOVE, nx, iy);
                            ADD_VERT(next_edge, ABOVE, RIGHT, nx, iy);
                            e1->outp[ABOVE]= prev_edge->outp[ABOVE];
                            ADD_VERT(e1, ABOVE, RIGHT, ix, iy);
                            new_tristrip(&tlist, e0, ix, iy);
                            next_edge->outp[ABOVE]= e0->outp[ABOVE];
                            ADD_VERT(next_edge, ABOVE, RIGHT, nx, iy);
                        }
                        break;
                    case EMM:
                        if (p && q)
                        {
                            ADD_VERT(e0, ABOVE, LEFT, ix, iy);
                            new_tristrip(&tlist, e1, ix, iy);
                            e0->outp[ABOVE]= e1->outp[ABOVE];
                        }
                        break;
                    default:
                        break;
                    } /* End of switch */
    } /* End of contributing intersection conditional */

                /* Swap bundle sides in response to edge crossing */
                if (e0->bundle[ABOVE][CLIP])
        e1->bside[CLIP]= !e1->bside[CLIP];
                if (e1->bundle[ABOVE][CLIP])
        e0->bside[CLIP]= !e0->bside[CLIP];
                if (e0->bundle[ABOVE][SUBJ])
        e1->bside[SUBJ]= !e1->bside[SUBJ];
                if (e1->bundle[ABOVE][SUBJ])
        e0->bside[SUBJ]= !e0->bside[SUBJ];

                /* Swap e0 and e1 bundles in the AET */
                prev_edge= e0->prev;
                next_edge= e1->next;
                if (e1->next)
                    e1->next->prev= e0;

                if (e0->bstate[ABOVE] == BUNDLE_HEAD)
                {
                    search= PLGC_TRUE;
                    while (search)
                    {
                        prev_edge= prev_edge->prev;
                        if (prev_edge)
                        {
                            if (prev_edge->bundle[ABOVE][CLIP]
               || prev_edge->bundle[ABOVE][SUBJ]
               || (prev_edge->bstate[ABOVE] == BUNDLE_HEAD))
                                search= PLGC_FALSE;
                        }
                        else
                            search= PLGC_FALSE;
                    }
                }
                if (!prev_edge)
                {
           e1->next= aet;
           aet= e0->next;
                }
                else
                {
                    e1->next= prev_edge->next;
                    prev_edge->next= e0->next;
                }
                e0->next->prev= prev_edge;
                e1->next->prev= e1;
                e0->next= next_edge;
            } /* End of IT loop*/

            /* Prepare for next scanbeam */
            for (edge= aet; edge; edge= next_edge)
            {
                next_edge= edge->next;
                succ_edge= edge->succ;

                if ((edge->top.y == yt) && succ_edge)
                {
                    /* Replace AET edge by its successor */
                    succ_edge->outp[BELOW]= edge->outp[ABOVE];
                    succ_edge->bstate[BELOW]= edge->bstate[ABOVE];
                    succ_edge->bundle[BELOW][CLIP]= edge->bundle[ABOVE][CLIP];
                    succ_edge->bundle[BELOW][SUBJ]= edge->bundle[ABOVE][SUBJ];
                    prev_edge= edge->prev;
                    if (prev_edge)
                        prev_edge->next= succ_edge;
                    else
                        aet= succ_edge;
                    if (next_edge)
                        next_edge->prev= succ_edge;
                    succ_edge->prev= prev_edge;
                    succ_edge->next= next_edge;
                }
                else
                {
                    /* Update this edge */
                    edge->outp[BELOW]= edge->outp[ABOVE];
                    edge->bstate[BELOW]= edge->bstate[ABOVE];
                    edge->bundle[BELOW][CLIP]= edge->bundle[ABOVE][CLIP];
                    edge->bundle[BELOW][SUBJ]= edge->bundle[ABOVE][SUBJ];
                    edge->xb= edge->xt;
                }
                edge->outp[ABOVE]= 0;
            }
        }
    } /* === END OF SCANBEAM PROCESSING ================================== */

    /* Generate result tristrip from tlist */
    result->strip= 0;
    result->num_strips= count_tristrips(tlist);
    if (result->num_strips > 0)
    {
        MALLOC_S(result->strip, vertexlist_t, result->num_strips,
            __FUNCTION__, __LINE__);

        s= 0;
        for (tn= tlist; tn; tn= tnn)
        {
            tnn= tn->next;

            if (tn->active > 2)
            {
                /* Valid tristrip: copy the vertices and free the heap */
                result->strip[s].numverts= tn->active;
                MALLOC_S(result->strip[s].vertex, vertex_t, tn->active,
                    __FUNCTION__, __LINE__);
                v= 0;
                if (INVERT_TRISTRIPS)
                {
                    lt= tn->v[RIGHT];
                    rt= tn->v[LEFT];
                }
                else
                {
                    lt= tn->v[LEFT];
                    rt= tn->v[RIGHT];
                }
                while (lt || rt)
                {
                    if (lt)
                    {
                        ltn= lt->next;
                        result->strip[s].vertex[v].x= lt->x;
                        result->strip[s].vertex[v].y= lt->y;
                        v++;
                        FREE_S(lt);
                        lt= ltn;
                    }
                    if (rt)
                    {
                        rtn= rt->next;
                        result->strip[s].vertex[v].x= rt->x;
                        result->strip[s].vertex[v].y= rt->y;
                        v++;
                        FREE_S(rt);
                        rt= rtn;
                    }
                }
                s++;
            }
            else
            {
                /* Invalid tristrip: just free the heap */
                for (lt= tn->v[LEFT]; lt; lt= ltn)
                {
                    ltn= lt->next;
                    FREE_S(lt);
                }
                for (rt= tn->v[RIGHT]; rt; rt=rtn)
                {
                    rtn= rt->next;
                    FREE_S(rt);
                }
            }
            FREE_S(tn);
        }
    }

    /* Tidy up */
    reset_it(&it);
    reset_lmt(&lmt);
    FREE_S(c_heap);
    FREE_S(s_heap);
    FREE_S(sbt);
}

static void gpc_polygon_to_tristrip(polygon_t *s, tristrip_t *t)
{
    polygon_t c;

    c.numparts= 0;
    c.holes= 0;
    c.parts= 0;
    gpc_tristrip_clip(PLGCLIP_DIFFER, s, &c, t);
}


/*=============================================================================
 *
 *                            Polygon Public APIs
 *
 *=============================================================================
 */

HPOLYGON PolygonAlloc (double tolerance, HPOLYGON *polygons, int num)
{
    polygon_t *plg = (polygon_t *) calloc (num, sizeof(polygon_t));
 
    if (plg) {
        PLGC_EPSILON = tolerance;

        if (num == 1) {
            if (polygons) {
                *polygons = plg;
            }
            return plg;
        } else {
            int i = 0;
            for (; i < num; i++) {
                polygons[i] = plg + i;
            }
            return plg;
        }
    }
    return 0;
}


void PolygonFree (HPOLYGON *polygons, int num)
{
    if (polygons) { 
        polygon_t *plg;
        int i = 0;
        for (; i < num; i++) {
            plg = polygons[i];
            if (plg) {
                PolygonClear(plg);
            }
        }
        plg = *polygons;
        *polygons = 0;
        free(plg);
    }
}

void PolygonClear (polygon_t *p)
{
    if (p) {
        int i = 0;
        for (; i < p->numparts; i++) {
            FREE_S(p->parts[i].vertex);
        }
        FREE_S(p->holes);
        FREE_S(p->parts);
        p->numparts= 0;
    }
}


/* ^ y
 * |
 * 3-------2
 * |       |
 * | rect  |
 * |       |
 * 0-------1---> x
 */
void PolygonBuildRect (HPOLYGON p, const RectType *rect)
{
    vertex_t *vl;

    PolygonClear(p);

    vl = (vertex_t *) malloc(4 * sizeof(vertex_t));

    vl[0].x = rect->xmin;
    vl[0].y = rect->ymin;
    vl[1].x = rect->xmax;
    vl[1].y = rect->ymin;
    vl[2].x = rect->xmax;
    vl[2].y = rect->ymax;
    vl[3].x = rect->xmin;
    vl[3].y = rect->ymax;

    PolygonAddPart (p, 4, vl, PLGPART_NOTHOLE, PLGC_TRUE); 
}


PLGC_BOOL PolygonEmpty (const HPOLYGON p)
{
    if (! p || ! p->parts || ! p->numparts || ! p->holes) {
        return PLGC_TRUE;
    } else {
        return PLGC_FALSE;
    }
}


void PolygonAddPart (polygon_t *p,
    int  numverts,
    vertex_t  *vertlist,
    PLGPART_TYPE hole,
    PLGC_BOOL attached)
{
    int             *extended_hole, c;
    vertexlist_t *extended_contour;

    /* Create an extended hole array */
    MALLOC_S(extended_hole, int, (p->numparts + 1), __FUNCTION__, __LINE__);

    /* Create an extended contour array */
    MALLOC_S(extended_contour, vertexlist_t, (p->numparts + 1),
        __FUNCTION__, __LINE__);

    /* Copy the old contour and hole data into the extended arrays */
    for (c= 0; c < p->numparts; c++)
    {
        extended_hole[c]= p->holes[c];
        extended_contour[c]= p->parts[c];
    }

    /* Copy the new contour and hole onto the end of the extended arrays */
    c= p->numparts;
    extended_hole[c]= hole;
    extended_contour[c].numverts= numverts;

    if (attached) {
        extended_contour[c].vertex = vertlist;
    } else {
        MALLOC_S(extended_contour[c].vertex, vertex_t, numverts,
            __FUNCTION__, __LINE__);
        memcpy(extended_contour[c].vertex, vertlist, numverts*sizeof(vertex_t));
    }

    /* Dispose of the old contour */
    FREE_S(p->parts);
    FREE_S(p->holes);

    /* Update the polygon information */
    p->numparts++;
    p->holes= extended_hole;
    p->parts= extended_contour;
}


void PolygonClip (PLGCLIP_MODE op,
    HPOLYGON subj, HPOLYGON clip, HPOLYGON result)
{
    sb_tree       *sbtree= 0;
    it_node       *it= 0, *intersect;
    edge_node     *edge, *prev_edge, *next_edge, *succ_edge, *e0, *e1;
    edge_node     *aet= 0, *c_heap= 0, *s_heap= 0;
    lmt_node      *lmt= 0, *local_min;
    polygon_node  *out_poly= 0, *p, *q, *poly, *npoly, *cf= 0;
    vertex_node   *vtx, *nv;
    hedge_state    horiz[2];

    int            in[2], exists[2], parity[2]= {LEFT, LEFT};
    int            c, v, contributing, search, scanbeam= 0, sbt_entries= 0;
    int            vclass, bl, br, tl, tr;

    double        *sbt= 0, xb, px, yb, yt, dy, ix, iy;

    /* Test for trivial 0 result cases */
    if (((subj->numparts == 0) && (clip->numparts == 0))
   || ((subj->numparts == 0) && ((op == PLGCLIP_INTERSECT) ||
            (op == PLGCLIP_DIFFER)))
   || ((clip->numparts == 0) &&  (op == PLGCLIP_INTERSECT))) {
        result->numparts= 0;
        result->holes= 0;
        result->parts= 0;
        return;
    }

    /* Identify potentialy contributing contours */
    if (((op == PLGCLIP_INTERSECT) || (op == PLGCLIP_DIFFER))
        && (subj->numparts > 0) && (clip->numparts > 0)) {
        minimax_test(subj, clip, op);
    }

    /* Build LMT */
    if (subj->numparts > 0) {
        s_heap= build_lmt(&lmt, &sbtree, &sbt_entries, subj, SUBJ, op);
    }
    if (clip->numparts > 0) {
        c_heap= build_lmt(&lmt, &sbtree, &sbt_entries, clip, CLIP, op);
    }

    /* Return a 0 result if no contours contribute */
    if (lmt == 0) {
        result->numparts= 0;
        result->holes= 0;
        result->parts= 0;
        reset_lmt(&lmt);
        FREE_S(s_heap);
        FREE_S(c_heap);
        return;
    }

    /* Build scanbeam table from scanbeam tree */
    MALLOC_S(sbt, double, sbt_entries, __FUNCTION__, __LINE__);
    build_sbt(&scanbeam, sbt, sbtree);
    scanbeam= 0;
    free_sbtree(&sbtree);

    /* Allow pointer re-use without causing memory leak */
    if (subj == result) {
        PolygonClear(subj);
    }
    if (clip == result) {
        PolygonClear(clip);
    }

    /* Invert clip polygon for difference operation */
    if (op == PLGCLIP_DIFFER) {
        parity[CLIP]= RIGHT;
    }

    local_min= lmt;

    /* Process each scanbeam */
    while (scanbeam < sbt_entries) {
        /* Set yb and yt to the bottom and top of the scanbeam */
        yb= sbt[scanbeam++];
        if (scanbeam < sbt_entries) {
            yt= sbt[scanbeam];
            dy= yt - yb;
        }

        /* If LMT node corresponding to yb exists */
        if (local_min) {
            if (local_min->y == yb) {
                /* Add edges starting at this local minimum to the AET */
                for (edge= local_min->first_bound; edge; edge= edge->next_bound) {
                    add_edge_to_aet(&aet, edge, 0);
                }

                local_min= local_min->next;
            }
        }

        /* Set dummy previous x value */
        px= -DBL_MAX;

        /* Create bundles within AET */
        e0= aet;
        e1= aet;

        /* Set up bundle fields of first edge */
        aet->bundle[ABOVE][ aet->type]= (aet->top.y != yb);
        aet->bundle[ABOVE][!aet->type]= PLGC_FALSE;
        aet->bstate[ABOVE]= UNBUNDLED;

        for (next_edge= aet->next; next_edge; next_edge= next_edge->next) {
            /* Set up bundle fields of next edge */
            next_edge->bundle[ABOVE][ next_edge->type]= (next_edge->top.y != yb);
            next_edge->bundle[ABOVE][!next_edge->type]= PLGC_FALSE;
            next_edge->bstate[ABOVE]= UNBUNDLED;

            /* Bundle edges above the scanbeam boundary if they coincide */
            if (next_edge->bundle[ABOVE][next_edge->type]) {
                if (EQUAL(e0->xb, next_edge->xb) &&
                    EQUAL(e0->dx, next_edge->dx) && (e0->top.y != yb)) {
                    next_edge->bundle[ABOVE][ next_edge->type]^= 
                        e0->bundle[ABOVE][ next_edge->type];
                    next_edge->bundle[ABOVE][!next_edge->type]= 
                        e0->bundle[ABOVE][!next_edge->type];
                    next_edge->bstate[ABOVE]= BUNDLE_HEAD;
                    e0->bundle[ABOVE][CLIP]= PLGC_FALSE;
                    e0->bundle[ABOVE][SUBJ]= PLGC_FALSE;
                    e0->bstate[ABOVE]= BUNDLE_TAIL;
                }
                e0= next_edge;
            }
        }

        horiz[CLIP]= NH;
        horiz[SUBJ]= NH;

        /* Process each edge at this scanbeam boundary */
        for (edge= aet; edge; edge= edge->next) {
            exists[CLIP]= edge->bundle[ABOVE][CLIP] + 
                   (edge->bundle[BELOW][CLIP] << 1);
            exists[SUBJ]= edge->bundle[ABOVE][SUBJ] + 
                   (edge->bundle[BELOW][SUBJ] << 1);

            if (exists[CLIP] || exists[SUBJ]) {
                /* Set bundle side */
                edge->bside[CLIP]= parity[CLIP];
                edge->bside[SUBJ]= parity[SUBJ];

                /* Determine contributing status and quadrant occupancies */
                switch (op) {
                case PLGCLIP_DIFFER:
                case PLGCLIP_INTERSECT:
                    contributing= (exists[CLIP] && (parity[SUBJ] || horiz[SUBJ]))
                     || (exists[SUBJ] && (parity[CLIP] || horiz[CLIP]))
                     || (exists[CLIP] && exists[SUBJ]
                     && (parity[CLIP] == parity[SUBJ]));
                    br= (parity[CLIP])
           && (parity[SUBJ]);
                    bl= (parity[CLIP] ^ edge->bundle[ABOVE][CLIP])
           && (parity[SUBJ] ^ edge->bundle[ABOVE][SUBJ]);
                    tr= (parity[CLIP] ^ (horiz[CLIP]!=NH))
           && (parity[SUBJ] ^ (horiz[SUBJ]!=NH));
                    tl= (parity[CLIP] ^ (horiz[CLIP]!=NH) ^ edge->bundle[BELOW][CLIP]) 
           && (parity[SUBJ] ^ (horiz[SUBJ]!=NH) ^ edge->bundle[BELOW][SUBJ]);
                    break;
                case PLGCLIP_XOR:
                    contributing= exists[CLIP] || exists[SUBJ];
                    br= (parity[CLIP])
                        ^ (parity[SUBJ]);
                    bl= (parity[CLIP] ^ edge->bundle[ABOVE][CLIP])
                        ^ (parity[SUBJ] ^ edge->bundle[ABOVE][SUBJ]);
                    tr= (parity[CLIP] ^ (horiz[CLIP]!=NH))
                        ^ (parity[SUBJ] ^ (horiz[SUBJ]!=NH));
                    tl= (parity[CLIP] ^ (horiz[CLIP]!=NH) ^ edge->bundle[BELOW][CLIP]) 
                        ^ (parity[SUBJ] ^ (horiz[SUBJ]!=NH) ^ edge->bundle[BELOW][SUBJ]);
                    break;
                case PLGCLIP_UNION:
                    contributing= (exists[CLIP] && (!parity[SUBJ] || horiz[SUBJ]))
                     || (exists[SUBJ] && (!parity[CLIP] || horiz[CLIP]))
                     || (exists[CLIP] && exists[SUBJ]
                     && (parity[CLIP] == parity[SUBJ]));
                    br= (parity[CLIP])
           || (parity[SUBJ]);
                    bl= (parity[CLIP] ^ edge->bundle[ABOVE][CLIP])
           || (parity[SUBJ] ^ edge->bundle[ABOVE][SUBJ]);
                    tr= (parity[CLIP] ^ (horiz[CLIP]!=NH))
           || (parity[SUBJ] ^ (horiz[SUBJ]!=NH));
                    tl= (parity[CLIP] ^ (horiz[CLIP]!=NH) ^ edge->bundle[BELOW][CLIP]) 
           || (parity[SUBJ] ^ (horiz[SUBJ]!=NH) ^ edge->bundle[BELOW][SUBJ]);
                    break;
                }

                /* Update parity */
                parity[CLIP]^= edge->bundle[ABOVE][CLIP];
                parity[SUBJ]^= edge->bundle[ABOVE][SUBJ];

                /* Update horizontal state */
                if (exists[CLIP]) {    
                    horiz[CLIP]=
                        next_h_state[horiz[CLIP]]
                                                [((exists[CLIP] - 1) << 1) + parity[CLIP]];
                }
                if (exists[SUBJ]) {
                    horiz[SUBJ]=
                        next_h_state[horiz[SUBJ]]
                                                [((exists[SUBJ] - 1) << 1) + parity[SUBJ]];
                }

                vclass= tr + (tl << 1) + (br << 2) + (bl << 3);

                if (contributing) {
                    xb= edge->xb;

                    switch (vclass) {
                    case EMN:
                    case IMN:
                        add_local_min(&out_poly, edge, xb, yb);
                        px= xb;
                        cf= edge->outp[ABOVE];
                        break;
                    case ERI:
                        if (xb != px) {
                            add_right(cf, xb, yb);
                            px= xb;
                        }
                        edge->outp[ABOVE]= cf;
                        cf= 0;
                        break;
                    case ELI:
                        add_left(edge->outp[BELOW], xb, yb);
                        px= xb;
                        cf= edge->outp[BELOW];
                        break;
                    case EMX:
                        if (xb != px) {
                            add_left(cf, xb, yb);
                            px= xb;
                        }
                        merge_right(cf, edge->outp[BELOW], out_poly);
                        cf= 0;
                        break;
                    case ILI:
                        if (xb != px) {
                            add_left(cf, xb, yb);
                            px= xb;
                        }
                        edge->outp[ABOVE]= cf;
                        cf= 0;
                        break;
                    case IRI:
                        add_right(edge->outp[BELOW], xb, yb);
                        px= xb;
                        cf= edge->outp[BELOW];
                        edge->outp[BELOW]= 0;
                        break;
                    case IMX:
                        if (xb != px) {
                            add_right(cf, xb, yb);
                            px= xb;
                        }
                        merge_left(cf, edge->outp[BELOW], out_poly);
                        cf= 0;
                        edge->outp[BELOW]= 0;
                        break;
                    case IMM:
                        if (xb != px) {
                            add_right(cf, xb, yb);
                            px= xb;
                        }
                        merge_left(cf, edge->outp[BELOW], out_poly);
                        edge->outp[BELOW]= 0;
                        add_local_min(&out_poly, edge, xb, yb);
                        cf= edge->outp[ABOVE];
                        break;
                    case EMM:
                        if (xb != px) {
                            add_left(cf, xb, yb);
                            px= xb;
                        }
                        merge_right(cf, edge->outp[BELOW], out_poly);
                        edge->outp[BELOW]= 0;
                        add_local_min(&out_poly, edge, xb, yb);
                        cf= edge->outp[ABOVE];
                        break;
                    case LED:
                        if (edge->bot.y == yb) {
                            add_left(edge->outp[BELOW], xb, yb);
                        }
                        edge->outp[ABOVE]= edge->outp[BELOW];
                        px= xb;
                        break;
                    case RED:
                        if (edge->bot.y == yb) {
                            add_right(edge->outp[BELOW], xb, yb);
                        }
                        edge->outp[ABOVE]= edge->outp[BELOW];
                        px= xb;
                        break;
                    default:
                        break;
                    } /* End of switch */
                } /* End of contributing conditional */
            } /* End of edge exists conditional */
        } /* End of AET loop */

        /* Delete terminating edges from the AET, otherwise compute xt */
        for (edge= aet; edge; edge= edge->next) {
            if (edge->top.y == yb) {
                prev_edge= edge->prev;
                next_edge= edge->next;

                if (prev_edge) {
                    prev_edge->next= next_edge;
                } else {
                    aet= next_edge;
                }

                if (next_edge) {
                    next_edge->prev= prev_edge;
                }

                /* Copy bundle head state to the adjacent tail edge if required */
                if ((edge->bstate[BELOW] == BUNDLE_HEAD) && prev_edge) {
                    if (prev_edge->bstate[BELOW] == BUNDLE_TAIL) {
                        prev_edge->outp[BELOW]= edge->outp[BELOW];
                        prev_edge->bstate[BELOW]= UNBUNDLED;
                        if (prev_edge->prev) {
                            if (prev_edge->prev->bstate[BELOW] == BUNDLE_TAIL) {
                                prev_edge->bstate[BELOW]= BUNDLE_HEAD;
                            }
                        }
                    }
                }
            } else {
                if (edge->top.y == yt) {
                    edge->xt= edge->top.x;
                } else {
                    edge->xt= edge->bot.x + edge->dx * (yt - edge->bot.y);
                }
            }
        }

        if (scanbeam < sbt_entries) {
            build_intersection_table(&it, aet, dy);

            /* Process each node in the intersection table */
            for (intersect= it; intersect; intersect= intersect->next) {
                e0= intersect->ie[0];
                e1= intersect->ie[1];

                /* Only generate output for contributing intersections */
                if ((e0->bundle[ABOVE][CLIP] || e0->bundle[ABOVE][SUBJ])
                    && (e1->bundle[ABOVE][CLIP] || e1->bundle[ABOVE][SUBJ])) {
                    p= e0->outp[ABOVE];
                    q= e1->outp[ABOVE];
                    ix= intersect->point.x;
                    iy= intersect->point.y + yb;
 
                    in[CLIP]= ( e0->bundle[ABOVE][CLIP] && !e0->bside[CLIP])
                 || ( e1->bundle[ABOVE][CLIP] &&  e1->bside[CLIP])
                 || (!e0->bundle[ABOVE][CLIP] && !e1->bundle[ABOVE][CLIP]
                     && e0->bside[CLIP] && e1->bside[CLIP]);
                    in[SUBJ]= ( e0->bundle[ABOVE][SUBJ] && !e0->bside[SUBJ])
                 || ( e1->bundle[ABOVE][SUBJ] &&  e1->bside[SUBJ])
                 || (!e0->bundle[ABOVE][SUBJ] && !e1->bundle[ABOVE][SUBJ]
                     && e0->bside[SUBJ] && e1->bside[SUBJ]);
             
                    /* Determine quadrant occupancies */
                    switch (op) {
                    case PLGCLIP_DIFFER:
                    case PLGCLIP_INTERSECT:
                        tr= (in[CLIP])
             && (in[SUBJ]);
                        tl= (in[CLIP] ^ e1->bundle[ABOVE][CLIP])
             && (in[SUBJ] ^ e1->bundle[ABOVE][SUBJ]);
                        br= (in[CLIP] ^ e0->bundle[ABOVE][CLIP])
             && (in[SUBJ] ^ e0->bundle[ABOVE][SUBJ]);
                        bl= (in[CLIP] ^ e1->bundle[ABOVE][CLIP] ^ e0->bundle[ABOVE][CLIP])
             && (in[SUBJ] ^ e1->bundle[ABOVE][SUBJ] ^ e0->bundle[ABOVE][SUBJ]);
                        break;
                    case PLGCLIP_XOR:
                        tr= (in[CLIP])
                            ^ (in[SUBJ]);
                        tl= (in[CLIP] ^ e1->bundle[ABOVE][CLIP])
                            ^ (in[SUBJ] ^ e1->bundle[ABOVE][SUBJ]);
                        br= (in[CLIP] ^ e0->bundle[ABOVE][CLIP])
                            ^ (in[SUBJ] ^ e0->bundle[ABOVE][SUBJ]);
                        bl= (in[CLIP] ^ e1->bundle[ABOVE][CLIP] ^ e0->bundle[ABOVE][CLIP])
                            ^ (in[SUBJ] ^ e1->bundle[ABOVE][SUBJ] ^ e0->bundle[ABOVE][SUBJ]);
                        break;
                    case PLGCLIP_UNION:
                        tr= (in[CLIP])
             || (in[SUBJ]);
                        tl= (in[CLIP] ^ e1->bundle[ABOVE][CLIP])
             || (in[SUBJ] ^ e1->bundle[ABOVE][SUBJ]);
                        br= (in[CLIP] ^ e0->bundle[ABOVE][CLIP])
             || (in[SUBJ] ^ e0->bundle[ABOVE][SUBJ]);
                        bl= (in[CLIP] ^ e1->bundle[ABOVE][CLIP] ^ e0->bundle[ABOVE][CLIP])
             || (in[SUBJ] ^ e1->bundle[ABOVE][SUBJ] ^ e0->bundle[ABOVE][SUBJ]);
                        break;
                    }
    
                    vclass= tr + (tl << 1) + (br << 2) + (bl << 3);

                    switch (vclass) {
                    case EMN:
                        add_local_min(&out_poly, e0, ix, iy);
                        e1->outp[ABOVE]= e0->outp[ABOVE];
                        break;
                    case ERI:
                        if (p) {
                            add_right(p, ix, iy);
                            e1->outp[ABOVE]= p;
                            e0->outp[ABOVE]= 0;
                        }
                        break;
                    case ELI:
                        if (q) {
                            add_left(q, ix, iy);
                            e0->outp[ABOVE]= q;
                            e1->outp[ABOVE]= 0;
                        }
                        break;
                    case EMX:
                        if (p && q) {
                            add_left(p, ix, iy);
                            merge_right(p, q, out_poly);
                            e0->outp[ABOVE]= 0;
                            e1->outp[ABOVE]= 0;
                        }
                        break;
                    case IMN:
                        add_local_min(&out_poly, e0, ix, iy);
                        e1->outp[ABOVE]= e0->outp[ABOVE];
                        break;
                    case ILI:
                        if (p) {
                            add_left(p, ix, iy);
                            e1->outp[ABOVE]= p;
                            e0->outp[ABOVE]= 0;
                        }
                        break;
                    case IRI:
                        if (q) {
                            add_right(q, ix, iy);
                            e0->outp[ABOVE]= q;
                            e1->outp[ABOVE]= 0;
                        }
                        break;
                    case IMX:
                        if (p && q) {
                            add_right(p, ix, iy);
                            merge_left(p, q, out_poly);
                            e0->outp[ABOVE]= 0;
                            e1->outp[ABOVE]= 0;
                        }
                        break;
                    case IMM:
                        if (p && q) {
                            add_right(p, ix, iy);
                            merge_left(p, q, out_poly);
                            add_local_min(&out_poly, e0, ix, iy);
                            e1->outp[ABOVE]= e0->outp[ABOVE];
                        }
                        break;
                    case EMM:
                        if (p && q) {
                            add_left(p, ix, iy);
                            merge_right(p, q, out_poly);
                            add_local_min(&out_poly, e0, ix, iy);
                            e1->outp[ABOVE]= e0->outp[ABOVE];
                        }
                        break;
                    default:
                        break;
                    } /* End of switch */
                } /* End of contributing intersection conditional */

                /* Swap bundle sides in response to edge crossing */
                if (e0->bundle[ABOVE][CLIP]) {
                    e1->bside[CLIP]= !e1->bside[CLIP];
                }
                if (e1->bundle[ABOVE][CLIP]) {
                    e0->bside[CLIP]= !e0->bside[CLIP];
                }
                if (e0->bundle[ABOVE][SUBJ]) {
                    e1->bside[SUBJ]= !e1->bside[SUBJ];
                }
                if (e1->bundle[ABOVE][SUBJ]) {
                    e0->bside[SUBJ]= !e0->bside[SUBJ];
                }

                /* Swap e0 and e1 bundles in the AET */
                prev_edge= e0->prev;
                next_edge= e1->next;
                if (next_edge) {
                    next_edge->prev= e0;
                }

                if (e0->bstate[ABOVE] == BUNDLE_HEAD) {
                    search= PLGC_TRUE;
                    while (search) {
                        prev_edge= prev_edge->prev;
                        if (prev_edge) {
                            if (prev_edge->bstate[ABOVE] != BUNDLE_TAIL) {
                                search= PLGC_FALSE;
                            }
                        } else {
                            search= PLGC_FALSE;
                        }
                    }
                }

                if (!prev_edge) {
                    aet->prev= e1;
                    e1->next= aet;
                    aet= e0->next;
                } else {
                    prev_edge->next->prev= e1;
                    e1->next= prev_edge->next;
                    prev_edge->next= e0->next;
                }
                e0->next->prev= prev_edge;
                e1->next->prev= e1;
                e0->next= next_edge;
            } /* End of IT loop*/

            /* Prepare for next scanbeam */
            for (edge= aet; edge; edge= next_edge) {
                next_edge= edge->next;
                succ_edge= edge->succ;

                if ((edge->top.y == yt) && succ_edge) {
                    /* Replace AET edge by its successor */
                    succ_edge->outp[BELOW]= edge->outp[ABOVE];
                    succ_edge->bstate[BELOW]= edge->bstate[ABOVE];
                    succ_edge->bundle[BELOW][CLIP]= edge->bundle[ABOVE][CLIP];
                    succ_edge->bundle[BELOW][SUBJ]= edge->bundle[ABOVE][SUBJ];
                    prev_edge= edge->prev;

                    if (prev_edge) {
                        prev_edge->next= succ_edge;
                    } else {
                        aet= succ_edge;
                    }

                    if (next_edge) {
                        next_edge->prev= succ_edge;
                    }

                    succ_edge->prev= prev_edge;
                    succ_edge->next= next_edge;
                } else {
                    /* Update this edge */
                    edge->outp[BELOW]= edge->outp[ABOVE];
                    edge->bstate[BELOW]= edge->bstate[ABOVE];
                    edge->bundle[BELOW][CLIP]= edge->bundle[ABOVE][CLIP];
                    edge->bundle[BELOW][SUBJ]= edge->bundle[ABOVE][SUBJ];
                    edge->xb= edge->xt;
                }
                edge->outp[ABOVE]= 0;
            }
        }
    }

    /* Generate result polygon from out_poly */
    result->parts= 0;
    result->holes= 0;
    result->numparts= count_contours(out_poly);
    if (result->numparts > 0) {
        MALLOC_S(result->holes, int, result->numparts, __FUNCTION__, __LINE__);
        MALLOC_S(result->parts, vertexlist_t, result->numparts,
            __FUNCTION__, __LINE__);

        c= 0;
        for (poly = out_poly; poly; poly = npoly) {
            npoly= poly->next;
            if (poly->active) {
                result->holes[c]= poly->actual->hole;
                result->parts[c].numverts= poly->active;
                MALLOC_S(result->parts[c].vertex, vertex_t, result->parts[c].numverts,
                    __FUNCTION__, __LINE__);
      
                v= result->parts[c].numverts - 1;
                for (vtx = poly->actual->v[LEFT]; vtx; vtx = nv) {
                    nv= vtx->next;
                    result->parts[c].vertex[v].x= vtx->x;
                    result->parts[c].vertex[v].y= vtx->y;
                    FREE_S(vtx);
                    v--;
                }
                c++;
            }
            FREE_S(poly);
        }
    } else {
        for (poly = out_poly; poly; poly = npoly) {
            npoly= poly->next;
            FREE_S(poly);
        }
    }

    /* Clean up */
    reset_it(&it);
    reset_lmt(&lmt);

    FREE_S(c_heap);
    FREE_S(s_heap);
    FREE_S(sbt);
}


PLGC_BOOL PolygonRect (const HPOLYGON p, RectType *rect)
{
    int i;
    rect_t rc;

    if (p->numparts == 0) {
        return PLGC_FALSE;
    }

    if (! vertexlist_rect (p->parts[0].vertex, p->parts[0].numverts, rect)) {
        return PLGC_FALSE;
    }

    for (i = 1; i < p->numparts; i++) {
        if (vertexlist_rect (p->parts[i].vertex, p->parts[i].numverts, &rc)) {
            RECT_UNION(*rect, rc);
        }
    }

    return PLGC_TRUE;
}


PLGC_BOOL PtInPolygon (
    const HPOLYGON    p,
    const RectType    *prc,
    double            x,
    double            y)
{
    int i;

    if (prc) {
        if (! PT_IN_RECT(x, y, *prc)) {
            return PLGC_FALSE;
        }
    }

    for (i = 0; i < p->numparts; i++) {
        if (p->holes[i] == PLGPART_CONTOUR) {
            if (vertex_in_poly (x, y, p->parts[i].vertex, p->parts[i].numverts, prc)) {
                return PLGC_TRUE;
            }
        }
    }

    return PLGC_FALSE;
}


PLGC_BOOL PtInPolygonContour (const HPOLYGON p,
    const RectType *prc,
    double x, double y)
{
    int i;

    if (prc) {
        if (! PT_IN_RECT(x, y, *prc)) {
            return PLGC_FALSE;
        }
    }

    for (i = 0; i < p->numparts; i++) {
        if (p->holes[i] == PLGPART_CONTOUR) {
            if (vertex_in_poly (x, y, p->parts[i].vertex, p->parts[i].numverts, prc)) {
                return PLGC_TRUE;
            }
        }
    }

    return PLGC_FALSE;
}


int PolygonGetPart (
    const HPOLYGON    plg,
    int               pid,
    VertexType        **ppverts,
    PLGPART_TYPE      *ptype)
{
    if (! plg) {
        return 0;
    }

    if (pid == -1) {
        return plg->numparts;
    }

    if (pid < 0 || pid >= plg->numparts) {
        return 0;
    }

    if (ppverts) {
        *ppverts = plg->parts[pid].vertex;
    }

    if (ptype) {
        * ((int*) ptype) = plg->holes[pid];
    }

    return plg->parts[pid].numverts;
}


int PolygonGetContour (
    const HPOLYGON    plg,
    int               contourid,
    VertexType        **ppverts)
{
    PLGPART_TYPE parttype;
    int partid, parts, verts, contours = 0;

    parts = PolygonGetPart (plg, -1, 0, 0);

    for (partid = 0; partid < parts; partid++) {
        verts = PolygonGetPart (plg, partid, ppverts, &parttype);
        if (parttype == PLGPART_CONTOUR) {
            if (contourid == contours) {
                return verts;
            }
            contours++;
        }
    }

    return contours;
}

double PolygonArea (const HPOLYGON plg)
{
    if (PolygonEmpty(plg)) {
        return 0;
    } else {
        double a, sum = 0;
        int CCW = 0, i = 0;
        for (; i < plg->numparts; i++) {
            a = vertexlist_area (plg->parts[i].vertex, plg->parts[i].numverts, &CCW);

            if (plg->holes[i] == PLGPART_NOTHOLE) {
                sum += a;
            } else {
                sum -= a;
            }
        }

        return sum;
    }
}


double PolygonLength (const HPOLYGON plg)
{
    if (PolygonEmpty(plg)) {
        return 0;
    } else {
        double l = 0;
        int i = 0, nv, v;

        vertex_t *vl;

        for (; i < plg->numparts; i++) {
            vl = plg->parts[i].vertex;
            nv = plg->parts[i].numverts;

            for (v = 1; v < nv; v++) {
                l += vertex_dist (vl[v-1].x - vl[v].x, vl[v-1].y - vl[v].y);
            }

            l += vertex_dist (vl[v-1].x - vl[0].x, vl[v-1].y - vl[0].y);
        }

        return l;
    }
}
