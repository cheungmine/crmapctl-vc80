/**
 * @file
 *    polygon.h
 *
 * @brief
 *    Polygon API: polygon struct and algorithmic
 *
 * @since
 *    2013-5-24
 */

#ifndef _POLYGON_H_
#define _POLYGON_H_

#ifdef    __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <math.h>
#include <float.h>

#ifndef PLGC_TOLERANCE
#define PLGC_TOLERANCE DBL_EPSILON
#endif

/**
 * PLGC_BOOL
 */
typedef int PLGC_BOOL;
#define PLGC_TRUE    1
#define PLGC_FALSE   0

/**
 * Polygon handle
 */
typedef struct _polygon_t *HPOLYGON;

/**
 * PLGCLIP_MODE
 */
typedef enum
{
    PLGCLIP_DIFFER,   /* Difference */
    PLGCLIP_INTERSECT,  /* Intersection */
    PLGCLIP_XOR,      /* Exclusive OR */
    PLGCLIP_UNION     /* Union */
} PLGCLIP_MODE;

/**
 * PLGPART_ISHOLE
 */
typedef enum
{
    PLGPART_CONTOUR = 0,
    PLGPART_NOTHOLE = PLGPART_CONTOUR,
    PLGPART_HOLE  = 1  
} PLGPART_TYPE;

/**
 * Vertex Type
 */
typedef struct
{
    double  x;
    double  y;
} VertexType;

typedef struct
{
    double  xmin;
    double  ymin;
    double  xmax;
    double  ymax;
} RectType;

#define PT_IN_RECT(x, y, rc) \
    ((x) >= (rc).xmin && (x) < (rc).xmax && (y) >= (rc).ymin && (y) < (rc).ymax)

#define RECT_OFFSET(rc, dx, dy) do { \
        rc.xmin += dx; \
        rc.xmax += dx; \
        rc.ymin += dy; \
        rc.ymax += dy; \
    } while (0)

#define RECT_UNION(result, clip) do { \
        if ((result).xmin > (clip).xmin) \
            (result).xmin = (clip).xmin; \
        if ((result).ymin > (clip).ymin) \
            (result).ymin = (clip).ymin; \
        if ((result).xmax < (clip).xmax) \
            (result).xmax = (clip).xmax; \
        if ((result).ymax < (clip).ymax) \
            (result).ymax = (clip).ymax; \
    } while (0)

/**
 * PolygonAlloc
 * @brief
 *
 * @param
 *
 * @return
 *
 * @retval
 */
extern
HPOLYGON PolygonAlloc (
    double            tolerance,
    HPOLYGON          *polygons,
    int               num);

/**
 * PolygonFree
 * @brief
 *
 * @param
 *
 * @return
 *
 * @retval
 */
extern
void PolygonFree (
    HPOLYGON          *polygons,
    int               num);

/**
 * PolygonClear
 * @brief
 *
 * @param
 *
 * @return
 *
 * @retval
 */
extern
void PolygonClear (
    HPOLYGON          polygon);

/**
 * PolygonBuildRect
 * @brief
 *
 * @param
 *
 * @return
 *
 * @retval
 */
extern
void PolygonBuildRect (
    HPOLYGON          polygon,
    const RectType    *rect);

/**
 * PolygonEmpty
 * @brief
 *    Test if polygon is empty
 *
 * @param
 *
 * @return
 *
 * @retval
 */
extern
PLGC_BOOL PolygonEmpty (
    const HPOLYGON    polygon);


/**
 * PolygonAddPart
 * @brief
 *
 * @param
 *
 * @return
 *
 * @retval
 */
extern
void PolygonAddPart (
    HPOLYGON          polygon,
    int               num_verts,
    VertexType        *vertlist,
    PLGPART_TYPE      type,
    PLGC_BOOL         attached);

/**
 * PolygonClip
 * @brief
 *
 * @param
 *
 * @return
 *
 * @retval
 */
extern
void PolygonClip (
    PLGCLIP_MODE      mode,
    HPOLYGON          subject,
    HPOLYGON          clip,
    HPOLYGON          result);

/**
 * PolygonRect
 * @brief
 *
 * @param
 *
 * @return
 *
 * @retval
 */
extern
PLGC_BOOL PolygonRect (
    const HPOLYGON    polygon,
    RectType          *rect);

/**
 * PtInPolygon
 * @brief
 *
 * @param
 *
 * @return
 *
 * @retval
 */
extern
PLGC_BOOL PtInPolygon (
    const HPOLYGON    polygon,
    const RectType    *plgrect,
    double            x,
    double            y);

/**
 * PtInPolygonContour
 * @brief
 *
 * @param
 *
 * @return
 *
 * @retval
 */
extern
PLGC_BOOL PtInPolygonContour (
    const HPOLYGON    polygon,
    const RectType    *plgrect,
    double            x,
    double            y);

/**
 * PolygonGetPart
 * @brief
 *
 * @param
 *
 * @return
 *
 * @retval
 */
extern
int PolygonGetPart (
    const HPOLYGON    polygon,
    int               partid,
    VertexType        **ppverts,
    PLGPART_TYPE      *ptype);

/**
 * PolygonGetContour
 * @brief
 *
 * @param
 *
 * @return
 *
 * @retval
 */
extern
int PolygonGetContour (
    const HPOLYGON    polygon,
    int               contourid,
    VertexType        **ppverts);

/**
 * PolygonArea
 * @brief
 *
 * @param
 *
 * @return
 *
 * @retval
 */
extern
double PolygonArea (
    const HPOLYGON    polygon);

/**
 * PolygonLength
 * @brief
 *
 * @param
 *
 * @return
 *
 * @retval
 */
extern
double PolygonLength (
    const HPOLYGON    polygon);

#ifdef    __cplusplus
}
#endif

#endif /* _POLYGON_H_ */
