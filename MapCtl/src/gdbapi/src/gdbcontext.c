/**
 * @file
 *    gdbcontext.c
 * @brief
 *    private implementation file for GDBContext.
 * @author
 *    Zhang Liang \n
 *    350137278@qq.com \n
 *    http://blog.csdn.net/cheungmine
 * @since
 *    2013/12/26
 * @date
 *    2013/12/26
 * @version
 *    0.0.1
 * @note
 *
 */

#ifdef _MSC_VER
    #pragma warning (disable : 4996)
#endif

#include "gdbcontext.h"
#include "gdblayer.h"
#include "gdbcoldef.h"


/**
 * init a spatial db: GDB_
 */
static GDBAPI_RESULT GDBCtxInit (HGDBCTX ctx, HGDBERR herr)
{
    HGDBContext hctx;
    HGDBStmt hstmt;
    char *sql;
    int ret;

    CAST_HANDLE_CONTEXT(ctx, herr, hctx);

    GDBHandleAlloc ((GDB_HANDLE*)&hstmt, GDB_HTYPE_STMT, 0, 1200);
    GDBHandleGetData ((GDB_HANDLE)hstmt, (void**) &sql);

    ret = snprintf (sql, 1200,
        "CREATE TABLE IF NOT EXISTS GDBLAYERS (\n"
            "LAYERID    INTEGER PRIMARY KEY AUTOINCREMENT,\n"
            "TABLESPACE VARCHAR2(%d),\n"
            "OWNER      VARCHAR2(%d) NOT NULL,\n"
            "TABLENAME  VARCHAR2(%d) UNIQUE NOT NULL,\n"
            "SPATIALCOL VARCHAR2(%d) NOT NULL,\n"
            "ROWIDCOL   VARCHAR2(%d) NOT NULL,\n"
            "XMINCOL    VARCHAR2(%d) NOT NULL DEFAULT ('_XMIN'),\n"
            "YMINCOL    VARCHAR2(%d) NOT NULL DEFAULT ('_YMIN'),\n"
            "XMAXCOL    VARCHAR2(%d) NOT NULL DEFAULT ('_XMAX'),\n"
            "YMAXCOL    VARCHAR2(%d) NOT NULL DEFAULT ('_YMAX'),\n"
            "LENGTHCOL  VARCHAR2(%d) DEFAULT ('_LENGTH'),\n"
            "AREACOL    VARCHAR2(%d) DEFAULT ('_AREA'),\n"
            "LAYERTYPE  INTEGER NOT NULL,\n"
            "LAYERMASK  INTEGER DEFAULT (-1),\n"
            "LEVELMIN   INTEGER,\n"
            "LEVELMAX   INTEGER,\n"
            "XMIN       DOUBLE NOT NULL,\n"
            "YMIN       DOUBLE NOT NULL,\n"
            "XMAX       DOUBLE NOT NULL,\n"
            "YMAX       DOUBLE NOT NULL,\n"
            "ZMIN       DOUBLE,\n"
            "ZMAX       DOUBLE,\n"
            "MMIN       DOUBLE,\n"
            "MMAX       DOUBLE,\n"
            "REFID      INTEGER NOT NULL DEFAULT (0),\n"
            "MINIMUMID  INTEGER DEFAULT (-1),\n"
            "DESCRIPT   VARCHAR2(%d),\n"
            "CREDATE    DATETIME DEFAULT (datetime('now', 'localtime')));",
        LAYER_COLNAME_LEN,
        LAYER_COLNAME_LEN,
        LAYER_COLNAME_LEN,
        LAYER_COLNAME_LEN,
        LAYER_COLNAME_LEN,
        LAYER_COLNAME_LEN,
        LAYER_COLNAME_LEN,
        LAYER_COLNAME_LEN,
        LAYER_COLNAME_LEN,
        LAYER_COLNAME_LEN,
        LAYER_COLNAME_LEN,
        LAYER_DESCRIPT_LEN);

    ret = GDBBeginTransaction(ctx, herr);

    if (ret == GDBAPI_SUCCESS) {
        ret = GDBExecuteSQL (ctx, sql, herr);

        if (ret == GDBAPI_SUCCESS) {
            GDBCommitTransaction(ctx, herr);

            ret = sprintf (sql,
                  "INSERT INTO GDBLAYERS (\n"
                      "TABLESPACE,OWNER,TABLENAME,SPATIALCOL,ROWIDCOL,"
                      "XMINCOL,YMINCOL,XMAXCOL,YMAXCOL,LENGTHCOL,AREACOL,"
                      "LAYERTYPE,LAYERMASK,"
                      "LEVELMIN,LEVELMAX,"
                      "XMIN,YMIN,XMAX,YMAX,"
                      "ZMIN,ZMAX,"
                      "MMIN,MMAX,"
                      "REFID,"
                      "MINIMUMID,"
                      "DESCRIPT) "
                  "VALUES("
                      ":TABLESPACE,:OWNER,:TABLENAME,:SPATIALCOL,:ROWIDCOL,"
                      ":XMINCOL,:YMINCOL,:XMAXCOL,:YMAXCOL,:LENGTHCOL,:AREACOL,"
                      ":LAYERTYPE,:LAYERMASK,"
                      ":LEVELMIN,:LEVELMAX,"
                      ":XMIN,:YMIN,:XMAX,:YMAX,"
                      ":ZMIN,:ZMAX,"
                      ":MMIN,:MMAX,"
                      ":REFID,"
                      ":MINIMUMID,"
                      ":DESCRIPT);");

            ret = GDBStmtPrepare (ctx, sql, ret+1, (GDB_STMT) hstmt, herr);

            if (ret == GDBAPI_SUCCESS) {
                _ContextSet_hstmt_INSERT_GDBLAYERS(hctx, (HGDBSTMT) hstmt, herr);
                hstmt = 0;
            } else {
                return
                    _GDBSetErrorInfo (herr, GDBAPI_SQLITE3_ERROR,
                        "GDBStmtPrepare failed",
                        ret, sqlite3_errmsg (hctx->gdb),
                        __FILE__, __LINE__, "GDBCtxInit");
            }
        } else {
            GDBRollbackTransaction (ctx, herr);

            return
                _GDBSetErrorInfo (herr, GDBAPI_SQLITE3_ERROR,
                    "GDBExecuteSQL failed",
                    ret, sqlite3_errmsg (hctx->gdb),
                    __FILE__, __LINE__, "GDBCtxInit");
        }
    } else {
        return
            _GDBSetErrorInfo (herr, GDBAPI_SQLITE3_ERROR,
                "GDBBeginTransaction failed",
                ret, sqlite3_errmsg (hctx->gdb),
                __FILE__, __LINE__, "GDBCtxInit");
    }

    GDBHandleFree ((GDB_HANDLE) hstmt);
    return ret;
}


/**
 * GDBContextOpen
 */
GDBAPI_RESULT GDBContextOpen (HGDBCTX ctx, HGDBERR herr)
{
    int ret, flags;
    HGDBContext hctx;
    CAST_HANDLE_CONTEXT(ctx, herr, hctx);

    GDBContextClose ((HGDBCTX) hctx, herr);

    if (hctx->singlethread) {
        ret = sqlite3_config (SQLITE_CONFIG_SINGLETHREAD);
        /*
        if (ret != SQLITE_OK) {
          return _GDBSetErrorInfo (herr,
              GDBAPI_SQLITE3_ERROR, "sqlite3_config(SQLITE_CONFIG_MULTITHREAD) failed",
              ret, sqlite3_errmsg (hctx->gdb),
              __FILE__, __LINE__, __FUNCTION__);
        }
        */
    } else {
        ret = sqlite3_config (SQLITE_CONFIG_MULTITHREAD);
        /*
        if (ret != SQLITE_OK) {
          return _GDBSetErrorInfo (herr,
              GDBAPI_SQLITE3_ERROR, "sqlite3_config(SQLITE_CONFIG_SINGLETHREAD) failed",
              ret, sqlite3_errmsg (hctx->gdb),
              __FILE__, __LINE__, __FUNCTION__);
        }
        */
    }

    ret = sqlite3_initialize ();
    if (ret != SQLITE_OK) {
        return
            _GDBSetErrorInfo (herr,
                GDBAPI_SQLITE3_ERROR,
                "sqlite3_initialize failed",
                ret, sqlite3_errmsg (hctx->gdb),
                __FILE__, __LINE__, __FUNCTION__);
    }

    flags = SQLITE_OPEN_FULLMUTEX;

    if (hctx->readonly) {
        flags |= SQLITE_OPEN_READONLY;
    } else {
        flags |= SQLITE_OPEN_READWRITE;
    }

    if (! hctx->nocreate) {
        flags |= SQLITE_OPEN_CREATE;
    }

    if (! hctx->nosharecache) {
        flags |= SQLITE_OPEN_SHAREDCACHE;
    }

    ret = sqlite3_open_v2 (hctx->gdbfile, &hctx->gdb, flags, 0);

    if (ret == SQLITE_OK) {
        sqlite3_limit (hctx->gdb, SQLITE_LIMIT_LENGTH, 1000000000);
        return GDBCtxInit (ctx, herr);
    } else {
        return
            _GDBSetErrorInfo (herr,
                GDBAPI_SQLITE3_ERROR, "sqlite3_open_v2 failed",
                ret, sqlite3_errmsg (hctx->gdb),
                __FILE__, __LINE__, __FUNCTION__);
    }
}


GDBAPI_RESULT GDBContextClose (HGDBCTX ctx, GDB_HANDLE herr)
{
    int ret;
    HGDBContext hctx;
    CAST_HANDLE_CONTEXT(ctx, herr, hctx);

    if (hctx->gdb) {
        sqlite3* db = hctx->gdb;
        hctx->gdb = 0;

        /* free hstmt_INSERT_GDBLAYERS */
        _ContextSet_hstmt_INSERT_GDBLAYERS(hctx, 0, herr);

        /* free hstmt_INSERT_COLUMNS */
        _ContextSet_hstmt_INSERT_COLUMNS(hctx, 0, herr);

        /* free hstmt_QUERY_LAYER */
        _ContextSet_hstmt_QUERY_LAYER(hctx, 0, herr);

        /* close sqlite3 db */
        ret = sqlite3_close_v2 (db);
        ret = sqlite3_shutdown ();

        if (ret == SQLITE_OK) {
            return GDBAPI_SUCCESS;
        } else {
            return
                _GDBSetErrorInfo (herr,
                    GDBAPI_SQLITE3_ERROR,
                    "sqlite3_close_v2 failed",
                    ret, sqlite3_errmsg (db),
                    __FILE__, __LINE__,
                    "GDBContextClose");
        }
    } else {
        return
            _GDBSetErrorInfo (herr,
                GDBAPI_EDBINST,
                "Invalid DB Instance",
                0, 0,
                __FILE__, __LINE__, __FUNCTION__);
    }
}


GDBAPI_RESULT GDBContextGetDbInst (HGDBCTX ctx, void **ppDB)
{
    HGDBContext hctx;
    CAST_HANDLE_CONTEXT(ctx, 0, hctx);

    if (ppDB) {
        *ppDB = (void*) hctx->gdb;
    }

    if (hctx->gdb) {
        return GDBAPI_SUCCESS;
    } else {
        return GDBAPI_EDBINST;
    }
}


GDBAPI_RESULT GDBExecuteSQL (
    GDB_HANDLE ctx,
    const char *sql,
    GDB_HANDLE herr)
{
    HGDBContext hctx;
    CAST_HANDLE_CONTEXT(ctx, herr, hctx);

    if (hctx->gdb) {
        char *errmsg = 0;
        int ret = sqlite3_exec (hctx->gdb, sql, 0, 0, &errmsg);  
        if (ret == SQLITE_OK) {
            return GDBAPI_SUCCESS;
        } else {
            if (herr) {
              /* TODO */
            }
            return GDBAPI_ERROR;
        }
    } else {
        if (herr) {
          /* TODO */
        }
        return GDBAPI_EDBINST;
    }
}


GDBAPI_RESULT GDBBeginTransaction (
    HGDBCTX ctx,
    HGDBERR herr)
{
    return GDBExecuteSQL (ctx, "BEGIN TRANSACTION", herr);
}


GDBAPI_RESULT GDBCommitTransaction (
    HGDBCTX ctx,
    HGDBERR herr)
{
    return GDBExecuteSQL (ctx, "COMMIT TRANSACTION", herr);
}


GDBAPI_RESULT GDBRollbackTransaction (
    HGDBCTX ctx,
    HGDBERR herr)
{
    return GDBExecuteSQL (ctx, "ROLLBACK TRANSACTION", herr);
}


GDBAPI_RESULT GDBStmtPrepare (
    HGDBCTX ctx,
    const char *sql,
    int size,
    GDB_STMT stmt,
    HGDBERR herr)
{
    HGDBContext hctx;
    CAST_HANDLE_CONTEXT(ctx, herr, hctx);
    if (hctx->gdb) {
        HGDBStmt hstmt;
        int ret;
        sqlite3_stmt *pStmt;
        CAST_HANDLE_STMT(stmt, herr, hstmt);

        ret = sqlite3_prepare_v2 (hctx->gdb, sql, size, &pStmt, 0);
        if (ret == SQLITE_OK) {
            assert (!hstmt->hctx);
            hstmt->hctx = hctx;
            hstmt->stmt = pStmt;
            return GDBAPI_SUCCESS;
        } else {
            return 
                _GDBSetErrorInfo (herr, GDBAPI_SQLITE3_ERROR, "sqlite3_prepare_v2 failed",
                    ret, sqlite3_errmsg (hctx->gdb), __FILE__, __LINE__, "GDBStmtPrepare");
        }
    } else {
        return
            _GDBSetErrorInfo (herr, GDBAPI_EDBINST, "Underground DB Instance Not Opened",
                0, 0, __FILE__, __LINE__, "GDBStmtPrepare");
    }
}

int GDBStmtGetParamIndex (GDB_STMT stmt, const char *param)
{
    HGDBStmt hstmt;
    CAST_HANDLE_STMT(stmt, 0, hstmt);
    return sqlite3_bind_parameter_index (hstmt->stmt, param);
}

GDBAPI_RESULT GDBStmtBindParam (
    GDB_HANDLE stmt,
    int paramIndex,
    void *paramValue,
    int cbParamSize,  
    GDB_PARAM_TYPE paramType,
    GDB_HANDLE herr)
{
    HGDBStmt hstmt;

    int ret = -1;

    CAST_HANDLE_STMT(stmt, 0, hstmt);
  
    if (!paramValue) {
        ret = sqlite3_bind_null (hstmt->stmt, paramIndex);
        if (ret != SQLITE_OK) {
            return
                _GDBSetErrorInfo (herr, GDBAPI_SQLITE3_ERROR,
                    "sqlite3_bind_null failed", ret,
                    sqlite3_errmsg (hstmt->hctx->gdb),
                    __FILE__, __LINE__,
                    __FUNCTION__);
        } else {
            return GDBAPI_SUCCESS;
        }
    }

    switch (paramType) {
    case GDB_PARAM_INT:
        if (cbParamSize == sizeof (sqlite_int64)) {
            ret = sqlite3_bind_int64 (hstmt->stmt, paramIndex, *((sqlite_int64*) paramValue));
            if (ret != SQLITE_OK) {
              return
                  _GDBSetErrorInfo (herr, GDBAPI_SQLITE3_ERROR,
                      "sqlite3_bind_int64 failed",
                      ret, sqlite3_errmsg (hstmt->hctx->gdb),
                      __FILE__, __LINE__,
                      "GDBStmtBindParam");
            }
        } else {
            ret = sqlite3_bind_int (hstmt->stmt, paramIndex, *((int*) paramValue));
            if (ret != SQLITE_OK) {
                return
                    _GDBSetErrorInfo (herr, GDBAPI_SQLITE3_ERROR,
                        "sqlite3_bind_int failed",
                        ret, sqlite3_errmsg (hstmt->hctx->gdb),
                        __FILE__, __LINE__,
                        "GDBStmtBindParam");
            }
        }    
        break;
    case GDB_PARAM_INT64:
        ret = sqlite3_bind_int64 (hstmt->stmt, paramIndex, *((sqlite_int64*) paramValue));
        if (ret != SQLITE_OK) {
          return _GDBSetErrorInfo (herr, GDBAPI_SQLITE3_ERROR,
            "sqlite3_bind_int64 failed",
            ret, sqlite3_errmsg (hstmt->hctx->gdb), __FILE__, __LINE__, "GDBStmtBindParam");
        }
        break;
    case GDB_PARAM_DOUBLE:
        ret = sqlite3_bind_double(hstmt->stmt, paramIndex, *((double*) paramValue));
        if (ret != SQLITE_OK) {
          return _GDBSetErrorInfo (herr, GDBAPI_SQLITE3_ERROR,
            "sqlite3_bind_double failed",
            ret, sqlite3_errmsg (hstmt->hctx->gdb), __FILE__, __LINE__, "GDBStmtBindParam");
        }
        break;
    case GDB_PARAM_TEXT:
        ret = sqlite3_bind_text(hstmt->stmt, paramIndex, (const char*) paramValue, cbParamSize, 0);
        if (ret != SQLITE_OK) {
            return
                _GDBSetErrorInfo (herr, GDBAPI_SQLITE3_ERROR,
                    "sqlite3_bind_text failed",
                    ret, sqlite3_errmsg (hstmt->hctx->gdb),
                    __FILE__, __LINE__,
                    "GDBStmtBindParam");
        }
        break;
    case GDB_PARAM_BLOB:
        ret = sqlite3_bind_blob(hstmt->stmt, paramIndex, paramValue, cbParamSize, 0);
        if (ret != SQLITE_OK) {
            return
                _GDBSetErrorInfo (herr, GDBAPI_SQLITE3_ERROR,
                    "sqlite3_bind_blob failed",
                    ret, sqlite3_errmsg (hstmt->hctx->gdb),
                    __FILE__, __LINE__,
                    "GDBStmtBindParam");
        }
        break;
    default:
        return
            _GDBSetErrorInfo (herr, GDBAPI_ERROR,
                "Invalid paramType",
                0, 0,
                __FILE__, __LINE__,
                "GDBStmtBindParam");
    }

    return GDBAPI_SUCCESS;
}


GDBAPI_RESULT GDBStmtFinish (
    GDB_STMT stmt,
    GDBAPI_BOOL bFreeStmt,
    GDB_HANDLE herr)
{
    int ret;
    HGDBStmt hstmt;

    CAST_HANDLE_STMT(stmt, 0, hstmt);

    if (hstmt->stmt) {
        sqlite3_stmt *st = hstmt->stmt;
        hstmt->stmt = 0;
        ret = sqlite3_finalize(st);
    } else {
        ret = SQLITE_OK;
    }

    FREE_S(hstmt->attachment);

    if (bFreeStmt) {
        GDBHandleFree (stmt);
    }

    if (ret == SQLITE_OK) {
        return GDBAPI_SUCCESS;
    } else {
        if (herr) {
            /* TODO */
        }
        return GDBAPI_ERROR;
    }
}


GDBAPI_RESULT GDBStmtExecute (
    GDB_HANDLE stmt,
    GDB_HANDLE herr)
{
    int ret;

    HGDBStmt hstmt;

    CAST_HANDLE_STMT(stmt, 0, hstmt);
 
    ret = sqlite3_step(hstmt->stmt);

    if (ret == SQLITE_DONE) {
        return GDBAPI_DB_DONE;
    } else if (ret == SQLITE_BUSY) {
        return GDBAPI_DB_BUSY;
    } else if (ret == SQLITE_ROW) {
        return GDBAPI_DB_ROW;
    } else if (ret == SQLITE_ERROR) {
        return GDBAPI_DB_ERROR;
    } else {
        return
            _GDBSetErrorInfo (herr, GDBAPI_SQLITE3_ERROR,
                "sqlite3_step failed",
                ret, sqlite3_errmsg (hstmt->hctx->gdb),
                __FILE__, __LINE__,
                "GDBStmtExecute");
    }
}


GDBAPI_RESULT GDBStmtReset (
    GDB_STMT stmt,
    HGDBERR herr)
{
    HGDBStmt hstmt;
    int ret;

    CAST_HANDLE_STMT(stmt, 0, hstmt);

    ret = sqlite3_reset(hstmt->stmt);

    if (ret == SQLITE_OK) {
        return GDBAPI_SUCCESS;
    } else {
        return
            _GDBSetErrorInfo (herr, GDBAPI_SQLITE3_ERROR,
                "sqlite3_reset failed",
                ret,
                sqlite3_errmsg (hstmt->hctx->gdb),
                __FILE__, __LINE__,
                "GDBStmtReset");
    }
}


GDBAPI_RESULT GDBStmtGetStmtInst (
    GDB_STMT stmt,
    void **ppStmt)
{
    HGDBStmt hstmt;
    CAST_HANDLE_STMT(stmt, 0, hstmt);

    if (ppStmt) {
        *ppStmt = (void*) hstmt->stmt;
    }

    if (hstmt->stmt) {
        return GDBAPI_SUCCESS;
    } else {
        return GDBAPI_EDBINST;
    }
}


GDBAPI_RESULT GDBStmtGetCol (
    GDB_STMT stmt,
    int colIndex,
    void *colValue,
    int *colValueSize,
    char **colName,
    GDB_PARAM_TYPE *colType,
    GDB_ERROR herr)
{
    HGDBStmt hstmt;
    CAST_HANDLE_STMT(stmt, 0, hstmt);

    assert (0);

    return 0;
}
