/*
 * drawshape.c
 *
 * cheungmine
 * 2013-5-5
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifdef _MSC_VER
# include <windows.h>
# pragma warning (disable : 4996)
#endif

#include "gdbshell.h"

#include "viewport.h"

/*
 * cairo api
 */
#include "../libs/cairo/include/cairo.h"
#include "../libs/cairo/include/cairo-features.h"
#include "../libs/cairo/include/cairo-svg.h"
#include "../libs/cairo/include/cairo-pdf.h"
#include "../libs/cairo/include/cairo-ps.h"
#include "../libs/cairo/include/cairo-version.h"
#include "../libs/cairo/include/cairo-win32.h"

# pragma comment (lib, "../../libs/cairo/lib/cairo.lib")

/*
 * shape file api
 */
#include "../libs/shapefile/dist/include/shapefile.h"

# pragma comment (lib, "../../libs/shapefile/dist/lib/libshapefile.lib")


static int strsplit (char *source, char *out[], int size, char *sep)
{
  char *tok;
  int n = 0;

  tok = strtok(source, sep);
  while (tok && n < size) {
    out[n++] = tok;

    /* next token */
    tok = strtok(0, sep);
  }

  return n;
}

void ft_example()
{
  /*
  cairo_surface_t *surface;
  cairo_status_t   status;
  cairo_t *cr;
  FT_Error error;
  FT_Library library;
  FT_Face face, face_bold;
  FT_Face face_ch;
  cairo_font_face_t *cr_face, *cr_face_bold;
  cairo_font_face_t *cr_face_ch;

  if (FT_Init_FreeType(&library) != 0)
  return;

  if (FT_New_Face(library, "arial.ttf", 0, &face) != 0)
  return;
  cr_face = cairo_ft_font_face_create_for_ft_face(face, 0);
  if (FT_New_Face(library, "arialbd.ttf", 0, &face_bold) != 0)
  return;
  cr_face_bold = cairo_ft_font_face_create_for_ft_face(face_bold, 0);
  if (FT_New_Face(library, "simhei.ttf", 0, &face_ch) != 0)
  return;
  cr_face_ch = cairo_ft_font_face_create_for_ft_face(face_ch, 0);
  surface = cairo_pdf_surface_create("C97.pdf", 20, 20);
  cr = cairo_create(surface);
  status = cairo_status(cr);
  */
}

#ifdef _MSC_VER
static int bstr2utf8(BSTR bstrIn, int bstrLen, byte *pOut)
{
  int cbSize;

    if (!bstrIn) {
        return  0;
  }

    cbSize = WideCharToMultiByte(CP_UTF8, 0, bstrIn, bstrLen, 0, 0, 0, 0);
  if (pOut) {
      cbSize = WideCharToMultiByte(CP_UTF8, 0, bstrIn, bstrLen, (LPSTR) pOut, cbSize, 0, 0);
    assert (pOut[cbSize-1]==0);
  }

    return cbSize;
}

static int bytes2bstr(UINT uCodePage, byte* pBytesIn, DWORD cbSizeIn, BSTR *pBstrOut)
{
  int   cchNeeded;
  LPSTR    pstr;

  *pBstrOut = 0;

    cchNeeded = MultiByteToWideChar(uCodePage, 0, (LPCSTR)pBytesIn, cbSizeIn, 0, 0);
    if (0 == cchNeeded) {
        return (0);
  }

    pstr = (LPSTR) CoTaskMemAlloc (sizeof(DWORD) + sizeof(WCHAR)*cchNeeded);
    if (!pstr) {
        return (-1);
  }

    if (cchNeeded != MultiByteToWideChar(uCodePage, 0, (LPCSTR)pBytesIn, cbSizeIn, 
    (LPWSTR)(pstr+sizeof(DWORD)), cchNeeded)) {
        CoTaskMemFree(pstr);
        return (-2);
    }

    *((DWORD*)pstr) = sizeof(WCHAR)*(cchNeeded-1);
    *pBstrOut = (BSTR)(pstr + sizeof(DWORD));

    return cchNeeded;
}

char * ansi2utf8(char *str)
{
  BSTR bstr = 0;
  byte *utf8 = 0;
  int len, cb;

  len = bytes2bstr(CP_ACP, (byte*) str, strlen(str)+1, &bstr);
  if (len > 0) {
    cb = bstr2utf8(bstr, len, utf8);
    if (cb > 0) {
      utf8 = (byte*) malloc(cb);
      bstr2utf8(bstr, len, utf8);
    }

    SysFreeString(bstr);
    return (char*) utf8;
  }

  return 0;
}
#else

#endif

static void draw_text (cairo_t *cr, char *str, _LFLOAT x0, _LFLOAT y0)
{
  char *font;
  char *text;

  font = ansi2utf8("文泉驿等宽正黑");  //"Arial Unicode MS"
  text = ansi2utf8(str);

  cairo_select_font_face (cr, font, CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
  cairo_set_font_size (cr, 32.0);

  cairo_move_to(cr, x0, y0);

  cairo_show_text(cr, text);

  free(font);
  free(text);
}

static void fill_rectangle (cairo_t *cr, const VWPT_LFRect *rect,
  gt_color3 *content, gt_color3 *contour)
{
  if (content) {
    cairo_set_source_rgb(cr, content->red, content->green, content->blue);

    cairo_new_path(cr);
    cairo_move_to(cr, rect->Xmin, rect->Ymin);
    cairo_line_to(cr, rect->Xmin, rect->Ymax);
    cairo_line_to(cr, rect->Xmax, rect->Ymax);
    cairo_line_to(cr, rect->Xmax, rect->Ymin);
    cairo_line_to(cr, rect->Xmin, rect->Ymin);
    cairo_close_path(cr);

    if (contour) {
      cairo_fill_preserve(cr);

      cairo_set_source_rgb(cr, contour->red, contour->green, contour->blue);
      cairo_stroke(cr);
    } else {
      cairo_fill(cr);
    }
  } else if (contour) {
    cairo_set_source_rgb(cr, contour->red, contour->green, contour->blue);
    cairo_move_to(cr, rect->Xmin, rect->Ymin);
    cairo_line_to(cr, rect->Xmin, rect->Ymax);
    cairo_line_to(cr, rect->Xmax, rect->Ymax);
    cairo_line_to(cr, rect->Xmax, rect->Ymin);
    cairo_line_to(cr, rect->Xmin, rect->Ymin);
    cairo_stroke(cr);
  }
}

static void fill_polygon (cairo_t *cr, VWPT_Transform *vwpt, const VWPT_LFPoint *pPoints, int numPoints)
{
  VWPT_LFPoint Vp;
  int i = 0;

  for (; i < numPoints; i++) {
    VWPT_Dp2Vp_LF(vwpt, &pPoints[i], &Vp);

    if (i == 0) {
      cairo_move_to(cr, Vp.X, Vp.Y);
    } else {
      cairo_line_to(cr, Vp.X, Vp.Y);
    }
  }
}

static int draw_polygon_shape (cairo_t *cr, VWPT_Transform *vwpt,
  const SHPObjectEx *pShpObj,
  gt_color3 *content,
  gt_color3 *contour)
{
  int ret = 0;
  int p = 0;

  for (; p < pShpObj->nParts; p++) {
    int at = pShpObj->panPartStart[p];
    int np = pShpObj->panPartStart[p+1] - at;

    if (p == 0 && np == 0) {
      /* error data */
      return (-1);
    }

    if (np > 0) {
      if (p == 0) {
        /* contour path */
        cairo_new_path(cr);
      } else {
        /* hole path */
        cairo_new_sub_path(cr);
      }

      fill_polygon(cr, vwpt, (const VWPT_LFPoint*) &pShpObj->pPoints[at], np);

      cairo_close_path(cr);
    } else {
      /* 1st error part no. */
      ret = p+1;
      break;
    }
  }

  if (p) {
    /* success returns 0; */
    cairo_set_source_rgb(cr, content->red, content->green, content->blue);
    cairo_fill_preserve(cr);

    cairo_set_source_rgb(cr, contour->red, contour->green, contour->blue);
    cairo_stroke(cr);
    return ret;
  } else {
    /* null data */
    return (-2);
  }
}

int drawshapefile (char *shapefile, char *shapeid, char *output, int width, int length, gt_surface_fmt format)
{
  clock_t        t0, t1;

  SHPHandle      hSHP;
	DBFHandle      hDBF;

  int            i, ret, shpId;
  int            nShapeType;
  int            nEntities;
	int			       nDBFFields;

  double         MinBounds[4];
	double         MaxBounds[4];

  GDBExtRect     layerMBR;

  SHPObjectEx   *pShpObjEx;

  VWPT_Transform vwpt;
  VWPT_LFRect dataRC;
  VWPT_LFRect drawShapesRC;
  VWPT_LFRect viewRC;

  char *idList[100];
  int arrShapeIds[100];
  int numShapeIds = 0;
  int drawnShapes = 0;

  gt_color3 color;
  gt_color3 color2;
  cairo_surface_t *surface;
  cairo_t *cr;

  surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, width, length);
  if (!surface) {
    printf("ERROR: cairo_image_surface_create ()\n");
    return (-101);
  }
  cr = cairo_create(surface);
  if (!cr) {
    cairo_surface_destroy(surface);
    printf("ERROR: cairo_create ()\n");
    return (-102);
  }

  printf("Open shapefile: %s\n", shapefile);
  hSHP = SHPOpen(shapefile, "rb" );
	if (NULL == hSHP) {
		printf("ERROR: Invalid .SHP file\n");
    cairo_destroy (cr);
    cairo_surface_destroy (surface);
		return (-1);
	}
		
	hDBF = DBFOpen(shapefile, "rb" );   
	if (NULL == hDBF) {
    printf("ERROR: Invalid .DBF file\n");
    SHPClose(hSHP);
    cairo_destroy(cr);
    cairo_surface_destroy(surface);
		return (-2);
	}

	SHPGetInfo(hSHP, &nEntities, &nShapeType, MinBounds, MaxBounds);
  nDBFFields = DBFGetFieldCount(hDBF);

  layerMBR.Xmin = MinBounds[0];
  layerMBR.Ymin = MinBounds[1];
  layerMBR.Xmax = MaxBounds[0];
  layerMBR.Ymax = MaxBounds[1];

  VWPT_SetRect(viewRC, 0, 0, width, length, _LFLOAT);
  VWPT_SetRect(dataRC, MinBounds[0], MinBounds[1], MaxBounds[0], MaxBounds[1], _LFLOAT);
  VWPT_TransformInitAll(&vwpt, &viewRC, &dataRC, 1);
  VWPT_ZoomAll(&vwpt, 1.0);

  VWPT_Drc2Vrc_LF(&vwpt, &dataRC, &viewRC);

  SHPCreateObjectEx(&pShpObjEx);

  color.red = 0.0;
  color.green = 1.0;
  color.blue = 0.0;

  color2.red = 0.0;
  color2.green = 0.0;
  color2.blue = 1.0;

  cairo_set_source_rgb(cr, color.red, color.green, color.blue);
  cairo_set_line_width(cr, 0.5);
  cairo_set_line_cap (cr, CAIRO_LINE_CAP_BUTT);

  cairo_set_antialias (cr, CAIRO_ANTIALIAS_SUBPIXEL);

  t0 = clock();

  /* get drawing shape id list: 0-based */
  shpId = 0;
  ret = strsplit(shapeid, idList, 100, ",");

  if (ret > 0) {
    for (i = 0; i < ret; i++) {
      if (!strcmp(idList[i], "*")) {
        shpId = -100; /* draw all shapes */
        break;
      }

      shpId = atoi(idList[i]);
      if (shpId >= 0 && shpId < nEntities) {
        arrShapeIds[numShapeIds++] = shpId;
      }
    }
  }

  /* get rect of drawing shapes */
  VWPT_ZeroRect(drawShapesRC);

  for (i = 0; i < numShapeIds; i++) {
    shpId = arrShapeIds[i];

    if (SHPReadObjectEx(hSHP, shpId, pShpObjEx)) {
      VWPT_LFRect clipRC;
      VWPT_SetRect(clipRC,
        pShpObjEx->dfXMin, pShpObjEx->dfYMin, pShpObjEx->dfXMax, pShpObjEx->dfYMax,
        double);
      VWPT_UnionRect(&drawShapesRC, &clipRC, &drawShapesRC);
    }
  }

  if (VWPT_IsZeroRect(drawShapesRC)) {
    VWPT_SetRect(drawShapesRC,
      MinBounds[0], MinBounds[1], MaxBounds[0], MaxBounds[1],
      double);
  }

  VWPT_ZoomExtent2(&vwpt,
    drawShapesRC.Xmin, drawShapesRC.Ymin, drawShapesRC.Xmax, drawShapesRC.Ymax, 100);

  VWPT_ZoomTimes(&vwpt, 0.9);

  /* draw backend shapes */
  cairo_set_line_width(cr, 1.0);
  cairo_set_line_cap (cr, CAIRO_LINE_CAP_SQUARE);
  cairo_set_antialias (cr, CAIRO_ANTIALIAS_NONE);

  drawnShapes = 0;

  for (i = 0; i < nEntities; i++) {
    if (SHPReadObjectEx(hSHP, i, pShpObjEx)) {
      VWPT_Drcxy2Vrc_LF(vwpt,
        pShpObjEx->dfXMin, pShpObjEx->dfYMin, pShpObjEx->dfXMax, pShpObjEx->dfYMax,
        viewRC);

      if (VWPT_IsInViewRect(&vwpt, &viewRC)) {
        color.red = 0.6;
        color.green = 0.6;
        color.blue = 0.6;

        color2.red = 0.2;
        color2.green = 0.2;
        color2.blue = 0.5;

        draw_polygon_shape(cr, &vwpt, pShpObjEx, &color, &color2);

        drawnShapes++;
      }
    }
  }

  /* draw selected shapes */
  cairo_set_line_width(cr, 3.0);
  cairo_set_line_cap (cr, CAIRO_LINE_CAP_ROUND);
  cairo_set_antialias (cr, CAIRO_ANTIALIAS_SUBPIXEL);

  if (numShapeIds > 0) {
    drawnShapes = 0;
  }

  for (i = 0; i < numShapeIds; i++) {
    shpId = arrShapeIds[i];

    if (SHPReadObjectEx(hSHP, shpId, pShpObjEx)) {
      if (pShpObjEx->nParts > 1) {
        ret = SHPObjectExValidatePolygon(pShpObjEx, 1);
      }
      color.red = 0.6;
      color.green = 1.0;
      color.blue = 0.5;

      color2.red = 1.0;
      color2.green = 0.2;
      color2.blue = 0.5;

      cairo_set_line_width(cr, 3.0);
      draw_polygon_shape(cr, &vwpt, pShpObjEx, &color, &color2);
      drawnShapes++;
    }
  }

  SHPDestroyObjectEx(pShpObjEx);
  DBFClose(hDBF);
  SHPClose(hSHP);

  cairo_set_source_rgb(cr, 0, 0, 0);
  draw_text(cr, "测试地图 v1.0", 100, 100);

  /* draw rect contour */
  color2.red = 1.0;
  color2.green = 0.0;
  color2.blue = 0.0;

  cairo_set_line_width(cr, 3);
  cairo_set_line_cap (cr, CAIRO_LINE_CAP_BUTT);

  VWPT_SetRect(viewRC, 0, 0, width, length, _LFLOAT);
  fill_rectangle(cr, &viewRC, 0, &color2);

  cairo_destroy(cr);
  cairo_surface_write_to_png(surface, output);
  cairo_surface_destroy(surface);

  t1 = clock();
  fprintf(stdout, "\nTotal %d/%d shapes drawn.\nElapsed time %d milliseconds.\n",
    drawnShapes, nEntities, (t1-t0));

  return 0;
}
�