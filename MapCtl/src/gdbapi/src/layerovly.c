/**
 * layerovly.c
 * @brief
 *    Layer Overlay Commands Implementations.
 * @author
 *    ZhangLiang
 * @since
 *    2013-5-15
 * @date
 *    2013-6-8
 * @note
 *
 */

#ifdef _MSC_VER
# define _CRTDBG_MAP_ALLOC
# pragma warning (disable : 4996)
#endif

#include<stdlib.h>

#ifdef _MSC_VER
# include<crtdbg.h>
#endif

#include <assert.h>
#include <stdio.h>
#include <time.h>

#include "gdbshell.h"

#define CHECK_ERROR_RESULT(ret, herror) do { \
        if (ret != GDBAPI_SUCCESS) { \
            if (herror) { \
                fprintf(stdout, "ERROR at %d: %s\n", \
                    __LINE__, GDBErrorGetMsg(herror)); \
            } \
            goto ERR_EXIT; \
        } \
    } while (0)

typedef struct ClipParam
{
    rowid_t offset;
    rowid_t rowcount;
    rowid_t step;
    int     percent;
} ClipParam;

static int LayerOverlayOnPrepare (
    GDB_CONTEXT ctx,
    rowid_t     numClipRows,
    GDB_LAYER   resultLayer,
    GDB_COLDEF  *resultColdefs,
    int         numColdefs,
    void        *inParam,
    GDB_ERROR   hError)
{
    ClipParam *param = (ClipParam *) inParam;

    param->rowcount = numClipRows;
    param->step = param->rowcount/100;
    param->percent = 0;

    if (param->step == 0) {
        param->step = 1;
    }

    if (sizeof(param->rowcount) == sizeof(double)) {
        fprintf (stdout, "Clipping shapes ( %lld )...\n", param->rowcount);
    } else {
        fprintf (stdout, "Clipping shapes ( %d )...\n", (int) param->rowcount);
    }
    fprintf (stdout, "%d%%\n", param->percent++);
    fflush(stdout);

    return GDBAPI_NEXT_ROW;
}

int LayerOverlayOnExecute (
    GDB_CONTEXT ctx,
    rowid_t     clipRowno,
    GDB_SHAPE   subjectShape,
    GDB_SHAPE   clipShape,
    GDB_SHAPE   resultShape,
    void        *inParam,
    GDB_ERROR   hError)
{
    ClipParam *param = (ClipParam *) inParam;

    if (clipRowno % param->step == 0) {
        fprintf (stdout, "%d%%\n", param->percent++);
        fflush (stdout);
    }

    return GDBAPI_NEXT_ROW;
}

static int LayerOverlayOnFinish (
    GDB_CONTEXT ctx,
    rowid_t     resultRowcount,
    GDB_LAYER   resultLayer,
    void        *inParam,
    GDB_ERROR   hError)
{
    fprintf(stdout, "100%%\n");
    fprintf(stdout, "\nResult shapes = %d.\n", resultRowcount);
    fflush (stdout);
    return GDBAPI_NEXT_ROW;
}


int areaovly (GDB_OVERLAY_MODE mode, char *gdb,
    char *subjectlayer, char *cliplayer, char *resultlayer,
    rowid_t offset, rowid_t rowcount)
{
    int ret;

    ClipParam param;

    clock_t t0, t1;

    GDB_CONTEXT    hctx = 0;
    GDB_ERROR      herr = 0;

    GDB_LAYER      hsubject = 0;
    GDB_LAYER      hclip = 0;
    GDB_LAYER      hresult = 0;

    GDB_TABLE_TYPE tabletype;
    char modename[20];

    if (mode == GDB_OVLY_ENV_OVERLAP) {
        sprintf(modename, "Env Overlap");
    } else if (mode == GDB_OVLY_AREA_INTERSECT) {
        sprintf(modename, "Area Intersect");
    } else if (mode == GDB_OVLY_AREA_DIFFER) {
        sprintf(modename, "Area Differ");
    } else if (mode == GDB_OVLY_AREA_UNION) {
        sprintf(modename, "Area Union");
    } else if (mode == GDB_OVLY_AREA_XOR) {
        sprintf(modename, "Area XOR");
    }

    fprintf(stdout, "\nGDB Layer Overlay: %s"
        "\nGDB input file: %s"
        "\n\tSubject Layer: %s"
        "\n\tClip Layer: %s",
        modename,
        gdb,
        subjectlayer,
        cliplayer);

    if (sizeof(rowid_t) == 8) {
        fprintf(stdout, "\n\t\tOffset of Clip Layer: %lld"
            "\n\t\tRowcount of Clip Layer: %lld",
            offset,
            rowcount);
    } else {
        fprintf(stdout, "\n\t\tOffset = %d"
            "\n\t\tRowcount = %d",
            (int) offset,
            (int) rowcount);
    }

    print_time ("\n");
    t0 = clock();

    /* create a context handle which represents a gdb instance */
    ret = GDBHandleAlloc (&hctx, GDB_HTYPE_CONTEXT, 0, 0);
    CHECK_ERROR_RESULT(ret, 0);

    ret = GDBHandleAlloc (&herr, GDB_HTYPE_ERROR, 0, 0);
    CHECK_ERROR_RESULT(ret, 0);

    ret = gdbopen (hctx, gdb, "gdbshell", "gdbshell", 1, herr);
    CHECK_ERROR_RESULT(ret, herr);

    ret = GDBHandleAlloc (&hsubject, GDB_HTYPE_LAYER, 0, 0);
    CHECK_ERROR_RESULT(ret, 0);

    ret = GDBHandleAlloc (&hclip, GDB_HTYPE_LAYER, 0, 0);
    CHECK_ERROR_RESULT(ret, 0);

    ret = GDBHandleAlloc (&hresult, GDB_HTYPE_LAYER, 0, 0);
    CHECK_ERROR_RESULT(ret, 0);

    tabletype = GDB_TABLE_LAYER;
    ret = GDBTableGetInfo (hctx, subjectlayer, &tabletype, 0, 0, 0, hsubject, herr);
    CHECK_ERROR_RESULT(ret, 0);

    tabletype = GDB_TABLE_LAYER;
    ret = GDBTableGetInfo (hctx, cliplayer, &tabletype, 0, 0, 0, hclip, herr);
    CHECK_ERROR_RESULT(ret, 0);

    GDBAttrSet (hresult, resultlayer, -1, GDB_ATTR_LAYER_TABLENAME, herr);

    param.offset = offset;
    param.rowcount = rowcount;

    /* get rowcount of clip */
    ret = GDBLayerOverlay (hctx, mode,
        hsubject, hclip, offset, &rowcount, hresult,
        LayerOverlayOnPrepare,
        LayerOverlayOnExecute,
        LayerOverlayOnFinish,
        (void*) &param,
        herr);
    if (ret != GDBAPI_SUCCESS) {
        goto ERR_EXIT;
    }

    GDBHandleFree (hsubject);
    GDBHandleFree (hclip);
    GDBHandleFree (hresult);

    GDBHandleFree (hctx);
    GDBHandleFree (herr);

    t1 = clock();
    print_time ("\n");

    fprintf (stdout, "\nGDB Layer Overlay exit with success.");
    fprintf(stdout, "\n\tResult Layer: %s", resultlayer);
    fprintf (stdout, "\n\tElapsed time %.3f seconds.\n",
        (float) (t1 - t0) * 0.001);

    return (0);

ERR_EXIT:
    fprintf(stdout, "\n******** Failed to GDB Layer Overlay! ******** \n");

    GDBHandleFree (hsubject);
    GDBHandleFree (hclip);
    GDBHandleFree (hresult);

    GDBHandleFree (hctx);
    GDBHandleFree (herr);

    return (-1);
}
