/**
 * gdbcoldef.c
 * @brief
 *    GDB_COLDEF, GDB_COLVAL implementation
 * @author
 *    ZhangLiang
 * @since
 *    2013-4-25
 * @date
 *    2013-5-31
 */

#include "gdbcoldef.h"
#include "gdblayer.h"
#include "gdbshape.h"
#include "gdbcontext.h"

#define return_CreateLayerTable(retval, ...) \
    PUSH_CALL_STACK(__FUNCTION__, \
        PTRPARAM(hctx), \
        STRPARAM(tablename), \
        PTRPARAM(coldefs), \
        INTPARAM(numcols), \
        PTRPARAM(hlayer), \
        PTRPARAM(herr)); \
        return retval

static GDBAPI_RESULT CreateLayerTable (
    HGDBContext hctx,
    const char *tablename,
    const GDB_COLDEF *coldefs,
    int numcols,
    HGDBLayer hlayer,
    HGDBERR herr)
{
    int  i, pos, cb, ret;
    char tmp[200];
    char *sql;
    GDB_TABLE_TYPE tabletype = GDB_TABLE_LAYER;

    SafeCopyString(hlayer->TABLENAME, tablename, sizeof(hlayer->TABLENAME));

    ret = GDBBeginTransaction((HGDBCTX) hctx, herr);
    if (ret != GDBAPI_SUCCESS) {
        return_CreateLayerTable(ret);
    }

    /* insert layer into GDBLAYERS */
    ret = LayerBindInsert((HGDBCTX) hctx, herr, hlayer);
    if (ret != GDBAPI_SUCCESS) {
        GDBRollbackTransaction ((HGDBCTX) hctx, herr);
        return_CreateLayerTable(ret);
    }

    ret = GDBStmtExecute(hctx->hstmt_INSERT_GDBLAYERS, herr);
    GDBStmtReset(hctx->hstmt_INSERT_GDBLAYERS, herr);
    if (ret != GDBAPI_DB_DONE) {
        GDBRollbackTransaction ((HGDBCTX) hctx, herr);
        return_CreateLayerTable(ret);
    }

    /* get layer info */
    ret = GDBTableGetInfo((HGDBCTX) hctx, hlayer->TABLENAME, &tabletype,
        0, 0, 0, (HGDBLAYER) hlayer, herr);
    if (ret != GDBAPI_SUCCESS) {
        GDBRollbackTransaction ((HGDBCTX) hctx, herr);
        return_CreateLayerTable(ret);
    }
    if (tabletype != GDB_TABLE_LAYER) {
        GDBRollbackTransaction ((HGDBCTX) hctx, herr);
        ret = _GDBSetErrorInfo (herr, GDBAPI_EAPPERR,
            "GDBTableGetInfo has an unexpected error: should never run to this",
            0, 0, __FILE__, __LINE__, __FUNCTION__);
        return_CreateLayerTable(ret);
    }

    /* prepare insert sql */
    cb = sprintf (tmp, "CREATE TABLE IF NOT EXISTS %s (\n", hlayer->TABLENAME);
    for (i = 0; i < numcols; i++) {
        cb += _GDBColDef2SQL((HGDBColDef) coldefs[i], tmp);
        cb += 2;
    }
    cb += (100 +
        strlen(hlayer->ROWIDCOL) +
        strlen(hlayer->XMINCOL) +
        strlen(hlayer->YMINCOL) +
        strlen(hlayer->XMAXCOL) +
        strlen(hlayer->YMAXCOL) +
        strlen(hlayer->SPATIALCOL) + 
        strlen(hlayer->LENGTHCOL) + 
        strlen(hlayer->AREACOL));

    sql = (char*) calloc(1, cb);
    if (!sql) {
        ret = _GDBSetErrorInfo (herr, GDBAPI_EOUTMEM, "calloc failed",
            0, 0, __FILE__, __LINE__, __FUNCTION__);
        return_CreateLayerTable(ret);
    }

    pos = sprintf (sql, "CREATE TABLE IF NOT EXISTS %s (\n",
        hlayer->TABLENAME);
    for (i = 0; i < numcols; i++) {
        _GDBColDef2SQL((HGDBColDef) coldefs[i], tmp);
        pos += sprintf (sql+pos, "%s,\n", tmp);
    }

    pos += sprintf (sql+pos,
        "%s INTEGER UNIQUE NOT NULL,\n"
        "%s DOUBLE,\n"
        "%s DOUBLE,\n"
        "%s DOUBLE,\n"
        "%s DOUBLE,\n"
        "%s BLOB",
        hlayer->ROWIDCOL,
        hlayer->XMINCOL,
        hlayer->YMINCOL,
        hlayer->XMAXCOL,
        hlayer->YMAXCOL,
        hlayer->SPATIALCOL);

    if (hlayer->LENGTHCOL[0]) {
        pos += sprintf (sql+pos, ",\n%s DOUBLE", hlayer->LENGTHCOL);
    }

    if (hlayer->AREACOL[0]) {
        pos += sprintf (sql+pos, ",\n%s DOUBLE", hlayer->AREACOL);
    }

    pos += sprintf (sql+pos, ");");
    assert (pos < cb);

    /* execute layer table creation */
    ret = GDBExecuteSQL ((HGDBCTX) hctx, sql, herr);
    free(sql);

    if (ret != GDBAPI_SUCCESS) {
        GDBRollbackTransaction ((HGDBCTX) hctx, herr);
        return_CreateLayerTable(ret);
    } else {
        ret = GDBCommitTransaction((HGDBCTX) hctx, herr);
    }

    return ret;
}

static GDBAPI_RESULT CreateImageTable (
    HGDBContext hctx,
    const char *tablename,
    const GDB_COLDEF *coldefs,
    int numcols,
    HGDBLayer hlayer,
    HGDBERR herr)
{
    assert (0 && "TODO");
    return GDBAPI_ENOTIMPL;
}

/**
 * GDB TABLE API
 */
GDBAPI_RESULT GDBTableCreate (HGDBCTX ctx,
    GDB_TABLE_TYPE tabletype,
    const char *tablename,
    const GDB_COLDEF *coldefs,
    int numcols,
    GDB_HANDLE hspatial,
    HGDBERR herr)
{
    HGDBContext hctx;
    CAST_HANDLE_CONTEXT(ctx, herr, hctx);
  
    if (tabletype == GDB_TABLE_LAYER) {
        /*
     * create spatial table
     */
        if (hspatial) {
            HGDBLayer hlayer;
            CAST_HANDLE_LAYER(hspatial, herr, hlayer);
            return CreateLayerTable(hctx, tablename, coldefs, numcols, hlayer, herr);
        } else {
            return _GDBSetErrorInfo (herr, GDBAPI_EHANDLE, "Invalid hspatial",
                0, 0, __FILE__, __LINE__, "GDBTableCreate");
        }
    } else if (tabletype == GDB_TABLE_IMAGE) {
        /*
     * create image table
     */
        if (hspatial) {
            //TODO: CAST_HANDLE_IMAGE(imageinfo, herr, himage);
            //return CreateLayerTable(hctx, tablename, coldefs, numcols, himage, herr);
            assert (0 && "TODO");
            return GDBAPI_ENOTIMPL;
        } else {
            return _GDBSetErrorInfo (herr, GDBAPI_EHANDLE, "Invalid imageinfo",
                0, 0, __FILE__, __LINE__, "GDBTableCreate");
        }
    } else {
        /*
     * create generic table
     */
        int  i, pos, cb;
        char tmp[200];
        char *sql = 0;

        cb = sprintf (tmp, "CREATE TABLE IF NOT EXISTS %s (\n", tablename);
        for (i = 0; i < numcols; i++) {
            cb += _GDBColDef2SQL((HGDBColDef) coldefs[i], tmp);
            cb += 2;
        }

        sql = (char*) calloc(1, cb);
        if (!sql) {
            return _GDBSetErrorInfo (herr, GDBAPI_EOUTMEM, "calloc failed",
                0, 0, __FILE__, __LINE__, "GDBTableCreate");
        }

        pos = sprintf (sql, "CREATE TABLE IF NOT EXISTS %s (\n", tablename);
        for (i = 0; i < numcols; i++) {
            _GDBColDef2SQL((HGDBColDef) coldefs[i], tmp);
            if (i < numcols -1) {
                pos += sprintf (sql+pos, "%s,\n", tmp);
            } else {
                pos += sprintf (sql+pos, "%s);", tmp);
            }
        }

        cb = GDBExecuteSQL ((HGDBCTX) hctx, sql, herr);
        free(sql);
        return cb;
    }
}

static GDBAPI_RESULT GetLayerInfo (HGDBCTX ctx,
    const char *tablename,
    GDB_LAYER layerinfo,
    HGDBERR herr)
{
    int   ret;
    double dbl;
    char *str;

    HGDBSTMT hstmt;
    sqlite3_stmt *pStmt;

    /* hlayer can be NULL so as to get if it is a LAYER table */
    HGDBLayer hlayer = (HGDBLayer) layerinfo;

    ret = GDBHandleAlloc ((GDB_HANDLE*) &hstmt, GDB_HTYPE_STMT, 0, 300);
    GDBHandleGetData (hstmt, (void**) &str);

    ret = snprintf (str, 300-1,
        "SELECT "
            "LAYERID,TABLESPACE,OWNER,"
            "TABLENAME,SPATIALCOL,ROWIDCOL,"
            "XMINCOL,YMINCOL,XMAXCOL,YMAXCOL,"
            "LENGTHCOL,AREACOL,"
            "LAYERTYPE,LAYERMASK,"
            "LEVELMIN,LEVELMAX,"
            "XMIN,YMIN,XMAX,YMAX,"
            "ZMIN,ZMAX,MMIN,MMAX,REFID,MINIMUMID,DESCRIPT,CREDATE "
        "FROM GDBLAYERS WHERE TABLENAME='%s';",
        tablename);
    str[300-1] = 0;

    ret = GDBStmtPrepare (ctx, str, ret, hstmt, herr);
    if (ret != GDBAPI_SUCCESS) {
        return ret;
    }

    pStmt = ((HGDBStmt) hstmt)->stmt;

    while (GDBAPI_DB_ROW == GDBStmtExecute(hstmt, herr)) {
        if (sqlite3_column_isnull(pStmt, PARAMID_LAYERID)) {
            GDBStmtFinish(hstmt, GDBAPI_TRUE, herr);
            return GDBAPI_EAPPERR;
        }

        if (hlayer) {
            ret = sqlite3_column_int(pStmt, PARAMID_LAYERID);
            hlayer->LAYERID = ret;

            str = (char*) sqlite3_column_text(pStmt, PARAMID_TABLESPACE);
            ret = GDBAttrSet (layerinfo, str, -1, GDB_ATTR_LAYER_TABLESPACE, herr);
            assert (ret == GDBAPI_SUCCESS);

            str = (char*) sqlite3_column_text(pStmt, PARAMID_OWNER);
            ret = GDBAttrSet (layerinfo, str, -1, GDB_ATTR_LAYER_OWNER, herr);
            assert (ret == GDBAPI_SUCCESS);

            str = (char*) sqlite3_column_text(pStmt, PARAMID_TABLENAME);
            ret = GDBAttrSet (layerinfo, str, -1, GDB_ATTR_LAYER_TABLENAME, herr);
            assert (ret == GDBAPI_SUCCESS);

            str = (char*) sqlite3_column_text(pStmt, PARAMID_SPATIALCOL);
            ret = GDBAttrSet (layerinfo, str, -1, GDB_ATTR_LAYER_SPATIALCOL, herr);
            assert (ret == GDBAPI_SUCCESS);

            str = (char*) sqlite3_column_text(pStmt, PARAMID_ROWIDCOL);
            ret = GDBAttrSet (layerinfo, str, -1, GDB_ATTR_LAYER_ROWIDCOL, herr);
            assert (ret == GDBAPI_SUCCESS);

            str = (char*) sqlite3_column_text(pStmt, PARAMID_XMINCOL);
            ret = GDBAttrSet (layerinfo, str, -1, GDB_ATTR_LAYER_XMINCOL, herr);
            assert (ret == GDBAPI_SUCCESS);
    
            str = (char*) sqlite3_column_text(pStmt, PARAMID_YMINCOL);
            ret = GDBAttrSet (layerinfo, str, -1, GDB_ATTR_LAYER_YMINCOL, herr);
            assert (ret == GDBAPI_SUCCESS);
        
            str = (char*) sqlite3_column_text(pStmt, PARAMID_XMAXCOL);
            ret = GDBAttrSet (layerinfo, str, -1, GDB_ATTR_LAYER_XMAXCOL, herr);
            assert (ret == GDBAPI_SUCCESS);

            str = (char*) sqlite3_column_text(pStmt, PARAMID_YMAXCOL);
            ret = GDBAttrSet (layerinfo, str, -1, GDB_ATTR_LAYER_YMAXCOL, herr);
            assert (ret == GDBAPI_SUCCESS);

            str = (char*) sqlite3_column_text(pStmt, PARAMID_LENGTHCOL);
            ret = GDBAttrSet (layerinfo, str, -1, GDB_ATTR_LAYER_LENGTHCOL, herr);
            assert (ret == GDBAPI_SUCCESS);

            str = (char*) sqlite3_column_text(pStmt, PARAMID_AREACOL);
            ret = GDBAttrSet (layerinfo, str, -1, GDB_ATTR_LAYER_AREACOL, herr);
            assert (ret == GDBAPI_SUCCESS);

            if (sqlite3_column_isnull(pStmt, PARAMID_LAYERTYPE)) {
                ret = GDBAttrSet (layerinfo, 0, 0, GDB_ATTR_LAYER_LAYERTYPE, herr);
            } else {
                ret = sqlite3_column_int(pStmt, PARAMID_LAYERTYPE);
                ret = GDBAttrSet (layerinfo, (void*)&ret, 0, GDB_ATTR_LAYER_LAYERTYPE, herr);
            }
            assert (ret == GDBAPI_SUCCESS);

            if (sqlite3_column_isnull(pStmt, PARAMID_LAYERMASK)) {
                ret = GDBAttrSet (layerinfo, 0, 0, GDB_ATTR_LAYER_LAYERMASK, herr);
            } else {
                ret = sqlite3_column_int(pStmt, PARAMID_LAYERMASK);
                ret = GDBAttrSet (layerinfo, (void*) &ret, 0, GDB_ATTR_LAYER_LAYERMASK, herr);
            }
            assert (ret == GDBAPI_SUCCESS);

            if (sqlite3_column_isnull(pStmt, PARAMID_LEVELMIN)) {
                ret = GDBAttrSet (layerinfo, 0, 0, GDB_ATTR_LAYER_LEVELMIN, herr);
            } else {
                ret = sqlite3_column_int (pStmt, PARAMID_LEVELMIN);
                ret = GDBAttrSet (layerinfo, (void*) &ret, 0, GDB_ATTR_LAYER_LEVELMIN, herr);
            }
            assert (ret == GDBAPI_SUCCESS);

            if (sqlite3_column_isnull(pStmt, PARAMID_LEVELMAX)) {
                ret = GDBAttrSet (layerinfo, 0, 0, GDB_ATTR_LAYER_LEVELMAX, herr);
            } else {
                ret = sqlite3_column_int (pStmt, PARAMID_LEVELMAX);
                ret = GDBAttrSet (layerinfo, (void*) &ret, 0, GDB_ATTR_LAYER_LEVELMAX, herr);
            }
            assert (ret == GDBAPI_SUCCESS);

            if (sqlite3_column_isnull(pStmt, PARAMID_XMIN)) {
                ret = GDBAttrSet (layerinfo, 0, 0, GDB_ATTR_LAYER_XMIN, herr);
            } else {
                dbl = sqlite3_column_double(pStmt, PARAMID_XMIN);
                ret = GDBAttrSet (layerinfo, (void*) &dbl, 0, GDB_ATTR_LAYER_XMIN, herr);
            }
            assert (ret == GDBAPI_SUCCESS);

            if (sqlite3_column_isnull(pStmt, PARAMID_YMIN)) {
                ret = GDBAttrSet (layerinfo, 0, 0, GDB_ATTR_LAYER_YMIN, herr);
            } else {
                dbl = sqlite3_column_double(pStmt, PARAMID_YMIN);
                ret = GDBAttrSet (layerinfo, (void*) &dbl, 0, GDB_ATTR_LAYER_YMIN, herr);
            }
            assert (ret == GDBAPI_SUCCESS);

            if (sqlite3_column_isnull(pStmt, PARAMID_XMAX)) {
                ret = GDBAttrSet (layerinfo, 0, 0, GDB_ATTR_LAYER_XMAX, herr);
            } else {
                dbl = sqlite3_column_double(pStmt, PARAMID_XMAX);
                ret = GDBAttrSet (layerinfo, (void*) &dbl, 0, GDB_ATTR_LAYER_XMAX, herr);
            }
            assert (ret == GDBAPI_SUCCESS);
 
            if (sqlite3_column_isnull(pStmt, PARAMID_YMAX)) {
                ret = GDBAttrSet (layerinfo, 0, 0, GDB_ATTR_LAYER_YMAX, herr);
            } else {
                dbl = sqlite3_column_double(pStmt, PARAMID_YMAX);
                ret = GDBAttrSet (layerinfo, (void*) &dbl, 0, GDB_ATTR_LAYER_YMAX, herr);
            }
            assert (ret == GDBAPI_SUCCESS);

            if (sqlite3_column_isnull(pStmt, PARAMID_ZMIN)) {
                ret = GDBAttrSet (layerinfo, 0, 0, GDB_ATTR_LAYER_ZMIN, herr);
            } else {
                dbl = sqlite3_column_double(pStmt, PARAMID_ZMIN);
                ret = GDBAttrSet (layerinfo, (void*) &dbl, 0, GDB_ATTR_LAYER_ZMIN, herr);
            }
            assert (ret == GDBAPI_SUCCESS);

            if (sqlite3_column_isnull(pStmt, PARAMID_ZMAX)) {
                ret = GDBAttrSet (layerinfo, 0, 0, GDB_ATTR_LAYER_ZMAX, herr);
            } else {
                dbl = sqlite3_column_double(pStmt, PARAMID_ZMAX);
                ret = GDBAttrSet (layerinfo, (void*) &dbl, 0, GDB_ATTR_LAYER_ZMAX, herr);
            }
            assert (ret == GDBAPI_SUCCESS);

            if (sqlite3_column_isnull(pStmt, PARAMID_MMIN)) {
                ret = GDBAttrSet (layerinfo, 0, 0, GDB_ATTR_LAYER_MMIN, herr);
            } else {
                dbl = sqlite3_column_double(pStmt, PARAMID_MMIN);
                ret = GDBAttrSet (layerinfo, (void*) &dbl, 0, GDB_ATTR_LAYER_MMIN, herr);
            }
            assert (ret == GDBAPI_SUCCESS);

            if (sqlite3_column_isnull(pStmt, PARAMID_MMAX)) {
                ret = GDBAttrSet (layerinfo, 0, 0, GDB_ATTR_LAYER_MMAX, herr);
            } else {
                dbl = sqlite3_column_double(pStmt, PARAMID_MMAX);
                ret = GDBAttrSet (layerinfo, (void*) &dbl, 0, GDB_ATTR_LAYER_MMAX, herr);
            }
            assert (ret == GDBAPI_SUCCESS);

            if (sqlite3_column_isnull(pStmt, PARAMID_REFID)) {
                ret = GDBAttrSet (layerinfo, 0, 0, GDB_ATTR_LAYER_REFID, herr);
            } else {
                ret = sqlite3_column_int(pStmt, PARAMID_REFID);
                ret = GDBAttrSet (layerinfo, (void*) &ret, 0, GDB_ATTR_LAYER_REFID, herr);
            }
            assert (ret == GDBAPI_SUCCESS);  

            if (sqlite3_column_isnull(pStmt, PARAMID_MINIMUMID)) {
                ret = GDBAttrSet (layerinfo, 0, 0, GDB_ATTR_LAYER_MINIMUMID, herr);
            } else {
                ret = sqlite3_column_int(pStmt, PARAMID_MINIMUMID);
                ret = GDBAttrSet (layerinfo, (void*) &ret, 0, GDB_ATTR_LAYER_MINIMUMID, herr);
            }
            assert (ret == GDBAPI_SUCCESS);

            str = (char*) sqlite3_column_text(pStmt, PARAMID_DESCRIPT);
            ret = GDBAttrSet (layerinfo, str, -1, GDB_ATTR_LAYER_DESCRIPT, herr);
            assert (ret == GDBAPI_SUCCESS);

            str = (char*) sqlite3_column_text(pStmt, PARAMID_CREDATE);
            SafeCopyString(hlayer->CREDATE, str, sizeof(hlayer->CREDATE));
        }
    }

    ret = GDBStmtFinish(hstmt, GDBAPI_TRUE, herr);
    assert (ret == GDBAPI_SUCCESS);

    return GDBAPI_SUCCESS;
}

static GDBAPI_RESULT GetImageInfo (HGDBCTX ctx,
    const char *tablename,
    GDB_LAYER imageinfo,
    HGDBERR herr)
{
    assert (0 && "TODO");
    return GDBAPI_ENOTIMPL;
}

static GDBAPI_RESULT GetTableInfo (HGDBCTX ctx,
    const char *tablename,
    GDB_COLDEF *coldefs,
    int *numcols,
    HGDBERR herr)
{
    HGDBStmt hstmt;
    char *sql;
    int ret;

    GDBHandleAlloc ((GDB_HANDLE*) &hstmt, GDB_HTYPE_STMT, 0, 100);
    GDBHandleGetData ((GDB_HANDLE) hstmt, (void**) &sql);

    ret = snprintf (sql, 100, "PRAGMA table_info('%s');", tablename);

    ret = GDBStmtPrepare (ctx, sql, ret, (GDB_HANDLE)hstmt, herr);

    if (ret == GDBAPI_SUCCESS) {
        int colindex = 0;

        while (GDBAPI_DB_ROW == GDBStmtExecute((HGDBSTMT) hstmt, herr)) {
            if (coldefs) {
                /* populate coldefs */
                HGDBColDef coldef; /* tmp variable */

                if (colindex < *numcols) {
                    coldef = (HGDBColDef) coldefs[colindex];

                    /* col name */
                    SafeCopyString(coldef->colname,
                        (const char*) sqlite3_column_text(hstmt->stmt, COLINDEX_NAME),
                        sizeof(coldef->colname));

                    /* col data type */
                    _GDBColDefSetType(coldef, (const char*) sqlite3_column_text(hstmt->stmt, COLINDEX_TYPE));

                    /* col NOT NULL */
                    coldef->notnull =
                        (sqlite3_column_int(hstmt->stmt, COLINDEX_NOTNULL) ? GDBAPI_TRUE : GDBAPI_FALSE);

                    /* col default value */
                    SafeCopyString(coldef->dfltvalue,
                        (const char*) sqlite3_column_text(hstmt->stmt, COLINDEX_DFLT_VALUE),
                        sizeof(coldef->dfltvalue));

                    /* col is Primary Key ? */
                    coldef->prikey =
                        (sqlite3_column_int(hstmt->stmt, COLINDEX_PK) ? GDBAPI_TRUE : GDBAPI_FALSE);
                } else {
                    return
                        _GDBSetErrorInfo (herr, GDBAPI_EPARAM, "Insufficent coldefs to recieve more cols",
                            0, 0, __FILE__, __LINE__, "GDBTableGetInfo");
                }
            }

            colindex++;
        }

        /* get columns count */
        if (!coldefs) {
            *numcols = colindex;
        }
    }

    GDBStmtFinish((GDB_STMT)hstmt, GDBAPI_TRUE, herr);

    return ret;
}

static GDBAPI_RESULT GetTableRowcount (HGDBCTX ctx,
    const char *tablename,
    rowid_t *rowcount,
    HGDBERR herr)
{
    int ret;

    char *sql;
    HGDBSTMT hstmt;
  
    ret = GDBHandleAlloc (&hstmt, GDB_HTYPE_STMT, 0, 128);
    if (ret != GDBAPI_SUCCESS) {
        GDBERROR_THROW_EOUTMEM(herr);
    }

    GDBHandleGetData (hstmt, (void**) &sql);
    ret = snprintf (sql, 128, "SELECT COUNT(*) FROM %s;", tablename);
    ret = GDBStmtPrepare (ctx, sql, ret, hstmt, herr);
    if (ret == GDBAPI_SUCCESS) {
        while (GDBAPI_DB_ROW == GDBStmtExecute (hstmt, herr)) {
            if (sizeof(rowid_t) == sizeof(sqlite3_int64)) {
                *rowcount = (rowid_t) sqlite3_column_int64 (((HGDBStmt) hstmt)->stmt, 0);
            } else {
                *rowcount = (rowid_t) sqlite3_column_int (((HGDBStmt) hstmt)->stmt, 0);
            }
        }
    }

    GDBHandleFree (hstmt);
    return ret;
}

GDBAPI_RESULT GDBTableGetInfo (
    HGDBCTX ctx,
    const char *tablename,
    GDB_TABLE_TYPE *tabletype,
    rowid_t *rowcount,
    int *numcols,
    GDB_COLDEF *coldefs,
    GDB_HANDLE hspatial,
    HGDBERR herr)
{
    int ret;
    int tabletypeSpecified;

    if (rowcount) {
        ret = GetTableRowcount (ctx, tablename, rowcount, herr);
        if (! tabletype) {
            return ret;
        }
    }

    /* get numcols */
    if (numcols) {
        /* table must exist */
        ret = GetTableInfo (ctx, tablename, coldefs, numcols, herr);
        if (ret != GDBAPI_SUCCESS) {
            return ret;
        } else if (!tabletype) {
            return GDBAPI_SUCCESS;
        }
    }

    if (!tabletype) {
        return _GDBSetErrorInfo (herr, GDBAPI_EPARAM, "Invalid pointer to tabletype",
                0, 0, __FILE__, __LINE__, "GDBTableGetInfo");
    }
    tabletypeSpecified = *tabletype;

    /* invalid hspatial (==0) show we only care about to get table type */
    if (!hspatial) {
        *tabletype = GDB_TABLE_GENERIC;

        if (tabletypeSpecified == GDB_TABLE_LAYER) {
            ret = GetLayerInfo (ctx, tablename, hspatial, herr);
            if (ret == GDBAPI_SUCCESS) {
                *tabletype = GDB_TABLE_LAYER;
            }
        } else if (tabletypeSpecified == GDB_TABLE_IMAGE) {
            ret = GetImageInfo (ctx, tablename, hspatial, herr);
            if (ret == GDBAPI_SUCCESS) {
                *tabletype = GDB_TABLE_IMAGE;
            }
        }

        return GDBAPI_SUCCESS;
    }

    /* valid hspatial (!=0) so as to get spatial info */
    if (tabletypeSpecified == GDB_TABLE_LAYER) {
        if (GDBHandleGetType(hspatial) != GDB_HTYPE_LAYER) {
            return _GDBSetErrorInfo (herr, GDBAPI_EHTYPE,
                "Wrong type with hspatial: A GDB_LAYER hspatial required",
                0, 0, __FILE__, __LINE__, "GDBTableGetInfo");
        } else {
            return GetLayerInfo(ctx, tablename, hspatial, herr);
        }
    } else if (tabletypeSpecified == GDB_HTYPE_IMAGE) {
        if (GDBHandleGetType(hspatial) != GDB_HTYPE_IMAGE) {
            return _GDBSetErrorInfo (herr, GDBAPI_EHTYPE,
                "Wrong type with hspatial: A GDB_RASTER hspatial required",
                0, 0, __FILE__, __LINE__, "GDBTableGetInfo");
        } else {
            assert (0 && "TODO");
            return GetImageInfo(ctx, tablename, hspatial, herr);
        }
    } else {
        return _GDBSetErrorInfo (herr, GDBAPI_EPARAM, "Invalid tabletype",
            0, 0, __FILE__, __LINE__, "GDBTableGetInfo");
    }
}


GDBAPI_RESULT GDBPrepareInsert (
    GDB_CONTEXT ctx,
    const char *tablename,
    GDB_TABLE_TYPE tabletype,
    const GDB_COLDEF *coldefs,
    int *insColinds,
    int numColinds,
    GDB_HANDLE hspatial,
    GDB_ERROR herr)
{
    int i, at, cb, ret;
    GDBColValList *collist;
    char *sql, *spatstr;
  
    HGDBContext hctx;
    HGDBSTMT   hstmt;
    HGDBColDef hcol;

    int saptcols_len = 0;
    HGDBLayer  hlayer = 0;
    HGDBIMAGE  himage = 0; // TODO

    char tmp[100];

    CAST_HANDLE_CONTEXT(ctx, herr, hctx);

    if (numColinds > GDB_TABLE_MAX_NUMCOLS) {
        return _GDBSetErrorInfo (herr, GDBAPI_ERROR, "Too many columns to insert",
                0, 0, __FILE__, __LINE__, __FUNCTION__);
    }

    if (hspatial) {
        if (tabletype == GDB_TABLE_LAYER && GDBHandleGetType(hspatial) == GDB_HTYPE_LAYER) {
            hlayer = (HGDBLayer) hspatial;
            saptcols_len = 100 +
                strlen(hlayer->ROWIDCOL) +
                strlen(hlayer->XMINCOL) +
                strlen(hlayer->YMINCOL) +
                strlen(hlayer->XMAXCOL) +
                strlen(hlayer->YMAXCOL) +
                strlen(hlayer->SPATIALCOL) + 
                strlen(hlayer->LENGTHCOL) + 
                strlen(hlayer->AREACOL);
        } else if (tabletype == GDB_TABLE_IMAGE && GDBHandleGetType(hspatial) == GDB_HTYPE_IMAGE) {
            himage = hspatial;
            assert (0);
        }
    }

    /* get size of sql clause */
    /* format sql clause */
    cb = sprintf (tmp, "INSERT INTO %s (", tablename);
    for (i = 0; i < numColinds; i++) {
        at = (insColinds? insColinds[i] : i);
        hcol = (HGDBColDef) coldefs[at];
        cb += sprintf (tmp, ",%s", hcol->colname) + 2;
    }
    cb += 100 + saptcols_len;

    ret = GDBHandleAlloc (&hstmt, GDB_HTYPE_STMT, 0, cb);
    if (ret != GDBAPI_SUCCESS) {
        return
            _GDBSetErrorInfo (herr, ret, "GDBHandleAlloc failed",
                0, 0, __FILE__, __LINE__, __FUNCTION__);
    }

    /* alloc memory block and attach it to hstmt */
    collist = _GDBColValListAlloc((HGDBColDef*) coldefs, insColinds, numColinds);
    if (!collist) {
        return
            _GDBSetErrorInfo (herr, GDBAPI_EOUTMEM, "_GDBColValListAlloc failed",
                0, 0, __FILE__, __LINE__, __FUNCTION__);
    }
    ((GDBStmt*) hstmt)->attachment = (void*) collist;

    GDBHandleGetData (hstmt, (void**) &spatstr);
    sql = &spatstr[100];

    /* format sql clause */
    cb = sprintf (sql, "INSERT INTO %s (", tablename);
    for (i = 0; i < numColinds; i++) {
        at = (insColinds? insColinds[i] : i);
        hcol = (HGDBColDef) coldefs[at];
        if (i == 0) {
            cb += sprintf (sql+cb, "%s", hcol->colname);
        } else {
            cb += sprintf (sql+cb, ",%s", hcol->colname);
        }
    }

    if (hlayer) {
        collist->hcolhead->_tbltype = GDB_TABLE_LAYER;
        if (i > 0) {
            cb += sprintf (sql+cb, ",");
        }
        cb += _GDBLayerFormatSpatCols(hlayer, sql+cb, spatstr, & collist->hcolhead->_spatcols);
    } else if (himage) {
        collist->hcolhead->_tbltype = GDB_TABLE_IMAGE;
        if (i > 0) {
            cb += sprintf (sql+cb, ",");
        }
        assert (0 && "TODO: insert image cols");
    }

    cb += sprintf (sql+cb, ") VALUES (");
    for (i = 0; i < numColinds; i++) {
        if (i == 0) {
            cb += sprintf (sql+cb, "?");
        } else {
            cb += sprintf (sql+cb, ",?");
        }
    }

    if (collist->hcolhead->_spatcols) {
        if (i > 0) {
            cb += sprintf (sql+cb, ",");
        }
        cb += sprintf (sql+cb, "%s", spatstr);
    }
  
    cb += sprintf (sql+cb, ");");

    ret = GDBStmtPrepare (ctx, sql, cb, hstmt, herr);
    if (ret == GDBAPI_SUCCESS) {
        _ContextSet_hstmt_INSERT_COLUMNS(hctx, hstmt, herr);
        return GDBAPI_SUCCESS;
    } else {
        return ret;
    }
}

extern
GDBAPI_RESULT GDBExecuteInsert (
    GDB_CONTEXT ctx,
    rowid_t startRowid,
    GDBCALLBACK_ExecuteInsertRow pfnInsertRow,
    void *pInsertParam,
    GDB_ERROR herr)
{
    rowid_t rowid;
    int ret, col;
  
    GDBColValList *colList;
    GDBColValHead *colHead;
    GDBColVal *colVal;

    HGDBContext hctx;

    HGDBSTMT    hstmt = 0;

    GDB_HANDLE  hspat = 0;

    int ROWIDCOL = 0;
    int SPATIALCOL = 0;
    int XMINCOL = 0;
    int YMINCOL = 0;
    int XMAXCOL = 0;
    int YMAXCOL = 0;
    int LENGTHCOL = 0;
    int AREACOL = 0;

    CAST_HANDLE_CONTEXT(ctx, herr, hctx);

    hstmt = hctx->hstmt_INSERT_COLUMNS;
    assert (hstmt);
    if (!hstmt) {
        return
            _GDBSetErrorInfo (herr, GDBAPI_EHANDLE, "Invalid GDB_STMT handle",
                0, 0, __FILE__, __LINE__, __FUNCTION__);
    }
    colList = (GDBColValList*) ((GDBStmt *) hstmt)->attachment;
    if (!colList) {
        return
            _GDBSetErrorInfo (herr, GDBAPI_EHANDLE, "Invalid GDB_STMT handle",
                0, 0, __FILE__, __LINE__, __FUNCTION__);
    }

    colHead = colList->hcolhead;
    if (colHead->_tbltype == GDB_TABLE_LAYER) {
     ret = GDBHandleAlloc (&hspat, GDB_HTYPE_SHAPE, 0, 0);

     XMINCOL = GDBStmtGetParamIndex(hstmt, ":XMINCOL");
     YMINCOL = GDBStmtGetParamIndex(hstmt, ":YMINCOL");
     XMAXCOL = GDBStmtGetParamIndex(hstmt, ":XMAXCOL");
     YMAXCOL = GDBStmtGetParamIndex(hstmt, ":YMAXCOL");
     ROWIDCOL = GDBStmtGetParamIndex(hstmt, ":ROWIDCOL");
     SPATIALCOL = GDBStmtGetParamIndex(hstmt, ":SPATIALCOL");
     LENGTHCOL = GDBStmtGetParamIndex(hstmt, ":LENGTHCOL");
     AREACOL = GDBStmtGetParamIndex(hstmt, ":AREACOL");
    } else if (colHead->_tbltype == GDB_TABLE_IMAGE) {
        assert (0);
        ret = GDBHandleAlloc (&hspat, GDB_HTYPE_IMGTILE, 0, 0);
    }

    rowid = startRowid;
    do {
        ret = GDBStmtReset(hstmt, herr);
        if (ret != GDBAPI_SUCCESS) {
            GDBHandleFree (hspat);
            return ret;
        }

        ret = pfnInsertRow (ctx, rowid,
            colList->num_hcolvals, (HGDBCOLVAL*) colList->hcolvals, hspat,
            pInsertParam, herr);
        if (ret == GDBAPI_STOP_END) {
            /* 0: stop and return */
            GDBHandleFree (hspat);
            return GDBAPI_SUCCESS;
        }

        if (ret == GDBAPI_STOP_ERR) {
            GDBHandleFree (hspat);
            return GDBAPI_STOP_ERR;
        }

        if (ret < 0) {
            /* -1: skip n row */
            rowid -= ret;
            ret = GDBAPI_DB_DONE;
            continue;
        }
         
        /* 1: next N row */
        rowid += ret;

        /* bind all attribute cols */
        for (col = 0; col < colList->num_hcolvals; col++) {
            colVal = colList->hcolvals[col];
      
            ret = GDBStmtBindParam (hstmt, col+1, colVal->coladdr, colVal->colsize,
                GDB_COLTYPE_TO_PARAMTYPE(colVal->coltype), herr);

            if (ret != GDBAPI_SUCCESS) {
                GDBHandleFree (hspat);
                return ret;
            }
        }

        /* bind spatial cols: shape or imgtile */
        if (colHead->_tbltype == GDB_TABLE_LAYER && hspat) {
            HGDBShape pShape;
            CAST_HANDLE_SHAPE(hspat, herr, pShape);

            if (XMINCOL > 0) {
                ret = GDBStmtBindParam (hstmt, XMINCOL, &pShape->bound.Xmin, 0,
                    GDB_PARAM_DOUBLE, herr);
                CHECK_RETVAL(ret);
            }
            if (YMINCOL > 0) {
                ret = GDBStmtBindParam (hstmt, YMINCOL, &pShape->bound.Ymin, 0,
                    GDB_PARAM_DOUBLE, herr);
                CHECK_RETVAL(ret);
            }
            if (XMAXCOL > 0) {
                ret = GDBStmtBindParam (hstmt, XMAXCOL, &pShape->bound.Xmax, 0,
                    GDB_PARAM_DOUBLE, herr);
                CHECK_RETVAL(ret);
            }
            if (YMAXCOL > 0) {
                ret = GDBStmtBindParam (hstmt, YMAXCOL, &pShape->bound.Ymax, 0,
                    GDB_PARAM_DOUBLE, herr);
                CHECK_RETVAL(ret);
            }
            if (ROWIDCOL > 0) {
                ret = GDBStmtBindParam (hstmt, ROWIDCOL, &pShape->rowid,
                    sizeof(pShape->rowid), GDB_PARAM_INT, herr);
                CHECK_RETVAL(ret);
            }
            if (SPATIALCOL > 0) {
                ret = GDBStmtBindParam (hstmt, SPATIALCOL, pShape->wkb,
                    pShape->wkbSize, GDB_PARAM_BLOB, herr); 
                CHECK_RETVAL(ret);
            }
            if (LENGTHCOL > 0) {
                ret = GDBStmtBindParam (hstmt, LENGTHCOL, &pShape->length, 0,
                    GDB_PARAM_DOUBLE, herr);
                CHECK_RETVAL(ret);
            }
            if (AREACOL > 0) {
                ret = GDBStmtBindParam (hstmt, AREACOL, &pShape->area, 0,
                    GDB_PARAM_DOUBLE, herr);
                CHECK_RETVAL(ret);
            }
        } else if (colHead->_tbltype == GDB_TABLE_IMAGE && hspat) {
            assert (0);
        }
    } while (GDBAPI_DB_DONE == GDBStmtExecute(hstmt, herr));

    GDBHandleFree (hspat);
    return GDBAPI_ERROR;
}

GDBAPI_RESULT GDBColValSetAddr (
    GDB_COLVAL hcolval,
    void *colValue,
    int cbSizeValue,
    GDB_ERROR herr)
{
    return GDBAttrSet (hcolval, colValue, cbSizeValue, GDB_ATTR_COLVAL_ADDR, herr);
}

GDBAPI_RESULT GDBColValSetCopy (
    GDB_COLVAL hcolval,
    void *colValue,
    int cbSizeValue,
    GDB_PARAM_TYPE type,
    GDB_ERROR herr)
{
    return GDBAttrSet (hcolval, colValue, cbSizeValue, GDB_ATTR_COLVAL_ADDR, herr);
}

GDBAPI_RESULT GDBFinishInsert (
    GDB_CONTEXT ctx,
    GDB_ERROR herr)
{
    HGDBContext hctx;
    CAST_HANDLE_CONTEXT(ctx, herr, hctx);
    _ContextSet_hstmt_INSERT_COLUMNS(hctx, 0, herr);
    return GDBAPI_SUCCESS;
}
