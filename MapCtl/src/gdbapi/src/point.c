/**
 * point.c
 *
 * @since
 *    2014-3-31
 */

#include "gdbapi.h"

#include "./matalg/matalg_base.h"


GDBAPI_RESULT GDBPointGetDistance (
    const GDBPointZ * start,
    const GDBPointZ * end,
    const GDB_SPATREF spref,
    double *val)
{
    if (! spref) {
        double dX = start->X - end->X;
        double dY = start->Y - end->Y;
        double dZ = start->Z - end->Z;
        *val = sqrt(dX*dX + dY*dY + dZ*dZ);

        return GDBAPI_SUCCESS;
    } else {
        return GDBAPI_ENOTIMPL;
    }
}


GDBAPI_RESULT GDBPointGetAzimuth (
    const GDBPointZ * start,
    const GDBPointZ * end,
    const GDB_SPATREF spref,
    double *val)
{
    if (! spref) {
        double az = atan2(end->Y - start->Y, end->X - start->X);
	    *val = (az < 0) ? (az+M_PI*2) : az;
    
        return GDBAPI_SUCCESS;
    } else {
        return GDBAPI_ENOTIMPL;
    }
}
