/**
 * gdberror.c
 * @brief
 *    GDB_ERROR implementation
 * @author
 *    ZhangLiang
 * @since
 *    2013-4-25
 * @date
 *    2013-5-31
 */
#ifndef _GDB_ERROR_H_
#define _GDB_ERROR_H_

#ifdef    __cplusplus
extern "C" {
#endif

#include "gdbtype.h"

#include <stdarg.h>

#define GDBERR_MESSAGE_SIZE 128
#define GDBERR_CALLSTACK_SIZE 1200


/**
 * error throw marcos
 */
#define CHECK_RETVAL(retval) \
    if ((retval) != GDBAPI_SUCCESS) \
        return (retval)

#define GDBERROR_THROW_EOUTMEM(herror) \
    return \
        _GDBSetErrorInfo (herror, GDBAPI_EOUTMEM, \
            "Out of memory", \
            0, 0, __FILE__, __LINE__, __FUNCTION__)

#define GDBERROR_THROW_EATTR(herror) \
    return \
        _GDBSetErrorInfo (herror, GDBAPI_EATTR, \
            "Attr not found", \
            0, 0, __FILE__, __LINE__, __FUNCTION__)

#define GDBERROR_THROW_ERDONLY(herror) \
    return \
        _GDBSetErrorInfo (herror, GDBAPI_ERDONLY, \
            "Attr is read only", \
            0, 0, __FILE__, __LINE__, __FUNCTION__)

#define GDBERROR_THROW_EATTRVAL(herror) \
    return \
        _GDBSetErrorInfo (hError, GDBAPI_EATTRVAL, \
            "Incorrect attr value", \
            0, 0, __FILE__, __LINE__, __FUNCTION__)

#define GDBERROR_THROW_EHTYPE(herror) \
    return \
        _GDBSetErrorInfo (herror, GDBAPI_EHTYPE, \
            "Incorrect handle type", \
            0, 0, __FILE__, __LINE__, __FUNCTION__)

#define GDBERROR_THROW_EHANDLE(herror) \
    return \
        _GDBSetErrorInfo (herror, GDBAPI_EHANDLE, \
            "Invalid handle", \
            0, 0, __FILE__, __LINE__, __FUNCTION__)

#define ATTR_CHECK_SET_SIZE(pSizeValue, RequiredSize, hErr) do { \
        if (pSizeValue) { \
            if (*pSizeValue < RequiredSize) { \
                return  \
                    _GDBSetErrorInfo (hErr, GDBAPI_EPARAM, "Insufficent cbSizeValue", \
                    0, 0, __FILE__, __LINE__, __FUNCTION__); \
            } \
            *pSizeValue = RequiredSize; \
        } \
    } while (0)

#define ATTR_GET_TYPED_VALUE(TYPE, member, pAttrValue, cbSizeValue, hError) \
    do { \
        if (pAttrValue) { \
            ATTR_CHECK_SET_SIZE(cbSizeValue, sizeof(TYPE), hError); \
            *((TYPE*) pAttrValue) = (member); \
        } else { \
            *cbSizeValue = sizeof(TYPE); \
        } \
    } while (0)

#define ATTR_GET_STRING_ADDR(member, pAttrValue, cbSizeValue, hError) \
    do { \
        assert(pAttrValue || cbSizeValue); \
        if (pAttrValue) { \
            if (!cbSizeValue || (*cbSizeValue) == 0) { \
                *((char**) pAttrValue) = (char*) (member); \
                if (cbSizeValue) { \
                    *cbSizeValue = (int) strlen(member) + 1; \
                } \
            } else { \
                int cb = (int) strlen(member) + 1; \
                if (*cbSizeValue >= cb) { \
                    memcpy(pAttrValue, (member), cb); \
                    *cbSizeValue = cb; \
                } \
            } \
        } else { \
            *cbSizeValue = (int) strlen(member) + 1; \
        } \
    } while (0)

typedef struct _GDBError
{
    GDBHandleOb __handle[1];
 
    /* error code */
    GDBAPI_RESULT gdbapi_errcode;
    int           sqlite3_errcode; /* gdbapi_errcode == GDBAPI_SQLITE3_ERROR */

    /* GDBAPI error message */
    char   gdbapi_errmsg[GDBERR_MESSAGE_SIZE];

    /* sqlite3 api error */
    char   sqlite3_errmsg[GDBERR_MESSAGE_SIZE];

    /* where error occurred */
    char   error_source[GDBERR_MESSAGE_SIZE];

    char   __errmsg[GDBERR_MESSAGE_SIZE*4];

    /* call stack */
    char   call_stack[GDBERR_CALLSTACK_SIZE];
} GDBError, *HGDBError;


#define CAST_HANDLE_ERROR(hdl, herr, herror) \
    CAST_HANDLE_TYPE(hdl, herr, GDB_HTYPE_ERROR, HGDBError, herror)


static const char *GDBErrorMessage (GDBAPI_RESULT errCode)
{
    return "";
}

static GDBAPI_RESULT _GDBSetErrorInfo (HGDBERR hError,
    GDBAPI_RESULT gdbErrCode,
    const char *szErrMessage,
    int sqlite3ErrCode,
    const char *sqlite3ErrMsg,
    const char *szFname,
    int nLine,
    const char *szFunc)
{
    HGDBError herr = (HGDBError) hError;
    if (herr) {
        herr->gdbapi_errcode = gdbErrCode;
        herr->sqlite3_errcode = sqlite3ErrCode;

        *herr->gdbapi_errmsg = 0;
        *herr->sqlite3_errmsg = 0;
        *herr->error_source = 0;

        if (gdbErrCode == GDBAPI_SQLITE3_ERROR) {
            herr->sqlite3_errcode = sqlite3ErrCode;
            if (sqlite3ErrMsg) {
                SafeCopyString(herr->sqlite3_errmsg, sqlite3ErrMsg, sizeof(herr->sqlite3_errmsg));
            }
        } else if (gdbErrCode != GDBAPI_SUCCESS) {
            _snprintf (herr->gdbapi_errmsg, sizeof(herr->gdbapi_errmsg), "%s. (%s)",
                GDBErrorMessage(gdbErrCode), szErrMessage? szErrMessage : "");
            herr->gdbapi_errmsg[sizeof(herr->gdbapi_errmsg)-1] = 0;
        }

        if (szFname) {
            char * filename = 0;
            if (strrchr(szFname, '/')) {
                filename = strrchr(szFname, '/');
                filename++;
            } else if (strrchr(szFname, '\\')) {
                filename = strrchr(szFname, '\\');
                filename++;
            }

            if (filename) {
                _snprintf (herr->error_source, sizeof(herr->error_source), "%s(): <%s: %d>",
                    szFunc, filename, nLine);
                herr->error_source[sizeof(herr->error_source)-1] = 0;
            }
        }
    }

    return gdbErrCode;
}

typedef char* (*OutputParamFuncType) (char*, size_t, void*);

static char* OutputStrParam (char *_outStr, size_t outStrSize, void *val)
{
    *_outStr = 0;
    if (sizeof(val) == 8) {
        sprintf (_outStr, "<0x%016l64x> \"%s\"", *((size_t*)val), *(char**)val);
    } else {
        sprintf (_outStr, "<0x%08x> \"%s\"", *((size_t*)val), *(char**)val);
    }
    return _outStr;
}

static char* OutputPtrParam (char *_outStr, size_t outStrSize, void *val)
{
    *_outStr = 0;
    if (sizeof(val) == 8) {
        sprintf (_outStr, "<0x%016l64x>", *((size_t*)val));
    } else {
        sprintf (_outStr, "<0x%08x>", *((size_t*)val));
    }
    return _outStr;
}

static char* OutputIntParam (char *_outStr, size_t outStrSize, void *val)
{
    *_outStr = 0;
    if (sizeof(val) == 8) {
        sprintf (_outStr, "<int64> %l64d", *((size_t*)val));
    } else {
        sprintf (_outStr, "<int> %d", *((size_t*)val));
    }
    return _outStr;
}

static char* OutputDblParam (char *_outStr, size_t outStrSize, void *val)
{
    *_outStr = 0;
    sprintf (_outStr, "<double> %lf", *((double*)val));
    return _outStr;
}

static char* OutputParams (char *destBuf, int destBufSize, const char *funcName,...)
{
    int cb, argc = 0;

    char *param = 0;
    OutputParamFuncType pfnOutput;
    void *value;

    char outStr[300];

    va_list argptr;

    cb = sprintf (destBuf, "%s ():", funcName);

    va_start(argptr, funcName);
        while ((param = va_arg(argptr, char*)) != 0) {
            pfnOutput = va_arg(argptr, OutputParamFuncType);
            value = va_arg(argptr, void *);

            cb += sprintf (destBuf+cb, "\n\t%s %s",
                param, pfnOutput(outStr, 300, value));
        }
    va_end(argptr);

    fprintf(stdout, "%s", destBuf);

    return destBuf;
} 

#define PUSH_CALL_STACK(funcname,...) do { \
        char output[1200]; \
        OutputParams(output, sizeof(output), funcname, __VA_ARGS__, 0); \
    } while (0)

#define VARNAME(name) (#name)

#define STRPARAM(var) VARNAME(var), &OutputStrParam, (void*) &var

#define INTPARAM(var) VARNAME(var), &OutputIntParam, (void*) &var

#define DBLPARAM(var) VARNAME(var), &OutputDblParam, (void*) &var

#define PTRPARAM(var) VARNAME(var), &OutputPtrParam, (void*) &var


static void GDBErrorSetCallStack (const char *funcName,...)
{
    va_list argptr;
    int argc = 0;

    va_start(argptr, funcName);
        if (argc == 0) {
            printf("Func Name: %s\n", va_arg(argptr, char*));
        } else if (argc % 2 == 1) {
            printf("\tParam Name: %s\n", va_arg(argptr, char*));
        } else if (argc % 2 == 0) {
            printf("\tParam Value: %s\n", va_arg(argptr, char*));
        }
        argc++;
    va_end(argptr);
} 

#ifdef    __cplusplus
}
#endif

#endif /* _GDB_ERROR_H_ */
