/**
 * gdblayer.c
 * @brief
 *    GDB_LAYER implementation
 * @author
 *    ZhangLiang
 * @since
 *    2013-4-25
 * @date
 *    2013-6-02
 */

#include "gdblayer.h"
#include "gdberror.h"
#include "gdbcoldef.h"
#include "gdbcontext.h"
#include "gdbfilter.h"
#include "gdbshape.h"

#define ExtRectTestOverlap(a, b) \
    ((a.Xmin < b.Xmax) && (a.Ymin < b.Ymax) \
    && (b.Xmin < a.Xmax) && (b.Ymin < a.Ymax))

/**
 * ShapeFileImport
 */
typedef struct ShapeFileImport
{
    SHPHandle      hSHP;
    DBFHandle      hDBF;
    SHPObjectEx   *pShpObjEx;

    int            nShapeType;
    int            nEntities;

    int            nDBFCols;
    DBFFieldInfo  *DBFFields;
    GDB_COLDEF    *DBFColdefs;

    int            numFreeHandles;
    GDB_HANDLE    *FreeHandles;

    int            cInsertShapes;
    int            cErrorShapes;
} ShapeFileImport;

typedef struct InsertShapeRowParam
{
    ShapeFileImport *shpFileImport;

    GDBCALLBACK_ImportSHPFileOnExecute   OnExecuteFunc;
    void *OnExecuteParam;
} InsertShapeRowParam;

static void FreeShapeFileImport (ShapeFileImport *shpImp)
{
    int num;
    char *psz;
    GDB_HANDLE hdl;

    SHPClose(shpImp->hSHP);
    DBFClose(shpImp->hDBF);

    SHPDestroyObjectEx(shpImp->pShpObjEx);

    num = shpImp->nDBFCols;
    shpImp->nDBFCols = 0;
    while (num-- > 0) {
        psz = shpImp->DBFFields[num].pszVal;
        shpImp->DBFFields[num].pszVal = 0;
        if (psz) {
            free(psz);
        }
    }

    num = shpImp->numFreeHandles;
    shpImp->numFreeHandles = 0;
    while (num-- > 0) {
        hdl = shpImp->FreeHandles[num];
        shpImp->FreeHandles[num] = 0;
        GDBHandleFree (hdl);
    }
}

static void DBFField2GdbColdef (DBFFieldInfo *field, GDB_COLDEF coldef)
{
    int coltype;

    GDBAttrSet (coldef, field->szFieldName, -1, GDB_ATTR_COLDEF_NAME, 0);
    GDBAttrSet (coldef, &field->nWidth, 0, GDB_ATTR_COLDEF_LENGTH, 0);
    GDBAttrSet (coldef, &field->nDecimals, 0, GDB_ATTR_COLDEF_SCALE, 0);
    GDBAttrSet (coldef, &field->isNotNull, 0, GDB_ATTR_COLDEF_NOTNULL, 0);

    switch (field->eType) {
    case FTString:
        coltype = GDB_CTYPE_STRING;
        break;
    case FTInteger:
        coltype = GDB_CTYPE_INTEGER;
        break;
    case FTDouble:
        coltype = GDB_CTYPE_NUMERIC;
        break;
    case FTLogical:
        coltype = GDB_CTYPE_STRING;
        break;
    case FTInvalid:
        coltype = GDB_CTYPE_BLOB;
        break;
    case FTDateS:
        coltype = GDB_CTYPE_STRING;
        break;
    }

    GDBAttrSet (coldef, (void*)&coltype, 0, GDB_ATTR_COLDEF_TYPE, 0);
}

static void QueryGetColName (int nDBFCols, DBFFieldInfo *DBFFields,
    char szColName[GDB_COLNAME_LEN+1])
{
    const char *pFieldName;
    char suffix_str[11];
    int suffix_id = 0;
    int i = 0;
    int n = strlen(szColName);

    for (; i < nDBFCols; i++) {
        pFieldName = DBFFields[i].szFieldName;

        if (! _stricmp(pFieldName, szColName)) {
            /* find repeat col name */
            sprintf (suffix_str, "%d", ++suffix_id);
            szColName[n] = 0;
            strcat(szColName, suffix_str);
            i = 0;
        }
    }
}

static int InsertShapeRow (
    GDB_CONTEXT ctx,
    rowid_t rowNumb,
    int numCols,
    GDB_COLVAL *hColValues,
    GDB_HANDLE hSpatial,
    void *insertParam,
    GDB_ERROR hError)
{
    int  iShape, col, j, cbSize, ret;

    void *wkbShape;
    double dblValue;

    GDB_COLVAL hColVal;
  
    DBFFieldInfo *pField;
    SHPObjectEx *hShpOb;

    ShapeFileImport *shpImp;

    InsertShapeRowParam *rowParam = (InsertShapeRowParam *) insertParam;
  
    shpImp = rowParam->shpFileImport;
  
    iShape = (int) rowNumb;

    if (iShape >= shpImp->nEntities) {
        return 0;
    }

    if (! SHPReadObjectEx (shpImp->hSHP, iShape, shpImp->pShpObjEx)) {
        shpImp->cErrorShapes++;
        return -1;
    }

    hShpOb = shpImp->pShpObjEx;
    ret = SHPObjectExValidatePolygon(hShpOb, 1);

    /* get size of wkb */
    cbSize = SHPObjectEx2WKB (hShpOb, 0, 0, 0, 0, 0);
    if (! cbSize) {
        shpImp->cErrorShapes++;
        return -1;
    }

    /* resize shape data buffer */
    GDBAttrSet (hSpatial, 0, cbSize, GDB_ATTR_SHAPE_WKBCOPY, hError);

    /* get addr of pointer to wkb data */
    cbSize = 0; /* tell GDBAPI we want addr of WKB */
    GDBAttrGet (hSpatial, (void*) &wkbShape, &cbSize, GDB_ATTR_SHAPE_WKBADDR, hError);

    /* convert to wkb */
    cbSize = SHPObjectEx2WKB (hShpOb, wkbShape, 0, 0, 0, 0);

    GDBAttrSet (hSpatial, (void*) &hShpOb->dfXMin, 0, GDB_ATTR_SHAPE_XMIN, hError);
    GDBAttrSet (hSpatial, (void*) &hShpOb->dfYMin, 0, GDB_ATTR_SHAPE_YMIN, hError);
    GDBAttrSet (hSpatial, (void*) &hShpOb->dfXMax, 0, GDB_ATTR_SHAPE_XMAX, hError);
    GDBAttrSet (hSpatial, (void*) &hShpOb->dfYMax, 0, GDB_ATTR_SHAPE_YMAX, hError);
                
    dblValue = SHPObjectExGetLength(hShpOb);
    GDBAttrSet (hSpatial, (void*)&dblValue, 0, GDB_ATTR_SHAPE_LENGTH, hError);
  
    dblValue = SHPObjectExGetArea(hShpOb);
    GDBAttrSet (hSpatial, (void*)&dblValue, 0, GDB_ATTR_SHAPE_AREA, hError);

    /* TODO */
    GDBAttrSet (hSpatial, (void*) &iShape, 0, GDB_ATTR_SHAPE_ROWID, hError);

    for (col = 0; col < numCols; col++) {
        hColVal = hColValues[col];
        GDBAttrGet (hColVal, (void*) &j, 0, GDB_ATTR_COLVAL_INDEX, hError);
  
        pField = &shpImp->DBFFields[j];

        switch (pField->eType) {
        case FTString:
            if (DBFIsAttributeNULL(shpImp->hDBF, iShape, j)) {
                GDBColValSetAddr (hColVal, 0, 0, hError);
            } else {
                cbSize = DBFReadCopyStringAttribute(shpImp->hDBF, iShape, j, pField->pszVal);
                GDBColValSetAddr(hColVal, pField->pszVal, cbSize, hError);
            }
            break;
        case FTDateS:
            if (DBFIsAttributeNULL(shpImp->hDBF, iShape, j)) {
                GDBColValSetAddr(hColVal, 0, 0, hError);
            } else {
                cbSize = DBFReadCopyStringAttribute(shpImp->hDBF, iShape, j, pField->dtVal);
                assert (cbSize <= 20);
                pField->dtVal[19] = 0;
                GDBColValSetAddr(hColVal, pField->dtVal, cbSize, hError);
            }
            break;
        case FTLogical:
            if (DBFIsAttributeNULL(shpImp->hDBF, iShape, j)) {
                GDBColValSetAddr(hColVal, 0, 0, hError);
            } else {
                strncpy(pField->logVal, DBFReadLogicalAttribute(shpImp->hDBF, iShape, j), 
                    pField->nWidth);
                pField->logVal[pField->nWidth] = 0;
                GDBColValSetAddr(hColVal, pField->logVal, pField->nWidth, hError);
            }
            break;
        case FTInteger:
            if (DBFIsAttributeNULL(shpImp->hDBF, iShape, j)) {
                GDBColValSetAddr(hColVal, 0, 0, hError);
            } else {
                pField->intVal = DBFReadIntegerAttribute(shpImp->hDBF, iShape, j);
                GDBColValSetAddr(hColVal, (void*) &pField->intVal, 0, hError);
            }
            break;
        case FTDouble:
            if (DBFIsAttributeNULL(shpImp->hDBF, iShape, j)) {
                GDBColValSetAddr(hColVal, 0, 0, hError);
            } else {
                pField->dblVal = DBFReadDoubleAttribute(shpImp->hDBF, iShape, j);
                GDBColValSetAddr(hColVal, (void*) &pField->dblVal, 0, hError);
            }
            break;
        case FTInvalid:
            break;
        }
    }

    ret = 1;
    if (rowParam->OnExecuteFunc) {
        ret = rowParam->OnExecuteFunc (ctx,
            shpImp->nEntities,
            iShape, hShpOb->nSHPType, hShpOb->nParts, hShpOb->nVertices,
            numCols, hColValues,
            hSpatial,
            rowParam->OnExecuteParam,
            hError);
    }

    if (ret == 1) {
        shpImp->cInsertShapes++;
    }

    return ret;
}


GDBAPI_RESULT LayerBindInsert (HGDBCTX ctx, HGDBERR herr, HGDBLayer layer)
{
        int ret;  
        GDB_STMT stmt;
    
        HGDBContext hctx = (HGDBContext) ctx;

        stmt = hctx->hstmt_INSERT_GDBLAYERS;
        if (!stmt) {
                return GDBAPI_ERROR;
        }

        if (layer->colflags[PARAMID_TABLESPACE]) {
                ret = GDBStmtBindParam (stmt, PARAMID_TABLESPACE, layer->TABLESPACE, -1,
                        GDB_PARAM_TEXT, herr);
                CHECK_RETVAL(ret);
        }
        if (layer->colflags[PARAMID_OWNER]) {
                ret = GDBStmtBindParam (stmt, PARAMID_OWNER, layer->OWNER, -1,
                        GDB_PARAM_TEXT, herr);
                CHECK_RETVAL(ret);
        }
        if (layer->colflags[PARAMID_TABLENAME]) {
                ret = GDBStmtBindParam (stmt, PARAMID_TABLENAME, layer->TABLENAME, -1,
                        GDB_PARAM_TEXT, herr);
                CHECK_RETVAL(ret);
        }
        if (layer->colflags[PARAMID_SPATIALCOL]) {
                GDBStmtBindParam (stmt, PARAMID_SPATIALCOL, layer->SPATIALCOL, -1,
                        GDB_PARAM_TEXT, herr);
                CHECK_RETVAL(ret);
        }
        if (layer->colflags[PARAMID_ROWIDCOL]) {
                ret = GDBStmtBindParam (stmt, PARAMID_ROWIDCOL, layer->ROWIDCOL, -1,
                        GDB_PARAM_TEXT, herr);
                CHECK_RETVAL(ret);
        }
        if (layer->colflags[PARAMID_XMINCOL]) {
                ret = GDBStmtBindParam (stmt, PARAMID_XMINCOL, layer->XMINCOL, -1,
                        GDB_PARAM_TEXT, herr);
                CHECK_RETVAL(ret);
        }
        if (layer->colflags[PARAMID_YMINCOL]) {
                ret = GDBStmtBindParam (stmt, PARAMID_YMINCOL, layer->YMINCOL, -1,
                        GDB_PARAM_TEXT, herr);
                CHECK_RETVAL(ret);
        }
        if (layer->colflags[PARAMID_XMAXCOL]) {
                ret = GDBStmtBindParam (stmt, PARAMID_XMAXCOL, layer->XMAXCOL, -1,
                        GDB_PARAM_TEXT, herr);
                CHECK_RETVAL(ret);
        }
        if (layer->colflags[PARAMID_YMAXCOL]) {
                ret = GDBStmtBindParam (stmt, PARAMID_YMAXCOL, layer->YMAXCOL, -1,
                        GDB_PARAM_TEXT, herr);
                CHECK_RETVAL(ret);
        }
        if (layer->colflags[PARAMID_LENGTHCOL]) {
                ret = GDBStmtBindParam (stmt, PARAMID_LENGTHCOL, layer->LENGTHCOL, -1,
                        GDB_PARAM_TEXT, herr);
                CHECK_RETVAL(ret);
        }
        if (layer->colflags[PARAMID_AREACOL]) {
                ret = GDBStmtBindParam (stmt, PARAMID_AREACOL, layer->AREACOL, -1,
                        GDB_PARAM_TEXT, herr);
                CHECK_RETVAL(ret);
        }
        if (layer->colflags[PARAMID_LAYERTYPE]) {
                ret = GDBStmtBindParam (stmt, PARAMID_LAYERTYPE, (void*)&layer->LAYERTYPE,
                        0, GDB_PARAM_INT, herr);
                CHECK_RETVAL(ret);
        }
        if (layer->colflags[PARAMID_LAYERMASK]) {
                ret = GDBStmtBindParam (stmt, PARAMID_LAYERMASK, (void*)&layer->LAYERMASK,
                        0, GDB_PARAM_INT, herr);
                CHECK_RETVAL(ret);
        }
        if (layer->colflags[PARAMID_LEVELMIN]) {
                ret = GDBStmtBindParam (stmt, PARAMID_LEVELMIN, (void*)&layer->LEVELMIN,
                        0, GDB_PARAM_INT, herr);
                CHECK_RETVAL(ret);
        }
        if (layer->colflags[PARAMID_LEVELMAX]) {
                ret = GDBStmtBindParam (stmt, PARAMID_LEVELMAX, (void*)&layer->LEVELMAX,
                        0, GDB_PARAM_INT, herr);
                CHECK_RETVAL(ret);
        }
        if (layer->colflags[PARAMID_XMIN]) {
                ret = GDBStmtBindParam (stmt, PARAMID_XMIN, (void*)&layer->XMIN, 0,
                        GDB_PARAM_DOUBLE, herr);
                CHECK_RETVAL(ret);
        }
        if (layer->colflags[PARAMID_YMIN]) {
                ret = GDBStmtBindParam (stmt, PARAMID_YMIN, (void*)&layer->YMIN, 0,
                        GDB_PARAM_DOUBLE, herr);
                CHECK_RETVAL(ret);
        }
        if (layer->colflags[PARAMID_XMAX]) {
                ret = GDBStmtBindParam (stmt, PARAMID_XMAX, (void*)&layer->XMAX, 0,
                        GDB_PARAM_DOUBLE, herr);
                CHECK_RETVAL(ret);
        }
        if (layer->colflags[PARAMID_YMAX]) {
                ret = GDBStmtBindParam (stmt, PARAMID_YMAX, (void*)&layer->YMAX, 0,
                        GDB_PARAM_DOUBLE, herr);
                CHECK_RETVAL(ret);
        }
        if (layer->colflags[PARAMID_ZMIN]) {
                ret = GDBStmtBindParam (stmt, PARAMID_ZMIN, (void*)&layer->ZMIN, 0,
                        GDB_PARAM_DOUBLE, herr);
                CHECK_RETVAL(ret);
        }
        if (layer->colflags[PARAMID_ZMAX]) {
                ret = GDBStmtBindParam (stmt, PARAMID_ZMAX, (void*)&layer->ZMAX, 0,
                        GDB_PARAM_DOUBLE, herr);
                CHECK_RETVAL(ret);
        }
        if (layer->colflags[PARAMID_MMIN]) {
                ret = GDBStmtBindParam (stmt, PARAMID_MMIN, (void*)&layer->MMIN, 0,
                        GDB_PARAM_DOUBLE, herr);
                CHECK_RETVAL(ret);
        }
        if (layer->colflags[PARAMID_MMAX]) {
                ret = GDBStmtBindParam (stmt, PARAMID_MMAX, (void*)&layer->MMAX, 0,
                        GDB_PARAM_DOUBLE, herr);
                CHECK_RETVAL(ret);
        }
        if (layer->colflags[PARAMID_REFID]) {
                ret = GDBStmtBindParam (stmt, PARAMID_REFID, (void*)&layer->REFID, 0,
                        GDB_PARAM_INT, herr);
                CHECK_RETVAL(ret);
        }
        if (layer->colflags[PARAMID_MINIMUMID]) {
                ret = GDBStmtBindParam (stmt, PARAMID_MINIMUMID, (void*)&layer->MINIMUMID,
                        sizeof(layer->MINIMUMID), GDB_PARAM_INT, herr);
                CHECK_RETVAL(ret);  
        }
        if (layer->colflags[PARAMID_DESCRIPT]) {
                ret = GDBStmtBindParam (stmt, PARAMID_DESCRIPT, layer->DESCRIPT, -1,
                        GDB_PARAM_TEXT, herr);
                CHECK_RETVAL(ret);   
        }

        return GDBAPI_SUCCESS;
}


static GDBAPI_RESULT LayerAreaIntersect (
        GDB_CONTEXT ctx,
        HGDBLayer   subjectLayer,
        HGDBLayer   clipperLayer,
        rowid_t     clipRowOffset,   /* 0 */
        rowid_t     *pClipRowCount,  /* -1 */
        HGDBLayer   resultLayer,
        char        *subjectRowidCol,
        char        *clipRowidCol,
        GDBCallback_LayerOverlayOnExecute pfnOnExecute,
        void        *inParam,
        rowid_t     *resultRowcount,
        GDB_ERROR   herr)
{
    int ret, clipRowno, digits = 3;

    int MINXI, MINYI, MAXXI, MAXYI,
        SHAPEID, XMIN, YMIN, XMAX, YMAX, SHAPE, AREA, LENG,
        SUBJSHAPEID, CLIPSHAPEID;

    char *buffer;
    GDBExtGrid  shapeGrid;

    int numHandles = 0;
    GDB_HANDLE FreeHandles[8];

    HGDBSTMT  hstmtClip,
                        hstmtSubject,
                        hstmtResult;

    GDBLayerRowShape rowClip;
    GDBLayerRowShape rowSubj;

    HPOLYGON hplgs[3];

    HGDBSHP  hshpResult;
    void     *wkbShapeData;
    int      wkbDataSize;

    RectType plgRect;
    double   dblValue;
    rowid_t  rowid;

    double X0, Y0, W0;

    int levelMin = subjectLayer->LEVELMIN;
    int levelMax = subjectLayer->LEVELMAX;
    int level;

    char index[16];

    memset (hplgs, 0, sizeof(hplgs));
    memset (FreeHandles, 0, sizeof(FreeHandles));

    /* create polygons
   */
    if (! PolygonAlloc (DBL_EPSILON, hplgs, sizeof(hplgs)/sizeof(hplgs[0]))) {
        goto RET_EXIT;
    }

    ret = GDBHandleAlloc (&hshpResult, GDB_HTYPE_SHAPE, 0, 0);
    if (ret != GDBAPI_SUCCESS) {
        goto RET_EXIT;
    }
    FreeHandles[numHandles++] = hshpResult;

    ret = GDBHandleAlloc (&hstmtClip, GDB_HTYPE_STMT, 0, 1000);
    if (ret != GDBAPI_SUCCESS) {
        goto RET_EXIT;
    }
    FreeHandles[numHandles++] = hstmtClip;

    GDBHandleGetData (hstmtClip, (void**) &buffer);

    /* clip si table: buffer+0 */
    sprintf (buffer, "GDBLAYER_SI_%d", clipperLayer->LAYERID);

    /* subject si table: buffer+50 */
    sprintf (buffer + 50, "GDBLAYER_SI_%d", subjectLayer->LAYERID);

    if (sizeof(clipRowOffset) == sizeof(sqlite3_int64)) {
        if (pClipRowCount) {
            if (*pClipRowCount == 0) {
                *pClipRowCount = -1;
            }
            sprintf (buffer+950, "%lld,%lld;", clipRowOffset, *pClipRowCount);
        } else {
            sprintf (buffer+950, "%lld,-1;", clipRowOffset);
        }
    } else {
        if (pClipRowCount) {
            if (*pClipRowCount == 0) {
                *pClipRowCount = -1;
            }
            sprintf (buffer+950, "%d,%d;", clipRowOffset, *pClipRowCount);
        } else {
            sprintf (buffer+950, "%d,-1;", clipRowOffset);
        }
    }

    /* query all shapes from clipper layer */
    ret = sprintf (buffer + 100,
        "SELECT %s,%s,%s,%s,%s,%s FROM %s LIMIT %s",
            clipperLayer->ROWIDCOL,
            clipperLayer->XMINCOL,
            clipperLayer->YMINCOL,
            clipperLayer->XMAXCOL,
            clipperLayer->YMAXCOL,
            clipperLayer->SPATIALCOL,
            clipperLayer->TABLENAME,
            buffer + 950);

    ret = GDBStmtPrepare (ctx, buffer + 100, ret, hstmtClip, herr);
    if (ret != GDBAPI_SUCCESS) {
        goto RET_EXIT;
    }

    ret = GDBStmtGetStmtInst (hstmtClip, (void**) &rowClip.stmt);
    if (ret != GDBAPI_SUCCESS) {
        goto RET_EXIT;
    }

    /* prepare query shapes from subject layer */
    ret = GDBHandleAlloc (&hstmtSubject, GDB_HTYPE_STMT, 0, 0);
    if (ret != GDBAPI_SUCCESS) {
        goto RET_EXIT;
    }
    FreeHandles[numHandles++] = hstmtSubject;

    ret = sprintf (buffer + 100,
            "SELECT DISTINCT \n"
                "\t%s.%s,\n"  /* ROWSHAPE_COLID_ROWID */
                "\t%s.%s,\n"  /* ROWSHAPE_COLID_XMIN */
                "\t%s.%s,\n"  /* ROWSHAPE_COLID_YMIN */
                "\t%s.%s,\n"  /* ROWSHAPE_COLID_XMAX */
                "\t%s.%s,\n"  /* ROWSHAPE_COLID_YMAX */
                "\t%s.%s \n"  /* ROWSHAPE_COLID_SPATIAL */
                "FROM %s INNER JOIN (\n"
                    "\tSELECT SHAPEID FROM %s \n"
                        "\t\tWHERE (XI>=:MINXI AND XI<:MAXXI) AND (YI>=:MINYI AND YI<:MAXYI)) t \n"
                        "ON t.SHAPEID=%s.%s;",
                subjectLayer->TABLENAME, subjectLayer->ROWIDCOL,   
                subjectLayer->TABLENAME, subjectLayer->XMINCOL,    
                subjectLayer->TABLENAME, subjectLayer->YMINCOL,    
                subjectLayer->TABLENAME, subjectLayer->XMAXCOL,    
                subjectLayer->TABLENAME, subjectLayer->YMAXCOL,    
                subjectLayer->TABLENAME, subjectLayer->SPATIALCOL,
                subjectLayer->TABLENAME,
                buffer + 50,
                subjectLayer->TABLENAME, subjectLayer->ROWIDCOL);

    ret = GDBStmtPrepare (ctx, buffer + 100, ret, hstmtSubject, herr);
    if (ret != GDBAPI_SUCCESS) {
        goto RET_EXIT;
    }

    MINXI = GDBStmtGetParamIndex (hstmtSubject, ":MINXI");
    MAXXI = GDBStmtGetParamIndex (hstmtSubject, ":MAXXI");
    MINYI = GDBStmtGetParamIndex (hstmtSubject, ":MINYI");
    MAXYI = GDBStmtGetParamIndex (hstmtSubject, ":MAXYI");

    GDBStmtGetStmtInst (hstmtSubject, (void**) &rowSubj.stmt);

    /*
   * prepare for insert stmt of result layer
   */
    ret = GDBHandleAlloc (&hstmtResult, GDB_HTYPE_STMT, 0, 0);
    if (ret != GDBAPI_SUCCESS) {
        goto RET_EXIT;
    }
    FreeHandles[numHandles++] = hstmtResult;

    ret = sprintf (buffer + 100,
        "INSERT INTO %s (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) VALUES ("
            ":SHAPEID,:XMIN,:YMIN,:XMAX,:YMAX,:SHAPE,:AREA,:LENG,:SUBJSHAPEID,:CLIPSHAPEID);",
            resultLayer->TABLENAME,
            resultLayer->ROWIDCOL,
            resultLayer->XMINCOL,
            resultLayer->YMINCOL,
            resultLayer->XMAXCOL,
            resultLayer->YMAXCOL,
            resultLayer->SPATIALCOL,
            resultLayer->AREACOL,
            resultLayer->LENGTHCOL,
            subjectRowidCol,
            clipRowidCol);

    ret = GDBStmtPrepare (ctx, buffer + 100, ret, hstmtResult, herr);
    if (ret != GDBAPI_SUCCESS) {
        goto RET_EXIT;
    }

    SHAPEID = GDBStmtGetParamIndex (hstmtResult, ":SHAPEID");
    XMIN = GDBStmtGetParamIndex (hstmtResult, ":XMIN");
    YMIN = GDBStmtGetParamIndex (hstmtResult, ":YMIN");
    XMAX = GDBStmtGetParamIndex (hstmtResult, ":XMAX");
    YMAX = GDBStmtGetParamIndex (hstmtResult, ":YMAX");
    SHAPE = GDBStmtGetParamIndex (hstmtResult, ":SHAPE");
    AREA = GDBStmtGetParamIndex (hstmtResult, ":AREA");
    LENG = GDBStmtGetParamIndex (hstmtResult, ":LENG");
    SUBJSHAPEID = GDBStmtGetParamIndex (hstmtResult, ":SUBJSHAPEID");
    CLIPSHAPEID = GDBStmtGetParamIndex (hstmtResult, ":CLIPSHAPEID");

    clipRowno = 0;
    rowid = 0;

    X0 = subjectLayer->_MBR.Xmin;
    Y0 = subjectLayer->_MBR.Ymin;
    W0 = MAX(subjectLayer->_MBR.Xmax - X0,  subjectLayer->_MBR.Ymax - Y0) + DBL_EPSILON;

    GDBBeginTransaction(ctx, herr);

    while ((ret = GDBStmtExecute (hstmtClip, herr)) == GDBAPI_DB_ROW) {
        clipRowno++;

        if (_GDBLayerRowShapeRead (&rowClip) < 5 ||
            ! ExtRectTestOverlap(subjectLayer->_MBR, rowClip._MBR)) {
            continue;
        }

        ret = BuildPolygonFromWKB (hplgs[0], (const byte_t*) rowClip.wkbdata, rowClip.wkbsize, digits);
        assert (ret);

        /* query shapes in subject layer */
        for (level = levelMin; level <= levelMax; level++) {
            /* get rowClip's grid extent in subjectLayer */
            quad_ext2grid_lvl (W0, 0, index, level,
                rowClip._MBR.Xmin-X0, rowClip._MBR.Ymin-Y0,
                rowClip._MBR.Xmax-X0, rowClip._MBR.Ymax-Y0,
                &shapeGrid.MinXI, &shapeGrid.MinYI,
                &shapeGrid.MaxXI, &shapeGrid.MaxYI);

            shapeGrid.MinXI += level*1000000;
            shapeGrid.MinYI += level*1000000;
            shapeGrid.MaxXI += level*1000000;
            shapeGrid.MaxYI += level*1000000;

            ret = GDBStmtReset (hstmtSubject, herr);
            assert (ret == GDBAPI_SUCCESS);

            ret = GDBStmtBindParam (hstmtSubject, MINXI, (void *) &shapeGrid.MinXI, 0,
                GDB_PARAM_INT, herr);
            assert (ret == GDBAPI_SUCCESS);
    
            ret = GDBStmtBindParam (hstmtSubject, MAXXI, (void *) &shapeGrid.MaxXI, 0,
                GDB_PARAM_INT, herr);
            assert (ret == GDBAPI_SUCCESS);
  
            ret = GDBStmtBindParam (hstmtSubject, MINYI, (void *) &shapeGrid.MinYI, 0,
                GDB_PARAM_INT, herr);
            assert (ret == GDBAPI_SUCCESS);

            ret = GDBStmtBindParam (hstmtSubject, MAXYI, (void *) &shapeGrid.MaxYI, 0,
                GDB_PARAM_INT, herr);
            assert (ret == GDBAPI_SUCCESS);

            while ((ret = GDBStmtExecute (hstmtSubject, herr)) == GDBAPI_DB_ROW) {
                if (_GDBLayerRowShapeRead (&rowSubj) < 5 ||
                    ! ExtRectTestOverlap (rowSubj._MBR, rowClip._MBR)) {
                    continue;            
                }
      
                ret = BuildPolygonFromWKB (hplgs[1],
                    (const byte_t*) rowSubj.wkbdata, rowSubj.wkbsize, digits);
                assert (ret);

                /* perform polygon clip between rowSubj and rowClip */
                PolygonClear (hplgs[2]);
                PolygonClip (PLGCLIP_INTERSECT, hplgs[1], hplgs[0], hplgs[2]);

                if (! PolygonEmpty (hplgs[2]) &&
                    PolygonRect (hplgs[2], &plgRect) &&
                    PolygonToShapeWKB (hplgs[2], hshpResult, herr)) {

                    ret = GDBStmtReset(hstmtResult, herr);
                    assert (ret == GDBAPI_SUCCESS);

                    ret = 1;
                    if (pfnOnExecute) {
                        ret = pfnOnExecute (ctx, clipRowno, 0, 0, hshpResult, inParam, herr);
                        if (ret == GDBAPI_STOP_END) {
                            break;
                        } else if (ret == GDBAPI_SKIP_ROW) {
                            continue;
                        } else if (ret == GDBAPI_STOP_ERR) {
                            GDBRollbackTransaction (ctx, herr);
                            goto RET_EXIT;
                        } else if (ret != GDBAPI_NEXT_ROW) {
                            GDBRollbackTransaction (ctx, herr);
                            ret = GDBAPI_EPARAM;
                            goto RET_EXIT;
                        }
                    }

                    GDBStmtBindParam (hstmtResult, SHAPEID,
                        (void*) &rowid, sizeof(rowid), GDB_PARAM_INT, herr);

                    GDBStmtBindParam (hstmtResult, SUBJSHAPEID,
                        (void*) &rowSubj.rowid, sizeof(rowSubj.rowid), GDB_PARAM_INT, herr);

                    GDBStmtBindParam (hstmtResult, CLIPSHAPEID,
                        (void*) &rowClip.rowid, sizeof(rowClip.rowid), GDB_PARAM_INT, herr);

                    GDBStmtBindParam (hstmtResult, XMIN, (void*) &plgRect.xmin,
                        0, GDB_PARAM_DOUBLE, herr);
                    GDBStmtBindParam (hstmtResult, YMIN, (void*) &plgRect.ymin,
                        0, GDB_PARAM_DOUBLE, herr);
                    GDBStmtBindParam (hstmtResult, XMAX, (void*) &plgRect.xmax,
                        0, GDB_PARAM_DOUBLE, herr);
                    GDBStmtBindParam (hstmtResult, YMAX, (void*) &plgRect.ymax,
                        0, GDB_PARAM_DOUBLE, herr);

                    dblValue = PolygonArea (hplgs[2]);
                    GDBStmtBindParam (hstmtResult, AREA, (void*) &dblValue,
                        0, GDB_PARAM_DOUBLE, herr);

                    dblValue = PolygonLength (hplgs[2]);
                    GDBStmtBindParam (hstmtResult, LENG, (void*) &dblValue,
                        0, GDB_PARAM_DOUBLE, herr);

                    wkbDataSize = 0; /* get addr of WKB */
                    GDBAttrGet (hshpResult, (void**) &wkbShapeData, &wkbDataSize,
                        GDB_ATTR_SHAPE_WKBADDR, herr);
                    GDBStmtBindParam (hstmtResult, SHAPE, wkbShapeData, wkbDataSize,
                        GDB_PARAM_BLOB, herr);

                    ret = GDBStmtExecute (hstmtResult, herr);
                    assert (ret == GDBAPI_DB_DONE);
                    if (ret != GDBAPI_DB_DONE) {
                        GDBRollbackTransaction (ctx, herr);
                        goto RET_EXIT;
                    }

                    rowid++;
                } else {
                }
            }
        }
    }

    *resultRowcount = rowid;
    ret = GDBCommitTransaction (ctx, herr);

RET_EXIT:

    PolygonFree (hplgs, sizeof(hplgs)/sizeof(hplgs[0]));

    while (numHandles-- > 0) {
        GDBHandleFree (FreeHandles[numHandles]);
    }

    return ret;
}

static GDBAPI_RESULT CreateResultLayerTable (HGDBCTX ctx,
        HGDBLayer clipperLayer,
        HGDBLayer subjectLayer,
        HGDBLAYER result,
        char *clipRowidCol,
        char *subjectRowidCol,
        int  sizeRowidCol,
        rowid_t clipRowcount,
        GDBCALLBACK_LayerOverlayOnPrepare pfnOnPrepare,
        void *inParam,
        HGDBERR herr)
{
    int ret;

    GDB_COLDEF coldef[2];
    char *resulttable;

    char descript[200];
    int  coltype = GDB_CTYPE_INTEGER;

    /* create result layer table */
    snprintf (clipRowidCol, sizeRowidCol, "%s_%s",
        clipperLayer->ROWIDCOL,
        clipperLayer->TABLENAME);
    clipRowidCol[sizeRowidCol -1] = 0;

    snprintf (subjectRowidCol, sizeRowidCol, "%s_%s",
        subjectLayer->ROWIDCOL, subjectLayer->TABLENAME);
    subjectRowidCol[sizeRowidCol -1] = 0;

    GDBAttrGet (result, (void*) &resulttable, 0, GDB_ATTR_LAYER_TABLENAME, herr);

    /* SHAPEID, ..., SHAPEID_SUBJECT, SHAPEID_CLIP */
    GDBAttrSet (result, "SHAPEID", -1, GDB_ATTR_LAYER_ROWIDCOL, herr);
    GDBAttrSet (result, subjectLayer->OWNER, -1, GDB_ATTR_LAYER_OWNER, herr);
    GDBAttrSet (result, & subjectLayer->LAYERTYPE, 0, GDB_ATTR_LAYER_LAYERTYPE, herr);
    GDBAttrSet (result, subjectLayer->XMINCOL, -1, GDB_ATTR_LAYER_XMINCOL, herr);
    GDBAttrSet (result, subjectLayer->YMINCOL, -1, GDB_ATTR_LAYER_YMINCOL, herr);
    GDBAttrSet (result, subjectLayer->XMAXCOL, -1, GDB_ATTR_LAYER_XMAXCOL, herr);
    GDBAttrSet (result, subjectLayer->YMAXCOL, -1, GDB_ATTR_LAYER_YMAXCOL, herr);
    GDBAttrSet (result, subjectLayer->SPATIALCOL, -1, GDB_ATTR_LAYER_SPATIALCOL, herr);
    GDBAttrSet (result, subjectLayer->AREACOL, -1, GDB_ATTR_LAYER_AREACOL, herr);
    GDBAttrSet (result, subjectLayer->LENGTHCOL, -1, GDB_ATTR_LAYER_LENGTHCOL, herr);

    GDBAttrSet (result, &subjectLayer->XMIN, 0, GDB_ATTR_LAYER_XMIN, herr);
    GDBAttrSet (result, &subjectLayer->YMIN, 0, GDB_ATTR_LAYER_YMIN, herr);
    GDBAttrSet (result, &subjectLayer->XMAX, 0, GDB_ATTR_LAYER_XMAX, herr);
    GDBAttrSet (result, &subjectLayer->YMAX, 0, GDB_ATTR_LAYER_YMAX, herr);

    GDBAttrSet (result, &subjectLayer->REFID, 0, GDB_ATTR_LAYER_REFID, herr);

    snprintf (descript, sizeof(descript), "%s: SUBJECTLAYER=%s, CLIPLAYER=%s",
        __FUNCTION__, subjectLayer->TABLENAME, clipperLayer->TABLENAME);
    GDBAttrSet (result, descript, -1, GDB_ATTR_LAYER_DESCRIPT, herr);

    GDBHandleAlloc (&coldef[0], GDB_HTYPE_COLDEF, 0, 0);

    GDBAttrSet (coldef[0], subjectRowidCol, -1, GDB_ATTR_COLDEF_NAME, herr);
    GDBAttrSet (coldef[0], (void*) &coltype, 0, GDB_ATTR_COLDEF_TYPE, herr);

    GDBHandleAlloc (&coldef[1], GDB_HTYPE_COLDEF, 0, 0);

    GDBAttrSet (coldef[1], clipRowidCol, -1, GDB_ATTR_COLDEF_NAME, herr);
    GDBAttrSet (coldef[1], (void*) &coltype, 0, GDB_ATTR_COLDEF_TYPE, herr);

    ret = 1;
    if (pfnOnPrepare) {
        ret = pfnOnPrepare (ctx, clipRowcount, result, 
            coldef, sizeof(coldef)/sizeof(coldef[0]), inParam, herr);
    }

    if (ret == 1) {
        ret = GDBTableCreate (ctx, GDB_TABLE_LAYER, resulttable,
            coldef, sizeof(coldef)/sizeof(coldef[0]), result, herr);

        GDBHandleFree (coldef[0]);
        GDBHandleFree (coldef[1]);

        return ret;
    } else {
        return GDBAPI_ERROR;
    }
}

static GDBAPI_RESULT DropResultLayerTable (HGDBCTX ctx,
        HGDBLAYER result)
{
    return GDBAPI_ERROR;
}


/*-----------------------------------------------------------------------------
 *                                Public Layer API
 *-----------------------------------------------------------------------------
 */
GDBAPI_RESULT GDBLayerCreateIndex (
        GDB_CONTEXT hCtx,
        GDB_LAYER   hLayerinfo,
        GDB_ERROR   hError)
{
    HGDBLayer hlayer;

    int  ret, xi, yi, rows = 0;

    int    level, levelMin, levelMax, numGrids;
    double X0, Y0, W0;
    char   index[GDB_LEVEL_LIMIT];

    char sitblname[GDB_TABLENAME_MAX_LEN + 1];
    char sitblindex[GDB_TABLENAME_MAX_LEN + 2];
    char siquadid[GDB_TABLENAME_MAX_LEN + 2];
    char sql[300];

    HGDBSTMT hstmtSelect;
    HGDBSTMT hstmtInsert;

    sqlite3_stmt *pstmtSel;
    sqlite3_stmt *pstmtIns;

    /* shape info */
    int   wkbsize;
    const void *wkbptr;
    int   layertype;

    rowid_t    shapeID;
    GDBExtRect shapeRect;
    RectType   gridRect;
    GDBExtGrid shapeGrid;

    CAST_HANDLE_LAYER(hLayerinfo, hError, hlayer);

    /* create spatial index table sql */
    ret = _GetLayerSITableCresql (hlayer->LAYERID,
        sitblname, sitblindex, siquadid, sql);
    ret = GDBExecuteSQL (hCtx, sql, hError);
    CHECK_RETVAL(ret);

    /* insert shape into SI table */
    ret = GDBHandleAlloc (&hstmtSelect, GDB_HTYPE_STMT, 0, 0);
    CHECK_RETVAL(ret);
    ret = sprintf (sql, "SELECT %s, %s, %s, %s, %s, %s FROM %s;",
        hlayer->ROWIDCOL, /* column: 0 */
        hlayer->XMINCOL,
        hlayer->YMINCOL,
        hlayer->XMAXCOL,
        hlayer->YMAXCOL,
        hlayer->SPATIALCOL,
        hlayer->TABLENAME);
    ret = GDBStmtPrepare (hCtx, sql, ret, hstmtSelect, hError);
    CHECK_RETVAL(ret);

    ret = GDBHandleAlloc (&hstmtInsert, GDB_HTYPE_STMT, 0, 0);
    CHECK_RETVAL(ret);
    ret = sprintf (sql, "INSERT INTO %s ("
        "ID,XI,YI,SHAPEID,MINXI,MINYI,MAXXI,MAXYI) "
        "VALUES (?1,?2,?3,?4,?5,?6,?7,?8)",
        sitblname);
    ret = GDBStmtPrepare (hCtx, sql, ret, hstmtInsert, hError);
    CHECK_RETVAL(ret);

    pstmtSel = ((HGDBStmt) hstmtSelect)->stmt;
    pstmtIns = ((HGDBStmt) hstmtInsert)->stmt;

    GDBBeginTransaction(hCtx, hError);

    X0 = hlayer->_MBR.Xmin;
    Y0 = hlayer->_MBR.Ymin;
    W0 = MAX(hlayer->_MBR.Xmax - X0,  hlayer->_MBR.Ymax - Y0) + DBL_EPSILON;

    levelMin = GDB_LEVEL_LIMIT;
    levelMax = 0;

    layertype = hlayer->LAYERTYPE % WKB_TYPEMASK;

    while ((ret = GDBStmtExecute (hstmtSelect, hError)) == GDBAPI_DB_ROW) {
        if ((wkbsize = sqlite3_column_bytes (pstmtSel, 5)) > 0 &&
            sqlite3_column_notnull (pstmtSel, 0) &&
            sqlite3_column_notnull (pstmtSel, 1) &&
            sqlite3_column_notnull (pstmtSel, 2) &&
            sqlite3_column_notnull (pstmtSel, 3) &&
            sqlite3_column_notnull (pstmtSel, 4)) {

            /* get shape info */
            shapeID = sqlite3_column_rowid (pstmtSel, 0);
            shapeRect.Xmin = sqlite3_column_double (pstmtSel, 1);
            shapeRect.Ymin = sqlite3_column_double (pstmtSel, 2);
            shapeRect.Xmax = sqlite3_column_double (pstmtSel, 3);
            shapeRect.Ymax = sqlite3_column_double (pstmtSel, 4);
            wkbptr = sqlite3_column_blob (pstmtSel, 5);

            /* get level and grid */
            level = quad_ext2grid (W0, 0,
                shapeRect.Xmin - X0, shapeRect.Ymin - Y0,
                shapeRect.Xmax - X0, shapeRect.Ymax - Y0,
                &shapeGrid.MinXI, &shapeGrid.MinYI,
                &shapeGrid.MaxXI, &shapeGrid.MaxYI);

            numGrids = (shapeGrid.MaxXI - shapeGrid.MinXI) * (shapeGrid.MaxYI - shapeGrid.MinYI);

            /* insert into SI table */
            for (xi = shapeGrid.MinXI; xi < shapeGrid.MaxXI; xi++) {
                for (yi = shapeGrid.MinYI; yi < shapeGrid.MaxYI; yi++) {

                    quad_grid2ext (W0, 0, xi, yi, level,
                        &gridRect.xmin, &gridRect.ymin, &gridRect.xmax, &gridRect.ymax);

                    RECT_OFFSET(gridRect, X0, Y0);

                    if (numGrids == 4) {
                        if (layertype == WKB_MultiPoint) {
                            /* TODO */
                            assert(0);
                        } else if (layertype != WKB_Point) {
                            /* if any edge of shape intersect with gridRect
               *    ---------- (xmax,
               *    |        |  ymax)
               *    |        |
               *    ----------
               * (xmin,
               *  ymin)
               */

                        }
                    }

                    ret = GDBStmtReset(hstmtInsert, hError);
                    assert (ret == GDBAPI_SUCCESS);

                    if (levelMin > level) {
                        levelMin = level;
                    }

                    if (levelMax < level) {
                        levelMax = level;
                    }

                    quad_grid2index (0, xi, yi, level, index);
                    sqlite3_bind_text (pstmtIns, 1, index, level, 0);

                    sqlite3_bind_int (pstmtIns, 2, level*1000000+xi);
                    sqlite3_bind_int (pstmtIns, 3, level*1000000+yi);

                    if (sizeof(shapeID) == sizeof(sqlite_int64)) {
                        sqlite3_bind_int64 (pstmtIns, 4, shapeID);
                    } else {
                        sqlite3_bind_int (pstmtIns, 4, shapeID);
                    }
                    sqlite3_bind_int (pstmtIns, 5, shapeGrid.MinXI);
                    sqlite3_bind_int (pstmtIns, 6, shapeGrid.MinYI);
                    sqlite3_bind_int (pstmtIns, 7, shapeGrid.MaxXI);
                    sqlite3_bind_int (pstmtIns, 8, shapeGrid.MaxYI);

                    ret = GDBStmtExecute(hstmtInsert, hError);
                    assert (ret == GDBAPI_DB_DONE);

                    rows++;
                }          
            }
        }
    }

    ret = GDBStmtFinish(hstmtInsert, GDBAPI_TRUE, hError);
    assert (ret == GDBAPI_SUCCESS);

    ret = GDBStmtFinish(hstmtSelect, GDBAPI_TRUE, hError);
    assert (ret == GDBAPI_SUCCESS);

    ret = GDBCommitTransaction(hCtx, hError);
    if (ret == GDBAPI_SUCCESS) {
        /* create index */
        ret = sprintf (sql, "DROP INDEX IF EXISTS %s;\n"
            "CREATE INDEX %s ON %s (\n"
            "XI, YI, SHAPEID, MINXI, MINYI, MAXXI, MAXYI);",
            sitblindex, sitblindex, sitblname);

        ret = GDBExecuteSQL (hCtx, sql, hError);
        if (ret == GDBAPI_SUCCESS) {
            ret = sprintf (sql, "DROP INDEX IF EXISTS %s;\n"
                "CREATE INDEX %s ON %s (ID);",
                siquadid, siquadid, sitblname);

            ret = GDBExecuteSQL (hCtx, sql, hError);
            if (ret == GDBAPI_SUCCESS) {
                printf("%s: total %d rows inserted.\n", __FUNCTION__, rows);

                /* GDBLAYERS: set LEVEL */
                ret = sprintf (sql, "UPDATE GDBLAYERS SET LEVELMIN=%d,LEVELMAX=%d WHERE TABLENAME='%s';",
                    levelMin, levelMax, hlayer->TABLENAME);
                ret = GDBExecuteSQL (hCtx, sql, hError);
                return GDBAPI_SUCCESS;
            }
        }
    }

    /* TODO: Drop table */  
    return ret;
}


GDBAPI_RESULT GDBLayerDropIndex (HGDBCTX ctx, GDB_LAYER layerinfo, HGDBERR herr)
{
    return 0;
}


GDBAPI_RESULT GDBLayerQuery (
    GDB_CONTEXT hCtx,
    GDB_LAYER   subject,
    GDB_COLDEF  *coldefs,
    int         *colinds,
    int         numcolinds,
    GDB_FILTER  *filters,
    int         numfilters,
    const char  *wherecond,
    GDBCALLBACK_OnFetchRow pfnOnFetchRow,
    void        *inParam,
    GDB_ERROR   herr)
{
    int col, ret;

    rowid_t rowno = 0;

    int numcols = 0;
    HGDBCOLVAL *colvals = 0;

    HGDBLayer subjectLayer;

    GDB_STMT hstmt = 0;
    GDB_SHAPE hshape = 0;

    /* temp variables */
    char *sql;
    sqlite3_stmt *pStmt;
    HGDBCOLVAL colval;
    int coltype;
    int colsize;
    const char *colname;
    int intVal;
    sqlite_int64 int64Val;
    double dblVal;

    /* spatial column indexs */
    int spatColInds[8] = {-1, -1, -1, -1, -1, -1, -1, -1};

    assert(! coldefs);
    assert(! filters);

    CAST_HANDLE_LAYER(subject, herr, subjectLayer);

    ret = GDBHandleAlloc (&hshape, GDB_HTYPE_SHAPE, 0, 0);
    if (ret != GDBAPI_SUCCESS) {
        return ret;
    }

    ret = GDBHandleAlloc (&hstmt, GDB_HTYPE_STMT, 0, 1200);
    if (ret != GDBAPI_SUCCESS) {
        GDBHandleFree (hshape);
        return ret;
    }
    GDBHandleGetData (hstmt, (void**) &sql);

    if (wherecond && *wherecond) {
        if (! _strnicmp (wherecond, "WHERE ", strlen("WHERE "))) {
            ret = sprintf (sql, "SELECT * FROM %s %s;",
                subjectLayer->TABLENAME, wherecond);
        } else {
            ret = sprintf (sql, "SELECT * FROM %s WHERE %s;",
                subjectLayer->TABLENAME, wherecond);
        }
    } else {
        ret = sprintf (sql, "SELECT * FROM %s;", subjectLayer->TABLENAME);
    }

    ret = GDBStmtPrepare (hCtx, sql, ret, hstmt, herr);
    if (ret != GDBAPI_SUCCESS) {
        GDBHandleFree (hshape);
        GDBHandleFree (hstmt);
        return ret;
    }

    ret = GDBStmtGetStmtInst (hstmt, (void**) &pStmt);
    if (ret != GDBAPI_SUCCESS) {
        GDBHandleFree (hshape);
        GDBHandleFree (hstmt);
        return ret;
    }

    numcols = sqlite3_column_count (pStmt);
    colvals = (HGDBCOLVAL*) calloc (numcols, sizeof(HGDBCOLVAL));

    for (col = 0; col < numcols; col++) {
        GDBHandleAlloc (&colval, GDB_HTYPE_COLVAL, 0, 0);

        GDBAttrSet (colval, (void*) &col, 0, GDB_ATTR_COLVAL_INDEX, herr);

        colname = sqlite3_column_name (pStmt, col);
        GDBAttrSet (colval, (void*) colname, -1, GDB_ATTR_COLVAL_COLNAME, herr);

        coltype = sqlite3_column_type (pStmt, col);
        coltype = sqlite3_coltype_2_coldef_type (coltype);

        GDBAttrSet (colval, (void*) &coltype, 0, GDB_ATTR_COLVAL_TYPE, herr);

        colvals[col] = colval;

        /* get spatial cols index */
        ret = _GDBLayerFindSpatCol (subjectLayer, colname);
        if (ret != GDBAPI_EINDEX) {
            spatColInds[ret] =  col;
        }
    }

    while ((ret = GDBStmtExecute (hstmt, herr)) == GDBAPI_DB_ROW) {
        /* get attributes columns */
        for (col = 0; col < numcols; col++) {
            colval = colvals[col];

            colsize = sqlite3_column_bytes(pStmt, col);
            if (colsize == 0) {
                GDBColValSetAddr (colval, (void*) 0, 0, herr);
            } else {
                coltype = sqlite3_column_type (pStmt, col);

                switch (coltype) {
                case SQLITE_INTEGER:
                    if (colsize <= sizeof(int)) {
                        intVal = sqlite3_column_int (pStmt, col);
                        GDBColValSetCopy (colval, &intVal, sizeof(intVal), GDB_PARAM_INT, herr);
                    } else {
                        int64Val = sqlite3_column_int64 (pStmt, col);
                        GDBColValSetCopy (colval, &int64Val, sizeof(int64Val), GDB_PARAM_INT64, herr);
                    }
                    break;
                case SQLITE_FLOAT:
                    dblVal = sqlite3_column_double (pStmt, col);
                    GDBColValSetCopy (colval, &dblVal, sizeof(dblVal), GDB_PARAM_INT64, herr);
                    break;
                case SQLITE_TEXT:
                    GDBColValSetAddr (colval, (void*) sqlite3_column_text(pStmt, col), colsize, herr);
                    break;
                case SQLITE_BLOB:
                    GDBColValSetAddr (colval, (void*) sqlite3_column_blob(pStmt, col), colsize, herr);
                    break;
                case SQLITE_NULL:
                    GDBColValSetAddr (colval, (void*) 0, 0, herr);
                    break;
                }
            }
        }

        /* get spatial columns */
        ret = spatColInds[ROWSHAPE_COLID_ROWID];
        if (ret == GDBAPI_EINDEX || sqlite3_column_isnull(pStmt, ret)) {
            GDBAttrSet (hshape, 0, 0, GDB_ATTR_SHAPE_ROWID, herr);
        } else {
            if (sizeof(rowid_t) == 8) {
                int64Val = sqlite3_column_int64 (pStmt, ret);
                GDBAttrSet (hshape, &int64Val, sizeof(int64Val), GDB_ATTR_SHAPE_ROWID, herr);
            } else {
                intVal = sqlite3_column_int (pStmt, ret);
                GDBAttrSet (hshape, &intVal, sizeof(intVal), GDB_ATTR_SHAPE_ROWID, herr);
            }      
        }

        ret = spatColInds[ROWSHAPE_COLID_XMIN];
        if (ret == GDBAPI_EINDEX || sqlite3_column_isnull(pStmt, ret)) {
            GDBAttrSet (hshape, 0, 0, GDB_ATTR_SHAPE_XMIN, herr);
        } else {
            dblVal = sqlite3_column_double (pStmt, ret);
            GDBAttrSet (hshape, &dblVal, sizeof(dblVal), GDB_ATTR_SHAPE_XMIN, herr);
        }

        ret = spatColInds[ROWSHAPE_COLID_YMIN];
        if (ret == GDBAPI_EINDEX || sqlite3_column_isnull(pStmt, ret)) {
            GDBAttrSet (hshape, 0, 0, GDB_ATTR_SHAPE_YMIN, herr);
        } else {
            dblVal = sqlite3_column_double (pStmt, ret);
            GDBAttrSet (hshape, &dblVal, sizeof(dblVal), GDB_ATTR_SHAPE_YMIN, herr);
        }

        ret = spatColInds[ROWSHAPE_COLID_XMAX];
        if (ret == GDBAPI_EINDEX || sqlite3_column_isnull(pStmt, ret)) {
            GDBAttrSet (hshape, 0, 0, GDB_ATTR_SHAPE_XMAX, herr);
        } else {
            dblVal = sqlite3_column_double (pStmt, ret);
            GDBAttrSet (hshape, &dblVal, sizeof(dblVal), GDB_ATTR_SHAPE_XMAX, herr);
        }

        ret = spatColInds[ROWSHAPE_COLID_YMAX];
        if (ret == GDBAPI_EINDEX || sqlite3_column_isnull(pStmt, ret)) {
            GDBAttrSet (hshape, 0, 0, GDB_ATTR_SHAPE_YMAX, herr);
        } else {
            dblVal = sqlite3_column_double (pStmt, ret);
            GDBAttrSet (hshape, &dblVal, sizeof(dblVal), GDB_ATTR_SHAPE_YMAX, herr);
        }

        ret = spatColInds[ROWSHAPE_COLID_SPATIAL];
        if (ret == GDBAPI_EINDEX || sqlite3_column_isnull(pStmt, ret)) {
            GDBAttrSet (hshape, 0, 0, GDB_ATTR_SHAPE_WKBADDR, herr);
        } else {
            GDBAttrSet (hshape, (void*) sqlite3_column_blob (pStmt, ret),
                sqlite3_column_bytes (pStmt, ret), GDB_ATTR_SHAPE_WKBADDR, herr);
        }

        ret = spatColInds[ROWSHAPE_COLID_AREA];
        if (ret == GDBAPI_EINDEX || sqlite3_column_isnull(pStmt, ret)) {
            GDBAttrSet (hshape, 0, 0, GDB_ATTR_SHAPE_AREA, herr);
        } else {
            dblVal = sqlite3_column_double (pStmt, ret);
            GDBAttrSet (hshape, &dblVal, sizeof(dblVal), GDB_ATTR_SHAPE_AREA, herr);
        }

        ret = spatColInds[ROWSHAPE_COLID_LENGTH];
        if (ret == GDBAPI_EINDEX || sqlite3_column_isnull(pStmt, ret)) {
            GDBAttrSet (hshape, 0, 0, GDB_ATTR_SHAPE_LENGTH, herr);
        } else {
            dblVal = sqlite3_column_double (pStmt, ret);
            GDBAttrSet (hshape, &dblVal, sizeof(dblVal), GDB_ATTR_SHAPE_LENGTH, herr);
        }

        ret = GDBAPI_NEXT_ROW;
        if (pfnOnFetchRow) {
            ret = pfnOnFetchRow (hCtx, rowno, numcols, colvals, hshape, inParam, herr);
        }

        if (ret == 0) {
            break;
        }

        /* TODO: ret */

        rowno++;
    }

    /* free colvals */
    for (col = 0; col < numcols; col++) {
        GDBHandleFree (colvals[col]);
    }
    free (colvals);

    GDBHandleFree (hshape);
    GDBHandleFree (hstmt);

    return GDBAPI_SUCCESS;
}


GDBAPI_RESULT GDBLayerOverlay (
    GDB_CONTEXT ctx,
    GDB_OVERLAY_MODE overlaymode,
    GDB_LAYER   subject,
    GDB_LAYER   clipper,
    rowid_t     clipRowOffset, /* 0 for begin */
    rowid_t     *pClipRowCount, /* NULL or -1 for all */
    GDB_LAYER   result,
    GDBCALLBACK_LayerOverlayOnPrepare pfnOnPrepare,
    GDBCallback_LayerOverlayOnExecute pfnOnExecute,
    GDBCallback_LayerOverlayOnFinish  pfnOnFinish,
    void        *inParam,
    GDB_ERROR   herr)
{
        int         ret;

        HGDBContext hctx;
        HGDBLayer   subjectLayer,
                                clipperLayer,
                                resultLayer;

        PLGCLIP_MODE   plgClipMode;

        rowid_t clipRowCount = 0;
        rowid_t resultRowCount = 0;

        if (overlaymode == GDB_OVLY_AREA_INTERSECT) {
                plgClipMode = PLGCLIP_INTERSECT;
        } else if (overlaymode == GDB_OVLY_AREA_DIFFER) {
                plgClipMode = PLGCLIP_DIFFER;
        } else {
                return
                        _GDBSetErrorInfo (herr, GDBAPI_ENOTIMPL,
                                "Not supported overlaymode",
                                0, 0,
                                __FILE__, __LINE__, __FUNCTION__);
        }

        CAST_HANDLE_CONTEXT(ctx, herr, hctx);

        if (! subject || ! result || ! clipper) {
                GDBERROR_THROW_EHANDLE(herr);
        }

        CAST_HANDLE_LAYER(subject, herr, subjectLayer);
        CAST_HANDLE_LAYER(result, herr, resultLayer);
        CAST_HANDLE_LAYER(clipper, herr, clipperLayer);

        if (clipRowOffset == GDBAPI_EINDEX) {
                return
                        _GDBSetErrorInfo (herr, GDBAPI_EPARAM,
                                "Invalid clipRowOffset",
                                0, 0,
                                __FILE__, __LINE__, __FUNCTION__);
        } else {
                char clipRowidCol[GDB_COLNAME_LEN + 1];
                char subjectRowidCol[GDB_COLNAME_LEN + 1];

                if (! pClipRowCount || *pClipRowCount == GDBAPI_EINDEX) {
                        GDBTableGetInfo (ctx, clipperLayer->TABLENAME,
                                0, &clipRowCount, 0, 0, 0, herr);
                } else {
                        clipRowCount = *pClipRowCount;
                }

                ret = CreateResultLayerTable (ctx,
                        clipperLayer,
                        subjectLayer,
                        result,
                        clipRowidCol,
                        subjectRowidCol,
                        sizeof(clipRowidCol),
                        clipRowCount,
                        pfnOnPrepare,
                        inParam,
                        herr);
                if (ret != GDBAPI_SUCCESS) {
                        return ret;
                }

                if (plgClipMode == PLGCLIP_INTERSECT) {
                        ret = LayerAreaIntersect (ctx,
                                subjectLayer,
                                clipperLayer,
                                clipRowOffset,
                                pClipRowCount,
                                resultLayer,
                                subjectRowidCol,
                                clipRowidCol,
                                pfnOnExecute,
                                inParam,
                                &resultRowCount,
                                herr);
                } else if (plgClipMode == PLGCLIP_DIFFER) {
                        /* TODO: */
                        ret = GDBAPI_ENOTIMPL;
                        assert(0);
                }

                if (ret != GDBAPI_SUCCESS) {
                        /* TODO: */
                        DropResultLayerTable (ctx, result);
                }

                if (pfnOnFinish) {
                        pfnOnFinish (ctx, resultRowCount, result, inParam, herr);
                }

                return ret;
        }
}


GDBAPI_RESULT GDBImportSHPFile (
        GDB_CONTEXT ctx,
        GDB_CREATE_MODE impmode,
        const char *shpFile,
        const char *layertable,
        const char *rowidcol,
        GDB_SPATREF spref,
        GDBCALLBACK_ImportSHPFileOnPrepare pfnOnPrepare,
        GDBCALLBACK_ImportSHPFileOnExecute pfnOnExecute,
        GDBCALLBACK_ImportSHPFileOnFinish pfnOnFinish,
        void *inParam,
        GDB_ERROR herr)
{
        int i, j, ret;

        GDB_LAYER hlayer;
        ShapeFileImport shpImp;
        GDB_TABLE_TYPE tabletype;
        InsertShapeRowParam rowParam;

        HGDBContext hctx;

        int wkbtype;
        int nHandles;
        int *colinds;

        int hasZ, hasM;
        double MinBounds[4];
            double MaxBounds[4];

        int numShapesImported = 0;
        int numShapesIgnored = 0;
  
        int refid_default = 0;

        CAST_HANDLE_CONTEXT(ctx, herr, hctx);

        /* TODO: Only support create new table mode */
        assert (impmode == GDB_ALWAYS_CREATE_NEW);

        memset(&shpImp, 0, sizeof(shpImp));
  
        shpImp.hSHP = SHPOpen (shpFile, "rb");
        shpImp.hDBF = DBFOpen (shpFile, "rb" );

            SHPGetInfo (shpImp.hSHP, &shpImp.nEntities, &shpImp.nShapeType,
            MinBounds, MaxBounds);

        SHPGetType (shpImp.hSHP, &hasZ, &hasM);

        wkbtype = SHPType2WKBType (shpImp.nShapeType);
        assert (wkbtype != -1);

        /* prepare coldefs */
        shpImp.nDBFCols = DBFGetFieldCount (shpImp.hDBF);

        /* create layer and extra data */
        nHandles = 1 + shpImp.nDBFCols;
        ret = GDBHandleAlloc (&hlayer, GDB_HTYPE_LAYER, 0,
            sizeof(GDB_HANDLE) * nHandles +
            (sizeof(int) + sizeof(DBFFieldInfo) + sizeof(GDB_COLDEF)) * shpImp.nDBFCols);
        assert (ret == GDBAPI_SUCCESS);

        GDBHandleGetData (hlayer, (void **) &shpImp.FreeHandles);

        colinds = (int*) &shpImp.FreeHandles[nHandles];
        shpImp.DBFFields = (DBFFieldInfo *) & colinds[shpImp.nDBFCols];
        shpImp.DBFColdefs = (GDB_COLDEF *) & shpImp.DBFFields[shpImp.nDBFCols];

        shpImp.FreeHandles[shpImp.numFreeHandles++] = hlayer;

        for (i = 0; i < shpImp.nDBFCols; i++) {
            GDB_COLDEF coldef = 0;
    
            j = i;
            colinds[i] = j;
    
            DBFGetFieldInfo2 (shpImp.hDBF, j, &shpImp.DBFFields[j]);

            if (shpImp.DBFFields[j].nWidth > 0) {
                shpImp.DBFFields[j].pszVal = (char*) malloc (shpImp.DBFFields[j].nWidth + 1);
            }

            GDBHandleAlloc (&coldef, GDB_HTYPE_COLDEF, 0, 0);
            DBFField2GdbColdef (&shpImp.DBFFields[j], coldef);

            shpImp.FreeHandles[shpImp.numFreeHandles++] = coldef;
            shpImp.DBFColdefs[j] = coldef;
        }

        /* prepare gdb layer cols */
        GDBAttrSet (hlayer, (void*) layertable, -1, GDB_ATTR_LAYER_TABLENAME, herr);

        /* set tablespace and owner */
        GDBAttrSet (hlayer, hctx->tablespace, -1, GDB_ATTR_LAYER_TABLESPACE, herr);
        GDBAttrSet (hlayer, hctx->owner, -1, GDB_ATTR_LAYER_OWNER, herr);

        /* set layer col names */
        GDBAttrSet (hlayer, "_SHAPE", -1, GDB_ATTR_LAYER_SPATIALCOL, herr);

        if (rowidcol) {
            GDBAttrSet (hlayer, (void*) rowidcol, -1, GDB_ATTR_LAYER_ROWIDCOL, herr);
        } else {
            GDBAttrSet (hlayer, "_SHAPEID", -1, GDB_ATTR_LAYER_ROWIDCOL, herr);
            QueryGetColName (shpImp.nDBFCols, shpImp.DBFFields, ((HGDBLayer) hlayer)->ROWIDCOL);
        }

        GDBAttrSet (hlayer, "_XMIN", -1, GDB_ATTR_LAYER_XMINCOL, herr);
        GDBAttrSet (hlayer, "_YMIN", -1, GDB_ATTR_LAYER_YMINCOL, herr);
        GDBAttrSet (hlayer, "_XMAX", -1, GDB_ATTR_LAYER_XMAXCOL, herr);
        GDBAttrSet (hlayer, "_YMAX", -1, GDB_ATTR_LAYER_YMAXCOL, herr);
        GDBAttrSet (hlayer, "_AREA", -1, GDB_ATTR_LAYER_AREACOL, herr);
        GDBAttrSet (hlayer, "_LENG", -1, GDB_ATTR_LAYER_LENGTHCOL, herr);

        /* ensure col names valid */
        QueryGetColName(shpImp.nDBFCols, shpImp.DBFFields, ((HGDBLayer) hlayer)->SPATIALCOL);
        QueryGetColName(shpImp.nDBFCols, shpImp.DBFFields, ((HGDBLayer) hlayer)->XMINCOL);
        QueryGetColName(shpImp.nDBFCols, shpImp.DBFFields, ((HGDBLayer) hlayer)->YMINCOL);
        QueryGetColName(shpImp.nDBFCols, shpImp.DBFFields, ((HGDBLayer) hlayer)->XMAXCOL);
        QueryGetColName(shpImp.nDBFCols, shpImp.DBFFields, ((HGDBLayer) hlayer)->YMAXCOL);
        QueryGetColName(shpImp.nDBFCols, shpImp.DBFFields, ((HGDBLayer) hlayer)->AREACOL);
        QueryGetColName(shpImp.nDBFCols, shpImp.DBFFields, ((HGDBLayer) hlayer)->LENGTHCOL);

        /* set layer info */
        GDBAttrSet (hlayer, &wkbtype, 0, GDB_ATTR_LAYER_LAYERTYPE, herr);
        GDBAttrSet (hlayer, 0, 0, GDB_ATTR_LAYER_LAYERMASK, herr);

        GDBAttrSet (hlayer, 0, 0, GDB_ATTR_LAYER_LEVELMIN, herr);
        GDBAttrSet (hlayer, 0, 0, GDB_ATTR_LAYER_LEVELMAX, herr);

        GDBAttrSet (hlayer, &MinBounds[0], 0, GDB_ATTR_LAYER_XMIN, herr);
        GDBAttrSet (hlayer, &MinBounds[1], 0, GDB_ATTR_LAYER_YMIN, herr);
        GDBAttrSet (hlayer, &MaxBounds[0], 0, GDB_ATTR_LAYER_XMAX, herr);
        GDBAttrSet (hlayer, &MaxBounds[1], 0, GDB_ATTR_LAYER_YMAX, herr);

        if (hasZ) {
            GDBAttrSet (hlayer, &MinBounds[2], 0, GDB_ATTR_LAYER_ZMIN, herr);
            GDBAttrSet (hlayer, &MaxBounds[2], 0, GDB_ATTR_LAYER_ZMAX, herr);
        }

        if (hasM) {
            GDBAttrSet (hlayer, &MinBounds[3], 0, GDB_ATTR_LAYER_MMIN, herr);
            GDBAttrSet (hlayer, &MinBounds[3], 0, GDB_ATTR_LAYER_MMAX, herr);
        }

        if (spref) {
            /* TODO */
            assert (0);
        } else {
            GDBAttrSet (hlayer, &refid_default, 0, GDB_ATTR_LAYER_REFID, herr);
        }

        GDBAttrSet (hlayer, (void*) shpFile, -1, GDB_ATTR_LAYER_DESCRIPT, herr);

        ret = 1;
        if (pfnOnPrepare) {
            char shpTName[20];
            strcpy(shpTName, SHPTypeName(shpImp.nShapeType));
            ret = pfnOnPrepare (ctx,
                shpTName,
                shpImp.nEntities,
                shpImp.DBFColdefs,
                colinds,
                shpImp.nDBFCols,
                hlayer,
                inParam,
                herr);
        }
        assert (ret == 1);
        /* todo ret==0*/

        SHPCreateObjectEx(&shpImp.pShpObjEx);

        ret = GDBTableCreate (ctx, GDB_TABLE_LAYER, layertable,
            shpImp.DBFColdefs, shpImp.nDBFCols, hlayer, herr);
        if (ret != GDBAPI_SUCCESS) {
            goto ERR_EXIT;
        }

        tabletype = GDB_TABLE_LAYER;
        ret = GDBTableGetInfo (ctx, layertable, &tabletype, 0, 0, 0, hlayer, herr);
        if (ret != GDBAPI_SUCCESS) {
            goto ERR_EXIT;
        }

        ret = GDBPrepareInsert (ctx, layertable, tabletype, shpImp.DBFColdefs, colinds,
            shpImp.nDBFCols, hlayer, herr);
        if (ret != GDBAPI_SUCCESS) {
            goto ERR_EXIT;
        }

        ret = GDBBeginTransaction(ctx, herr);
        if (ret != GDBAPI_SUCCESS) {
            goto ERR_EXIT;
        }

        rowParam.OnExecuteParam = inParam;
        rowParam.OnExecuteFunc = pfnOnExecute;
        rowParam.shpFileImport = &shpImp;

        ret = GDBExecuteInsert (ctx, 0, (GDBCALLBACK_ExecuteInsertRow)
            &InsertShapeRow, (void*) &rowParam, herr);

        if (ret != GDBAPI_SUCCESS) {
            GDBFinishInsert(ctx, herr);
            GDBRollbackTransaction (ctx, herr);
            goto ERR_EXIT;
        }

        GDBFinishInsert(ctx, herr);

        GDBCommitTransaction(ctx, herr);

        ret = 1;
        if ( pfnOnFinish ) {
            ret = pfnOnFinish (ctx,
                shpImp.cInsertShapes, shpImp.cErrorShapes,
                hlayer, inParam, herr);
        }

        FreeShapeFileImport(&shpImp);
        return GDBAPI_SUCCESS;

ERR_EXIT:
        FreeShapeFileImport(&shpImp);
        return GDBAPI_ERROR;
}


GDBAPI_RESULT GDBExportSHPFile (
    GDB_CONTEXT ctx,
    GDB_CREATE_MODE mode,
    GDB_LAYER   layer,
    rowid_t     offset,
    rowid_t     rowcount,
    const char *shpFile,
    const char *whereCondition,
    GDBCALLBACK_ExecuteInsertShape pfnInsertShape,
    void        *inParam,
    GDB_ERROR   herr)
{
    return GDBAPI_ERROR;
}
