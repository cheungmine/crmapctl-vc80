/******************************************************************************
 * shp2wkb.h
 *
 * v 1.0 2013/04/28
 *
 * Project:  Shapelib
 * Purpose:  Shapelib extending functions
 * Author:   cheungmine@gmail.com
 * 2013/04/28
 *
 * Copyright (c) 2013, cheungmine
 *
 * This software is available under the following "MIT Style" license,
 * or at the option of the licensee under the LGPL (see LICENSE.LGPL).  This
 * option is discussed in more detail in shapelib.html.
 *
 * --
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *****************************************************************************/

/*
 * Well-known text (WKT) is a text markup language for representing vector 
 * geometry objects on a map, spatial reference systems of spatial objects 
 * and transformations between spatial reference systems. A binary equivalent, 
 * known as well-known binary (WKB) is used to transfer and store the same 
 * information on databases, such as PostGIS, Microsoft SQL Server and DB2. 
 * The formats were originally defined by the Open Geospatial Consortium (OGC) 
 * and described in their Simple Feature Access and Coordinate Transformation
 * Service specifications. The current standard definition is in the ISO/IEC 
 * 13249-3:2011 standard, "Information technology -- Database languages -- 
 * SQL multimedia and application packages -- Part 3: Spatial" (SQL/MM).
 *
 * reference:
 *   http://en.wikipedia.org/wiki/Well-known_text
 *
 * byte order: 
 *   1 little-endian (NDR)
 *   0 big-endian (XDR)
 */

#ifndef _SHP2WKB_H_INCLUDED
#define _SHP2WKB_H_INCLUDED

#ifdef _MSC_VER
 /* disable annoying warning: 4996 */
 #pragma warning (disable : 4996)
#endif

#include "shapefile.h"
#include "wkbtype.h"

static union
{
    char c[4];
    byte_t f;
} __endianess__ = {{'l','0','0','b'}};

#define __NDR__ (((char)__endianess__.f)=='l')
#define __XDR__ (((char)__endianess__.f)=='b')

#define swap_uint32(x) \
    ((uint32_t)(((((uint32_t)(x))&0xff000000)>>24)|((((uint32_t)(x))&0x00ff0000)>>8)|((((uint32_t)(x))&0x0000ff00)<<8)|((((uint32_t)(x))&0x000000ff)<<24)))

/*
 * uint32: host to big-endian
 */
static uint32_t uint32_htobe (uint32_t value)
{
    if (__NDR__) {
        return swap_uint32(value);
    } else {
        /* __XDR__ */
        return value;
    }
}

/*
 * double: host to big-endian
 */
#define __swap__(a, b, t) (t) = (a); (a) = (b); (b) = (t)

static double float64_htobe (double value)
{
    if (__NDR__) {
        byte_t _t;
        byte_t *v = (byte_t*) &value;
        __swap__(v[0], v[7], _t);
        __swap__(v[1], v[6], _t);
        __swap__(v[2], v[5], _t);
        __swap__(v[3], v[4], _t);
    }

    /* __XDR__ */
    return value;
}


static byte_t wkb_xdr = WKB_BYTEORDER_XDR;

#define val_copy(pv, cb, v) \
    memcpy((byte_t*)pv + cb, &v, sizeof(v)); \
    cb += sizeof(v)

#define val_copy_dummy(pv, cb, v) \
    cb += sizeof(v)

#define val_copy_dummy_n(pv, cb, v, n) \
    cb += (sizeof(v)*(n))

#define WKBHeaderSize    (sizeof(byte_t) + sizeof(uint32_t))

/*************************** Well-known Binary (WKB) *************************/

#define WKBPointSize(n)   (n)*(WKBHeaderSize + 2*sizeof(double))
#define WKBPointZSize(n)  (n)*(WKBHeaderSize + 3*sizeof(double))
#define WKBPointMSize(n)  WKBPointZSize(n)
#define WKBPointZMSize(n) (n)*(WKBHeaderSize + 4*sizeof(double))

static int Point2WKB (const SHPObject *pObj, void *pv,
    double _DX, double _DY)
{
    uint32_t v4;
    double v8;
    int cb = 0;

    if (pv) {
        val_copy(pv, cb, wkb_xdr);

        v4 = uint32_htobe(WKB_Point2D);
        val_copy(pv, cb, v4);

        v8 = float64_htobe(*pObj->padfX + _DX);
        val_copy(pv, cb, v8);

        v8 = float64_htobe(*pObj->padfY + _DY);
        val_copy(pv, cb, v8);
    } else {
        cb = WKBPointSize(1);
    }

    return cb;
}

#define WKBLineStringSize(np)  \
    (WKBHeaderSize + sizeof(uint32_t) + (np)*2*sizeof(double))

#define WKBLineStringZSize(np)  \
    (WKBHeaderSize + sizeof(uint32_t) + (np)*3*sizeof(double))

#define WKBLineStringMSize(np)  WKBLineStringZSize(np)

static int Arc2WKB (const SHPObject *pObj, void *pv,
    double _DX, double _DY)
{
    uint32_t v4;
    double   v8;
    int cb = 0;

    if (pv) {
        int iPoint = 0;

        /* byte order flag */
        val_copy(pv, cb, wkb_xdr);

        /* wkb type */
        v4 = uint32_htobe(WKB_LineString2D);
        val_copy(pv, cb, v4);

        /* num of points */
        v4 = uint32_htobe(pObj->nVertices);
        val_copy(pv, cb, v4);

        for (; iPoint < pObj->nVertices; iPoint++) {
            v8 = float64_htobe(pObj->padfX[iPoint] + _DX);
            val_copy(pv, cb, v8);

            v8 = float64_htobe(pObj->padfY[iPoint] + _DY);
            val_copy(pv, cb, v8);
        }
    } else {
        WKBLineStringSize(pObj->nVertices);
    }

    return cb;
}

static int Polygon2WKB (const SHPObject *pObj, void *pv,
    double _DX, double _DY)
{
    uint32_t v4;
    double   v8;
    int cb = 0;

    if (pv) {
        int iPart = 0;

        /* byte order flag */
        val_copy(pv, cb, wkb_xdr);

        /* wkb type */
        v4 = uint32_htobe(WKB_Polygon2D);
        val_copy(pv, cb, v4);

        /* num of rings */
        v4 = uint32_htobe(pObj->nParts);
        val_copy(pv, cb, v4);

        for (; iPart < pObj->nParts; iPart++) {
            int p = pObj->panPartStart[iPart];
            int p1 = pObj->panPartStart[iPart+1];

            /* num of points */
            v4 = uint32_htobe(p1 - p);
            val_copy(pv, cb, v4);

            for (; p < p1; p++) {
                v8 = float64_htobe(pObj->padfX[p] + _DX);
                val_copy(pv, cb, v8);

                v8 = float64_htobe(pObj->padfY[p] + _DY);
                val_copy(pv, cb, v8);
            }
        }
    } else {
        int iPart = 0;

        cb = WKBHeaderSize;

        /* num of rings */
        val_copy_dummy(pv, cb, v4);

        for (; iPart < pObj->nParts; iPart++) {
            int np = pObj->panPartStart[iPart+1] - pObj->panPartStart[iPart];

            /* num of points */
            val_copy_dummy(pv, cb, v4);

            val_copy_dummy_n(pv, cb, v8, np);
            val_copy_dummy_n(pv, cb, v8, np);
        }
    }

    return cb;
}

static int MultiPoint2WKB (const SHPObject *pObj, void *pv,
    double _DX, double _DY)
{
    uint32_t v4;
    double v8;
    int cb = 0;

    if (pv) {
        int iPoint = 0;
        val_copy(pv, cb, wkb_xdr);

        v4 = uint32_htobe(WKB_MultiPoint2D);
        val_copy(pv, cb, v4);

        v4 = uint32_htobe(pObj->nVertices);
        val_copy(pv, cb, v4);

        for (; iPoint < pObj->nVertices; iPoint++) {
            val_copy(pv, cb, wkb_xdr);
      
            v4 = uint32_htobe(WKB_Point2D);
            val_copy(pv, cb, v4);

            v8 = float64_htobe(pObj->padfX[iPoint] + _DX);
            val_copy(pv, cb, v8);

            v8 = float64_htobe(pObj->padfY[iPoint] + _DY);
            val_copy(pv, cb, v8);
        }
    } else {
        cb = WKBHeaderSize + sizeof(v4) + WKBPointSize(pObj->nVertices);
    }

    return cb;
}

static int PointZ2WKB (const SHPObject *pObj, void *pv,
    double _DX, double _DY, double _DZ)
{
    uint32_t v4;
    double v8;
    int cb = 0;

    if (pv) {
        val_copy(pv, cb, wkb_xdr);

        v4 = uint32_htobe(WKB_PointZ);
        val_copy(pv, cb, v4);

        v8 = float64_htobe(*pObj->padfX + _DX);
        val_copy(pv, cb, v8);

        v8 = float64_htobe(*pObj->padfY + _DY);
        val_copy(pv, cb, v8);

        v8 = float64_htobe(*pObj->padfZ + _DZ);
        val_copy(pv, cb, v8);
    } else {
        cb = WKBPointZSize(1);
    }

    return cb;
}

static int ArcZ2WKB (const SHPObject *pObj, void *pv,
    double _DX, double _DY, double _DZ)
{
    uint32_t v4;
    double   v8;
    int cb = 0;

    if (pv) {
        int iPoint = 0;

        /* byte order flag */
        val_copy(pv, cb, wkb_xdr);

        /* wkb type */
        v4 = uint32_htobe(WKB_LineStringZ);
        val_copy(pv, cb, v4);

        /* num of points */
        v4 = uint32_htobe(pObj->nVertices);
        val_copy(pv, cb, v4);

        for (; iPoint < pObj->nVertices; iPoint++) {
            v8 = float64_htobe(pObj->padfX[iPoint] + _DX);
            val_copy(pv, cb, v8);

            v8 = float64_htobe(pObj->padfY[iPoint] + _DY);
            val_copy(pv, cb, v8);

            v8 = float64_htobe(pObj->padfZ[iPoint] + _DZ);
            val_copy(pv, cb, v8);
        }
    } else {
        cb = WKBLineStringZSize(pObj->nVertices);
    }

    return cb;
}

static int PolygonZ2WKB (const SHPObject *pObj, void *pv,
    double _DX, double _DY, double _DZ)
{
    uint32_t v4;
    double   v8;
    int cb = 0;

    if (pv) {
        int iPart = 0;

        /* byte order flag */
        val_copy(pv, cb, wkb_xdr);

        /* wkb type */
        v4 = uint32_htobe(WKB_PolygonZ);
        val_copy(pv, cb, v4);

        /* num of rings */
        v4 = uint32_htobe(pObj->nParts);
        val_copy(pv, cb, v4);

        for (; iPart < pObj->nParts; iPart++) {
            int p = pObj->panPartStart[iPart];
            int p1 = pObj->panPartStart[iPart+1];

            /* num of points */
            v4 = uint32_htobe(p1 - p);
            val_copy(pv, cb, v4);

            for (; p < p1; p++) {
                v8 = float64_htobe(pObj->padfX[p] + _DX);
                val_copy(pv, cb, v8);

                v8 = float64_htobe(pObj->padfY[p] + _DY);
                val_copy(pv, cb, v8);

                v8 = float64_htobe(pObj->padfZ[p] + _DZ);
                val_copy(pv, cb, v8);
            }
        }
    } else {
        int iPart = 0;

        /* byte order flag */
        val_copy_dummy(pv, cb, wkb_xdr);

        /* wkb type */
        val_copy_dummy(pv, cb, v4);

        /* num of rings */
        val_copy_dummy(pv, cb, v4);

        for (; iPart < pObj->nParts; iPart++) {
            int np = pObj->panPartStart[iPart+1] - pObj->panPartStart[iPart];

            /* num of points */
            val_copy_dummy(pv, cb, v4);

            val_copy_dummy_n(pv, cb, v8, np);
            val_copy_dummy_n(pv, cb, v8, np);
            val_copy_dummy_n(pv, cb, v8, np);
        }
    }

    return cb;
}

static int MultiPointZ2WKB (const SHPObject *pObj, void *pv,
    double _DX, double _DY, double _DZ)
{
    uint32_t v4;
    double v8;
    int cb = 0;

    if (pv) {
        int iPoint = 0;
        val_copy(pv, cb, wkb_xdr);

        v4 = uint32_htobe(WKB_MultiPointZ);
        val_copy(pv, cb, v4);

        v4 = uint32_htobe(pObj->nVertices);
        val_copy(pv, cb, v4);

        for (; iPoint < pObj->nVertices; iPoint++) {
            val_copy(pv, cb, wkb_xdr);
      
            v4 = uint32_htobe(WKB_PointZ);
            val_copy(pv, cb, v4);

            v8 = float64_htobe(pObj->padfX[iPoint] + _DX);
            val_copy(pv, cb, v8);

            v8 = float64_htobe(pObj->padfY[iPoint] + _DY);
            val_copy(pv, cb, v8);

            v8 = float64_htobe(pObj->padfZ[iPoint] + _DZ);
            val_copy(pv, cb, v8);
        }
    } else {
        cb = WKBHeaderSize + sizeof(v4) + WKBPointZSize(pObj->nVertices);
    }

    return cb;
}

static int PointM2WKB (const SHPObject *pObj, void *pv,
    double _DX, double _DY, double _DM)
{
    uint32_t v4;
    double v8;
    int cb = 0;

    if (pv) {
        val_copy(pv, cb, wkb_xdr);

        v4 = uint32_htobe(WKB_PointM);
        val_copy(pv, cb, v4);

        v8 = float64_htobe(*pObj->padfX + _DX);
        val_copy(pv, cb, v8);

        v8 = float64_htobe(*pObj->padfY + _DY);
        val_copy(pv, cb, v8);

        v8 = float64_htobe(*pObj->padfM + _DM);
        val_copy(pv, cb, v8);
    } else {
        cb = WKBPointMSize(1);
    }

    return cb;
}

static int ArcM2WKB (const SHPObject *pObj, void *pv,
    double _DX, double _DY, double _DM)
{
    uint32_t v4;
    double   v8;
    int cb = 0;

    if (pv) {
        int iPoint = 0;

        /* byte order flag */
        val_copy(pv, cb, wkb_xdr);

        /* wkb type */
        v4 = uint32_htobe(WKB_LineStringM);
        val_copy(pv, cb, v4);

        /* num of points */
        v4 = uint32_htobe(pObj->nVertices);
        val_copy(pv, cb, v4);

        for (; iPoint < pObj->nVertices; iPoint++) {
            v8 = float64_htobe(pObj->padfX[iPoint] + _DX);
            val_copy(pv, cb, v8);

            v8 = float64_htobe(pObj->padfY[iPoint] + _DY);
            val_copy(pv, cb, v8);

            v8 = float64_htobe(pObj->padfM[iPoint] + _DM);
            val_copy(pv, cb, v8);
        }
    } else {
        cb = WKBLineStringMSize(pObj->nVertices);
    }

    return cb;
}

static int PolygonM2WKB (const SHPObject *pObj, void *pv,
    double _DX, double _DY, double _DM)
{
    uint32_t v4;
    double   v8;
    int cb = 0;

    if (pv) {
        int iPart = 0;

        /* byte order flag */
        val_copy(pv, cb, wkb_xdr);

        /* wkb type */
        v4 = uint32_htobe(WKB_PolygonM);
        val_copy(pv, cb, v4);

        /* num of rings */
        v4 = uint32_htobe(pObj->nParts);
        val_copy(pv, cb, v4);

        for (; iPart < pObj->nParts; iPart++) {
            int p = pObj->panPartStart[iPart];
            int p1 = pObj->panPartStart[iPart+1];

            /* num of points */
            v4 = uint32_htobe(p1 - p);
            val_copy(pv, cb, v4);

            for (; p < p1; p++) {
                v8 = float64_htobe(pObj->padfX[p] + _DX);
                val_copy(pv, cb, v8);

                v8 = float64_htobe(pObj->padfY[p] + _DY);
                val_copy(pv, cb, v8);

                v8 = float64_htobe(pObj->padfM[p] + _DM);
                val_copy(pv, cb, v8);
            }
        }
    } else {
        int iPart = 0;

        /* byte order flag */
        val_copy_dummy(pv, cb, wkb_xdr);

        /* wkb type */
        val_copy_dummy(pv, cb, v4);

        /* num of rings */
        val_copy_dummy(pv, cb, v4);

        for (; iPart < pObj->nParts; iPart++) {
            int np = pObj->panPartStart[iPart+1] - pObj->panPartStart[iPart];

            /* num of points */
            val_copy_dummy(pv, cb, v4);

            val_copy_dummy_n(pv, cb, v8, np);
            val_copy_dummy_n(pv, cb, v8, np);
            val_copy_dummy_n(pv, cb, v8, np);
        }
    }

    return cb;
}

static int MultiPointM2WKB (const SHPObject *pObj, void *pv,
    double _DX, double _DY, double _DM)
{
    uint32_t v4;
    double v8;
    int cb = 0;

    if (pv) {
        int iPoint = 0;
        val_copy(pv, cb, wkb_xdr);

        v4 = uint32_htobe(WKB_MultiPointM);
        val_copy(pv, cb, v4);

        v4 = uint32_htobe(pObj->nVertices);
        val_copy(pv, cb, v4);

        for (; iPoint < pObj->nVertices; iPoint++) {
            val_copy(pv, cb, wkb_xdr);
      
            v4 = uint32_htobe(WKB_PointM);
            val_copy(pv, cb, v4);

            v8 = float64_htobe(pObj->padfX[iPoint] + _DX);
            val_copy(pv, cb, v8);

            v8 = float64_htobe(pObj->padfY[iPoint] + _DY);
            val_copy(pv, cb, v8);

            v8 = float64_htobe(pObj->padfM[iPoint] + _DM);
            val_copy(pv, cb, v8);
        }
    } else {
        cb = WKBHeaderSize + sizeof(v4) + WKBPointMSize(pObj->nVertices);
    }

    return cb;
}

static int exPoint2WKB (const SHPObjectEx *pObj, void *pv,
    double _DX, double _DY)
{
    uint32_t v4;
    double v8;
    int cb = 0;

    if (pv) {
        val_copy(pv, cb, wkb_xdr);

        v4 = uint32_htobe(WKB_Point2D);
        val_copy(pv, cb, v4);

        v8 = float64_htobe(pObj->pPoints[0].x + _DX);
        val_copy(pv, cb, v8);

        v8 = float64_htobe(pObj->pPoints[0].y + _DY);
        val_copy(pv, cb, v8);
    } else {
        cb = WKBPointSize(1);
    }

    return cb;
}

static int exArc2WKB (const SHPObjectEx *pObj, void *pv,
    double _DX, double _DY)
{
    uint32_t v4;
    double   v8;
    int cb = 0;

    if (pv) {
        int iPoint = 0;

        /* byte order flag */
        val_copy(pv, cb, wkb_xdr);

        /* wkb type */
        v4 = uint32_htobe(WKB_LineString2D);
        val_copy(pv, cb, v4);

        /* num of points */
        v4 = uint32_htobe(pObj->nVertices);
        val_copy(pv, cb, v4);

        for (; iPoint < pObj->nVertices; iPoint++) {
            v8 = float64_htobe(pObj->pPoints[iPoint].x + _DX);
            val_copy(pv, cb, v8);

            v8 = float64_htobe(pObj->pPoints[iPoint].y + _DY);
            val_copy(pv, cb, v8);
        }
    } else {
        cb = WKBLineStringSize(pObj->nVertices);
    }

    return cb;
}

static int exPolygon2WKB (const SHPObjectEx *pObj, void *pv,
    double _DX, double _DY)
{
    uint32_t v4;
    double   v8;
    int cb = 0;

    if (pv) {
        int iPart = 0;

        /* byte order flag */
        val_copy(pv, cb, wkb_xdr);

        /* wkb type */
        v4 = uint32_htobe(WKB_Polygon2D);
        val_copy(pv, cb, v4);

        /* num of rings */
        v4 = uint32_htobe(pObj->nParts);
        val_copy(pv, cb, v4);

        for (; iPart < pObj->nParts; iPart++) {
            int p = pObj->panPartStart[iPart];
            int p1 = pObj->panPartStart[iPart+1];

            /* num of points */
            v4 = uint32_htobe(p1 - p);
            val_copy(pv, cb, v4);

            for (; p < p1; p++) {
                v8 = float64_htobe(pObj->pPoints[p].x + _DX);
                val_copy(pv, cb, v8);

                v8 = float64_htobe(pObj->pPoints[p].y + _DY);
                val_copy(pv, cb, v8);
            }
        }
    } else {
        int iPart = 0;

        /* byte order flag */
        val_copy_dummy(pv, cb, wkb_xdr);

        /* wkb type */
        val_copy_dummy(pv, cb, v4);

        /* num of rings */
        val_copy_dummy(pv, cb, v4);

        for (; iPart < pObj->nParts; iPart++) {
            int np = pObj->panPartStart[iPart+1] - pObj->panPartStart[iPart];

            /* num of points */
            val_copy_dummy(pv, cb, v4);

            val_copy_dummy_n(pv, cb, v8, np);
            val_copy_dummy_n(pv, cb, v8, np);
        }
    }

    return cb;
}

static int exMultiPoint2WKB (const SHPObjectEx *pObj, void *pv,
    double _DX, double _DY)
{
    uint32_t v4;
    double v8;
    int cb = 0;

    if (pv) {
        int iPoint = 0;
        val_copy(pv, cb, wkb_xdr);

        v4 = uint32_htobe(WKB_MultiPoint2D);
        val_copy(pv, cb, v4);

        v4 = uint32_htobe(pObj->nVertices);
        val_copy(pv, cb, v4);

        for (; iPoint < pObj->nVertices; iPoint++) {
            val_copy(pv, cb, wkb_xdr);
      
            v4 = uint32_htobe(WKB_Point2D);
            val_copy(pv, cb, v4);

            v8 = float64_htobe(pObj->pPoints[iPoint].x + _DX);
            val_copy(pv, cb, v8);

            v8 = float64_htobe(pObj->pPoints[iPoint].y + _DY);
            val_copy(pv, cb, v8);
        }
    } else {
        cb = WKBHeaderSize + sizeof(v4) + WKBPointSize(pObj->nVertices);
    }

    return cb;
}

static int exPointZ2WKB (const SHPObjectEx *pObj, void *pv,
    double _DX, double _DY, double _DZ)
{
    uint32_t v4;
    double v8;
    int cb = 0;

    if (pv) {
        val_copy(pv, cb, wkb_xdr);

        v4 = uint32_htobe(WKB_PointZ);
        val_copy(pv, cb, v4);

        v8 = float64_htobe(pObj->pPoints[0].x + _DX);
        val_copy(pv, cb, v8);

        v8 = float64_htobe(pObj->pPoints[0].y + _DY);
        val_copy(pv, cb, v8);

        v8 = float64_htobe(*pObj->padfZ + _DZ);
        val_copy(pv, cb, v8);
    } else {
        cb = WKBPointZSize(1);
    }

    return cb;
}

static int exArcZ2WKB (const SHPObjectEx *pObj, void *pv,
    double _DX, double _DY, double _DZ)
{
    uint32_t v4;
    double   v8;
    int cb = 0;

    if (pv) {
        int iPoint = 0;

        /* byte order flag */
        val_copy(pv, cb, wkb_xdr);

        /* wkb type */
        v4 = uint32_htobe(WKB_LineStringZ);
        val_copy(pv, cb, v4);

        /* num of points */
        v4 = uint32_htobe(pObj->nVertices);
        val_copy(pv, cb, v4);

        for (; iPoint < pObj->nVertices; iPoint++) {
            v8 = float64_htobe(pObj->pPoints[iPoint].x + _DX);
            val_copy(pv, cb, v8);

            v8 = float64_htobe(pObj->pPoints[iPoint].y + _DY);
            val_copy(pv, cb, v8);

            v8 = float64_htobe(pObj->padfZ[iPoint] + _DZ);
            val_copy(pv, cb, v8);
        }
    } else {
        cb = WKBLineStringZSize(pObj->nVertices);
    }

    return cb;
}

static int exPolygonZ2WKB (const SHPObjectEx *pObj, void *pv,
    double _DX, double _DY, double _DZ)
{
    uint32_t v4;
    double   v8;
    int cb = 0;

    if (pv) {
        int iPart = 0;

        /* byte order flag */
        val_copy(pv, cb, wkb_xdr);

        /* wkb type */
        v4 = uint32_htobe(WKB_PolygonZ);
        val_copy(pv, cb, v4);

        /* num of rings */
        v4 = uint32_htobe(pObj->nParts);
        val_copy(pv, cb, v4);

        for (; iPart < pObj->nParts; iPart++) {
            int p = pObj->panPartStart[iPart];
            int p1 = pObj->panPartStart[iPart+1];

            /* num of points */
            v4 = uint32_htobe(p1 - p);
            val_copy(pv, cb, v4);

            for (; p < p1; p++) {
                v8 = float64_htobe(pObj->pPoints[p].x + _DX);
                val_copy(pv, cb, v8);

                v8 = float64_htobe(pObj->pPoints[p].y + _DY);
                val_copy(pv, cb, v8);

                v8 = float64_htobe(pObj->padfZ[p] + _DZ);
                val_copy(pv, cb, v8);
            }
        }
    } else {
        int iPart = 0;

        /* byte order flag */
        val_copy_dummy(pv, cb, wkb_xdr);

        /* wkb type */
        val_copy_dummy(pv, cb, v4);

        /* num of rings */
        val_copy_dummy(pv, cb, v4);

        for (; iPart < pObj->nParts; iPart++) {
            int np = pObj->panPartStart[iPart+1] - pObj->panPartStart[iPart];

            /* num of points */
            val_copy_dummy(pv, cb, v4);

            val_copy_dummy_n(pv, cb, v8, np);
            val_copy_dummy_n(pv, cb, v8, np);
            val_copy_dummy_n(pv, cb, v8, np);
        }
    }

    return cb;
}

static int exMultiPointZ2WKB (const SHPObjectEx *pObj, void *pv,
    double _DX, double _DY, double _DZ)
{
    uint32_t v4;
    double v8;
    int cb = 0;

    if (pv) {
        int iPoint = 0;
        val_copy(pv, cb, wkb_xdr);

        v4 = uint32_htobe(WKB_MultiPointZ);
        val_copy(pv, cb, v4);

        v4 = uint32_htobe(pObj->nVertices);
        val_copy(pv, cb, v4);

        for (; iPoint < pObj->nVertices; iPoint++) {
            val_copy(pv, cb, wkb_xdr);
      
            v4 = uint32_htobe(WKB_PointZ);
            val_copy(pv, cb, v4);

            v8 = float64_htobe(pObj->pPoints[iPoint].x + _DX);
            val_copy(pv, cb, v8);

            v8 = float64_htobe(pObj->pPoints[iPoint].y + _DY);
            val_copy(pv, cb, v8);

            v8 = float64_htobe(pObj->padfZ[iPoint] + _DZ);
            val_copy(pv, cb, v8);
        }
    } else {
        cb = WKBHeaderSize + sizeof(v4) + WKBPointZSize(pObj->nVertices);
    }

    return cb;
}

static int exPointM2WKB (const SHPObjectEx *pObj, void *pv,
    double _DX, double _DY, double _DM)
{
    uint32_t v4;
    double v8;
    int cb = 0;

    if (pv) {
        val_copy(pv, cb, wkb_xdr);

        v4 = uint32_htobe(WKB_PointM);
        val_copy(pv, cb, v4);

        v8 = float64_htobe(pObj->pPoints[0].x + _DX);
        val_copy(pv, cb, v8);

        v8 = float64_htobe(pObj->pPoints[0].y + _DY);
        val_copy(pv, cb, v8);

        v8 = float64_htobe(*pObj->padfM + _DM);
        val_copy(pv, cb, v8);
    } else {
        cb = WKBPointMSize(1);
    }

    return cb;
}

static int exArcM2WKB (const SHPObjectEx *pObj, void *pv,
    double _DX, double _DY, double _DM)
{
    uint32_t v4;
    double   v8;
    int cb = 0;

    if (pv) {
        int iPoint = 0;

        /* byte order flag */
        val_copy(pv, cb, wkb_xdr);

        /* wkb type */
        v4 = uint32_htobe(WKB_LineStringM);
        val_copy(pv, cb, v4);

        /* num of points */
        v4 = uint32_htobe(pObj->nVertices);
        val_copy(pv, cb, v4);

        for (; iPoint < pObj->nVertices; iPoint++) {
            v8 = float64_htobe(pObj->pPoints[iPoint].x + _DX);
            val_copy(pv, cb, v8);

            v8 = float64_htobe(pObj->pPoints[iPoint].y + _DY);
            val_copy(pv, cb, v8);

            v8 = float64_htobe(pObj->padfM[iPoint] + _DM);
            val_copy(pv, cb, v8);
        }
    } else {
        cb = WKBLineStringMSize(pObj->nVertices);
    }

    return cb;
}

static int exPolygonM2WKB (const SHPObjectEx *pObj, void *pv,
    double _DX, double _DY, double _DM)
{
    uint32_t v4;
    double   v8;
    int cb = 0;

    if (pv) {
        int iPart = 0;

        /* byte order flag */
        val_copy(pv, cb, wkb_xdr);

        /* wkb type */
        v4 = uint32_htobe(WKB_PolygonM);
        val_copy(pv, cb, v4);

        /* num of rings */
        v4 = uint32_htobe(pObj->nParts);
        val_copy(pv, cb, v4);

        for (; iPart < pObj->nParts; iPart++) {
            int p = pObj->panPartStart[iPart];
            int p1 = pObj->panPartStart[iPart+1];

            /* num of points */
            v4 = uint32_htobe(p1 - p);
            val_copy(pv, cb, v4);

            for (; p < p1; p++) {
                v8 = float64_htobe(pObj->pPoints[p].x + _DX);
                val_copy(pv, cb, v8);

                v8 = float64_htobe(pObj->pPoints[p].y + _DY);
                val_copy(pv, cb, v8);

                v8 = float64_htobe(pObj->padfM[p] + _DM);
                val_copy(pv, cb, v8);
            }
        }
    } else {
        int iPart = 0;

        /* byte order flag */
        val_copy_dummy(pv, cb, wkb_xdr);

        /* wkb type */
        val_copy_dummy(pv, cb, v4);

        /* num of rings */
        val_copy_dummy(pv, cb, v4);

        for (; iPart < pObj->nParts; iPart++) {
            int np = pObj->panPartStart[iPart+1] - pObj->panPartStart[iPart];

            /* num of points */
            val_copy_dummy(pv, cb, v4);

            val_copy_dummy_n(pv, cb, v8, np);
            val_copy_dummy_n(pv, cb, v8, np);
            val_copy_dummy_n(pv, cb, v8, np);
        }
    }

    return cb;
}

static int exMultiPointM2WKB (const SHPObjectEx *pObj, void *pv,
    double _DX, double _DY, double _DM)
{
    uint32_t v4;
    double v8;
    int cb = 0;

    if (pv) {
        int iPoint = 0;
        val_copy(pv, cb, wkb_xdr);

        v4 = uint32_htobe(WKB_MultiPointM);
        val_copy(pv, cb, v4);

        v4 = uint32_htobe(pObj->nVertices);
        val_copy(pv, cb, v4);

        for (; iPoint < pObj->nVertices; iPoint++) {
            val_copy(pv, cb, wkb_xdr);
      
            v4 = uint32_htobe(WKB_PointM);
            val_copy(pv, cb, v4);

            v8 = float64_htobe(pObj->pPoints[iPoint].x + _DX);
            val_copy(pv, cb, v8);

            v8 = float64_htobe(pObj->pPoints[iPoint].y + _DY);
            val_copy(pv, cb, v8);

            v8 = float64_htobe(pObj->padfM[iPoint] + _DM);
            val_copy(pv, cb, v8);
        }
    } else {
        cb = WKBHeaderSize + sizeof(v4) + WKBPointMSize(pObj->nVertices);
    }

    return cb;
}

static int MultiPatch2WKB (const SHPObject *pObj, void *pv,
    double _DX, double _DY)
{
    /* TODO */
    return 0;
}

static int exMultiPatch2WKB (const SHPObjectEx *pObj, void *pv,
    double _DX, double _DY)
{
    /* TODO */
    return 0;
}

#endif /* _SHP2WKB_H_INCLUDED */
