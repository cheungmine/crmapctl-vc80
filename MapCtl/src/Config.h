// Config.h
// included by all .h files in the same folder
//
// **** WinCE 组件依赖的 DLL（如:libgdbapi.dll）必须部署到 Windows，否则无法注册！ ****
//
#ifndef _CONFIG_H__
#define _CONFIG_H__


#include "../CRMapCtl/stdafx.h"

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Windows CE 平台(如不提供完全 DCOM 支持的 Windows Mobile 平台)上无法正确支持单线程 COM 对象。\
定义 _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA 可强制 ATL 支持创建单线程 COM 对象实现并允许使用其单线程 COM 对象实现。\
rgs 文件中的线程模型已被设置为“Free”，原因是该模型是非 DCOM Windows CE 平台支持的唯一线程模型。"
#endif


#include "ComException.h"
#include "Common.h"
#include "DrawInfo.h"

/////////////////////////////////
// Link to: libgdbapi.lib
// Library: libgdbapi.dll
/////////////////////////////////
#if defined(_WIN32_WCE)
    #if defined(_DEBUG) || defined(DEBUG)
        // #pragma message("debug link to WinCE libgdbapi.lib")

        #if defined(WM6_5_3)
            #pragma comment(lib, "../../MapCtl/libgdbapi/Windows Mobile 6.5.3 Professional DTK (ARMV4I)/Debug/libgdbapi.lib")
        #elif defined(WM6)
            #pragma comment(lib, "../../MapCtl/libgdbapi/Windows Mobile 6 Professional SDK (ARMV4I)/Debug/libgdbapi.lib")
        #elif defined(PPC2003)
            #pragma comment(lib, "../../MapCtl/libgdbapi/Pocket PC 2003 (ARMV4)/Debug/libgdbapi.lib")
        #elif defined(SMP2003)
            #pragma comment(lib, "../../MapCtl/libgdbapi/Smartphone 2003 (ARMV4)/Debug/libgdbapi.lib")
        #elif defined(CE500_ARMV4I)
            #pragma comment(lib, "../../MapCtl/libgdbapi/STANDARDSDK_500 (ARMV4I)/Debug/libgdbapi.lib")
        #elif defined(CE500_MIPSII)
            #pragma comment(lib, "../../MapCtl/libgdbapi/STANDARDSDK_500 (MIPSII)/Debug/libgdbapi.lib")
        #elif defined(CE500_MIPSII_FP)
            #pragma comment(lib, "../../MapCtl/libgdbapi/STANDARDSDK_500 (MIPSII_FP)/Debug/libgdbapi.lib")
        #elif defined(CE500_MIPSIV)
            #pragma comment(lib, "../../MapCtl/libgdbapi/STANDARDSDK_500 (MIPSIV)/Debug/libgdbapi.lib")
        #elif defined(CE500_MIPSIV_FP)
            #pragma comment(lib, "../../MapCtl/libgdbapi/STANDARDSDK_500 (MIPSIV_FP)/Debug/libgdbapi.lib")
        #elif defined(CE500_SH4)
            #pragma comment(lib, "../../MapCtl/libgdbapi/STANDARDSDK_500 (SH4)/Debug/libgdbapi.lib")
        #elif defined(CE500_X86)
            #pragma comment(lib, "../../MapCtl/libgdbapi/STANDARDSDK_500 (x86)/Debug/libgdbapi.lib")
        #elif defined(CE6_ARMV4I)
            #pragma comment(lib, "../../MapCtl/libgdbapi/CHSINT SDK For WinCE 6.0 (ARMV4I)/Debug/libgdbapi.lib")
        #else        
            #error "Unknown platform definition"
        #endif
    #else
        // #pragma message("release link to WinCE libgdbapi.lib")

        #if defined(WM6_5_3)
            #pragma comment(lib, "../../MapCtl/libgdbapi/Windows Mobile 6.5.3 Professional DTK (ARMV4I)/Release/libgdbapi.lib")
        #elif defined(WM6)
            #pragma comment(lib, "../../MapCtl/libgdbapi/Windows Mobile 6 Professional SDK (ARMV4I)/Release/libgdbapi.lib")
        #elif defined(PPC2003)
            #pragma comment(lib, "../../MapCtl/libgdbapi/Pocket PC 2003 (ARMV4)/Release/libgdbapi.lib")
        #elif defined(SMP2003)
            #pragma comment(lib, "../../MapCtl/libgdbapi/Smartphone 2003 (ARMV4)/Release/libgdbapi.lib")
        #elif defined(CE500_ARMV4I)
            #pragma comment(lib, "../../MapCtl/libgdbapi/STANDARDSDK_500 (ARMV4I)/Release/libgdbapi.lib")
        #elif defined(CE500_MIPSII)
            #pragma comment(lib, "../../MapCtl/libgdbapi/STANDARDSDK_500 (MIPSII)/Release/libgdbapi.lib")
        #elif defined(CE500_MIPSII_FP)
            #pragma comment(lib, "../../MapCtl/libgdbapi/STANDARDSDK_500 (MIPSII_FP)/Release/libgdbapi.lib")
        #elif defined(CE500_MIPSIV)
            #pragma comment(lib, "../../MapCtl/libgdbapi/STANDARDSDK_500 (MIPSIV)/Release/libgdbapi.lib")
        #elif defined(CE500_MIPSIV_FP)
            #pragma comment(lib, "../../MapCtl/libgdbapi/STANDARDSDK_500 (MIPSIV_FP)/Release/libgdbapi.lib")
        #elif defined(CE500_SH4)
            #pragma comment(lib, "../../MapCtl/libgdbapi/STANDARDSDK_500 (SH4)/Release/libgdbapi.lib")
        #elif defined(CE500_X86)
            #pragma comment(lib, "../../MapCtl/libgdbapi/STANDARDSDK_500 (x86)/Release/libgdbapi.lib")
        #elif defined(CE6_ARMV4I)
            #pragma comment(lib, "../../MapCtl/libgdbapi/CHSINT SDK For WinCE 6.0 (ARMV4I)/Release/libgdbapi.lib")
        #else
            #error "Unknown platform definition"
        #endif
    #endif
#else
    // Win32
    #if defined(_DEBUG) || defined(DEBUG)
        // #pragma message("debug link to Win32 libgdbapi.lib")
        #pragma comment(lib, "../../MapCtl/libgdbapi/Debug/libgdbapi.lib")
    #else
        // #pragma message("release link to Win32 libgdbapi.lib")
        #pragma comment(lib, "../../MapCtl/libgdbapi/Release/libgdbapi.lib")
    #endif
#endif


// liblog4c.lib => liblog4c.dll
//
// log4c-embed-win source code:
// https://code.google.com/p/log4c-embed/
// $ svn checkout http://log4c-embed.googlecode.com/svn/trunk/ log4c-embed-read-only
#if defined(_WIN32_WCE)
    // WinCE
    #define LOG4C_RCPATH "\\Program Files\\CRMapCtl"
    #define LOG_CATEGORY_NAME "crmapctl-wce"
#else
    // Win32
    #define LOG_CATEGORY_NAME "crmapctl-w32"
#endif

#include <log4c-wrapper.h>

#if defined(LOG4C_ENABLED)
    // Link liblog4c.lib

    #if defined(_WIN32_WCE)
        // WinCE
        #if defined(_DEBUG) || defined(DEBUG)
            #if defined(WM6_5_3)
                #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/Windows Mobile 6.5.3 Professional DTK (ARMV4I)/Debug/liblog4c.lib")
            #elif defined(WM6)
                #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/Windows Mobile 6 Professional SDK (ARMV4I)/Debug/liblog4c.lib")
            #elif defined(PPC2003)
                #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/Pocket PC 2003 (ARMV4)/Debug/liblog4c.lib")
            #elif defined(SMP2003)
                #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/Smartphone 2003 (ARMV4)/Debug/liblog4c.lib")
            #elif defined(CE500_ARMV4I)
                #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/STANDARDSDK_500 (ARMV4I)/Debug/liblog4c.lib")
            #elif defined(CE500_MIPSII)
                #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/STANDARDSDK_500 (MIPSII)/Debug/liblog4c.lib")
            #elif defined(CE500_MIPSII_FP)
                #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/STANDARDSDK_500 (MIPSII_FP)/Debug/liblog4c.lib")
            #elif defined(CE500_MIPSIV)
                #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/STANDARDSDK_500 (MIPSIV)/Debug/liblog4c.lib")
            #elif defined(CE500_MIPSIV_FP)
                #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/STANDARDSDK_500 (MIPSIV_FP)/Debug/liblog4c.lib")
            #elif defined(CE500_SH4)
                #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/STANDARDSDK_500 (SH4)/Debug/liblog4c.lib")
            #elif defined(CE500_X86)
                #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/STANDARDSDK_500 (x86)/Debug/liblog4c.lib")
            #elif defined(CE6_ARMV4I)
                #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/CHSINT SDK For WinCE 6.0 (ARMV4I)/Debug/liblog4c.lib")
            #else        
                #error "Unknown platform definition"
            #endif
        #else
            #if defined(WM6_5_3)
                #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/Windows Mobile 6.5.3 Professional DTK (ARMV4I)/Release/liblog4c.lib")
            #elif defined(WM6)
                #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/Windows Mobile 6 Professional SDK (ARMV4I)/Release/liblog4c.lib")
            #elif defined(PPC2003)
                #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/Pocket PC 2003 (ARMV4)/Release/liblog4c.lib")
            #elif defined(SMP2003)
                #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/Smartphone 2003 (ARMV4)/Release/liblog4c.lib")
            #elif defined(CE500_ARMV4I)
                #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/STANDARDSDK_500 (ARMV4I)/Release/liblog4c.lib")
            #elif defined(CE500_MIPSII)
                #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/STANDARDSDK_500 (MIPSII)/Release/liblog4c.lib")
            #elif defined(CE500_MIPSII_FP)
                #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/STANDARDSDK_500 (MIPSII_FP)/Release/liblog4c.lib")
            #elif defined(CE500_MIPSIV)
                #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/STANDARDSDK_500 (MIPSIV)/Release/liblog4c.lib")
            #elif defined(CE500_MIPSIV_FP)
                #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/STANDARDSDK_500 (MIPSIV_FP)/Release/liblog4c.lib")
            #elif defined(CE500_SH4)
                #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/STANDARDSDK_500 (SH4)/Release/liblog4c.lib")
            #elif defined(CE500_X86)
                #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/STANDARDSDK_500 (x86)/Release/liblog4c.lib")
            #elif defined(CE6_ARMV4I)
                #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/CHSINT SDK For WinCE 6.0 (ARMV4I)/Release/liblog4c.lib")
            #else        
                #error "Unknown platform definition"
            #endif
        #endif
    #else
        // Win32
        #if defined(_DEBUG) || defined(DEBUG)
            #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/Win32/Debug/liblog4c.lib")
        #else
            #pragma comment(lib, "../../MapCtl/liblog4c/build/vc8/Win32/Release/liblog4c.lib")
        #endif
    #endif // _WIN32_WCE
#endif

#endif // _CONFIG_H_
