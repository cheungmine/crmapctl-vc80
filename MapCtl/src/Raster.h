// Raster.h : CRaster ������

#pragma once

#include "Config.h"


// CRaster

class ATL_NO_VTABLE CRaster :
	public CComObjectRootEx<CComSingleThreadModel>,
    public CComCoClass<CRaster>,
	public ISupportErrorInfo,
	public IDispatchImpl<IRaster, &IID_IRaster, &LIBID_CRMapCtlLib, 1, 0>
{
public:
	CRaster()
	{
	}

DECLARE_NO_REGISTRY()


BEGIN_COM_MAP(CRaster)
	COM_INTERFACE_ENTRY(IRaster)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
    STDMETHODIMP InterfaceSupportsErrorInfo(REFIID riid)
    {
	    static const IID* arr[] = 
	    {
		    &IID_IRaster
	    };

	    for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	    {
		    if (InlineIsEqualGUID(*arr[i],riid))
			    return S_OK;
	    }
	    return S_FALSE;
    }

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

    void Draw (CDrawInfo *di);

public:

};

//DEL:OBJECT_ENTRY_AUTO(__uuidof(Raster), CRaster)
