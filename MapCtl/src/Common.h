// Common.h
//   Public APIs
//
#ifndef _COMMON_H__
#define _COMMON_H__

#ifndef ERR_INDEX
    #define ERR_INDEX (-1)
#endif


#define COLL_ITER(coll, IElem)	 coll< CAdapt< CComPtr<IElem> > >::iterator
#define COLL_RITER(coll, IElem)	 coll< CAdapt< CComPtr<IElem> > >::reverse_iterator


#define VariantTypeIsNull(var)  ( \
    (var).vt == VT_ERROR         /* foo() */ \
    || (var).vt == VT_NULL       /* foo(null) */ \
    || (var).vt == VT_EMPTY      /* foo(undefined) */ \
)

#define VariantTypeIsDispatch(var) ( \
    (var).vt == VT_DISPATCH && (var).pdispVal  \
)

#define VariantTypeIsBSTR(var) ( \
    (var).vt == VT_BSTR && (var).bstrVal  \
)

#define VariantTypeIsBSTRWithMaxLen(var,len) ( \
    (var).vt==VT_BSTR && (var).bstrVal && ::SysStringLen((var).bstrVal)<=(UINT)(len) \
)

#define VariantTypeIsLong(var) ( \
    (var).vt == VT_I4 || \
    (var).vt == VT_I2 || \
    (var).vt == VT_I8 || \
    (var).vt == VT_I1 || \
    (var).vt == VT_UI4 || \
    (var).vt == VT_UI2 || \
    (var).vt == VT_UI8 || \
    (var).vt == VT_UI1 || \
    (var).vt == VT_INT || \
    (var).vt == VT_UINT \
)

static BOOL VariantToLong(VARIANT *var, long *pval)
{
    if (var->vt == VT_I4) {
        *pval = (long) var->lVal;

        return TRUE;
    } else if (VariantTypeIsLong(*var)) {
        VARIANT varI4;
        ::VariantInit(&varI4);

        if (S_OK == ::VariantChangeType(&varI4, var, 0, VT_I4)) {
		    *pval = (long) varI4.lVal;
            ::VariantClear(&varI4);
            return TRUE;
        }

        ::VariantClear(&varI4);
    }

    return FALSE;
}


/**
 * 在WinCE设备上, 将包含下面的路径的替换为WinCE路径符.
 * 在Windows上的配置文件不需要改变就可以直接应用到WinCE设备上
 *
 * 例1:
 *   "C:\MyData\$(DEVICE_ROOT)\ShapeMap\River.shp"
 * 变为:
 *   "\ShapeMap\River.shp"
 *
 * 例2:
 *   "C:\$(STORAGE_CARD)\ShapeMap\River.shp"
 * 变为:
 *   "\存储卡\ShapeMap\River.shp"
 *
 * "...$(DEVICE_ROOT)"    => "\"
 * "...$(STORAGE_CARD)"   => 第1块存储卡
 * "...$(STORAGE_CARD_n)" => 第?块存储卡
 */
static char *strrpl(char *s, const char *s1, const char *s2)
{
    char *ptr;

    while (ptr = strstr(s, s1)) {
        memmove(ptr + strlen(s2) , ptr + strlen(s1), strlen(ptr) - strlen(s1) + 1);
        memcpy(ptr, &s2[0], strlen(s2));
    }

    return s;
}


static wchar_t *wcsrpl(wchar_t *s, const wchar_t *s1, const wchar_t *s2)
{
    wchar_t *ptr;

    while (ptr = wcsstr(s, s1)) {
        memmove(ptr + wcslen(s2) , ptr + wcslen(s1), wcslen(ptr) - wcslen(s1) + 1);
        memcpy(ptr, &s2[0], wcslen(s2));
    }

    return s;
}


static void wstrrpl(std::wstring & s, const wchar_t *s1, const wchar_t *s2)
{
    wstring::size_type at;
    wstring::size_type l = wcslen(s1);

    while ((at = s.find(s1)) != wstring::npos) {
        s.replace(at, l, s2);
    }
}


static void ReplaceDevicePath (const wchar_t *inPath, std::wstring& outPath)
{
	outPath.assign(inPath);
    wstrrpl(outPath, L"/", L"\\");

#if defined(_WIN32_WCE)
    wstrrpl(outPath, L"$(DEVICE_ROOT)\\", L"\\");
    wstrrpl(outPath, L"$(DEVICE_ROOT)", L"\\");

    int i;
    int CardNo = 0;
    wchar_t card[20];

    if (outPath.find(L"$(STORAGE_CARD)") != std::wstring::npos) {
        swprintf(card, L"$(STORAGE_CARD)");
        CardNo = 1;
    } else {
        for (i=1; i <= MAX_STORAGE_CARD_NO; i++) {
            swprintf(card, L"$(STORAGE_CARD_%d)", i);
            if (outPath.find(card) != std::wstring::npos) {
                CardNo = i;
                break;
            }
        }
    }

    if (CardNo > 0) {
    	WIN32_FIND_DATA  ffd = {0};
		HANDLE hFlash = FindFirstFlashCard(&ffd);

        if (hFlash) {
            if (CardNo == 1) {
                wstrrpl(outPath, card, ffd.cFileName);
            } else {
                i = 1;
                while (i++ < CardNo && FindNextFlashCard(hFlash, &ffd)) {
                    if (i == CardNo) {
                        wstrrpl(outPath, card, ffd.cFileName);
                    }
                }
            }

            FindClose(hFlash);
        }
    }

#endif
}


// 判断文件是否存在
static BOOL PathfileExists (LPCWSTR lpPathFile)
{
#if defined(_WIN32_WCE)
	HANDLE  hFile = ::CreateFile(lpPathFile,
						GENERIC_READ,
						FILE_SHARE_READ,
						NULL,
						OPEN_EXISTING,
						FILE_ATTRIBUTE_NORMAL,
						NULL);
    if ((hFile==INVALID_HANDLE_VALUE) || (hFile==NULL)) {
		return	FALSE;
    }
	::CloseHandle(hFile);  
    return   TRUE;

#else
    return ATLPath::FileExists(lpPathFile);
#endif
}


static BOOL DirectoryExists(LPCWSTR lpPath)
{
#if defined(_WIN32_WCE)
    DWORD dwAttrib = GetFileAttributes(lpPath);

    return (dwAttrib != INVALID_FILE_ATTRIBUTES && 
             (dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
#else
    return ATLPath::IsDirectory(lpPath);
#endif
}


/**
 * platform-agnostic solution (using the standard C library)
 *
 * Edit: For this to compile in Linux, replace <io.h> with <unistd.h> 
 *       and _access with access.
 */
#if !defined(WINDOWS) && !defined(_WINDOWS) && !defined(_WIN32_WCE)

#include <unistd.h>         // For access().
#include <sys/types.h>  // For stat().
#include <sys/stat.h>   // For stat().

static bool directory_exists (const char* absolutePath)
{
    if ( access( absolutePath, 0 ) == 0 ) {
        struct stat status;
        stat( absolutePath, &status );
        return (status.st_mode & S_IFDIR) != 0;
    }
    return false;
}

#endif


static const char * pathfile_ext_a (const char *pathfile)
{
    return strrchr(pathfile, '.');
}


static const wchar_t * pathfile_ext_w (const wchar_t *pathfile)
{
    return wcsrchr(pathfile, L'.');
}


#endif // _COMMON_H__
