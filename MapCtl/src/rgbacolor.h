/**
 * rgbacolor.h
 */
#ifndef RGBA_COLOR_H_INCLUDED
#define RGBA_COLOR_H_INCLUDED

#if defined (_SVR4) || defined (SVR4) || defined (__OpenBSD__) || defined (_sgi) || defined (__sun) || defined (sun) || defined (__digital__) || defined (__HP_cc)
    #include <inttypes.h>
#elif defined (_MSC_VER) && _MSC_VER < 1600
    /* VS 2010 (_MSC_VER 1600) has stdint.h */
    typedef __int8 int8_t;
    typedef unsigned __int8 uint8_t;
    typedef __int16 int16_t;
    typedef unsigned __int16 uint16_t;
    typedef __int32 int32_t;
    typedef unsigned __int32 uint32_t;
    typedef __int64 int64_t;
    typedef unsigned __int64 uint64_t;
#elif defined (_AIX)
    #include <sys/inttypes.h>
#else
    #include <stdint.h>
#endif


#ifndef min3v
    #define min3v(a, b, c)    ((a)>(b)? ((b)>(c)?(c):(b)): ((a)>(c)?(c):(a)))
#endif

#ifndef max3v
    #define max3v(a, b, c)    ((a)>(b)? ((a)>(c)?(a):(c)): ((b)>(c)?(b):(c)))
#endif

#define COLOR_ALPHA_TRANSPARENT  0x00
#define COLOR_ALPHA_OPAQUE       0xFF


#ifndef RGB
    #define RGB(r,g,b)  \
        ((uint32_t)(((uint8_t)(r)|((uint16_t)((uint8_t)(g))<<8))|(((uint32_t)(uint8_t)(b))<<16)))
#endif


#ifndef RGBA
    #define RGBA(r,g,b,a)  \
        ((uint32_t)((((uint32_t)(uint8_t)(a))<<24)|RGB(r,g,b)))
#endif


/* Win PALRGB */
#ifndef PALRGB
    #define PALRGB(r,g,b)  (0x02000000 | RGB(r,g,b))
#endif


/* Win PALETTEINDEX */
#ifndef PALINDEX
    #define PALINDEX(i)  ((uint32_t)(0x01000000 | (uint32_t)(uint16_t)(i)))
#endif


/* Win GetRValue */
#ifndef GET_RED
    #define GET_RED(rgb)    ((uint8_t)(((uint32_t)(rgb)) & 0xff))
#endif


/* Win GetGValue */
#ifndef GET_GREEN
    #define GET_GREEN(rgb)  ((uint8_t)(((uint32_t)(((uint16_t)(rgb)) >> 8)) & 0xff))
#endif


/* Win GetBValue */
#ifndef GET_BLUE
    #define GET_BLUE(rgb)   ((uint8_t)(((uint32_t)((rgb)>>16)) & 0xff))
#endif


#ifndef GET_ALPHA
    #define GET_ALPHA(rgba) ((uint8_t)(((uint32_t)((rgba)>>24)) & 0xff))
#endif


/**
 * 24-bits colors <=> 16-bits colors
 */
#define RGB888toRGB565(r,g,b)  \
    ((uint16_t)(((uint16_t(r)<<8)&0xF800)|((uint16_t(g)<<3)&0x7E0)|((uint16_t(b) >> 3))))

#define RGBtoRGB565(rgb)       \
    ((uint16_t)(((((uint16_t)((rgb)>>3))&(0x1F))<<11)|((((uint16_t)((rgb)>>10))&(0x3F))<<5)|(((uint16_t)((rgb)>>19))&(0x1F))))

#define RGB888toRGB555(r,g,b)  \
    ((uint16_t)(((uint16_t(r)<<7)&0x7C00)|((uint16_t(g)<<2)&0x3E0)|((uint16_t(b)>>3))))

#define RGBtoRGB555(rgb)       \
    ((uint16_t)(((((uint16_t)((rgb)>>3))&(0x1F))<<10)|((((uint16_t)((rgb)>>11))&(0x1F))<<5)|(((uint16_t)((rgb)>>19))&(0x1F))))

#define RGB555toRGB(rgb555)    \
    ((uint32_t)(((uint8_t)(((rgb555)>>7)&0xF8)|((uint16_t)((uint8_t)(((rgb555)>>2)&0xF8))<<8))|(((uint32_t)(uint8_t)(((rgb555)<<3)&0xF8))<<16)))

#define RGB565toRGB(rgb565)    \
    ((uint32_t)(((uint8_t)((((rgb565)&0xF800)>>11)<<3)|((uint16_t)((uint8_t)((((rgb565)&0x07E0)>>5)<<2))<<8))|(((uint32_t)(uint8_t)(((rgb565)&0x001F)<<3))<<16)))


typedef struct
{
    uint8_t    blue;
    uint8_t    green;
    uint8_t    red;
    uint8_t    alpha;
} rgba_color_t;


typedef struct
{
    float hue;              /* [0,360] */
    float sat;              /* [0,100] */
    float lum;              /* [0,100] */
} hsl_color_t;


typedef struct
{
    uint8_t hue;            /* [0,240] */
    uint8_t sat;            /* [0,240] */
    uint8_t lum;            /* [0,240] */
} winhsl_color_t;


static void HSLtoWinHSL (const hsl_color_t *hsl, winhsl_color_t *whsl)
{
    int h, s, l;

    h = (int) (hsl->hue*240.f/360.f + 0.5f);
    s = (int) (hsl->sat*2.4f + 0.5f);
    l = (int) (hsl->lum*2.4f + 0.5f);

    whsl->hue = (uint8_t) (h>240? 240 : (h<0? 0: h));
    whsl->sat = (uint8_t) (s>240? 240 : (s<0? 0: s));
    whsl->lum = (uint8_t) (l>240? 240 : (l<0? 0: l));
}


static void WinHSLtoHSL (const winhsl_color_t *whsl, hsl_color_t *hsl)
{
    hsl->hue = whsl->hue*1.5f;   /* 1.5 = 360/240 */
    hsl->sat = whsl->sat/2.4f;
    hsl->lum = whsl->lum/2.4f;
}


static void RGBtoHSL (const rgba_color_t *rgb, hsl_color_t *hsl)
{
    float h=0, s=0, l=0;

    /* normalizes red-green-blue values */
    float r = rgb->red/255.f;
    float g = rgb->green/255.f;
    float b = rgb->blue/255.f;

    float maxVal = max3v(r, g, b);
    float minVal = min3v(r, g, b);

    /* hue */
    if (maxVal == minVal) {
        h = 0; /* undefined */
    } else if (maxVal == r && g >= b) {
        h = 60.0f*(g-b)/(maxVal-minVal);
    } else if (maxVal == r && g < b)    {
        h = 60.0f*(g-b)/(maxVal-minVal) + 360.0f;
    } else if (maxVal==g) {
        h = 60.0f*(b-r)/(maxVal-minVal) + 120.0f;
    } else if(maxVal==b) {
        h = 60.0f*(r-g)/(maxVal-minVal) + 240.0f;
    }

    /* lum */
    l = (maxVal+minVal)/2.0f;

    /* sat */
    if (l == 0 || maxVal == minVal) {
        s = 0;
    } else if (0 < l && l <= 0.5f) {
        s = (maxVal-minVal)/(maxVal+minVal);
    } else if (l > 0.5f) {
        s = (maxVal-minVal)/(2 - (maxVal+minVal));
    }

    hsl->hue = (h>360)? 360 : ((h<0)?0:h); 
    hsl->sat = ((s>1)? 1 : ((s<0)?0:s))*100;
    hsl->lum = ((l>1)? 1 : ((l<0)?0:l))*100;
}


static void HSLtoRGB(const hsl_color_t *hsl, rgba_color_t *rgb) 
{
    float h = hsl->hue;
    float s = hsl->sat/100.f;
    float l = hsl->lum/100.f;
    float R, G, B;
    int   iR, iG, iB;

    if (hsl->sat == 0) {
        /* achromatic color (gray scale) */
        R = G = B = l*255.f;
    } else {
        int i;
        float q = (l<0.5f)?(l * (1.0f+s)):(l+s - (l*s));
        float p = (2.0f * l) - q;

        float Hk = h/360.0f;
        float T[3];

        T[0] = Hk + 0.3333333f; /* Tr 0.3333333f=1.0/3.0 */
        T[1] = Hk;              /* Tb */
        T[2] = Hk - 0.3333333f; /* Tg */

        for (i=0; i<3; i++) {
            if (T[i] < 0) {
                T[i] += 1.0f;
            }

            if (T[i] > 1) {
                T[i] -= 1.0f;
            }

            if ((T[i]*6) < 1) {
                T[i] = p + ((q-p)*6.0f*T[i]);
            } else if ((T[i]*2.0f) < 1) {
                /* (1.0/6.0)<=T[i] && T[i]<0.5 */
                T[i] = q;
            } else if ((T[i]*3.0f) < 2) {
                /* 0.5<=T[i] && T[i]<(2.0/3.0) */
                T[i] = p + (q-p) * ((2.0f/3.0f) - T[i]) * 6.0f;
            } else {
                T[i] = p;
            }
        }

        R = T[0]*255.0f;
        G = T[1]*255.0f;
        B = T[2]*255.0f;
    }

    iR = (int) (R + 0.5f);
    iG = (int) (G + 0.5f);
    iB = (int) (B + 0.5f);

    rgb->red = (uint8_t)((iR>255)? 255 : ((iR<0)? 0:iR));
    rgb->green = (uint8_t)((iG>255)? 255 : ((iG<0)? 0:iG));
    rgb->blue = (uint8_t)((iB>255)? 255 : ((iB<0)? 0:iB));
}

#endif /* RGBA_COLOR_H_INCLUDED */
