// Filters.h : CFilters 的声明

#pragma once

#include "Config.h"


typedef CComEnumOnSTL<IEnumVARIANT, &IID_IEnumVARIANT, VARIANT, 
    _CopyVariantFromAdaptItf<IFilter>,
    vector< CAdapt< CComPtr<IFilter> > > >
CComEnumVariantOnListOfFilters;

typedef ICollectionOnSTLArray<IDispatchImpl<IFilters, &IID_IFilters>,
    vector< CAdapt< CComPtr<IFilter> > >,
    IFilter*,
    _CopyItfFromAdaptItf<IFilter>,
    CComEnumVariantOnListOfFilters>
IFilterCollImpl;

typedef  COLL_ITER(vector, IFilter)  FILTER_ITER;
typedef  COLL_RITER(vector, IFilter) FILTER_RITER;

// 指定索引的指针
#define  FILTER_ITER_AT(it,index)  FILTER_ITER (##it) = m_coll.begin()+index


// CFilters

class ATL_NO_VTABLE CFilters :
	public CComObjectRootEx<CComSingleThreadModel>,
    public CComCoClass<CFilters>,
	public ISupportErrorInfo,
    public IFilterCollImpl
{
public:
	CFilters()
	{
	}

DECLARE_NO_REGISTRY()


BEGIN_COM_MAP(CFilters)
	COM_INTERFACE_ENTRY(IFilters)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid)
    {
	    static const IID* arr[] = 
	    {
		    &IID_IFilters
	    };

	    for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	    {
		    if (InlineIsEqualGUID(*arr[i],riid))
			    return S_OK;
	    }
	    return S_FALSE;
    }


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

};

//DEL:OBJECT_ENTRY_AUTO(__uuidof(Filters), CFilters)
