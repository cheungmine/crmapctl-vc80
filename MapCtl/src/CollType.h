// CollType.h - 集合定义文件
//
#ifndef _COLL_TYPE_H__
#define _COLL_TYPE_H__

#include <atlcom.h>

#ifndef MAX_ITEM_NAME_LEN
    #define MAX_ITEM_NAME_LEN  255
#endif


// ICollOnSTLListWithMapImpl
//
template <class T, class CollType, class ItemType, class CopyItem, class EnumType,
    class ItemKey, class ItemClass, class ItemIface>
class ICollOnSTLListWithMapImpl:
    public ICollectionOnSTLImpl<T, CollType, ItemType, CopyItem, EnumType>
{
#define coll_pair_iter std::map<ItemKey, ItemType>::iterator
#define coll_pair_citer std::map<ItemKey, ItemType>::const_iterator
#define coll_pair_bool  std::pair<coll_pair_citer, bool>
#define coll_pair_item  std::pair<ItemKey, ItemType>

protected:

    std::map<ItemKey, ItemType>  m_map;

public:

    STDMETHOD(get_Item)(VARIANT at, ItemType* pvar)
	{
        if (pvar == NULL) {
            return E_POINTER;
        }

        long Index = -1;

        if ( VariantTypeIsBSTR(at) ) {
            if (::SysStringLen(at.bstrVal) > MAX_ITEM_NAME_LEN) {
                return E_INVALIDARG;
            }

            // get item by key
            ItemType pitem = 0;
            if ( FindItemNotAddRef(ItemKey(at.bstrVal), &pitem) ) {
                pitem->AddRef();
                *pvar = pitem;
                return S_OK;
            }
        } else if ( VariantTypeIsDispatch(at) ) {
            // javascript String
            ATLASSERT(0);
            return E_INVALIDARG;
        } else if (! VariantToLong(&at, &Index)) {
            return E_INVALIDARG;
        }

        // get item by 0-based Index
        if (Index < 0) {
            return E_INVALIDARG;
        }

        HRESULT hr = E_FAIL;
        CollType::iterator iter = m_coll.begin();
        while (iter != m_coll.end() && Index > 0) {
            iter++;
            Index--;
        }
        if (iter != m_coll.end()) {
            hr = CopyItem::copy(pvar, &*iter);
        }
        return hr;
    }


    STDMETHOD(Add)(VARIANT name, LONG index, ItemType* pvar)
    {
        HRESULT hr = E_FAIL;
        ItemType pItem = 0;

        if (index < ERR_INDEX || index > (long) m_coll.size()) {
            return E_INVALIDARG;
        }

        if ( VariantTypeIsBSTR(name) ) {
            if (::SysStringLen(name.bstrVal) > MAX_ITEM_NAME_LEN) {
                return E_INVALIDARG;
            }

            ItemKey key(name.bstrVal);

            if (FindItemNotAddRef(key, 0)) {
                // specified item has already existed
                return E_INVALIDARG;
            }

            // Create an ItemType and hand back to the client
            hr = ItemClass::CreateInstance(&pItem);

            if ( SUCCEEDED(hr) ) {
                coll_pair_bool br = m_map.insert(coll_pair_item(key, pItem));

                if (br.second) {
                    if (index == ERR_INDEX || index == m_coll.size()) {
                        // Put the Item on the back of coll
                        m_coll.push_back(CComPtr<ItemIface> (pItem));
                    } else {
                        // Insert the Item at index
                        CollType::iterator iter = m_coll.begin();
                        while (iter != m_coll.end() && index > 0) {
                            iter++;
                            index--;
                        }
                        ATLASSERT(iter != m_coll.end());
                        m_coll.insert(iter, CComPtr<ItemIface> (pItem));
                    }

                    ((ItemClass*) pItem)->InitData(key.c_str(), this);

                    *pvar = pItem;
                    return hr;
                }
            }
        } else if (VariantTypeIsNull(name)) {
            hr = ItemClass::CreateInstance(&pItem);

            if ( SUCCEEDED(hr) ) {
                wchar_t key[30];
                long keyid = AutogenItemKey(key, (long) m_coll.size());

                coll_pair_bool br = m_map.insert(coll_pair_item(key, pItem));

                while (! br.second) {
                    keyid = AutogenItemKey(key, keyid);
                    br = m_map.insert(coll_pair_item(key, pItem));
                }

                if (index == ERR_INDEX || index == m_coll.size()) {
                    // Put the Item on the back of coll
                    m_coll.push_back(CComPtr<ItemIface> (pItem));
                } else {
                    // Insert the Item at index
                    CollType::iterator iter = m_coll.begin();
                    while (iter != m_coll.end() && index > 0) {
                        iter++;
                        index--;
                    }
                    ATLASSERT(iter != m_coll.end());
                    m_coll.insert(iter, CComPtr<ItemIface> (pItem));
                }

                ((ItemClass*) pItem)->InitData(key, this);

                *pvar = pItem;
                return hr;
            }
        } else if (index == ERR_INDEX && VariantToLong(&name, &index)) {
            if (index < ERR_INDEX || index > (long) m_coll.size()) {
                return E_INVALIDARG;
            }

            hr = ItemClass::CreateInstance(&pItem);

            if ( SUCCEEDED(hr) ) {
                wchar_t key[30];
                long keyid = AutogenItemKey(key, (index == ERR_INDEX) ? (long) m_coll.size() : index);

                coll_pair_bool br = m_map.insert(coll_pair_item(key, pItem));

                while (! br.second) {
                    keyid = AutogenItemKey(key, keyid);
                    br = m_map.insert(coll_pair_item(key, pItem));
                }

                if (index == ERR_INDEX || index == m_coll.size()) {
                    // Put the Item on the back of coll
                    m_coll.push_back(CComPtr<ItemIface> (pItem));
                } else {
                    // Insert the Item at index
                    CollType::iterator iter = m_coll.begin();
                    while (iter != m_coll.end() && index > 0) {
                        iter++;
                        index--;
                    }
                    ATLASSERT(iter != m_coll.end());
                    m_coll.insert(iter, CComPtr<ItemIface> (pItem));
                }

                ((ItemClass*) pItem)->InitData(key, this);

                *pvar = pItem;
                return hr;
            }
        } else if (VariantTypeIsDispatch(name)) {
            CComQIPtr<ItemIface> spQI(name.pdispVal);
            if (spQI) {
                CComBSTR bstrName;
                hr = spQI->get_Name(&bstrName);

                if (SUCCEEDED(hr) && bstrName) {
                    ItemKey key((BSTR) bstrName);

                    coll_pair_bool br = m_map.insert(coll_pair_item(key, pItem));

                    if (br.second) {
                        if (index == ERR_INDEX || index == m_coll.size()) {
                            // Put the Item on the back of coll
                            m_coll.push_back(spQI);
                        } else {
                            // Insert the Item at index
                            CollType::iterator iter = m_coll.begin();
                            while (iter != m_coll.end() && index > 0) {
                                iter++;
                                index--;
                            }
                            ATLASSERT(iter != m_coll.end());
                            m_coll.insert(iter, spQI);
                        }

                        ((ItemClass*) pItem)->InitData(key.c_str(), this);

                        *pvar = spQI.Detach();
                        return hr;
                    }
                }
            }
        }

        return E_INVALIDARG;
    }


    STDMETHOD(Remove)(VARIANT _where)
    {
        long index = ERR_INDEX;
        HRESULT hr = S_FALSE;

        if ( VariantTypeIsBSTR(_where) ) {
            // remove Item by name
            if (::SysStringLen(_where.bstrVal) > MAX_ITEM_NAME_LEN) {
                return E_INVALIDARG;
            }

            ItemKey key(_where.bstrVal);

            coll_pair_iter at = m_map.find(key);
            if (at != m_map.end()) {
                // find in list
                CollType::iterator iter = m_coll.begin();
                while (iter != m_coll.end()) {
                    if (at->second == iter->m_T.p) {
                        break;
                    }
                    iter++;
                }

                // remove from list and map
                if (iter != m_coll.end()) {
                    m_coll.erase(iter);
                    m_map.erase(at);
                    hr = S_OK;
                }
            }
        } else if (VariantToLong(&_where, &index)) {
            // remove by index: 0-based

            if (index == ERR_INDEX || index == (long) m_coll.size() - 1) {
                // remove from back
                if (RemoveItemFromMap(m_coll.back().m_T.p)) {
                    m_coll.pop_back();
                    hr = S_OK;
                }
            } else if (index == 0) {
                // remove from front
                if (RemoveItemFromMap(m_coll.front().m_T.p)) {
                    m_coll.pop_front();
                    hr = S_OK;
                }
            } else if (index > 0 && index < (long) m_coll.size() - 1) {
                CollType::iterator iter = m_coll.begin();
                while (iter != m_coll.end() && index > 0) {
                    iter++;
                    index--;
                }
                ATLASSERT(iter != m_coll.end());
                if (iter != m_coll.end()) {
                    if (RemoveItemFromMap(iter->m_T.p)) {
                        m_coll.erase(iter);
                        hr = S_OK;
                    }
                }
            } else {
                hr = E_INVALIDARG;
            }
        } else {
            return E_NOTIMPL;
        }

        return hr;
    }


    STDMETHOD(Clear)(void)
    {
        m_coll.clear();
        m_map.clear();
        return S_OK;
    }


    STDMETHOD(_RenameItem)(ItemType pItem, BSTR newVal)
    {
        if (!newVal) {
            return E_POINTER;
        }

        if (::SysStringLen(newVal) > MAX_ITEM_NAME_LEN) {
            return E_INVALIDARG;
        }

        ItemKey newKey(newVal);
        if (FindItemNotAddRef(newKey, 0)) {
            // Item with specified Name has already existed
            return E_INVALIDARG;
        }

        CComBSTR oldVal;
        pItem->get_Name(&oldVal);

        ItemKey oldKey(oldVal);
        coll_pair_iter at = m_map.find(oldKey);
        if (at != m_map.end()) {
            // remove it first
            m_map.erase(at);

            // insert then
            coll_pair_bool br = m_map.insert(coll_pair_item(newKey, pItem));
            if (br.second) {
                return ((ItemClass*) pItem)->ForceResetName(newVal);
            }
        }

        return E_FAIL;
    }


private:

  	BOOL FindItemNotAddRef (const ItemKey & key, ItemType* pItem)
    {
        coll_pair_citer it = m_map.find(key);
        if (it != m_map.end()) {
            if (pItem) {
                *pItem = it->second;
            }
            return TRUE;
	    }
		return FALSE;
	}


    BOOL RemoveItemFromMap (ItemType /* IUnknown* */ item)
    {
        BSTR name = 0;

        if (S_OK == item->get_Name(&name) && name) {
            ItemKey key(name);
            ::SysFreeString(name);

            coll_pair_iter it = m_map.find(key);
            if (it != m_map.end()) {
                ATLASSERT(it->second == item);
                m_map.erase(it);
                return TRUE;
            }
        }

        return FALSE;
    }

    long AutogenItemKey (wchar_t key[], long keyid)
    {
        CComBSTR s(typeid(ItemIface).name());
        if (! s) {
            s.Append("__untitled_");
        }

        swprintf(key, L"__%s_%d", s, keyid);

        wchar_t *p = key;
        while (p && *p) {
            if (*p == L' ') {
                *p = '_';
            }
            p++;
        }

        return keyid+1;
    }
};


// ICollectionOnSTLArray
//
template <class T, class CollType, class ItemType, class CopyItem, class EnumType>
class ICollectionOnSTLArray :
    public ICollectionOnSTLImpl<T, CollType, ItemType, CopyItem, EnumType>
{
public:

    STDMETHOD(get_Item)(VARIANT id, ItemType* pvar)
	{
        long Index = -1;

        if (!VariantToLong(&id, &Index)) {
            return E_INVALIDARG;
        }

        // Index is 0-based
        if (pvar == NULL) {
            return E_POINTER;
        }
        if (Index < 0) {
            return E_INVALIDARG;
        }
        HRESULT hr = E_FAIL;
        if (Index >= 0 && Index < (long)m_coll.size()) {
            CollType::iterator iter = m_coll.begin() + Index;

            if (iter != m_coll.end()) {
                hr = CopyItem::copy(pvar, &*iter);
            }
        }
        return hr;
	}
}; 


template <typename T>
struct _CopyVariantFromAdaptItf
{
    static HRESULT copy(VARIANT* p1, const CAdapt< CComPtr<T> >* p2)
    {
        HRESULT hr = p2->m_T->QueryInterface(IID_IDispatch, (void**)&p1->pdispVal);
        if (SUCCEEDED(hr)) {
            p1->vt = VT_DISPATCH;
        } else {
            hr = p2->m_T->QueryInterface(IID_IUnknown, (void**)&p1->punkVal);
            if( SUCCEEDED(hr) ) {
                p1->vt = VT_UNKNOWN;
            }
        }
        return hr;
    }


    static void init(VARIANT* p)
    {
        VariantInit(p);
    }


    static void destroy(VARIANT* p)
    {
        VariantClear(p);
    }
};


template <typename T>
struct _CopyItfFromAdaptItf
{
    static HRESULT copy(T** p1, const CAdapt< CComPtr<T> >* p2)
    {
        if ( *p1 = p2->m_T ) {
            return (*p1)->AddRef(), S_OK;
        }

        return E_POINTER;
    }


    static void init(T** p)
    {
    }


    static void destroy(T** p)
    {
        if ( *p ) {
            (*p)->Release();
        }
    }
};


#endif  // _COLL_TYPE_H__
