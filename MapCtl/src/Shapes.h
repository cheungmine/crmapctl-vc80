// Shapes.h : CShapes ������
//
#pragma once

#include "Config.h"
#include "CollType.h"
#include "Shape.h"


typedef CComEnumOnSTL<IEnumVARIANT, &IID_IEnumVARIANT, VARIANT, 
    _CopyVariantFromAdaptItf<IShape>,
    deque< CAdapt< CComPtr<IShape> > > >
CComEnumVariantOnDequeOfShapes;


typedef ICollectionOnSTLImpl<IDispatchImpl<IShapes, &IID_IShapes>,
    deque< CAdapt< CComPtr<IShape> > >,
    IShape*,
    _CopyItfFromAdaptItf<IShape>,
    CComEnumVariantOnDequeOfShapes>
IShapeCollImpl;

typedef CAdapt< CComPtr<IShape> >	   SHAPE_ITEM;
typedef deque< SHAPE_ITEM >::iterator  SHAPE_ITER;


// CShapes

class ATL_NO_VTABLE CShapes :
	public CComObjectRootEx<CComSingleThreadModel>,
    public CComCoClass<CShapes>,
	public ISupportErrorInfo,
    public IShapeCollImpl
{
public:
    VARIANT_BOOL        m_bVisible;

	CShapes() : m_bVisible (VARIANT_TRUE)
	{
	}

DECLARE_NO_REGISTRY()

BEGIN_COM_MAP(CShapes)
	COM_INTERFACE_ENTRY(IShapes)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

    void Draw (CDrawInfo *di);

public:

    STDMETHOD(get_Visible)(VARIANT_BOOL* pVal)
    {
        *pVal = m_bVisible;
        return S_OK;
    }

    STDMETHOD(put_Visible)(VARIANT_BOOL newVal)
    {
        m_bVisible = newVal;
        return S_OK;
    }

    STDMETHOD(Add)(/*[out,retval]*/ IShape** ppVal)
    {
        // Create an IShape to hand back to the client
        CComPtr<IShape> spObj;
        HRESULT hr = CShape::CreateInstance(ppVal);

        if ( SUCCEEDED(hr) ) {
            // Put the ILayer on the list
            CComPtr<IShape> spObj = *ppVal;
            m_coll.push_back(spObj);
        }

        return hr;
    }
};

//DEL:OBJECT_ENTRY_AUTO(__uuidof(Shapes), CShapes)
