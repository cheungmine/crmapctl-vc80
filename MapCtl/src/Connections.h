// Connections.h
//
#pragma once

#include "Config.h"
#include "CollType.h"
#include "Connection.h"


typedef list< CAdapt< CComPtr<IConnection> > > ConnectionCollType;

typedef CComEnumOnSTL<IEnumVARIANT, &IID_IEnumVARIANT, VARIANT, 
    _CopyVariantFromAdaptItf<IConnection>,
    ConnectionCollType >
CComEnumVariantOnListOfConnections;

typedef ICollOnSTLListWithMapImpl<IDispatchImpl<IConnections, &IID_IConnections>,
    ConnectionCollType,
    IConnection*,
    _CopyItfFromAdaptItf<IConnection>,
    CComEnumVariantOnListOfConnections,
    std::wstring,
    CConnection,
    IConnection >
IConnectionCollImpl;


typedef  COLL_ITER(vector, IConnection)  CONN_ITER;
typedef  COLL_RITER(vector, IConnection) CONN_RITER;

// 指定索引的指针
#define  CONN_ITER_AT(it,index)  CONN_ITER (##it) = m_coll.begin()+index


// CConnections

class ATL_NO_VTABLE CConnections :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CConnections>,
	public ISupportErrorInfo,
    public IConnectionCollImpl
{
public:
	CConnections()
	{
	}

DECLARE_NO_REGISTRY()


BEGIN_COM_MAP(CConnections)
	COM_INTERFACE_ENTRY(IConnections)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid)
    {
	    static const IID* arr[] = 
	    {
		    &IID_IConnections
	    };

	    for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	    {
		    if (InlineIsEqualGUID(*arr[i],riid))
			    return S_OK;
	    }
	    return S_FALSE;
    }


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

};

//DEL:OBJECT_ENTRY_AUTO(__uuidof(Connections), CConnections)
