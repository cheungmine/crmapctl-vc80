// Layer.h : CLayer ������
//
#pragma once

#include "Config.h"
#include "ItemType.h"

#include "Shapes.h"
#include "Rasters.h"

class CLayers;


// CLayer

class ATL_NO_VTABLE CLayer :
	public CComObjectRootEx<CComSingleThreadModel>,
    public CComCoClass<CLayer>,
	public ISupportErrorInfo,
	public IItemTypeDispatchImpl<ILayers, ILayer, &IID_ILayer, &LIBID_CRMapCtlLib, 1, 0>
{
public:

    VARIANT_BOOL        m_bVisible;

    CComPtr<IShapes>    m_spShapes;

    CComPtr<IRasters>   m_spRasters;

    CLayer() : m_bVisible (VARIANT_TRUE)
	{
	}

DECLARE_NO_REGISTRY()

BEGIN_COM_MAP(CLayer)
	COM_INTERFACE_ENTRY(ILayer)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid)
    {
	    static const IID* arr[] = 
	    {
		    &IID_ILayer
	    };

	    for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	    {
		    if (InlineIsEqualGUID(*arr[i],riid))
			    return S_OK;
	    }
	    return S_FALSE;
    }


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
        HRESULT hr;

        hr = CShapes::CreateInstance(&m_spShapes);

        hr = CRasters::CreateInstance(&m_spRasters);

        return hr;
	}

	void FinalRelease()
	{
	}


    void Draw (CDrawInfo *di)
    {
        if (! IsDraw(di)) {
            return;
        }

        // Draw Shapes
		//
		if (m_spShapes) {
			//m_spRenderer->_Init(di, m_spRenders.p, m_spRender.p, m_spColumns.p);
			//m_spSymbolizer->_Init(di, m_spColumns.p, m_spSymbols.p);			
			((CShapes*) m_spShapes.p)->Draw(di);			
			//m_spSymbolizer->_Uninit(di);
			//m_spRenderer->_Uninit(di);			
        }

		// Draw Rasters
		//
		if (m_spRasters) {
			((CRasters*)m_spRasters.p)->Draw(di);
		}
    }


private:

    // ͼ���Ƿ����
	BOOL IsDraw (CDrawInfo* di);

    HRESULT LoadFileSHP (LPCSTR filename);

    HRESULT LoadFileDWG (LPCSTR filename);

    HRESULT LoadFileDXF (LPCSTR filename);

    HRESULT LoadFilePNG (LPCSTR filename);

    HRESULT LoadFileBMP (LPCSTR filename);

    HRESULT LoadFileGIF (LPCSTR filename);

    HRESULT LoadFileJPG (LPCSTR filename);

    HRESULT LoadFileTIF (LPCSTR filename);

public:

    STDMETHOD(get_Visible)(VARIANT_BOOL* pVal)
    {
        *pVal = m_bVisible;
        return S_OK;
    }

    STDMETHOD(put_Visible)(VARIANT_BOOL newVal)
    {
        m_bVisible = newVal;
        return S_OK;
    }

    STDMETHOD(get_Shapes)(IShapes** ppVal)
    {
        return m_spShapes.CopyTo(ppVal);
    }

    STDMETHOD(get_Rasters)(IRasters** ppVal)
    {
        return m_spRasters.CopyTo(ppVal);
    }

    STDMETHOD(Open)(VARIANT arg1, VARIANT arg2);
};

//DEL:OBJECT_ENTRY_AUTO(__uuidof(Layer), CLayer)
