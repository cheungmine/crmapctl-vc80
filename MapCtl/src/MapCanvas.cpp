// MapCanvas.cpp : CMapCanvas 的实现
//
#pragma message("**** WinCE 组件依赖的 DLL（如:libgdbapi.dll）必须部署到 %CSIDL_WINDOWS%，否则无法注册！ ****")

#include "Config.h"

#include "MapCanvas.h"

#include "Bands.h"
#include "Bound.h"
#include "Coldef.h"
#include "_Color.h"
#include "GpsDevice.h"
#include "GpsPosition.h"
#include "ImageButton.h"
#include "ImageButtons.h"
#include "Coordref.h"

#include "Connection.h"
#include "Connections.h"

#include "Dataset.h"
#include "Filter.h"
#include "Filters.h"

#include "Layer.h"
#include "Layers.h"
#include "Layout.h"
#include "_Point.h"
#include "_Points.h"
#include "PrintSettings.h"
#include "Raster.h"
#include "Rasters.h"
#include "Render.h"
#include "Renders.h"
#include "_Rowset.h"  
#include "Shape.h"
#include "Shapes.h"
#include "Symbol.h"
#include "Symbols.h"
#include "TrackingShape.h"


#pragma warning (push)
#pragma warning (disable: 4996)

static int CoClassLookup(const WCHAR *name)
{
    static int last_i = 0;
        
    static const WCHAR * COCLASS_NAMES[] = {
        L"Band",
        L"Bands",
        L"Bound",
        L"Coldef",
        L"Color",
        L"Connection",
        L"Connections",
        L"Coordref",
        L"Dataset",
        L"Filter",
        L"Filters",
        L"GpsDevice",
        L"GpsPosition",
        L"ImageButton",
        L"ImageButtons",
        L"Layer",
        L"Layers",
        L"Layout",
        L"Point",
        L"Points",
        L"PrintSettings",
        L"Raster",
        L"Rasters",
        L"Render",
        L"Renders",
        L"Rowset",  
        L"Shape",
        L"Shapes",
        L"Symbol",
        L"Symbols",
        L"TrackingShape",
        0
    };

    static const int COCLASS_IDS[] = {
        coBand,
        coBands,
        coBound,
        coColdef,
        coColor,
        coConnection,
        coConnections,
        coCoordref,
        coDataset,
        coFilter,
        coFilters,
        coGpsDevice,
        coGpsPosition,
        coImageButton,
        coImageButtons,
        coLayer,
        coLayers,
        coLayout,
        coPoint,
        coPoints,
        coPrintSettings,
        coRaster,
        coRasters,
        coRender,
        coRenders,
        coRowset,  
        coShape,
        coShapes,
        coSymbol,
        coSymbols,
        coTrackingShape,
        -1
    };

    if (!wcsicmp(COCLASS_NAMES[last_i], name)) {
        return COCLASS_IDS[last_i];
    }

    int i = 0;
    while ( COCLASS_NAMES[i] && wcsicmp(COCLASS_NAMES[i], name) ) {
        i++;
    }

    if (i < sizeof(COCLASS_NAMES)/sizeof(COCLASS_NAMES[0])) {
        last_i = i;
        return COCLASS_IDS[i];
    }

    return (-1);
}

#pragma warning (pop)


#define COCLASS_CREATE_INSTANCE(clsname) \
    hres = clsname##::CreateInstance(&pUnk); \
    if (SUCCEEDED(hres)) { \
        hres = pUnk->QueryInterface(IID_IDispatch, (void**)ppObject); \
        pUnk->Release(); \
    }


///////////////////////////////////////////////////////////////////
// CMapCanvas
//
#pragma warning (push)
#pragma warning (disable: 4996)

STDMETHODIMP CMapCanvas::CreateObject(VARIANT coClassName, IDispatch** ppObject)
{
    LOG_TRACE0();

    HRESULT hres = E_INVALIDARG;

    long coclsid = ERR_INDEX;

    if (!ppObject) {
		return E_POINTER;
    }

    if (VariantTypeIsLong(coClassName)) {
        VariantToLong(&coClassName, &coclsid);
    } else if (VariantTypeIsBSTR(coClassName)) {
        if (::SysStringLen(coClassName.bstrVal) < 30) {
            coclsid = CoClassLookup(coClassName.bstrVal);
        }
    } else if (VariantTypeIsDispatch(coClassName)) {
        // TODO:
        return E_NOTIMPL;
    } else {
        return E_INVALIDARG;
    }

    if (coclsid == ERR_INDEX) {
        return E_INVALIDARG;
    }

    IUnknown * pUnk = 0;

    switch (coclsid) {
    case coBand:
        COCLASS_CREATE_INSTANCE(CBand)
        break;
    case coBands:
        COCLASS_CREATE_INSTANCE(CBands)
        break;
    case coBound:
        COCLASS_CREATE_INSTANCE(CBound)
        break;
    case coColdef:
        COCLASS_CREATE_INSTANCE(CColdef)
        break;
    case coColor:
        COCLASS_CREATE_INSTANCE(_CColor)
        break;
    case coConnection:
        COCLASS_CREATE_INSTANCE(CConnection)
        break;
    case coConnections:
        COCLASS_CREATE_INSTANCE(CConnections)
        break;
    case coCoordref:
        COCLASS_CREATE_INSTANCE(CCoordref)
        break;
    case coDataset:
        COCLASS_CREATE_INSTANCE(CDataset)
        break;
    case coFilter:
        COCLASS_CREATE_INSTANCE(CFilter)
        break;
    case coFilters:
        COCLASS_CREATE_INSTANCE(CFilters)
        break;
    case coGpsDevice:
        COCLASS_CREATE_INSTANCE(CGpsDevice)
        break;
    case coGpsPosition:
        COCLASS_CREATE_INSTANCE(CGpsPosition)
        break;
    case coImageButton:
        COCLASS_CREATE_INSTANCE(CImageButton)
        break;
    case coImageButtons:
        COCLASS_CREATE_INSTANCE(CImageButtons)
        break;
    case coLayer:
        COCLASS_CREATE_INSTANCE(CLayer)
        break;
    case coLayers:
        COCLASS_CREATE_INSTANCE(CLayers)
        break;
    case coLayout:
        COCLASS_CREATE_INSTANCE(CLayout)
        break;
    case coPoint:
        COCLASS_CREATE_INSTANCE(_CPoint)
        break;
    case coPoints:
        COCLASS_CREATE_INSTANCE(_CPoints)
        break;
    case coPrintSettings:
        COCLASS_CREATE_INSTANCE(CPrintSettings)
        break;
    case coRaster:
        COCLASS_CREATE_INSTANCE(CRaster)
        break;
    case coRasters:
        COCLASS_CREATE_INSTANCE(CRasters)
        break;
    case coRender:
        COCLASS_CREATE_INSTANCE(CRender)
        break;
    case coRenders:
        COCLASS_CREATE_INSTANCE(CRenders)
        break;
    case coRowset:
        COCLASS_CREATE_INSTANCE(_CRowset)
        break;
    case coShape:
        COCLASS_CREATE_INSTANCE(CShape)
        break;
    case coShapes:
        COCLASS_CREATE_INSTANCE(CShapes)
        break;
    case coSymbol:
        COCLASS_CREATE_INSTANCE(CSymbol)
        break;
    case coSymbols:
        COCLASS_CREATE_INSTANCE(CSymbols)
        break;
    case coTrackingShape:
        COCLASS_CREATE_INSTANCE(CTrackingShape)
        break;
    }

    return hres;
}

#pragma warning (pop)


LRESULT CMapCanvas::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    LOG_TRACE0();
    return 0;
}


LRESULT CMapCanvas::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    LOG_TRACE0();
    return 0;
}


LRESULT CMapCanvas::OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    LOG_TRACE0();
    return 0;
}


LRESULT CMapCanvas::OnContextMenu(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    LOG_TRACE0();
    return 0;
}


LRESULT CMapCanvas::OnEraseBkgnd(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    return 0;
}


LRESULT CMapCanvas::OnLButtonDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    LOG_TRACE0();
    return 0;
}


LRESULT CMapCanvas::OnLButtonUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    LOG_TRACE0();
    return 0;
}


LRESULT CMapCanvas::OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    return 0;
}
