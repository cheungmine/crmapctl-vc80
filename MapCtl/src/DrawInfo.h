// DrawInfo.h
//
#ifndef _DRAW_INFO_H__
#define _DRAW_INFO_H__


#include "viewport.h"

class CDrawInfo
{
public:
    VIEWPORT_STATE    vwport;

    HDC               hdcDest;
};


#endif // _DRAW_INFO_H__
