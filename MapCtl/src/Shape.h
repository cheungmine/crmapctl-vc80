// Shape.h : CShape ������
//
#pragma once

#include "Config.h"

#include "./gdbapi/src/gdbapi.h"


class CShapes;

// CShape

class ATL_NO_VTABLE CShape :
	public CComObjectRootEx<CComSingleThreadModel>,
    public CComCoClass<CShape>,
	public ISupportErrorInfo,
	public IDispatchImpl<IShape, &IID_IShape, &LIBID_CRMapCtlLib, 1, 0>
{
public:

    GDB_SHAPE  hshape;

    /*
    GDB_ATTR_SHAPE_ROWID,
    GDB_ATTR_SHAPE_TYPE,
    GDB_ATTR_SHAPE_TYPENAME,
    GDB_ATTR_SHAPE_BOUND,
    GDB_ATTR_SHAPE_BOUNDADDR,
    GDB_ATTR_SHAPE_RECT,
    GDB_ATTR_SHAPE_XMIN,
    GDB_ATTR_SHAPE_YMIN,
    GDB_ATTR_SHAPE_XMAX,
    GDB_ATTR_SHAPE_YMAX,
    GDB_ATTR_SHAPE_ZMIN,
    GDB_ATTR_SHAPE_ZMAX,
    GDB_ATTR_SHAPE_MMIN,
    GDB_ATTR_SHAPE_MMAX,
    GDB_ATTR_SHAPE_WKBCOPY,
    GDB_ATTR_SHAPE_WKBADDR,
    GDB_ATTR_SHAPE_LENGTH,
    GDB_ATTR_SHAPE_AREA,
    GDB_ATTR_SHAPE_ISNULL,
    */

    CShape() : hshape(0)
	{
	}


DECLARE_NO_REGISTRY()

BEGIN_COM_MAP(CShape)
	COM_INTERFACE_ENTRY(IShape)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
    STDMETHODIMP InterfaceSupportsErrorInfo(REFIID riid)
    {
	    static const IID* arr[] = 
	    {
		    &IID_IShape
	    };

	    for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	    {
		    if (InlineIsEqualGUID(*arr[i],riid))
			    return S_OK;
	    }
	    return S_FALSE;
    }



	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

    void Draw (CDrawInfo *di);

public:

};

//DEL:OBJECT_ENTRY_AUTO(__uuidof(Shape), CShape)
