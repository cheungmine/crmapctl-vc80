/******************************************************************************
 * viewport.h
 *   viewport API
 * cheungmine
 * 2014-4-09
 *****************************************************************************/
#ifndef _VIEWPORT_H_INCLUDED
#define _VIEWPORT_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

/**
 * common headers
 */
#include <assert.h>

#include <float.h>
#include <memory.h>
#include <math.h>


#ifndef VWPT_SCALEFACTOR_MIN
    #define VWPT_SCALEFACTOR_MIN      FLT_MIN
#endif

#ifndef VWPT_SCALEFACTOR_MAX
    #define VWPT_SCALEFACTOR_MAX      FLT_MAX
#endif


#ifndef _ASSERT
    #define _ASSERT(cond) assert (cond)
#endif

#ifndef M_2PI
    #define M_2PI  (M_PI*2)
#endif

#ifndef _TANGENT
    #if defined(_WIN32_WCE)
        #define  _TANGENT  tan
    #else
        #define  _TANGENT  tanf
    #endif
#endif

#ifndef _MIN
    #define _MIN(v1, v2) ((v1)<(v2)? (v1):(v2))
#endif

#ifndef _MAX
    #define _MAX(v1, v2) ((v1)>(v2)? (v1):(v2))
#endif

#ifndef _ABS
    #define _ABS(v) ((v)>=0?(v):(-v))
#endif

#ifndef _SWAP
    # define _SWAP(a, b, type)  do { \
            type t = (a); \
            (a) = (b); \
            (b) = t; \
        } while (0)
#endif

#ifndef BOOL
    typedef int BOOL;
#endif

#ifndef TRUE
    #define TRUE  1
#endif

#ifndef FALSE
    #define FALSE  0
#endif


#ifndef __INLINE
    #ifdef _MSC_VER
        #define __INLINE static __forceinline
        #define __INLINE_ALL __INLINE
    #else
        #define __INLINE static inline
        #if (defined(__APPLE__) && defined(__ppc__))
            /* static inline __attribute__ here breaks osx ppc gcc42 build */
            #define __INLINE_ALL static __attribute__((always_inline))
        #else
            #define __INLINE_ALL static inline __attribute__((always_inline))
        #endif
    #endif
#endif /* __INLINE */


/*
 * Public Structs Definitions
 */

/**
 * same as RECT in <windef.h>
 */
typedef struct vwpt_rect_i
{
    int      Xmin,    /* left */
             Ymin,    /* top */
             Xmax,    /* right */
             Ymax;    /* bottom */
} VWPT_RECT_INT;


typedef struct vwpt_rect_f
{
    float  Xmin,
           Ymin,
           Xmax,
           Ymax;
} VWPT_RECT_FLT;


typedef struct vwpt_rect_d
{
    double Xmin, 
           Ymin, 
           Xmax, 
           Ymax;
} VWPT_RECT_DBL;


/**
 * same as POINT in <windef.h>
 */
typedef struct vwpt_point_i
{
    int X,
        Y;
} VWPT_POINT_INT;


typedef struct vwpt_point_f
{
    float X, 
          Y;
} VWPT_POINT_FLT;


typedef struct vwpt_point_d
{
    double X,
           Y;
} VWPT_POINT_DBL;


/**
 * viewport state struct
 */
typedef struct viewport_state_t
{
    /* viewport rect in logical reference */
    vwpt_rect_d  viewRC;

    /* data rect in world coords reference */
    vwpt_rect_d  dataRC;

    /* viewport center point32_t in logical unit */
    vwpt_point_d viewCP;

    /* viewed data rect center in world coords */
    vwpt_point_d dataCP;

    /* X_DPI/Y_DPI, default is 1.0. used when transformation */
    double      ratioXY;

    /* current scaleFactor: viewRC/vdata_rc  (like: pixels/meter) */
    double      scaleFactor;

    /* const values */
    double      scaleFactorMin;
    double      scaleFactorMax;
} VIEWPORT_STATE;


#define VWPT_RECT_SET(rc, xmin, ymin, xmax, ymax, type) do { \
        (rc).Xmin = (type) (xmin); \
        (rc).Ymin = (type) (ymin); \
        (rc).Xmax = (type) (xmax); \
        (rc).Ymax = (type) (ymax); \
    } while (0)


/**
 * test two rects overlapped
 */
#define VWPT_RECT_OVERLAPPED_XY(aXmin, aYmin, aXmax, aYmax, bXmin, bYmin, bXmax, bYmax) \
    ((aXmin)<(bXmax) && (aYmin)<(bYmax) && (bXmin)<(aXmax) && (bYmin)<(aYmax))


#define VWPT_RECT_OVERLAPPED(a, b) \
    VWPT_RECT_OVERLAPPED_XY((a).Xmin, (a).Ymin, (a).Xmax, (a).Ymax, (b).Xmin, (b).Ymin, (b).Xmax, (b).Ymax)


#define VWPT_RECT_GET_WIDTH(rc)    ((rc).Xmax - (rc).Xmin)


#define VWPT_RECT_GET_HEIGHT(rc)   ((rc).Ymax - (rc).Ymin)


#define VWPT_RECT_GET_CENTER(rc, cp) do { \
        (cp).X = ((rc).Xmin + (rc).Xmax)/2; \
        (cp).Y = ((rc).Ymin + (rc).Ymax)/2; \
    } while(0)


#define VWPT_RECT_OFFSET(rc, dx, dy) do { \
        (rc).Xmin += (dx); \
        (rc).Xmax += (dx); \
        (rc).Ymin += (dy); \
        (rc).Ymax += (dy); \
    } while(0)


#define VWPT_RECT_SET_ZERO(rect) \
    (rect).Xmin = (rect).Xmax = (rect).Ymin = (rect).Ymax = 0


#define VWPT_RECT_IS_ZERO(rect) \
    ((rect).Xmin == 0 && (rect).Xmax == 0 && (rect).Ymin == 0 && (rect).Ymax == 0)


#define VWPT_RECT_VALIDATE(rect, type) do { \
        if ((rect).Xmin > (rect).Xmax) { \
            _SWAP((rect).Xmin, (rect).Xmax, type); \
        } \
        if ((rect).Ymin > (rect).Ymax) { \
            _SWAP((rect).Ymin, (rect).Ymax, type); \
        } \
    } while (0)


#define VWPT_RECT_SHRINK(rect, dx, dy) do { \
        (rect).Xmin += dx; \
        (rect).Xmax -= dx; \
        (rect).Ymin += dy; \
        (rect).Ymax -= dy; \
    } while(0)


static BOOL vwpt_rect_intersect (vwpt_rect_d *subject, vwpt_rect_d *clip, vwpt_rect_d *result)
{
    if (subject == clip) {
        if (result != subject) {
            *result = *subject;
        }
        return TRUE;
    } else if (VWPT_RECT_IS_ZERO(*subject) || VWPT_RECT_IS_ZERO(*clip)) {
        return FALSE;
    } else {
        if (VWPT_RECT_OVERLAPPED(*subject, *clip)) {
            result->Xmin = _MAX(subject->Xmin, clip->Xmin);
            result->Ymin = _MAX(subject->Ymin, clip->Ymin);
            result->Xmax = _MIN(subject->Xmax, clip->Xmax);
            result->Ymax = _MIN(subject->Ymax, clip->Ymax);
            _ASSERT(result->Xmax > result->Xmin && result->Ymax > result->Ymin);
            return TRUE;
        } else {
            return FALSE;
        }
    }
}


static void vwpt_rect_union (vwpt_rect_d *subject, vwpt_rect_d *clip, vwpt_rect_d *result)
{
    if (subject == clip) {
        if (result != subject) {
            *result = *subject;
        }
    } else if (VWPT_RECT_IS_ZERO(*subject)) {
        if (result != clip) {
            *result = *clip;
        }
    } else if (VWPT_RECT_IS_ZERO(*clip)) {
        if (result != subject) {
            *result = *subject;
        }
    } else {
        result->Xmin = _MIN(subject->Xmin, clip->Xmin);
        result->Ymin = _MIN(subject->Ymin, clip->Ymin);
        result->Xmax = _MAX(subject->Xmax, clip->Xmax);
        result->Ymax = _MAX(subject->Ymax, clip->Ymax);
    }
}


/**
 * Set new scale factor
 */
__INLINE void vwpt_state_set_scale (viewport_state_t *s, double newScaleFactor)
{
    double scaleValue = _ABS(newScaleFactor);
    s->scaleFactor = (scaleValue < s->scaleFactorMin) ?
        s->scaleFactorMin : (scaleValue > s->scaleFactorMax? s->scaleFactorMax : scaleValue);
}


/**
 * Update scale and returns it
 */
__INLINE double vwpt_state_calc_scale (viewport_state_t *s, BOOL is_set)
{
    double w = VWPT_RECT_GET_WIDTH(s->viewRC) / VWPT_RECT_GET_WIDTH(s->dataRC);
    double h = VWPT_RECT_GET_HEIGHT(s->viewRC) / VWPT_RECT_GET_HEIGHT(s->dataRC);
    double sc = _MIN(w, h);

    if (is_set) {
        vwpt_state_set_scale(s, sc);
    }

    return sc;
}


/**
 * initialize only view
 */
static void vwpt_state_init_view (viewport_state_t *vwpt, double viewWidth, double viewHeight)
{
    vwpt->ratioXY = 1.0; 

    vwpt->scaleFactorMin = VWPT_SCALEFACTOR_MIN; 
    vwpt->scaleFactorMax = VWPT_SCALEFACTOR_MAX;

    vwpt->viewRC.Xmin = 0;
    vwpt->viewRC.Ymin = 0;
    vwpt->viewRC.Xmax = viewWidth;
    vwpt->viewRC.Ymax = viewHeight;

    vwpt->dataRC = vwpt->viewRC;

    VWPT_RECT_GET_CENTER(vwpt->viewRC, vwpt->viewCP);
    VWPT_RECT_GET_CENTER(vwpt->dataRC, vwpt->dataCP);

    vwpt_state_calc_scale(vwpt, TRUE);
}


/**
 * initialize all
 */
static void vwpt_state_init_all (viewport_state_t *s, const vwpt_rect_d *view, const vwpt_rect_d *data, double ratioXY /* 1.0 */)
{
    s->viewRC = *view; 
    s->dataRC = *data; 
    s->ratioXY = ratioXY; 

    s->scaleFactorMin = VWPT_SCALEFACTOR_MIN; 
    s->scaleFactorMax = VWPT_SCALEFACTOR_MAX;

    VWPT_RECT_GET_CENTER(s->viewRC, s->viewCP);
    VWPT_RECT_GET_CENTER(s->dataRC, s->dataCP);

    vwpt_state_calc_scale(s, TRUE);
}


static void vwpt_state_copy(viewport_state_t *dst, const viewport_state_t *src)
{
    *dst = *src;
}


/**
 * Set the view port when view's size changed, such as: WM_ONSIZE
 *   (vrcXmin, vrcYmin, vrcXmax, vrcYmax) are coords of view rect
 */
static void vwpt_state_update_view_rect(viewport_state_t *s, double Xmin, double Ymin, double Xmax, double Ymax)
{
    s->viewRC.Xmin = Xmin;
    s->viewRC.Ymin = Ymin;
    s->viewRC.Xmax = Xmax;
    s->viewRC.Ymax = Ymax;

    VWPT_RECT_GET_CENTER(s->viewRC, s->viewCP);
}


/**
 * Set the data rect when data changed:
 *   (drcXmin, drcYmin, drcXmax, drcYmax) are coords of data rect
 */
static void vwpt_state_update_data_rect(viewport_state_t *s, double Xmin, double Ymin, double Xmax, double Ymax)
{
    s->dataRC.Xmin = Xmin;
    s->dataRC.Ymin = Ymin;
    s->dataRC.Xmax = Xmax;
    s->dataRC.Ymax = Ymax;

    VWPT_RECT_GET_CENTER(s->dataRC, s->dataCP);
    vwpt_state_calc_scale(s, TRUE);
}


/**
 * get current data rect of view
 */
static void vwpt_state_get_viewdata_rect(const viewport_state_t *s, vwpt_rect_d *vdrc)
{
    double val;

    val = VWPT_RECT_GET_WIDTH(s->viewRC) / s->scaleFactor / 2;    
    vdrc->Xmin = s->dataCP.X - val;
    vdrc->Xmax = s->dataCP.X + val;

    val = VWPT_RECT_GET_HEIGHT(s->viewRC) / s->scaleFactor / 2;
    vdrc->Ymin = s->dataCP.Y - val;
    vdrc->Ymax = s->dataCP.Y + val;
}


/* current display scale: DataRect / ViewedDataRect */
static double vwpt_state_get_viewdata_scale(const viewport_state_t *s)
{
    double sw, sh;
    vwpt_rect_d vdrc;

    vwpt_state_get_viewdata_rect(s, &vdrc);

    sw = VWPT_RECT_GET_WIDTH(s->dataRC) / VWPT_RECT_GET_WIDTH(vdrc);
    sh = VWPT_RECT_GET_HEIGHT(s->dataRC) / VWPT_RECT_GET_HEIGHT(vdrc);

    return _MAX(sw, sh);
}


/*===========================================================================*/
/*                                                                           */
/*                     View to Data Functions                                */
/*                                                                           */
/*===========================================================================*/
#define VWPT_VP2DP(s, vx, vy, dx, dy, type) \
    (dx) = (type) (s->dataCP.X + ((vx) - s->viewCP.X) / s->scaleFactor); \
    (dy) = (type) (s->dataCP.Y + (s->viewCP.Y -(vy)*s->ratioXY) / s->scaleFactor)


#define VWPT_VP2DP_ARRAY(s, vps, dps, type, num_pts) do { \
        int np = (num_pts); \
        while (np-- > 0) { \
            VWPT_VP2DP(s, vps[np].X, vps[np].Y, dps[np].X, dps[np].Y, type); \
        } \
    } while (0)


__INLINE void vwpt_state_vxy2dxy(const viewport_state_t *s, double vx, double vy, double *dx, double *dy)
{
    VWPT_VP2DP(s, vx, vy, *dx, *dy, double);
}


__INLINE void vwpt_state_vp2dp(const viewport_state_t *s, const vwpt_point_d* vp, vwpt_point_d* dp)
{
    dp->X = s->dataCP.X + (vp->X - s->viewCP.X) / s->scaleFactor;
    dp->Y = s->dataCP.Y + (s->viewCP.Y - vp->Y*s->ratioXY) / s->scaleFactor;
}


__INLINE void vwpt_state_vrc2drc(const viewport_state_t *s, const vwpt_rect_i* vrc, vwpt_rect_d* drc)
{
    VWPT_VP2DP(s, vrc->Xmin, vrc->Ymax, drc->Xmin, drc->Ymin, double);
    VWPT_VP2DP(s, vrc->Xmax, vrc->Ymin, drc->Xmax, drc->Ymax, double);

    _ASSERT(drc->Xmax >= drc->Xmin && drc->Ymax >= drc->Ymin);
}


/**
 * view length(vl) to data length(dl)
 */
#define VWPT_VL2DL(s, vlen) ((vlen)/(s)->scaleFactor)


/**
 * data length(dl) to view length(vl)
 */
#define VWPT_DL2VL(s, dlen) ((dlen)*(s)->scaleFactor)


/*===========================================================================*/
/*                                                                           */
/*                     Data to View Functions                                */
/*                                                                           */
/*===========================================================================*/
__INLINE void vwpt_state_dxy2vxy(const viewport_state_t *s, double dx, double dy, int *vx, int *vy)
{
    *vx = (int) (s->viewCP.X + s->scaleFactor*(dx - s->dataCP.X) + 0.5);
    *vy = (int) ((s->viewCP.Y - s->scaleFactor*(dy - s->dataCP.Y))/s->ratioXY + 0.5);
}


__INLINE void vwpt_state_dxy2vxy_dbl(const viewport_state_t *s, double dx, double dy, double *vx, double *vy)
{
    *vx = s->viewCP.X + s->scaleFactor*(dx - s->dataCP.X);
    *vy = (s->viewCP.Y - s->scaleFactor*(dy - s->dataCP.Y))/s->ratioXY;
}


__INLINE void vwpt_state_dp2vp(const viewport_state_t *s, const vwpt_point_d* dp, vwpt_point_i* vp)
{
    vp->X = (int) (s->viewCP.X+s->scaleFactor*(dp->X - s->dataCP.X) + 0.5);
    vp->Y = (int) ((s->viewCP.Y-s->scaleFactor*(dp->Y - s->dataCP.Y))/s->ratioXY + 0.5);
}


__INLINE void vwpt_state_dp2vp_flt(const viewport_state_t *s, const vwpt_point_d* dp, vwpt_point_f* vp)
{
    vp->X = (float) (s->viewCP.X + s->scaleFactor * (dp->X - s->dataCP.X));
    vp->Y = (float) ((s->viewCP.Y - s->scaleFactor * (dp->Y - s->dataCP.Y))/s->ratioXY);
}


__INLINE void vwpt_state_dp2vp_dbl(const viewport_state_t *s, const vwpt_point_d* dp, vwpt_point_d* vp)
{
    vp->X = s->viewCP.X + s->scaleFactor * (dp->X - s->dataCP.X);
    vp->Y = (s->viewCP.Y - s->scaleFactor * (dp->Y - s->dataCP.Y))/s->ratioXY;
}


__INLINE void vwpt_state_dp2vp_array_dbl(const viewport_state_t *s, const vwpt_point_d *dps, vwpt_point_d *vps, int np)
{
    const vwpt_point_d *dp = dps;
    vwpt_point_d *vp = vps;

    while (np-- > 0) {
        vwpt_state_dp2vp_dbl(s, dp++, vp++);
    }
}


__INLINE void vwpt_state_dp2vp_array(const viewport_state_t *s, const vwpt_point_d *dps, vwpt_point_i *vps, int np)
{
    const vwpt_point_d *dp = dps;
    vwpt_point_i *vp = vps;

    while (np-- > 0) {
        vwpt_state_dp2vp(s, dp++, vp++);
    }
}


__INLINE void vwpt_state_dp2vp_array_flt(const viewport_state_t *s, const vwpt_point_d *dps, vwpt_point_f *vps, int np)
{
    const vwpt_point_d *dp = dps;
    vwpt_point_f *vp = vps;

    while (np-- > 0) {
        vwpt_state_dp2vp_flt(s, dp++, vp++);
    }
}


/**
 * ^         max         o------------->
 * |  --------+          |  min 
 * |  |       |          |   +--------|
 * |  |  Drc  |    ==>   |   |   Vrc  |
 * |  +--------          |   |        |
 * | min                 |   ---------+
 *-o------------->       V           max
 */
__INLINE void vwpt_state_drc2vrc(const viewport_state_t *s, const vwpt_rect_d *drc, vwpt_rect_i *vrc)
{
    vwpt_state_dxy2vxy(s, drc->Xmin, drc->Ymax, &vrc->Xmin, &vrc->Ymin);
    vwpt_state_dxy2vxy(s, drc->Xmax, drc->Ymin, &vrc->Xmax, &vrc->Ymax);
}


__INLINE void vwpt_state_drc2vrc_dbl(const viewport_state_t *s, const vwpt_rect_d *drc, vwpt_rect_d *vrc)
{
    vwpt_state_dxy2vxy_dbl(s, drc->Xmin, drc->Ymax, &vrc->Xmin, &vrc->Ymin);
    vwpt_state_dxy2vxy_dbl(s, drc->Xmax, drc->Ymin, &vrc->Xmax, &vrc->Ymax);
}


/*===========================================================================*/
/*                                                                           */
/*                     View  Manipulation                                    */
/*                                                                           */
/*===========================================================================*/

/**
 * move viewport's view center to position
 */
static void vwpt_state_center_view (viewport_state_t *s, double vx, double vy)
{
    vwpt_state_vxy2dxy(s, vx, vy, &s->dataCP.X, &s->dataCP.Y);
}


/**
 * Zoom in or out view. times is scaleFactor, must be > 0
 */
static void vwpt_state_zoom_times (viewport_state_t *s, double times)
{
    vwpt_state_set_scale(s, s->scaleFactor*times);
}


/**
 * Just like the above. the difference is the below reset view data's center
 */
static void vwpt_state_zoom_center (viewport_state_t *s, float times)
{
    vwpt_state_set_scale(s, s->scaleFactor*times);
    VWPT_RECT_GET_CENTER(s->dataRC, s->dataCP);
}


/**
 * display all destination with times of given parameter
 */
static void vwpt_state_zoom_all (viewport_state_t *s, float times)
{
    VWPT_RECT_GET_CENTER(s->dataRC, s->dataCP);
    vwpt_state_set_scale(s, times*vwpt_state_calc_scale(s, FALSE));
}


/**
 * move view, only the center of view data center changed
 * VpOffsetX and VpOffsetY are offsets of Vp
 */
static void vwpt_state_pan_view(viewport_state_t *s, double offs_vx, double offs_vy)
{
    s->dataCP.X -= (offs_vx / s->scaleFactor);
    s->dataCP.Y += (offs_vy / s->scaleFactor);
}


/**
 * Zoom at given viewport position Vp (X, Y)
 */
static void vwpt_state_zoom_view_at(viewport_state_t *s, int vx, int vy, float times /* 1.0 */)
{
    double dx, dy, vx1, vy1, vx2, vy2;
    vwpt_state_vxy2dxy(s, vx, vy, &dx, &dy);
    vwpt_state_dxy2vxy_dbl(s, dx, dy, &vx1, &vy1);
    vwpt_state_zoom_times(s, times);
    vwpt_state_dxy2vxy_dbl(s, dx, dy, &vx2, &vy2);
    vwpt_state_pan_view(s, vx1-vx2, vy1-vy2);
}


#define vwpt_state_pan_view_xy(s, vx1, vy1, vx2, vy2)  \
    vwpt_state_pan_view(s, vx2-vx1, vy2-vy1)


/**
 * flip view by direction and overlapped percent
 *       ^Y
 *       |  1  2  3
 *       +  4 (5) 6
 *       |  7  8  9
 *       o-----+---->X
 */
static void vwpt_state_flip_view(viewport_state_t *s, int direction, float overlap)
{
    double  dx = VWPT_RECT_GET_WIDTH(s->viewRC) / s->scaleFactor;
    double  dy = VWPT_RECT_GET_HEIGHT(s->viewRC) / s->scaleFactor;
    float   fv = (float) (1.0 - overlap / 100);

    switch (direction) {
    case 2:
        s->dataCP.Y += dy*fv;
        break;
    case 8:
        s->dataCP.Y -= dy*fv;
        break;
    case 4:
        s->dataCP.X -= dx*fv;
        break;
    case 6:
        s->dataCP.X += dx*fv;
        break;
    case 1:
        s->dataCP.X -= dx*fv;
        s->dataCP.Y += dy*fv;
        break;
    case 3:
        s->dataCP.X += dx*fv;
        s->dataCP.Y += dy*fv;
        break;
    case 7:
        s->dataCP.X -= dx*fv;
        s->dataCP.Y -= dy*fv;
        break;
    case 9:
        s->dataCP.X += dx*fv;
        s->dataCP.Y -= dy*fv;
        break;
    case 5:
        vwpt_state_zoom_times(s, overlap / 100);
        break;
    }
}


/**
 * Zoom in(from left to right) or zoom out(from right to left), in viewport
 */
static void vwpt_state_zoom_view_xy(viewport_state_t *s, double vx1, double vy1, double vx2, double vy2, double percentage)
{  
    double n = _ABS(percentage);

    int w = (int) ((vx1 - vx2) * n + 0.5);  /* w as sign flag */
    int h = (int) ((vy1 - vy2) * n + 0.5);  /* h > 0 */

    h = _ABS(h);

    if (w != 0 && h > 0) {
        vwpt_state_center_view(s, (vx1 + vx2)/2, (vy1 + vy2)/2);

        if (w > 0) {
             /* zoom out */
            vwpt_state_set_scale(s, s->scaleFactor *
                _MIN(w/VWPT_RECT_GET_WIDTH(s->viewRC)/n, h/VWPT_RECT_GET_HEIGHT(s->viewRC)/n));
        } else {
            /* zoom in */
            vwpt_state_set_scale(s, s->scaleFactor *
                _MIN(-VWPT_RECT_GET_WIDTH(s->viewRC)*n/w, VWPT_RECT_GET_HEIGHT(s->viewRC)*n/h));
        }
    }
}


static void vwpt_state_zoom_view(viewport_state_t *s, vwpt_point_d *vp1, vwpt_point_d *vp2, float percentage)
{
    vwpt_state_zoom_view_xy(s, vp1->X, vp1->Y, vp2->X, vp2->Y, percentage);
}


static void vwpt_state_zoom_data_xy(viewport_state_t *s, double dx1, double dy1, double dx2, double dy2, float percentage)
{
    double vx1, vy1, vx2, vy2;
    vwpt_state_dxy2vxy_dbl(s, dx1, dy1, &vx1, &vy1);
    vwpt_state_dxy2vxy_dbl(s, dx2, dy2, &vx2, &vy2);
    vwpt_state_zoom_view_xy(s, vx1, vy1, vx2, vy2, percentage);
}


static void vwpt_state_zoom_data(viewport_state_t *s, vwpt_point_d *p1, vwpt_point_d *p2, float percentage)
{
    vwpt_state_zoom_data_xy(s, p1->X, p1->Y, p2->X, p2->Y, percentage);
}

#ifdef __cplusplus
}
#endif

#endif  /* _VIEWPORT_H_INCLUDED */
