// GpsPosition.h : CGpsPosition ������

#pragma once

#include "Config.h"


// CGpsPosition

class ATL_NO_VTABLE CGpsPosition :
	public CComObjectRootEx<CComSingleThreadModel>,
    public CComCoClass<CGpsPosition>,
	public ISupportErrorInfo,
	public IDispatchImpl<IGpsPosition, &IID_IGpsPosition, &LIBID_CRMapCtlLib, 1, 0>
{
public:
	CGpsPosition()
	{
	}

DECLARE_NO_REGISTRY()


BEGIN_COM_MAP(CGpsPosition)
	COM_INTERFACE_ENTRY(IGpsPosition)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid)
    {
	    static const IID* arr[] = 
	    {
		    &IID_IGpsPosition
	    };

	    for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	    {
		    if (InlineIsEqualGUID(*arr[i],riid))
			    return S_OK;
	    }
	    return S_FALSE;
    }


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

};

//DEL:OBJECT_ENTRY_AUTO(__uuidof(GpsPosition), CGpsPosition)
