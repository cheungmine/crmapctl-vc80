// Bands.h : CBands 的声明

#pragma once

#include "Config.h"

#include "Band.h"

typedef list< CAdapt< CComPtr<IBand> > > BandCollType;

typedef CComEnumOnSTL<IEnumVARIANT, &IID_IEnumVARIANT, VARIANT, 
    _CopyVariantFromAdaptItf<IBand>,
    BandCollType >
CComEnumVariantOnListOfBands;

typedef ICollOnSTLListWithMapImpl<IDispatchImpl<IBands, &IID_IBands>,
    BandCollType,
    IBand*,
    _CopyItfFromAdaptItf<IBand>,
    CComEnumVariantOnListOfBands,
    std::wstring,
    CBand,
    IBand >
IBandCollImpl;


typedef  COLL_ITER(vector, IBand)  BAND_ITER;
typedef  COLL_RITER(vector, IBand) BAND_RITER;

// 指定索引的指针
#define  BAND_ITER_AT(it,index)  BAND_ITER (##it) = m_coll.begin()+index


// CBands

class ATL_NO_VTABLE CBands :
	public CComObjectRootEx<CComSingleThreadModel>,
    public CComCoClass<CBands>,
	public ISupportErrorInfo,
    public IBandCollImpl
{
public:
    CBands()
	{
	}

DECLARE_NO_REGISTRY()


BEGIN_COM_MAP(CBands)
	COM_INTERFACE_ENTRY(IBands)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid)
    {
	    static const IID* arr[] = 
	    {
		    &IID_IBands
	    };

	    for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	    {
		    if (InlineIsEqualGUID(*arr[i],riid))
			    return S_OK;
	    }
	    return S_FALSE;
    }


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
        Clear();
	}

public:

};

//DEL:OBJECT_ENTRY_AUTO(__uuidof(Bands), CBands)
