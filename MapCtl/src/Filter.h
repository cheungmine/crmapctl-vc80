// Filter.h : CFilter ������

#pragma once

#include "Config.h"


// CFilter

class ATL_NO_VTABLE CFilter :
	public CComObjectRootEx<CComSingleThreadModel>,
    public CComCoClass<CFilter>,
	public ISupportErrorInfo,
	public IDispatchImpl<IFilter, &IID_IFilter, &LIBID_CRMapCtlLib, 1, 0>
{
public:
	CFilter()
	{
	}

DECLARE_NO_REGISTRY()


BEGIN_COM_MAP(CFilter)
	COM_INTERFACE_ENTRY(IFilter)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid)
    {
	    static const IID* arr[] = 
	    {
		    &IID_IFilter
	    };

	    for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	    {
		    if (InlineIsEqualGUID(*arr[i],riid))
			    return S_OK;
	    }
	    return S_FALSE;
    }


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

};

//DEL:OBJECT_ENTRY_AUTO(__uuidof(Filter), CFilter)
