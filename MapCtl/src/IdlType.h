// IdlType.h
// included by all .idl files in the same folder
#ifndef _IDL_TYPE_H__
#define _IDL_TYPE_H__

#ifndef ERR_INDEX
    #define ERR_INDEX  -1
#endif

#ifndef DISPID_WINDOWONLY
    #define	DISPID_WINDOWONLY   101
#endif

#ifndef DISPID_RENAMEITEM
    #define	DISPID_RENAMEITEM	102
#endif

#ifndef VARIANT_TRUE
    #define VARIANT_TRUE ((VARIANT_BOOL)0xffff)
    #define VARIANT_FALSE ((VARIANT_BOOL)0)
#endif

#define EVTID_ONMAPREADY  1
#define EVTID_ONMAPFINAL  2

#endif // _IDL_TYPE_H__
