// Render.h : CRender ������

#pragma once

#include "Config.h"
#include "ItemType.h"

class CRenders;

// CRender

class ATL_NO_VTABLE CRender :
	public CComObjectRootEx<CComSingleThreadModel>,
    public CComCoClass<CRender>,    
	public ISupportErrorInfo,
	public IItemTypeDispatchImpl<IRenders, IRender, &IID_IRender, &LIBID_CRMapCtlLib, 1, 0>
{
public:
	CRender()
	{
	}

DECLARE_NO_REGISTRY()


BEGIN_COM_MAP(CRender)
	COM_INTERFACE_ENTRY(IRender)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

};

//DEL:OBJECT_ENTRY_AUTO(__uuidof(Render), CRender)
