// Rasters.cpp
//
#include "Rasters.h"


void CRasters::Draw (CDrawInfo *di)
{
    if ( m_bVisible && m_coll.size() ) {
        for (RASTER_ITER iter = m_coll.begin(); iter != m_coll.end(); ++iter) {
            ((CRaster*)(iter->m_T.p))->Draw(di);
        }
    }
}

// CRasters

STDMETHODIMP CRasters::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IRasters
	};

	for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}
