// Coordref.h : CCoordref ������

#pragma once

#include "Config.h"


// CCoordref

class ATL_NO_VTABLE CCoordref :
	public CComObjectRootEx<CComSingleThreadModel>,
    public CComCoClass<CCoordref>,
	public ISupportErrorInfo,
	public IDispatchImpl<ICoordref, &IID_ICoordref, &LIBID_CRMapCtlLib, 1, 0>
{
public:
	CCoordref()
	{
	}

DECLARE_NO_REGISTRY()


BEGIN_COM_MAP(CCoordref)
	COM_INTERFACE_ENTRY(ICoordref)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid)
    {
	    static const IID* arr[] = 
	    {
		    &IID_ICoordref
	    };

	    for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	    {
		    if (InlineIsEqualGUID(*arr[i],riid))
			    return S_OK;
	    }
	    return S_FALSE;
    }


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:
};

//DEL:OBJECT_ENTRY_AUTO(__uuidof(Coordref), CCoordref)
