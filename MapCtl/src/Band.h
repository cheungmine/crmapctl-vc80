// Band.h : CBand ������

#pragma once

#include "Config.h"
#include "ItemType.h"

class CBands;


// CBand

class ATL_NO_VTABLE CBand :
	public CComObjectRootEx<CComSingleThreadModel>,
    public CComCoClass<CBand>,
	public ISupportErrorInfo,
	public IItemTypeDispatchImpl<IBands, IBand, &IID_IBand, &LIBID_CRMapCtlLib, 1, 0>
{
public:
	CBand()
	{
	}

DECLARE_NO_REGISTRY()


BEGIN_COM_MAP(CBand)
	COM_INTERFACE_ENTRY(IBand)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid)
    {
        static const IID* arr[] = 
	    {
		    &IID_IBand
	    };

	    for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	    {
		    if (InlineIsEqualGUID(*arr[i],riid))
			    return S_OK;
	    }
	    return S_FALSE;
    }


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

};

//DEL:OBJECT_ENTRY_AUTO(__uuidof(Band), CBand)
