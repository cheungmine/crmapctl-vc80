// Layout.h : CLayout ������

#pragma once

#include "Config.h"


// CLayout

class ATL_NO_VTABLE CLayout :
	public CComObjectRootEx<CComSingleThreadModel>,
    public CComCoClass<CLayout>,
	public ISupportErrorInfo,
	public IDispatchImpl<ILayout, &IID_ILayout, &LIBID_CRMapCtlLib, 1, 0>
{
public:
	CLayout()
	{
	}

DECLARE_NO_REGISTRY()


BEGIN_COM_MAP(CLayout)
	COM_INTERFACE_ENTRY(ILayout)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid)
    {
	    static const IID* arr[] = 
	    {
		    &IID_ILayout
	    };

	    for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	    {
		    if (InlineIsEqualGUID(*arr[i],riid))
			    return S_OK;
	    }
	    return S_FALSE;
    }


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

};

//DEL:OBJECT_ENTRY_AUTO(__uuidof(Layout), CLayout)
