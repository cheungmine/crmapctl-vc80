/* uuid32.c 
   2007-09-15 Last created by cheungmine.
   Partly rights reserved by cheungmine.
*/

#include <stdio.h>
#include <string.h>

#include <time.h>

#ifndef WINCE
  #include <sys/types.h>
  #include <sys/timeb.h>
#endif

#include "uuid32.h"
#include "md5.h"

/* microsecond per second. 1s=1000000us=1000000000ns*/
#define NSec100_Per_Sec		10000000
#define USec_Per_Sec		1000000
#define USec_Per_MSec		1000
#define NSec_Since_1582		((uint64)(0x01B21DD213814000))

#pragma warning (disable: 4996)	// avoids 'This function or variable may be unsafe' message

/*========================================================================================
							Private Functions
========================================================================================*/
static char *ASCTIME_S(struct tm *t, char *_tmbuf, size_t bufSize)
{
	*_tmbuf = 0;

#ifndef _M8SDK_ARMV4I
	strncpy(_tmbuf, asctime(t), bufSize-1);
#else
	// with line number
	#define STRING2(x) #x
	#define STRING(x) STRING2(x)

	#pragma message(__FILE__ " [line " STRING(__LINE__) "]: asctime() not impl for _M8SDK_ARMV4I")
#endif
	return _tmbuf;
}

static void GETTIME(time_t *t)
{
	*t = 0;

#ifndef _M8SDK_ARMV4I
	time(t);
#else
	// with line number
	#define STRING2(x) #x
	#define STRING(x) STRING2(x)

	#pragma message(__FILE__ " [line " STRING(__LINE__) "]: time() not impl for _M8SDK_ARMV4I")
#endif
}

static BOOL  _is_bigendian = (('4321'>>24)=='1');

static void swap_word( int size_bytes, void * ptr_word )
{
    int        i;
    unsigned char       temp;
    for( i=0; i < size_bytes/2; i++ )
    {
        temp = ((unsigned char *) ptr_word)[i];
        ((unsigned char *) ptr_word)[i] = ((unsigned char *) ptr_word)[size_bytes-i-1];
        ((unsigned char *) ptr_word)[size_bytes-i-1] = temp;
    }
};

static void write_word( unsigned char* stream, word_t val )
{
	memcpy(stream, &val, 2);
    if( _is_bigendian ) swap_word( 2, stream );
};

static void write_dword( unsigned char* stream, dword_t val )
{
	memcpy(stream, &val, 4);
    if( _is_bigendian ) swap_word( 4, stream );
};

static void  read_word( const unsigned char* stream, word_t* val )
{
	memcpy( val, stream, 2 );
	if( _is_bigendian )	swap_word( 2, val );
};

static void  read_dword( const unsigned char* stream, dword_t* val )
{
	memcpy( val, stream, 4 );
	if( _is_bigendian )	swap_word( 4, val );
};

static BOOL is_xdigit(char c)
{
	/* isxdigit returns a non-zero value if c is a hexadecimal digit (A �C F, a �C f, or 0 �C 9). */
	return ((c>='A'&&c<='F')||(c>='a'&&c<='f')||(c>='0'&&c<='9'))? TRUE : FALSE;
};


/* make a pseudorandom numbel based on current time*/
static int pseudo_rand()
{
#ifdef _USE_32BIT_TIME_T
	assert(0);
#endif

#pragma warning(push)	/* C4996 */
#pragma warning( disable : 4996 )

#ifndef WINCE
	struct _timeb  timebuf;
	_ftime64(&timebuf);
	srand((uint32) ((((uint32)timebuf.time&0xFFFF)+(uint32)timebuf.millitm)^(uint32)timebuf.millitm));
#else
	time_t   ltime;
	GETTIME(&ltime);
	srand((uint32) ltime);
#endif
  
#pragma warning(pop)	/* C4996 */

	return rand();
};


/*========================================================================================
							Public Functions
========================================================================================*/

BOOL is_uuid_string(const char *uuid) 
{	
    static const char uuid_fmt[] = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx";
    size_t i;
	size_t si = sizeof(uuid_fmt);
	assert(uuid != NULL);
    for (i = 0; i < si; i++)
		if (uuid_fmt[i] == 'x')
			if (!is_xdigit(uuid[i]))
				return FALSE;
		else if (uuid[i] != uuid_fmt[i])
			return FALSE;

	return TRUE;
}


/**
 * \internal
 * \ingroup uuid
 * The thread synchronization lock used to guarantee UUID uniqueness
 * for all the threads running within a process.
 */
void uuid_create(uuidtype_t* u) 
{	
	static BOOL		initialized = FALSE;
	static int64	timestamp;
	static uint32	advance;
	static uint16	clockseq;
	static uint16	node_high;
	static uint32	node_low;
	int64			nstime;    /* unit of 100ns */
	uint16			nowseq;
	int				r;

	#ifdef _USE_32BIT_TIME_T
		assert(0);
	#endif

#pragma warning(push)	/* C4996 */
#pragma warning( disable : 4996 )

#ifndef _WIN32_WCE
	struct _timeb  tv;
	assert(u);
	_ftime64(&tv);
	
	/* time is counter of 100ns time interval since Oct.15, 1582 (NOT 1852) */
	nstime = ((uint64) tv.time) * USec_Per_Sec + ((uint64) tv.millitm*USec_Per_MSec);
	nstime = nstime * 10 + NSec_Since_1582;

#else
	time_t  ltime;
	GETTIME(&ltime);

	/* time is counter of 100ns time interval since Oct.15, 1582 (NOT 1852) */
	nstime = ((uint64) ltime) * USec_Per_Sec;
	nstime = nstime * 10 + NSec_Since_1582;
#endif

#pragma warning(pop)	/* C4996 */

	if (!initialized) 
	{
		timestamp = nstime;
		advance = 0;

		r = pseudo_rand();

		clockseq = r >> 16;
		node_high = r | 0x0100;
		
		node_low = pseudo_rand();
		
		initialized = TRUE;
    } 
	else if (nstime < timestamp) 
	{
		timestamp = nstime;
		advance = 0;
		clockseq++;
    } 
	else if (nstime == timestamp) 
	{
		advance++;
		nstime += advance;
	} 
	else 
	{
		timestamp = nstime;
		advance = 0;
    }
    nowseq = clockseq;

	assert(u);
	u->data1 = (dword_t) nstime;
	u->data2 = (word_t) ((nstime >> 32) & 0xffff);
	u->data3 = (word_t) (((nstime >> 48) & 0x0ffff) | 0x1000);
	write_word(&(u->data4[6]), (word_t) ((nowseq & 0x3fff) | 0x8000));	
	write_word(&(u->data4[4]), (word_t) (node_high));					
	write_dword(&(u->data4[0]), (dword_t) (node_low));			
}

/**
 * \internal
 * \ingroup uuid
 * The thread synchronization lock used to guarantee UUID uniqueness
 * for all the threads running within a process.
 */
char *uuid_create_string(char uuid_str_37[]) 
{
	uuidtype_t  u;
	uuid_create(&u);
	return uuid_to_string(&u, uuid_str_37);
}

char *uuid_to_string(const uuidtype_t* u, char uuid_str_37[] )
{
	ushort a,b;
	uint32  c;
	read_word(&(u->data4[6]), &a);
	read_word(&(u->data4[4]), &b);
	read_dword(&(u->data4[0]), &c);

#pragma warning(push)	/* C4996 */
#pragma warning( disable : 4996 )
	sprintf(uuid_str_37, "%08lx-%04x-%04x-%04x-%04x%08lx", 
				u->data1,
				u->data2,
				u->data3,
				a, b, c);
#pragma warning(pop)	/* C4996 */

	uuid_str_37[UUIDTYPE_LEN] = 0;
	return uuid_str_37;
}

/**
 * \internal
 * \ingroup uuid
 * The predefined namespace UUID. Expressed in binary format
 * to avoid unnecessary conversion when generating name based UUIDs.
 */
static unsigned char namespace_uuid[] = {
        0x9c, 0xfb, 0xd9, 0x1f, 0x11, 0x72, 0x4a, 0xf6,
        0xbd, 0xcb, 0x9f, 0x34, 0xe4, 0x6f, 0xa0, 0xfb
};

void  uuid_create_external(const char *external, uuidtype_t* u) 
{
	MD5_CTX md5;
	assert(external != NULL);

	MD5Init(&md5, 0);
	MD5Update(&md5, namespace_uuid, sizeof(namespace_uuid));
	MD5Update(&md5, (unsigned char *) external, (unsigned int) strlen(external));
	MD5Final(&md5);

	u->data1 = (dword_t) (md5.digest[0] << 24 | md5.digest[1] << 16 | md5.digest[2] << 8 | md5.digest[3]);
	u->data2 = (word_t)  (md5.digest[4] << 8 | md5.digest[5]);
	u->data3 = (word_t)  (((md5.digest[6] & 0x0f) | 0x30) << 8 | md5.digest[7]);	
	
	/* BYTE 6-7 */
	write_word(&(u->data4[6]), (word_t) (((md5.digest[8] & 0x3f) | 0x80) << 8 | md5.digest[9]));		
	/* BYTE 4-5 */
	write_word(&(u->data4[4]), (word_t) (md5.digest[10] << 8 | md5.digest[11]));						
	/* BYTE 0-3 */
	write_dword(&(u->data4[0]), (dword_t) (md5.digest[12] << 24 | md5.digest[13] << 16 | md5.digest[14] << 8 | md5.digest[15]));
}

/**
 * Get timestamp from a UUID
 **/
int uuid_to_timestamp(const uuidtype_t* u, timestamp_t* t)
{
#ifndef WINCE
	errno_t err;

	int64   time, t2, t3;
	struct  tm  p;
	
	assert(u);

	t2 = u->data2;
	t3 = u->data3;

	time = u->data1 + (t2<<32) + ((t3&0x0fff)<<48);		/* 100ns */
	time -= NSec_Since_1582;

	t->tm_fraction = (long)(time%NSec100_Per_Sec);
	
	time /= 10;
	time /= USec_Per_Sec; 
	
	err = _localtime64_s(&p, &time);
	if (0==err){
		t->tm_hour = p.tm_hour;
		t->tm_mday = p.tm_mday;
		t->tm_min = p.tm_min;
		t->tm_mon = p.tm_mon;
		t->tm_sec = p.tm_sec;
		t->tm_wday = p.tm_wday;
		t->tm_yday = p.tm_yday;
		t->tm_year = p.tm_year;
	}
	return (int)err;
#else
	// not impl
	return -1;
#endif
}

int timestamp_to_string(const timestamp_t* time, char* timeBuf, size_t nBufSize)
{
	struct tm t;
	t.tm_hour = time->tm_hour;
	t.tm_mday = time->tm_mday;
	t.tm_min = time->tm_min;
	t.tm_mon = time->tm_mon;
	t.tm_sec = time->tm_sec;
	t.tm_wday = time->tm_wday;
	t.tm_yday = time->tm_yday;
	t.tm_year = time->tm_year;

#ifndef WINCE
	if (0==asctime_s(timeBuf, nBufSize, &t))
		return (int)strnlen_s(timeBuf, nBufSize)+1;
	else
		return 0;
#else
	ASCTIME_S(&t, timeBuf, nBufSize);
	return (int)strlen(timeBuf)+1;
#endif
}

/**
 * Compare two UUID's lexically
 *	return
 *	  -1   u1 is lexically before u2
 *     0   u1 is equal to u2
 *     1   u1 is lexically after u2
*/
int uuid_compare(const uuidtype_t *u1, const uuidtype_t *u2)
{
	int i;

#define CHECK_COMP(f1, f2)  if ((f1) != (f2)) return ((f1) < (f2) ? -1 : 1);
	
	CHECK_COMP(u1->data1, u2->data1);
	CHECK_COMP(u1->data2, u2->data2);
	CHECK_COMP(u1->data3, u2->data3);

	for(i=0; i<8; i++)
		CHECK_COMP(u1->data4[i], u1->data4[i]);

#undef CHECK_COMP

    return 0;
}

/**
 * Compare two UUID's temporally
 *	return
 *	  -1   u1 is temporally before u2
 *     0   u1 is equal to u2
 *     1   u1 is temporally after u2
*/
int uuid_compare_time(const uuidtype_t *u1, const uuidtype_t *u2)
{	
#define CHECK_COMP(f1, f2)  if ((f1) != (f2)) return ((f1) < (f2) ? -1 : 1);
	
	CHECK_COMP(u1->data1, u2->data1);
	CHECK_COMP(u1->data2, u2->data2);
	CHECK_COMP(u1->data3, u2->data3);

#undef CHECK_COMP

	return 0;
}
