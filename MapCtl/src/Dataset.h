// Dataset.h : CDataset 的声明

#pragma once

#include "Config.h"


typedef list< CAdapt< CComPtr<IColdef> > > ColdefCollType;

typedef CComEnumOnSTL<IEnumVARIANT, &IID_IEnumVARIANT, VARIANT, 
    _CopyVariantFromAdaptItf<IColdef>,
    ColdefCollType >
CComEnumVariantOnListOfColdefs;

typedef ICollOnSTLListWithMapImpl<IDispatchImpl<IDataset, &IID_IDataset>,
    ColdefCollType,
    IColdef*,
    _CopyItfFromAdaptItf<IColdef>,
    CComEnumVariantOnListOfColdefs,
    std::wstring,
    CColdef,
    IColdef >
IColdefCollImpl;


typedef  COLL_ITER(vector, IColdef)  COLDEF_ITER;
typedef  COLL_RITER(vector, IColdef) COLDEF_RITER;

// 指定索引的指针
#define  COLDEF_ITER_AT(it,index)  COLDEF_ITER (##it) = m_coll.begin()+index


// CDataset

class ATL_NO_VTABLE CDataset :
	public CComObjectRootEx<CComSingleThreadModel>,
    public CComCoClass<CDataset>,
	public ISupportErrorInfo,
    public IColdefCollImpl
{
public:
	CDataset()
	{
	}

DECLARE_NO_REGISTRY()


BEGIN_COM_MAP(CDataset)
	COM_INTERFACE_ENTRY(IDataset)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid)
    {
	    static const IID* arr[] = 
	    {
		    &IID_IDataset
	    };

	    for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	    {
		    if (InlineIsEqualGUID(*arr[i],riid))
			    return S_OK;
	    }
	    return S_FALSE;
    }



	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

};

//DEL:OBJECT_ENTRY_AUTO(__uuidof(Dataset), CDataset)
