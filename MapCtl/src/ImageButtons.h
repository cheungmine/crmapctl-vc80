// ImageButtons.h : CImageButtons 的声明

#pragma once

#include "Config.h"

#include "ImageButton.h"


typedef list< CAdapt< CComPtr<IImageButton> > > ImageButtonCollType;

typedef CComEnumOnSTL<IEnumVARIANT, &IID_IEnumVARIANT, VARIANT, 
    _CopyVariantFromAdaptItf<IImageButton>,
    ImageButtonCollType >
CComEnumVariantOnListOfImageButtons;

typedef ICollOnSTLListWithMapImpl<IDispatchImpl<IImageButtons, &IID_IImageButtons>,
    ImageButtonCollType,
    IImageButton*,
    _CopyItfFromAdaptItf<IImageButton>,
    CComEnumVariantOnListOfImageButtons,
    std::wstring,
    CImageButton,
    IImageButton >
IImageButtonCollImpl;


typedef  COLL_ITER(vector, IImageButton)  IMGBTN_ITER;
typedef  COLL_RITER(vector, IImageButton) IMGBTN_RITER;

// 指定索引的指针
#define  IMGBTN_ITER_AT(it,index)  IMGBTN_ITER (##it) = m_coll.begin()+index


// CImageButtons

class ATL_NO_VTABLE CImageButtons :
	public CComObjectRootEx<CComSingleThreadModel>,
    public CComCoClass<CImageButtons>,
	public ISupportErrorInfo,
    public IImageButtonCollImpl
{
public:
	CImageButtons()
	{
	}

DECLARE_NO_REGISTRY()


BEGIN_COM_MAP(CImageButtons)
	COM_INTERFACE_ENTRY(IImageButtons)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid)
    {
	    static const IID* arr[] = 
	    {
		    &IID_IImageButtons
	    };

	    for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	    {
		    if (InlineIsEqualGUID(*arr[i],riid))
			    return S_OK;
	    }
	    return S_FALSE;
    }


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

};

//DEL:OBJECT_ENTRY_AUTO(__uuidof(ImageButtons), CImageButtons)
