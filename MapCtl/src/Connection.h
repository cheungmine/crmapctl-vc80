// Connection.h : CConnection ������

#pragma once

#include "Config.h"
#include "ItemType.h"

class CConnections;

// CConnection

class ATL_NO_VTABLE CConnection :
	public CComObjectRootEx<CComSingleThreadModel>,
    public CComCoClass<CConnection>,
	public ISupportErrorInfo,
	public IItemTypeDispatchImpl<IConnections, IConnection, &IID_IConnection, &LIBID_CRMapCtlLib, 1, 0>
{
public:
	CConnection()
	{
	}

DECLARE_NO_REGISTRY()


BEGIN_COM_MAP(CConnection)
	COM_INTERFACE_ENTRY(IConnection)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

};

//DEL:OBJECT_ENTRY_AUTO(__uuidof(Connection), CConnection)
