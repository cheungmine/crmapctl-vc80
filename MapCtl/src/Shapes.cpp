// Shapes.cpp
//
#include "Shapes.h"


void CShapes::Draw (CDrawInfo *di)
{
    if ( m_bVisible && m_coll.size() ) {
        for (SHAPE_ITER iter = m_coll.begin(); iter != m_coll.end(); ++iter) {
            ((CShape*)(iter->m_T.p))->Draw(di);
        }
    }
}

// CShapes

STDMETHODIMP CShapes::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IShapes
	};

	for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}
