// ImageButton.h : CImageButton ������

#pragma once

#include "Config.h"
#include "ItemType.h"

class CImageButtons;

// CImageButton

class ATL_NO_VTABLE CImageButton :
	public CComObjectRootEx<CComSingleThreadModel>,
    public CComCoClass<CImageButton>,
	public ISupportErrorInfo,
	public IItemTypeDispatchImpl<IImageButtons, IImageButton, &IID_IImageButton, &LIBID_CRMapCtlLib, 1, 0>
{
public:
	CImageButton()
	{
	}

DECLARE_NO_REGISTRY()


BEGIN_COM_MAP(CImageButton)
	COM_INTERFACE_ENTRY(IImageButton)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid)
    {
	    static const IID* arr[] = 
	    {
		    &IID_IImageButton
	    };

	    for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	    {
		    if (InlineIsEqualGUID(*arr[i],riid))
			    return S_OK;
	    }
	    return S_FALSE;
    }


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

};

//DEL:OBJECT_ENTRY_AUTO(__uuidof(ImageButton), CImageButton)
