// TrackingShape.h : CTrackingShape ������

#pragma once

#include "Config.h"


// CTrackingShape

class ATL_NO_VTABLE CTrackingShape :
	public CComObjectRootEx<CComSingleThreadModel>,
    public CComCoClass<CTrackingShape>,
	public ISupportErrorInfo,
	public IDispatchImpl<ITrackingShape, &IID_ITrackingShape, &LIBID_CRMapCtlLib, 1, 0>
{
public:
	CTrackingShape()
	{
	}

DECLARE_NO_REGISTRY()


BEGIN_COM_MAP(CTrackingShape)
	COM_INTERFACE_ENTRY(ITrackingShape)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid)
    {
        static const IID* arr[] = 
	    {
		    &IID_ITrackingShape
	    };

	    for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	    {
		    if (InlineIsEqualGUID(*arr[i],riid))
			    return S_OK;
	    }
	    return S_FALSE;
    }


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

};

//DEL:OBJECT_ENTRY_AUTO(__uuidof(TrackingShape), CTrackingShape)
