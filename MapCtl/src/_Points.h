// Points.h : _CPoints 的声明

#pragma once

#include "Config.h"

#include "_Point.h"

typedef CComEnumOnSTL<IEnumVARIANT, &IID_IEnumVARIANT, VARIANT, 
    _CopyVariantFromAdaptItf<IPoint>,
    vector< CAdapt< CComPtr<IPoint> > > >
CComEnumVariantOnListOfPoints;


typedef ICollectionOnSTLArray<IDispatchImpl<IPoints, &IID_IPoints>,
    vector< CAdapt< CComPtr<IPoint> > >,
    IPoint*,
    _CopyItfFromAdaptItf<IPoint>,
    CComEnumVariantOnListOfPoints>
IPointCollImpl;

typedef  COLL_ITER(vector, IPoint)  POINT_ITER;
typedef  COLL_RITER(vector, IPoint) POINT_RITER;

// 指定索引的指针
#define  POINT_ITER_AT(it,index)  POINT_ITER (##it) = m_coll.begin()+index


// _CPoints

class ATL_NO_VTABLE _CPoints :
	public CComObjectRootEx<CComSingleThreadModel>,
    public CComCoClass<_CPoints>,
	public ISupportErrorInfo,
    public IPointCollImpl
{
public:
	_CPoints()
	{
	}

DECLARE_NO_REGISTRY()


BEGIN_COM_MAP(_CPoints)
	COM_INTERFACE_ENTRY(IPoints)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid)
    {
	    static const IID* arr[] = 
	    {
		    &IID_IPoints
	    };

	    for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	    {
		    if (InlineIsEqualGUID(*arr[i],riid))
			    return S_OK;
	    }
	    return S_FALSE;
    }


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:
    STDMETHOD(Add)(LONG index, VARIANT pt, VARIANT_BOOL isRef, IPoint** ppRef)
    {
        HRESULT hres = E_FAIL;

        if ( VariantTypeIsNull(pt) ) {
            // Create an IPoint to hand back to the client
            hres = _CPoint::CreateInstance(ppRef);
            if ( SUCCEEDED(hres) ) {
                // Put the IPoint on the list at index
                CComPtr<IPoint> spNewOb(*ppRef);

                if (index == ERR_INDEX || index == m_coll.size()) {
                    m_coll.push_back(spNewOb);
                } else if (index >= 0 && index < (long)m_coll.size()) {
                    m_coll.insert(m_coll.begin()+index, spNewOb);
                } else {
                    // BAD INDEX
                    hres = E_INVALIDARG;
                }
            }
            return hres;
        } else if ( VariantTypeIsDispatch(pt) ) {
            CComQIPtr<IPoint> spRefPt(pt.pdispVal);
            if (spRefPt) {
                if (index == ERR_INDEX || index == m_coll.size()) {
                    if (isRef) {
                        m_coll.push_back(spRefPt);
                        hres = spRefPt.CopyTo(ppRef);
                    } else {
                        hres = _CPoint::CreateInstance(ppRef);
                        if ( SUCCEEDED(hres) ) {
                            CComPtr<IPoint> spNewOb(*ppRef);
                            m_coll.push_back(spNewOb);
                        }
                    }
                    return hres;
                } else if (index >= 0 && index < (long) m_coll.size()) {
                    if (isRef) {
                        m_coll.insert(m_coll.begin()+index, spRefPt);
                        hres = spRefPt.CopyTo(ppRef);
                    } else {
                        hres = _CPoint::CreateInstance(ppRef);
                        if ( SUCCEEDED(hres) ) {
                            CComPtr<IPoint> spNewOb(*ppRef);
                            m_coll.insert(m_coll.begin()+index, spNewOb);
                        }
                    }
                    return hres;
                } else {
                    // BAD INDEX
                    return E_INVALIDARG;
                }
            } else {
                CComQIPtr<IPoints> spPts(pt.pdispVal);
                if (spPts) {
                    // TODO
                    return E_NOTIMPL;
                } else {
                    return E_INVALIDARG;
                }
            }
        } else if ( VariantTypeIsBSTR(pt) ) {
            // TODO
            return E_NOTIMPL;
        } else {
            return E_INVALIDARG;
        }

        return hres;
    }


    STDMETHOD(Remove)(LONG index, LONG count)
    {
        if (index == ERR_INDEX) {
            if (count == 1) {
                // fast remove last element
                m_coll.pop_back();
                return S_OK;
            } else if (count < 0) {
                // remove from end up
                if ((long)m_coll.size() + count > 0) {
                    m_coll.erase(m_coll.begin()+m_coll.size()+count, m_coll.end());
                } else {
                    m_coll.clear();
                }
                return S_OK;
            } else {
                return E_INVALIDARG;
            }
        }

        // invalid index
        if (count == 0 || index < 0 || index >= (long) m_coll.size()) {
            return E_INVALIDARG;
        }

        if (count > 0) {
            if (index+count >= (long)m_coll.size()) {
                m_coll.erase(m_coll.begin()+index, m_coll.end());
            } else {
                m_coll.erase(m_coll.begin()+index, m_coll.begin()+index+count);
            }
            return S_OK;
        } else {
            if (index+count <= 0) {
                m_coll.erase(m_coll.begin(), m_coll.begin()+index);
                return S_OK;
            } else {
                m_coll.erase(m_coll.begin()+index+count, m_coll.begin()+index);
                return S_OK;
            }
        }

        return E_INVALIDARG;
    }


	STDMETHOD(Move)(LONG fromIndex, LONG toIndex)
    {
        if (fromIndex == ERR_INDEX) {
            fromIndex = (long)m_coll.size() - 1;
        }

        if (toIndex == ERR_INDEX) {
            toIndex = (long)m_coll.size() - 1;
        }

        if (fromIndex < 0 || fromIndex >= (long)m_coll.size()) {
            return E_INVALIDARG;
        }

        if (toIndex < 0 || toIndex >= (long)m_coll.size()) {
            return E_INVALIDARG;
        }

        if (fromIndex < toIndex) {
            // move down
            POINT_ITER_AT(p1,fromIndex);
            POINT_ITER_AT(p2,toIndex+1);

		    m_coll.insert(p2, (*p1));
		    m_coll.erase(p1);
        } else if (fromIndex > toIndex)	{
            // move up
            POINT_ITER_AT(p1,fromIndex);
            POINT_ITER_AT(p2,toIndex);

			m_coll.insert(p2, (*p1));

            POINT_ITER_AT(p,fromIndex+1);
            m_coll.erase(p);
        }
		return S_OK;
    }


    STDMETHOD(Inverse)(void)
    {
        if (m_coll.size() >= 2) {
            long size = (long) m_coll.size();
	        long half = size / 2;

	        for (long i=0; i<half; i++)	{
		        CAdapt< CComPtr<IPoint> > tmp = m_coll.at(i);
		        m_coll.at(i) = m_coll.at(size-i-1);
		        m_coll.at(size-i-1) = tmp;
	        }
        }
        return S_OK;
    }


    STDMETHOD(get_Bound)(IBound** ppVal)
    {
        return E_NOTIMPL;
    }
};

//DEL:OBJECT_ENTRY_AUTO(__uuidof(Points), _CPoints)
