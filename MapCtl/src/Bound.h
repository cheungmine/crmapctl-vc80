// Bound.h : CBound ������

#pragma once

#include "Config.h"

#include "./gdbapi/src/gdbapi.h"

// CBound

class ATL_NO_VTABLE CBound :
    public CComObjectRootEx<CComSingleThreadModel>,
    //DEL:public CComCoClass<CBound, &CLSID_Bound>,
    public CComCoClass<CBound>,
    public ISupportErrorInfo,
    public IDispatchImpl<IBound, &IID_IBound, &LIBID_CRMapCtlLib, 1, 0>
{
public:
    GDBExtBound  bound;

    CBound()
    {
        ZeroMemory(&bound, sizeof(bound));
    }

DECLARE_NO_REGISTRY()

BEGIN_COM_MAP(CBound)
    COM_INTERFACE_ENTRY(IBound)
    COM_INTERFACE_ENTRY(IDispatch)
    COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
    STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid)
    {
	    static const IID* arr[] = 
	    {
		    &IID_IBound
	    };

	    for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	    {
		    if (InlineIsEqualGUID(*arr[i],riid))
			    return S_OK;
	    }
	    return S_FALSE;
    }

    DECLARE_PROTECT_FINAL_CONSTRUCT()

    HRESULT FinalConstruct()
    {
        return S_OK;
    }

    void FinalRelease()
    {
    }

    void SetBound(double Xmin, double Ymin, double Xmax, double Ymax,
        double Zmin, double Zmax, double Mmin, double Mmax)
	{
		bound.Xmin = Xmin;
        bound.Ymin = Ymin;
        bound.Xmax = Xmax;
        bound.Ymax = Ymax;
        bound.Zmin = Zmin;
        bound.Zmax = Zmax;
        bound.Mmin = Mmin;
        bound.Mmax = Mmax;
	}

	void GetBound(double* Xmin, double* Ymin, double* Xmax, double* Ymax,
        double* Zmin, double* Zmax, double* Mmin, double* Mmax)
	{
        if (Xmin) {
            *Xmin = bound.Xmin;
        }
        if (Ymin) {
            *Ymin = bound.Ymin;
        }
        if (Xmax) {
            *Xmax = bound.Xmax;
        }
        if (Ymax) {
            *Ymax = bound.Ymax;
        }
        if (Zmin) {
            *Zmin = bound.Zmin;
        }
        if (Zmax) {
            *Zmax = bound.Zmax;
        }
        if (Mmin) {
            *Mmin = bound.Mmin;
        }
        if (Mmax) {
            *Mmax = bound.Mmax;
        }
	}

public:
    STDMETHODIMP get_Xmin(double* pVal)
    {
        *pVal = bound.Xmin;
        return S_OK;
    }

    STDMETHODIMP put_Xmin(double newVal)
    {
        bound.Xmin = newVal;
        return S_OK;
    }

    STDMETHODIMP get_Xmax(double* pVal)
    {
        *pVal = bound.Xmax;
        return S_OK;
    }

    STDMETHODIMP put_Xmax(double newVal)
    {
        bound.Xmax = newVal;
        return S_OK;
    }

    STDMETHODIMP get_Ymin(double* pVal)
    {
        *pVal = bound.Ymin;
        return S_OK;
    }

    STDMETHODIMP put_Ymin(double newVal)
    {
        bound.Ymin = newVal;
        return S_OK;
    }

    STDMETHODIMP get_Ymax(double* pVal)
    {
        *pVal = bound.Ymax;
        return S_OK;
    }

    STDMETHODIMP put_Ymax(double newVal)
    {
        bound.Ymax = newVal;
        return S_OK;
    }

    STDMETHODIMP get_Zmin(double* pVal)
    {
        *pVal = bound.Zmin;
        return S_OK;
    }

    STDMETHODIMP put_Zmin(double newVal)
    {
        bound.Zmin = newVal;
        return S_OK;
    }

    STDMETHODIMP get_Zmax(double* pVal)
    {
        *pVal = bound.Zmax;
        return S_OK;
    }

    STDMETHODIMP put_Zmax(double newVal)
    {
        bound.Zmax = newVal;
        return S_OK;
    }

    STDMETHODIMP get_Mmin(double* pVal)
    {
        *pVal = bound.Mmin;
        return S_OK;
    }

    STDMETHODIMP put_Mmin(double newVal)
    {
        bound.Mmin = newVal;
        return S_OK;
    }

    STDMETHODIMP get_Mmax(double* pVal)
    {
        *pVal = bound.Mmax;
        return S_OK;
    }

    STDMETHODIMP put_Mmax(double newVal)
    {
        bound.Mmax = newVal;
        return S_OK;
    }

    STDMETHODIMP get_IsValid(VARIANT_BOOL* pVal)
    {
        *pVal = (GDBExtBoundIsValid(&bound)==GDBAPI_TRUE) ? VARIANT_TRUE : VARIANT_FALSE;
        return S_OK;
    }

    STDMETHODIMP get_ToShape(IShape** result)
    {
        return E_NOTIMPL;
    }

    STDMETHODIMP Clone(IBound* source)
    {
        if (! source) {
            return E_POINTER;
        }

        memcpy(&bound, &((CBound*) (source))->bound, sizeof(bound));
        return S_OK;
    }

    STDMETHODIMP Union(IBound* clip)
    {
        if (! clip) {
            return E_POINTER;
        }

        GDBExtBoundUnion(&bound, &((CBound*) clip)->bound, &bound);

        return S_OK;
    }

    STDMETHODIMP Intersect(IBound* clip, VARIANT resultBound, VARIANT_BOOL* pVal)
    {
        GDBAPI_BOOL bres = VARIANT_FALSE;

        if (! clip) {
            return E_POINTER;
        }

        if (resultBound.vt == VT_DISPATCH) {
            CComQIPtr<IBound> spResult(resultBound.pdispVal);

            if (spResult) {
                bres = GDBExtBoundIntersect(&bound, &((CBound*) clip)->bound, &((CBound*) spResult.p)->bound);
            } else {
                return E_INVALIDARG;
            }
        } else {
            bres = GDBExtBoundIntersect(&bound, &((CBound*) clip)->bound, 0);
        }

        *pVal = (bres == GDBAPI_TRUE) ? VARIANT_TRUE : VARIANT_FALSE;

        return S_OK;
    }

};

//DEL://DEL:OBJECT_ENTRY_AUTO(__uuidof(Bound), CBound)
