/**
 * color.js
 *
 *   javascript test for IColor
 *
 */
(function (window, undefined) {
    "use strict";

    var test = function(obMap, output) {
        output.value += "\r\ntest Color ...";

        try {
            var clr = obMap.CreateObject("Color");
            var clr2 = obMap.CreateObject("Color");

            if (clr.WebColor != "#000000") {
                throw new CRMapCtlError("color.js", 13, "Color.Clone() failed.");
            }

            var colors = new Array (
                "aliceblue",               // RGB(240, 248, 255)    #f0f8ff  艾利斯兰
                "antiquewhite",            // RGB(250, 235, 215)    #faebd7  古董�?
                "aqua",                    // RGB(  0, 255, 255)    #00ffff  浅绿�?
                "aquamarine",              // RGB(127, 255, 212)    #7fffd4  碧绿�?
                "azure",                   // RGB(240, 255, 255)    #f0ffff  天蓝�?
                "beige",                   // RGB(245, 245, 220)    #f5f5dc  米色
                "bisque",                  // RGB(255, 228, 196)    #ffe4c4  桔黄�?
                "black",                   // RGB(  0,   0,   0)    #000000  黑色
                "blanchedalmond",          // RGB(255, 235, 205)    #ffebcd  白杏�?
                "blue",                    // RGB(  0,   0, 255)    #0000ff  蓝色
                "blueviolet",              // RGB(138,  43, 226)    #8a2be2  紫罗兰色
                "brown",                   // RGB(165,  42,  42)    #a52a2a  褐色
                "burlywood",               // RGB(222, 184, 135)    #deb887  实木�?
                "cadetblue",               // RGB( 95, 158, 160)    #5f9ea0  军兰�?
                "chartreuse",              // RGB(127, 255,   0)    #7fff00  黄绿�?
                "chocolate",               // RGB(210, 105,  30)    #d2691e  巧可力色
                "coral",                   // RGB(255, 127,  80)    #ff7f50  珊瑚�?
                "cornflowerblue",          // RGB(100, 149, 237)    #6495ed  菊兰�?
                "cornsilk",                // RGB(255, 248, 220)    #fff8dc  米绸�?
                "crimson",                 // RGB(220,  20,  60)    #dc143c  暗深红色
                "cyan",                    // RGB(  0, 255, 255)    #00ffff  青色
                "darkblue",                // RGB(  0,   0, 139)    #00008b  暗蓝�?
                "darkcyan",                // RGB(  0, 139, 139)    #008b8b  暗青�?
                "darkgoldenrod",           // RGB(184, 134,  11)    #b8860b  暗金黄色
                "darkgray",                // RGB(169, 169, 169)    #a9a9a9  暗灰�?
                "darkgreen",               // RGB(  0, 100,   0)    #006400  暗绿�?
                "darkgrey",                // RGB(169, 169, 169)    #a9a9a9  暗灰�?
                "darkkhaki",               // RGB(189, 183, 107)    #bdb76b  暗黄褐色
                "darkmagenta",             // RGB(139,   0, 139)    #8b008b  暗洋�?
                "darkolivegreen",          // RGB( 85, 107,  47)    #556b2f  暗橄榄绿
                "darkorange",              // RGB(255, 140,   0)    #ff8c00  暗桔黄色
                "darkorchid",              // RGB(153,  50, 204)    #9932cc  暗紫�?
                "darkred",                 // RGB(139,   0,   0)    #8b0000  暗红�?
                "darksalmon",              // RGB(233, 150, 122)    #e9967a  暗肉�?
                "darkseagreen",            // RGB(143, 188, 143)    #8fbc8f  暗海兰色
                "darkslateblue",           // RGB( 72,  61, 139)    #483d8b  暗灰蓝色
                "darkslategray",           // RGB( 47,  79,  79)    #2f4f4f  暗瓦灰色
                "darkslategrey",           // RGB( 47,  79,  79)    #2f4f4f  暗瓦灰色
                "darkturquoise",           // RGB(  0, 206, 209)    #00ced1  暗宝石绿
                "darkviolet",              // RGB(148,   0, 211)    #9400d3  暗紫罗兰�?
                "deeppink",                // RGB(255,  20, 147)    #ff1493  深粉红色
                "deepskyblue",             // RGB(  0, 191, 255)    #00bfff  深天蓝色
                "dimgray",                 // RGB(105, 105, 105)    #696969  暗灰�?
                "dimgrey",                 // RGB(105, 105, 105)    #696969  暗灰�?
                "dodgerblue",              // RGB( 30, 144, 255)    #1e90ff  闪兰�?
                "firebrick",               // RGB(178,  34,  34)    #b22222  火砖�?
                "floralwhite",             // RGB(255, 250, 240)    #fffaf0  花白�?
                "forestgreen",             // RGB( 34, 139,  34)    #228b22  森林�?
                "fuchsia",                 // RGB(255,   0, 255)    #ff00ff  紫红�?
                "gainsboro",               // RGB(220, 220, 220)    #dcdcdc  淡灰�?
                "ghostwhite",              // RGB(248, 248, 255)    #f8f8ff  幽灵�?
                "gold",                    // RGB(255, 215,   0)    #ffd700  金色
                "goldenrod",               // RGB(218, 165,  32)    #daa520  金麒麟色
                "gray",                    // RGB(128, 128, 128)    #808080  灰色
                "grey",                    // RGB(128, 128, 128)    #808080  灰色
                "green",                   // RGB(  0, 128,   0)    #008000  绿色
                "greenyellow",             // RGB(173, 255,  47)    #adff2f  黄绿�?
                "honeydew",                // RGB(240, 255, 240)    #f0fff0  蜜色 
                "hotpink",                 // RGB(255, 105, 180)    #ff69b4  热粉红色
                "indianred",               // RGB(205,  92,  92)    #cd5c5c  印第安红
                "indigo",                  // RGB( 75,   0, 130)    #4b0082  靛青�?
                "ivory",                   // RGB(255, 255, 240)    #fffff0  象牙�?
                "khaki",                   // RGB(240, 230, 140)    #f0e68c  黄褐�?
                "lavender",                // RGB(230, 230, 250)    #e6e6fa  淡紫�?
                "lavenderblush",           // RGB(255, 240, 245)    #fff0f5  淡紫�?
                "lawngreen",               // RGB(124, 252,   0)    #7cfc00  草绿�?
                "lemonchiffon",            // RGB(255, 250, 205)    #fffacd  柠檬绸色
                "lightblue",               // RGB(173, 216, 230)    #add8e6  亮蓝�?
                "lightcoral",              // RGB(240, 128, 128)    #f08080  亮珊瑚色
                "lightcyan",               // RGB(224, 255, 255)    #e0ffff  亮青�?
                "lightgoldenrodyellow",    // RGB(250, 250, 210)    #fafad2  亮金黄色
                "lightgray",               // RGB(211, 211, 211)    #d3d3d3  亮灰�?
                "lightgreen",              // RGB(144, 238, 144)    #90ee90  亮绿�?
                "lightgrey",               // RGB(211, 211, 211)    #d3d3d3  亮灰�?
                "lightpink",               // RGB(255, 182, 193)    #ffb6c1  亮粉红色
                "lightsalmon",             // RGB(255, 160, 122)    #ffa07a  亮肉�?
                "lightseagreen",           // RGB( 32, 178, 170)    #20b2aa  亮海蓝色
                "lightskyblue",            // RGB(135, 206, 250)    #87cefa  亮天蓝色
                "lightslategray",          // RGB(119, 136, 153)    #778899  亮蓝�?
                "lightslategrey",          // RGB(119, 136, 153)    #778899  亮蓝�?
                "lightsteelblue",          // RGB(176, 196, 222)    #b0c4de  亮钢兰色
                "lightyellow",             // RGB(255, 255, 224)    #ffffe0  亮黄�?
                "lime",                    // RGB(  0, 255,   0)    #00ff00  酸橙�?
                "limegreen",               // RGB( 50, 205,  50)    #32cd32  橙绿�?
                "linen",                   // RGB(250, 240, 230)    #faf0e6  亚麻�?
                "magenta",                 // RGB(255,   0, 255)    #ff00ff  红紫�?
                "maroon",                  // RGB(128,   0,   0)    #800000  粟色 
                "mediumaquamarine",        // RGB(102, 205, 170)    #66cdaa  中绿�?
                "mediumblue",              // RGB(  0,   0, 205)    #0000cd  中兰�?
                "mediumorchid",            // RGB(186,  85, 211)    #ba55d3  中粉紫色
                "mediumpurple",            // RGB(147, 112, 219)    #9370db  中紫�?
                "mediumseagreen",          // RGB( 60, 179, 113)    #3cb371  中海�?
                "mediumslateblue",         // RGB(123, 104, 238)    #7b68ee  中暗蓝色
                "mediumspringgreen",       // RGB(  0, 250, 154)    #00fa9a  中春绿色
                "mediumturquoise",         // RGB( 72, 209, 204)    #48d1cc  中绿宝石
                "mediumvioletred",         // RGB(199,  21, 133)    #c71585  中紫罗兰�?
                "midnightblue",            // RGB( 25,  25, 112)    #191970  中灰兰色
                "mintcream",               // RGB(245, 255, 250)    #f5fffa  薄荷�?
                "mistyrose",               // RGB(255, 228, 225)    #ffe4e1  浅玫瑰色
                "moccasin",                // RGB(255, 228, 181)    #ffe4b5  鹿皮�?
                "navajowhite",             // RGB(255, 222, 173)    #ffdead  纳瓦�?
                "navy",                    // RGB(  0,   0, 128)    #000080  海军�?
                "oldlace",                 // RGB(253, 245, 230)    #fdf5e6  老花�?
                "olive",                   // RGB(128, 128,   0)    #808000  橄榄�?
                "olivedrab",               // RGB(107, 142,  35)    #6b8e23  深绿褐色
                "orange",                  // RGB(255, 165,   0)    #ffa500  橙色
                "orangered",               // RGB(255,  69,   0)    #ff4500  红橙�?
                "orchid",                  // RGB(218, 112, 214)    #da70d6  淡紫�?
                "palegoldenrod",           // RGB(238, 232, 170)    #eee8aa  苍麒麟色
                "palegreen",               // RGB(152, 251, 152)    #98fb98  苍绿�?
                "paleturquoise",           // RGB(175, 238, 238)    #afeeee  苍宝石绿
                "palevioletred",           // RGB(219, 112, 147)    #db7093  苍紫罗兰�?
                "papayawhip",              // RGB(255, 239, 213)    #ffefd5  番木�?
                "peachpuff",               // RGB(255, 218, 185)    #ffdab9  桃色
                "peru",                    // RGB(205, 133,  63)    #cd853f  秘鲁�?
                "pink",                    // RGB(255, 192, 203)    #ffc0cb  粉红�?
                "plum",                    // RGB(221, 160, 221)    #dda0dd  洋李�?
                "powderblue",              // RGB(176, 224, 230)    #b0e0e6  粉蓝�?
                "purple",                  // RGB(128,   0, 128)    #800080  紫色
                "red",                     // RGB(255,   0,   0)    #ff0000  红色
                "rosybrown",               // RGB(188, 143, 143)    #bc8f8f  褐玫瑰红
                "royalblue",               // RGB( 65, 105, 225)    #4169e1  皇家�?
                "saddlebrown",             // RGB(139,  69,  19)    #8b4513  重褐�?
                "salmon",                  // RGB(250, 128, 114)    #fa8072  鲜肉�?
                "sandybrown",              // RGB(244, 164,  96)    #f4a460  沙褐�?
                "seagreen",                // RGB( 46, 139,  87)    #2e8b57  海绿�?
                "seashell",                // RGB(255, 245, 238)    #fff5ee  海贝�?
                "sienna",                  // RGB(160,  82,  45)    #a0522d  赭色 
                "silver",                  // RGB(192, 192, 192)    #c0c0c0  银色
                "skyblue",                 // RGB(135, 206, 235)    #87ceeb  天蓝�?
                "slateblue",               // RGB(106,  90, 205)    #6a5acd  石蓝�?
                "slategray",               // RGB(112, 128, 144)    #708090  灰石�?
                "slategrey",               // RGB(112, 128, 144)    #708090  灰石�?
                "snow",                    // RGB(255, 250, 250)    #fffafa  雪白�?
                "springgreen",             // RGB(  0, 255, 127)    #00ff7f  春绿�?
                "steelblue",               // RGB( 70, 130, 180)    #4682b4  钢兰�?
                "tan",                     // RGB(210, 180, 140)    #d2b48c  茶色
                "teal",                    // RGB(  0, 128, 128)    #008080  水鸭�?
                "thistle",                 // RGB(216, 191, 216)    #d8bfd8  蓟色
                "tomato",                  // RGB(255,  99,  71)    #ff6347  西红柿色
                "turquoise",               // RGB( 64, 224, 208)    #40e0d0  青绿�?
                "violet",                  // RGB(238, 130, 238)    #ee82ee  紫罗兰色
                "wheat",                   // RGB(245, 222, 179)    #f5deb3  浅黄�?
                "white",                   // RGB(255, 255, 255)    #ffffff  白色
                "whitesmoke",              // RGB(245, 245, 245)    #f5f5f5  烟白�?
                "yellow",                  // RGB(255, 255,   0)    #ffff00  黄色
                "yellowgreen"              // RGB(154, 205,  50)    #9acd32  黄绿�?
            );

            var rgb = new Array (
                "#f0f8ff",    // 艾利斯兰
                "#faebd7",    // 古董�?
                "#00ffff",    // 浅绿�?
                "#7fffd4",    // 碧绿�?
                "#f0ffff",    // 天蓝�?
                "#f5f5dc",    // 米色
                "#ffe4c4",    // 桔黄�?
                "#000000",    // 黑色
                "#ffebcd",    // 白杏�?
                "#0000ff",    // 蓝色
                "#8a2be2",    // 紫罗兰色
                "#a52a2a",    // 褐色
                "#deb887",    // 实木�?
                "#5f9ea0",    // 军兰�?
                "#7fff00",    // 黄绿�?
                "#d2691e",    // 巧可力色
                "#ff7f50",    // 珊瑚�?
                "#6495ed",    // 菊兰�?
                "#fff8dc",    // 米绸�?
                "#dc143c",    // 暗深红色
                "#00ffff",    // 青色
                "#00008b",    // 暗蓝�?
                "#008b8b",    // 暗青�?
                "#b8860b",    // 暗金黄色
                "#a9a9a9",    // 暗灰�?
                "#006400",    // 暗绿�?
                "#a9a9a9",    // 暗灰�?
                "#bdb76b",    // 暗黄褐色
                "#8b008b",    // 暗洋�?
                "#556b2f",    // 暗橄榄绿
                "#ff8c00",    // 暗桔黄色
                "#9932cc",    // 暗紫�?
                "#8b0000",    // 暗红�?
                "#e9967a",    // 暗肉�?
                "#8fbc8f",    // 暗海兰色
                "#483d8b",    // 暗灰蓝色
                "#2f4f4f",    // 暗瓦灰色
                "#2f4f4f",    // 暗瓦灰色
                "#00ced1",    // 暗宝石绿
                "#9400d3",    // 暗紫罗兰�?
                "#ff1493",    // 深粉红色
                "#00bfff",    // 深天蓝色
                "#696969",    // 暗灰�?
                "#696969",    // 暗灰�?
                "#1e90ff",    // 闪兰�?
                "#b22222",    // 火砖�?
                "#fffaf0",    // 花白�?
                "#228b22",    // 森林�?
                "#ff00ff",    // 紫红�?
                "#dcdcdc",    // 淡灰�?
                "#f8f8ff",    // 幽灵�?
                "#ffd700",    // 金色
                "#daa520",    // 金麒麟色
                "#808080",    // 灰色
                "#808080",    // 灰色
                "#008000",    // 绿色
                "#adff2f",    // 黄绿�?
                "#f0fff0",    // 蜜色 
                "#ff69b4",    // 热粉红色
                "#cd5c5c",    // 印第安红
                "#4b0082",    // 靛青�?
                "#fffff0",    // 象牙�?
                "#f0e68c",    // 黄褐�?
                "#e6e6fa",    // 淡紫�?
                "#fff0f5",    // 淡紫�?
                "#7cfc00",    // 草绿�?
                "#fffacd",    // 柠檬绸色
                "#add8e6",    // 亮蓝�?
                "#f08080",    // 亮珊瑚色
                "#e0ffff",    // 亮青�?
                "#fafad2",    // 亮金黄色
                "#d3d3d3",    // 亮灰�?
                "#90ee90",    // 亮绿�?
                "#d3d3d3",    // 亮灰�?
                "#ffb6c1",    // 亮粉红色
                "#ffa07a",    // 亮肉�?
                "#20b2aa",    // 亮海蓝色
                "#87cefa",    // 亮天蓝色
                "#778899",    // 亮蓝�?
                "#778899",    // 亮蓝�?
                "#b0c4de",    // 亮钢兰色
                "#ffffe0",    // 亮黄�?
                "#00ff00",    // 酸橙�?
                "#32cd32",    // 橙绿�?
                "#faf0e6",    // 亚麻�?
                "#ff00ff",    // 红紫�?
                "#800000",    // 粟色 
                "#66cdaa",    // 中绿�?
                "#0000cd",    // 中兰�?
                "#ba55d3",    // 中粉紫色
                "#9370db",    // 中紫�?
                "#3cb371",    // 中海�?
                "#7b68ee",    // 中暗蓝色
                "#00fa9a",    // 中春绿色
                "#48d1cc",    // 中绿宝石
                "#c71585",    // 中紫罗兰�?
                "#191970",    // 中灰兰色
                "#f5fffa",    // 薄荷�?
                "#ffe4e1",    // 浅玫瑰色
                "#ffe4b5",    // 鹿皮�?
                "#ffdead",    // 纳瓦�?
                "#000080",    // 海军�?
                "#fdf5e6",    // 老花�?
                "#808000",    // 橄榄�?
                "#6b8e23",    // 深绿褐色
                "#ffa500",    // 橙色
                "#ff4500",    // 红橙�?
                "#da70d6",    // 淡紫�?
                "#eee8aa",    // 苍麒麟色
                "#98fb98",    // 苍绿�?
                "#afeeee",    // 苍宝石绿
                "#db7093",    // 苍紫罗兰�?
                "#ffefd5",    // 番木�?
                "#ffdab9",    // 桃色
                "#cd853f",    // 秘鲁�?
                "#ffc0cb",    // 粉红�?
                "#dda0dd",    // 洋李�?
                "#b0e0e6",    // 粉蓝�?
                "#800080",    // 紫色
                "#ff0000",    // 红色
                "#bc8f8f",    // 褐玫瑰红
                "#4169e1",    // 皇家�?
                "#8b4513",    // 重褐�?
                "#fa8072",    // 鲜肉�?
                "#f4a460",    // 沙褐�?
                "#2e8b57",    // 海绿�?
                "#fff5ee",    // 海贝�?
                "#a0522d",    // 赭色 
                "#c0c0c0",    // 银色
                "#87ceeb",    // 天蓝�?
                "#6a5acd",    // 石蓝�?
                "#708090",    // 灰石�?
                "#708090",    // 灰石�?
                "#fffafa",    // 雪白�?
                "#00ff7f",    // 春绿�?
                "#4682b4",    // 钢兰�?
                "#d2b48c",    // 茶色
                "#008080",    // 水鸭�?
                "#d8bfd8",    // 蓟色
                "#ff6347",    // 西红柿色
                "#40e0d0",    // 青绿�?
                "#ee82ee",    // 紫罗兰色
                "#f5deb3",    // 浅黄�?
                "#ffffff",    // 白色
                "#f5f5f5",    // 烟白�?
                "#ffff00",    // 黄色
                "#9acd32"     // 黄绿�?
            );

            for (i = 0; i < colors.length; i++) {
                clr.WebColor = colors[i];
                clr2.Clone(clr);

                if (rgb[i] != clr.WebColor) {
                    throw new CRMapCtlError("color.js", 321, "Color.WebColor failed.");
                }

                if (rgb[i] != clr2.WebColor) {
                    throw new CRMapCtlError("color.js", 325, "Color.WebColor failed.");
                }

                clr.RgbaColor = rgb[i];
                if (rgb[i] != clr.WebColor) {
                    throw new CRMapCtlError("color.js", 330, "Color.RgbaColor failed.");
                }

                // 测试反色
                clr.Neg();
                clr.Neg();
                if (clr.OleColor != clr2.OleColor) {
                    throw new CRMapCtlError("color.js", 337, "Color.Neg() failed.");
                }
            }

            // 测试色调Hue和饱和度Sat,亮度Lum
            clr.WebColor = "#f0f8ff";
            if (clr.Hue != 139 || clr.Sat != 240 || clr.Lum != 233) {
                throw new CRMapCtlError("color.js", 344, "Color.Hue failed.");
            }
           
            clr.Hue = 139;
            clr.Sat = 240;
            clr.Lum = 233;
            if (clr.WebColor != "#f0f8ff") {
                throw new CRMapCtlError("color.js", 351, "Color.Hue failed.");
            }

            clr.WebColor = "#bc8f8f";
            if (clr.Hue != 0 || clr.Sat != 60 || clr.Lum != 156) {
                throw new CRMapCtlError("color.js", 356, "Color.Hue failed.");
            }

            clr.Hue = 0;
            clr.Sat = 60;
            clr.Lum = 156;
            if (clr.WebColor != "#bc8f8f") {
                throw new CRMapCtlError("color.js", 363, "Color.Hue failed.");
            }

            clr.WebColor = "#000080";
            if (clr.Hue != 160 || clr.Sat != 240 || clr.Lum != 60) {
                throw new CRMapCtlError("color.js", 369, "Color.Hue failed.");
            }

            clr.WebColor = "#ff0000";
            if (clr.Hue != 0 || clr.Sat != 240 || clr.Lum != 120) {
                throw new CRMapCtlError("color.js", 373, "Color.Hue failed.");
            }

            clr.WebColor = "#000000";
            if (clr.Hue != 0 || clr.Sat != 0 || clr.Lum != 0) {
                throw new CRMapCtlError("color.js", 378, "Color.Hue failed.");
            }

            clr.WebColor = "#ffffff"; 
            if (clr.Hue != 0 || clr.Sat != 0 || clr.Lum != 240) {
                throw new CRMapCtlError("color.js", 383, "Color.Hue failed.");
            }

            clr.Hue = 0;
            clr.Sat = 60;
            clr.Lum = 240;
            if (clr.WebColor != "#ffffff") {
                throw new CRMapCtlError("color.js", 390, "Color.Hue failed.");
            }
           
            clr.WebColor = "#ffffff";
            clr2.WebColor = "#ffffff"; 
            clr.Tune();
            if (clr.WebColor != clr2.WebColor) {
                throw new CRMapCtlError("color.js", 397, "Color.Tune() failed.");
            }

            clr.RgbaColor = "#0000ff00";
            clr.Tune(30);
            if (clr.RgbaColor != "#4040ff00") {
                throw new CRMapCtlError("color.js", 404, "Color.Tune() failed.");
            }

            output.value += "OK";
        } catch (err) {
            if (err.print) {
                output.value += "\r\n" + err.print();
            } else {
                output.value += "\r\n" + err.message;
            }
        }
    };

    if (! window.CRMapCtlTest) {
        window.CRMapCtlTest = {};
    }

    window.CRMapCtlTest.testColor = test;
}(window));
