/**
 * points.js
 *
 *   javascript test for IPoints
 *
 */
 
(function (window, undefined) {
    "use strict";

    var IntDiv = function (exp1, exp2) {
        var n1 = Math.round(exp1);
        var n2 = Math.round(exp2);
        var rslt = n1 / n2;
        if (rslt >= 0) {
            rslt = Math.floor(rslt);
        } else {
            rslt = Math.ceil(rslt);
        }
        return rslt;
    }; 

    var test = function (mapCtl, output, MAXNUM) {
        if (!MAXNUM) {
            MAXNUM = 1000;
        }
        output.value += "\r\ntest Points (MAXNUM=" + MAXNUM + ") ...";
 
        try {
            var i;
            var count;
            var pts = mapCtl.CreateObject("Points");
            
            // add 999 points
            count = 0;
            for (i = 0; i < MAXNUM; i++) {
                count++;
                var p = pts.Add();
                p.Set(i, 10000+i, 0, 0);
            }

            if (pts.Count !== count) {
                throw new CRMapCtlError("points.js", 39, "Points.Add() failed.");
            }

            for (i = 0; i < MAXNUM; i++) {
                if (i%2 == 0) {
                    count--;
                    pts.Remove();
                }
            }

            if (pts.Count !== count) {
                throw new CRMapCtlError("points.js", 51, "Points.Add() failed.");
            }

            // remove all
            pts.Remove(0, pts.Count);
            if (pts.Count !== 0) {
                throw new CRMapCtlError("points.js", 57, "Points.Remove() failed.");
            }

            for (i = 0; i < MAXNUM; i++) {
                pts.Add();
            }
            
            // remove 1 point at back
            pts.Remove(-1);
            pts.Remove();
            if (pts.Count !== MAXNUM-2) {
                throw new CRMapCtlError("points.js", 68, "Points.Remove() failed.");
            }

            // remove 1 point at front
            pts.Remove(0);
            if (pts.Count !== MAXNUM-3) {
                throw new CRMapCtlError("points.js", 74, "Points.Remove() failed.");
            }

            // remove 3 points from back to front
            pts.Remove(-1, -3);

            if (pts.Count !== MAXNUM-6) {
                throw new CRMapCtlError("points.js", 80, "Points.Remove() failed.");
            }

            // remove all points from back to front
            pts.Remove(-1, -pts.Count);
            pts.Inverse();
            if (pts.Count !== 0) {
                throw new CRMapCtlError("points.js", 86, "Points.Remove() failed.");
            }

            count = 0;
            for (i = 0; i < 10; i++) {
                count++;
                var p = pts.Add();
                p.Set(i, 100+i, 0, 0);

                if (pts(i).X !== i || pts(i).Y !== 100+i || pts(i).Z !== 0 || pts(i).M !== 0) {
                    throw new CRMapCtlError("points.js", 97, "Points.Add() failed.");
                }
            }

            pts.Inverse();
            pts.Inverse();
            for (i = 0; i < 10; i++) {
                if (pts(i).X !== i || pts(i).Y !== 100+i || pts(i).Z !== 0 || pts(i).M !== 0) {
                    throw new CRMapCtlError("points.js", 97, "Points.Add() failed.");
                }
            }

            // do nothing
            pts.Move(6, 6);

            // Move Bottom
            pts.Move(0, -1);
            if (pts(0).Y !== 101 || pts(pts.Count-1).Y !== 100) {
                throw new CRMapCtlError("points.js", 113, "Points.Move() failed.");
            }

            // Move Top
            pts.Move(-1, 0);
            if (pts(0).X !== 0 || pts(0).Y !== 100) {
                throw new CRMapCtlError("points.js", 119, "Points.Move() failed.");
            }

            // Move Up: 0,1,2,3,4,5,6,7,8,9=>0,1,2,3,4,8,5,6,7,9
            pts.Move(8, 5);
            if (pts(5).X !== 8 || pts(6).X !== 5) {
                throw new CRMapCtlError("points.js", 128, "Points.Move() failed.");
            }

            // Move Down: 0,1,2,3,4,8,5,6,7,9=>0,1,3,4,8,5,6,2,7,9
            pts.Move(2, 7);
            if (pts(2).X !== 3 || pts(3).X !== 4 || pts(4).X !== 8 || pts(5).X !== 5 ||
                pts(6).X !== 6 || pts(7).X !== 2 || pts(8).X !== 7 || pts(9).X !== 9) {
                throw new CRMapCtlError("points.js", 134, "Points.Move() failed.");
            }

            pts.Move(0, 1);
            // 1,0,3,4,8,5,6,2,7,9
            if (pts(0).X !== 1 || pts(1).X !== 0 || pts(2).X !== 3) {
                throw new CRMapCtlError("points.js", 141, "Points.Move() failed.");
            }
            pts.Move(1, 0);
            pts.Move(0, 1);
            if (pts(0).X !== 1 || pts(1).X !== 0 || pts(2).X !== 3) {
                throw new CRMapCtlError("points.js", 141, "Points.Move() failed.");
            }

            // test ... pts.Bound

            output.value += "OK"; 
        } catch (err) {
            if (err.print) {
                output.value += "\r\n" + err.print();
            } else {
                output.value += "\r\n" + err.message;
            }
        }
    };


    if (! window.CRMapCtlTest) {
        window.CRMapCtlTest = {}
    }
    window.CRMapCtlTest.testPoints = test;
}(window));
