/**
 * layer.js
 *
 *   javascript test for IBound
 *
 */

(function (window, undefined) {
    "use strict";

    var test = function (mapCtl, output) {
        output.value += "\r\ntest Layer ...";
    
        try {
            var a = mapCtl.Layers.Add();

            //a.Open("C:\\mapdata\\shp\\DLTB.shp");
          
            a.Open("C:\\mapdata/shp", "DLTB.shp"); 


            output.value += "OK";
        } catch (err) {
            if (err.print) {
                output.value += "\r\n" + err.print();
            } else {
                output.value += "\r\n" + err.message;
            }
        }
    };


    if (! window.CRMapCtlTest) {
        window.CRMapCtlTest = {}
    }
    window.CRMapCtlTest.testLayer = test;
}(window));
