/**
 * bound.js
 *
 *   javascript test for IBound
 *
 */

(function (window, undefined) {
    "use strict";

    var width = function (b) {
        return b.Xmax - b.Xmin;
    };

    var height = function (b) {
        return b.Ymax - b.Ymin;
    };

    var depth = function (b) {
        return b.Zmax - b.Zmin;
    };

    var offsetRect = function (b, dX, dY) {
        b.Xmin += dX;
        b.Xmax += dX;
        b.Ymin += dY;
        b.Ymax += dY;
    };

    var isEqualRect = function (b1, b2) {
        if (b1.Xmin !== b2.Xmin ||
            b1.Ymin !== b2.Ymin ||
            b1.Xmax !== b2.Xmax ||
            b1.Ymax !== b2.Ymax) {
            return false;
        } else {
            return true;
        }
    };

    var test = function (mapCtl, output) {
        output.value += "\r\ntest Bound ...";
    
        try {
            var b1 = mapCtl.CreateObject("Bound");

            b1.Xmin = 0;
            b1.Ymin = 0;
            b1.Xmax = 100;
            b1.Ymax = 100;

            var b2 = mapCtl.CreateObject("Bound");
            b2.Clone(b1);

            if (!isEqualRect(b1, b2)) {
                throw new CRMapCtlError("bound.js", 97, "Bound.Clone() failed.");
            }

            b1.Union(b2);
            if (!isEqualRect(b1, b2)) {
                throw new CRMapCtlError("bound.js", 102, "Bound.Union() failed.");
            }

            b1.Intersect(b2, b1);
            if (!isEqualRect(b1, b2)) {
                throw new CRMapCtlError("bound.js", 107, "Bound.Intersect() failed.");
            }

            offsetRect(b2, 200, 200);
            if (b1.Intersect(b2)) {
                throw new CRMapCtlError("bound.js", 112, "Bound.Intersect() failed.");
            }

            offsetRect(b2, -200, -200);
            offsetRect(b2, 50, 50);

            b1.Intersect(b2, b1);
            if (b1.Xmin !== 50 || b1.Xmax !== 100 || b1.Ymin !== 50 || b1.Ymax !== 100) {
                throw new CRMapCtlError("bound.js", 120, "Bound.Intersect() failed.");
            }

            b1.Xmin = 0;
            b1.Ymin = 0;
            b1.Xmax = 100;
            b1.Ymax = 100;

            b2.Xmin = 100;
            b2.Ymin = 100;
            b2.Xmax = 200;
            b2.Ymax = 200;

            b1.Intersect(b2, b1);
            if (width(b1) !== 0 || height(b1) !== 0) {
                throw new CRMapCtlError("bound.js", 135, "Bound.Intersect() failed.");
            }

            if (! b1.IsValid) {
                throw new CRMapCtlError("bound.js", 139, "Bound.IsValid failed.");
            }
            b1.Xmin += 100;

            if (b1.IsValid) {
                throw new CRMapCtlError("bound.js", 144, "Bound.IsValid failed.");
            }

            if (!validateBound(b1)) {
                throw new CRMapCtlError("bound.js", 148, "validateBound failed.");
            }

            b1.Xmin = 0;
            b1.Ymin = 0;
            b1.Xmax = 100;
            b1.Ymax = 100;
            b1.Zmin = 0;
            b1.Zmax = 0;

            b2.Xmin = 50;
            b2.Ymin = -50;
            b2.Xmax = 80;
            b2.Ymax = 150;
            b2.Zmax = 10

            b1.Union(b2);

            // b2 not changed after Union()
            if (b2.Xmin !== 50 || b2.Xmax !== 80 ||
                b2.Ymin !== -50 || b2.Ymax !== 150 ||
                b2.Zmin !== 0 || b2.Zmax !== 10 ||
                b2.Mmin !== 0 || b2.Mmax !== 0) {
                throw new CRMapCtlError("bound.js", 166, "Bound.Union() failed.");
            }

            // b1 may changed after Union()
            if (b1.Xmin !== 0 || b1.Xmax !== 100 ||
                b1.Ymin !== -50 || b1.Ymax !== 150 ||
                b1.Zmax !== 10) {
                throw new CRMapCtlError("bound.js", 172, "Bound.Union() failed.");
            }
            
            if (depth(b1) !== 10) {
                throw new CRMapCtlError("bound.js", 182, "Bound.Union() failed.");
            };

            b1.Xmin = 0;
            b1.Ymin = 0;
            b1.Xmax = 1;
            b1.Ymax = 1;
            b1.Zmin = 0;
            b1.Zmax = 0;
            b1.Mmin = 0;
            b1.Mmax = 0;

            b2.Xmin = 2;
            b2.Ymin = 2;
            b2.Xmax = 4;
            b2.Ymax = 4;
            b2.Zmin = 0;
            b2.Zmax = 0;
            b2.Mmin = 0;
            b2.Mmax = 0;

            // Only test if intersect
            if (b2.Intersect(b1)) {
                throw new CRMapCtlError("bound.js", 204, "Bound.Intersect() failed.");
            }

            offsetRect(b2, -1.5, -1.5);
            if (!b2.Intersect(b1)) {
                throw new CRMapCtlError("bound.js", 210, "Bound.Intersect() failed.");
            }

            // M-values do not affect Intersect()
            b1.Mmin = 0;
            b1.Mmax = 10;
            b2.Mmin = 100;
            b2.Mmax = 1000;
            if (!b2.Intersect(b1, b2)) {
                throw new CRMapCtlError("bound.js", 218, "Bound.Intersect() failed.");
            }

            if (b2.Xmin !== 0.5 || b2.Ymin !== 0.5 ||
                b2.Xmax !== 1 || b2.Ymax !== 1 ||
                b2.Zmin !== 0 || b2.Zmax !== 0 ||
                b2.Mmin !== 100 || b2.Mmax !== 1000) {
                // M-values do not affect by Intersect()
                throw new CRMapCtlError("bound.js", 227, "Bound.Intersect() failed.");
            }

            // Z-values do affect Intersect()
            b1.Xmin = 0;
            b1.Ymin = 0;
            b1.Xmax = 1;
            b1.Ymax = 1;
            b1.Zmin = 10;
            b1.Zmax = 10;
            b1.Mmin = 0;
            b1.Mmax = 0;

            b2.Xmin = 0;
            b2.Ymin = 0;
            b2.Xmax = 1;
            b2.Ymax = 1;
            b2.Zmin = 0;
            b2.Zmax = 0;
            b2.Mmin = 99;
            b2.Mmax = 999;
            if (b2.Intersect(b1)) {
                throw new CRMapCtlError("bound.js", 249, "Bound.Intersect() failed.");
            }

            output.value += "OK";
        } catch (err) {
            if (err.print) {
                output.value += "\r\n" + err.print();
            } else {
                output.value += "\r\n" + err.message;
            }
        }
    };

    var validateBound = function (b) {
        if (!b.IsValid) {
            b.Xmin = Math.min(b.Xmin, b.Xmax);
            b.Xmax = Math.max(b.Xmin, b.Xmax);
            b.Ymin = Math.min(b.Ymin, b.Ymax);
            b.Ymax = Math.max(b.Ymin, b.Ymax);
            b.Zmin = Math.min(b.Zmin, b.Zmax);
            b.Zmax = Math.max(b.Zmin, b.Zmax);
            b.Mmin = Math.min(b.Mmin, b.Mmax);
            b.Mmax = Math.max(b.Mmin, b.Mmax);
        }
        return b.IsValid;
    };

    if (! window.CRMapCtlTest) {
        window.CRMapCtlTest = {}
    }
    window.CRMapCtlTest.testBound = test;
}(window));
