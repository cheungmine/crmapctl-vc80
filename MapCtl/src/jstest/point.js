/**
 * point.js
 *
 *   javascript test for IPoint
 *
 */
 
(function (window, undefined) {
    "use strict";
  
    var offsetPoint = function (p, dX, dY, dZ) {
        p.X += dX;        
        p.Y += dY;
        p.Z += dZ;
    };

    var test = function (mapCtl, output) {
        output.value += "\r\ntest Point ...";

        try {
            var a = mapCtl.CreateObject("Point");
            var b = mapCtl.CreateObject("Point");
            
            a.X = 1;
            a.Y = 2;
            a.Z = 3;
            a.M = 4;
  
            b.X = 100;
            b.Y = 200;

            a.Clone(b);

            if (a.X !== b.X || a.Y !== b.Y || a.Z !== b.Z || a.M !== b.M) {
                throw new CRMapCtlError("point.js", 29, "Point.Clone() failed."); 
            }

            // The below 3 methods are equivalent:
            //   a.Distance(b);
            //   a.Distance(b, null);
            //   a.Distance(b, undefined);
            //
            if (a.Distance(b) !== 0) {
                throw new CRMapCtlError("point.js", 33, "Point.Distance() failed."); 
            }

            offsetPoint(a, -100, 0, 0);
            if (a.Distance(b, null) !== 100) {
                throw new CRMapCtlError("point.js", 44, "Point.Distance() failed."); 
            }
            if (a.Azimuth(b) !== 0) {
                throw new CRMapCtlError("point.js", 51, "Point.Azimuth() failed."); 
            }
            if (b.Azimuth(a) !== Math.PI) {
                throw new CRMapCtlError("point.js", 54, "Point.Azimuth() failed."); 
            }

            offsetPoint(a, 100, 200, 0);
            if (a.Distance(b, undefined) !== 200) {
                throw new CRMapCtlError("point.js", 49, "Point.Distance() failed."); 
            }
  
            a.Set(0,0,100,100);
            b.Set(100,100,0,0);
            if (a.Distance(b) !== b.Distance(a)) {
                throw new CRMapCtlError("point.js", 59, "Point.Distance() failed."); 
            }

            if (a.Azimuth(b) !== Math.PI/4) {
                throw new CRMapCtlError("point.js", 69, "Point.Azimuth() failed.");
            }

            if (b.Azimuth(a) !== Math.PI*5/4) {
                throw new CRMapCtlError("point.js", 73, "Point.Azimuth() failed.");
            }
  
            a.Set(0,0,0,0);
            b.Set(1,-1,0,0);
            if (a.Azimuth(b) !== Math.PI*7/4) {
                throw new CRMapCtlError("point.js", 79, "Point.Azimuth() failed.");
            }
            if (b.Azimuth(a) !== Math.PI*3/4) {
                throw new CRMapCtlError("point.js", 82, "Point.Azimuth() failed.");
            }

            a.Set(0,0,0,0);
            b.Set(0,0,0,0);
            if (a.Azimuth(b) !== 0) {
                throw new CRMapCtlError("point.js", 87, "Point.Azimuth() failed.");
            }

            output.value += "OK";
        } catch (err) {
            if (err.print) {
                output.value += "\r\n" + err.print();
            } else {
                output.value += "\r\n" + err.message;
            }
        }
    };


    if (! window.CRMapCtlTest) {
        window.CRMapCtlTest = {}
    }
    window.CRMapCtlTest.testPoint = test;
}(window));
