/**
 * layers.js
 *
 */
(function (window, undefined) {
    "use strict";

    var test = function(obMap, output, MAXNUM) {
        if (!MAXNUM) {
            MAXNUM = 1000;
        }
        output.value += "\r\ntest Layers (MAXNUM=" + MAXNUM + ") ...";

        try { 
            var i;

            // section A=B=C=D=E

            // section A
            //
            obMap.Layers.Clear();
            if (obMap.Layers.Count !== 0) {
                 throw new CRMapCtlError("layers.js", 1, "Layers.Clear() failed.");
            }

            i = 0;
            while (i++ < MAXNUM) {
                 var layer = obMap.Layers.Add();
                 if (obMap.Layers.Count != i) {
                     throw new CRMapCtlError("layers.js", 2, "Layers.Add() failed.");
                 }

                 var l = obMap.Layers("__struct_ILayer_"+(i-1));
                 if (!l) {
                     throw new CRMapCtlError("layers.js", 3, "Layers.Item() failed."); 
                 }   
            }

            // section B
            //
            obMap.Layers.Clear();
            if (obMap.Layers.Count !== 0) {
                 throw new CRMapCtlError("layers.js", 4, "Layers.Clear() failed.");
            }

            i = 0;
            while (i++ < MAXNUM) {
                 var layer = obMap.Layers.Add(null);
                 if (obMap.Layers.Count != i) {
                     throw new CRMapCtlError("layers.js", 5, "Layers.Add() failed.");
                 }

                 var l = obMap.Layers("__struct_ILayer_"+(i-1));
                 if (!l) {
                     throw new CRMapCtlError("layers.js", 6, "Layers.Item() failed."); 
                 }
            }

            // section C
            //
            obMap.Layers.Clear();
            if (obMap.Layers.Count !== 0) {
                 throw new CRMapCtlError("layers.js", 7, "Layers.Clear() failed.");
            }

            i = 0;
            while (i++ < MAXNUM) {
                 var layer = obMap.Layers.Add(null, -1);
                 if (obMap.Layers.Count != i) {
                     throw new CRMapCtlError("layers.js", 8, "Layers.Add() failed.");
                 }
                 
                 var l = obMap.Layers("__struct_ILayer_"+(i-1));
                 if (!l) {
                     throw new CRMapCtlError("layers.js", 9, "Layers.Item() failed."); 
                 }
            }

            // section D
            //
            obMap.Layers.Clear();
            if (obMap.Layers.Count !== 0) {
                 throw new CRMapCtlError("layers.js", 10, "Layers.Clear() failed.");
            }

            i = 0;
            var index = 0;
            while (i++ < MAXNUM) {
                 var layer = obMap.Layers.Add(null, index++);
                 if (obMap.Layers.Count != i) {
                     throw new CRMapCtlError("layers.js", 11, "Layers.Add() failed.");
                 }
                 
                 var l = obMap.Layers("__struct_ILayer_"+(i-1));
                 if (!l) {
                     throw new CRMapCtlError("layers.js", 12, "Layers.Item() failed."); 
                 }
            }

            // section E
            //
            obMap.Layers.Clear();
            if (obMap.Layers.Count !== 0) {
                 throw new CRMapCtlError("layers.js", 13, "Layers.Clear() failed.");
            }

            i = 0;
            var index = 0;
            while (i++ < MAXNUM) {
                 var layer = obMap.Layers.Add(index++);
                 if (obMap.Layers.Count != i) {
                     throw new CRMapCtlError("layers.js", 14, "Layers.Add() failed.");
                 }

                 var key = "__struct_ILayer_"+(i-1);
                 //var key = new String("__struct_ILayer_"+(i-1));

                 var l = obMap.Layers(key);
                 if (!l) {
                     throw new CRMapCtlError("layers.js", 15, "Layers.Item() failed."); 
                 }

                 if (l.Name != key) {
                     throw new CRMapCtlError("layers.js", 1501, "Layers.Name failed.");
                 }

                 l.Name = key;
                 l.Name = key + "_newName";
            }

            // test Remove()
            obMap.Layers.Clear();
            if (obMap.Layers.Count !== 0) {
                 throw new CRMapCtlError("layers.js", 16, "Layers.Clear() failed.");
            }

            i = 0;
            while (i++ < MAXNUM) {
                 var layer = obMap.Layers.Add("LayerName_" + i);
                 if (obMap.Layers.Count != i) {
                     throw new CRMapCtlError("layers.js", 17, "Layers.Add() failed.");
                 }
            }

            i = 0;
            while (i++ < MAXNUM) {
                 obMap.Layers.Remove("LayerName_" + i);
            }  
            if (obMap.Layers.Count !== 0) {
                 throw new CRMapCtlError("layers.js", 18, "Layers.Remove() failed.");
            }
 
            output.value += "OK";
        } catch (err) {
            if (err.print) {
                output.value += "\r\n" + err.print();
            } else {
                output.value += "\r\n" + err.message;
            }
        } 
    };

    if (! window.CRMapCtlTest) {
        window.CRMapCtlTest = {}
    }

    window.CRMapCtlTest.testLayers = test;
}(window));
