/**
 * shapes.js
 *
 */
(function (window, undefined) {
    "use strict";
  
    var test = function(obLayer) {
        try { 
            var shapes = obLayer.Shapes;

            if (shapes.Count != 0) {
                 throw new Error(2001, "Shapes.Count");
            }

            var i = 0;
            while (i++ < 1000) {
                 shapes.Add();

                 if (shapes.Count != i) {
                     throw new Error(2002, "Shapes.Count mismatched");
                 }
            }
 
            output.value += "OK";
        } catch (err) {
            if (err.print) {
                output.value += "\r\n" + err.print();
            } else {
                output.value += "\r\n" + err.message;
            }
        } 
    };

    if (! window.CRMapCtlTest) {
        window.CRMapCtlTest = {}
    }

    window.CRMapCtlTest.testShapes = test;
}(window));
