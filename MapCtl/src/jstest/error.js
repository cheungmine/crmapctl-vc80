/**
 * error.js
 *
 *   javascript test for CRMapCtl
 */

(function (window, undefined) {
    "use strict";

    var CRMapCtlError = function (source, lineno, msg) {
        this.name = "CRMapCtlError";
        this.message = msg;
        this.source = source;
        this.lineno = lineno;

        this.print = function () {
            if (this.source && this.lineno) {
                return "[" + this.source + " : " + this.lineno + "] <" + this.name + "> " + this.message;
            } else if (this.source) {
                return "[" + this.source + "] <" + this.name + "> " + this.message;
            } else if (this.lineno) {
                return "[" + this.lineno + "] <" + this.name + "> " + this.message;
            } else {
                return "<" + this.name + "> " + this.message;
            }
        };
    };

    CRMapCtlError.prototype = new Error;

    if (! window.CRMapCtlTest) {
        window.CRMapCtlTest = {}
    }
    window.CRMapCtlError = CRMapCtlError;
}(window));
