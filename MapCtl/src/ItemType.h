// ItemType.h - 集合元素基类
//
#ifndef _ITEM_TYPE_H__
#define _ITEM_TYPE_H__

#include <atlcom.h>

#ifndef MAX_ITEM_NAME_LEN
    #define MAX_ITEM_NAME_LEN  255
#endif


// IItemTypeDispatchImpl
//
template <class ICollType,
    class T, const IID* piid = &__uuidof(T), const GUID* plibid = &CAtlModule::m_libid,
    WORD wMajor = 1, WORD wMinor = 0, class tihclass = CComTypeInfoHolder>
class ATL_NO_VTABLE IItemTypeDispatchImpl : 
    public IDispatchImpl<T, piid, plibid, wMajor, wMinor, tihclass>
{
protected:
    CComBSTR    m_name;

    ICollType  *m_pIColl;

    IItemTypeDispatchImpl () : m_pIColl(0)
    {
    }

public:
    void InitData (const wchar_t *name, void *parent)
    {
        ATLASSERT(m_pIColl == 0);
        m_name = name;
        m_pIColl = (ICollType *) parent;
    }

    HRESULT ForceResetName (BSTR newVal)
    {
        return m_name.AssignBSTR(newVal);
    }

    STDMETHOD(get_Name)(BSTR* pVal)
    {
        return m_name.CopyTo(pVal);
    }

    STDMETHOD(put_Name)(BSTR newVal)
    {
        if (!newVal) {
            return E_POINTER;
        }

        if (::SysStringLen(newVal) > MAX_ITEM_NAME_LEN) {
            return E_INVALIDARG;
        }

        if ( ! wcsncmp(m_name, newVal, MAX_ITEM_NAME_LEN) ) {
            return S_OK;
        }

        if (! m_pIColl) {
            return m_name.AssignBSTR(newVal);
        }

        return m_pIColl->_RenameItem(dynamic_cast<T*>(this), newVal);
    }
};


#endif  // _ITEM_TYPE_H__
