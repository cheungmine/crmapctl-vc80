// Point.h : _CPoint ������

#pragma once

#include "Config.h"

#include "Coordref.h"

#include "./gdbapi/src/gdbapi.h"

// _CPoint

class ATL_NO_VTABLE _CPoint :
	public CComObjectRootEx<CComSingleThreadModel>,
    public CComCoClass<_CPoint>,
	public ISupportErrorInfo,
	public IDispatchImpl<IPoint, &IID_IPoint, &LIBID_CRMapCtlLib, 1, 0>
{
public:
    GDBPointZM  point;

	_CPoint()
	{
        ZeroMemory(&point, sizeof(point));
	}

DECLARE_NO_REGISTRY()


BEGIN_COM_MAP(_CPoint)
	COM_INTERFACE_ENTRY(IPoint)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid)
    {
	    static const IID* arr[] = 
	    {
		    &IID_IPoint
	    };

	    for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	    {
		    if (InlineIsEqualGUID(*arr[i],riid))
			    return S_OK;
	    }
	    return S_FALSE;
    }


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:
    STDMETHOD(get_X)(DOUBLE* pVal)
    {
        *pVal = point.X;
        return S_OK;
    }

    STDMETHOD(put_X)(DOUBLE newVal)
    {
        point.X = newVal;
        return S_OK;
    }
      
    STDMETHOD(get_Y)(DOUBLE* pVal)
    {
        *pVal = point.Y;
        return S_OK;
    }

    STDMETHOD(put_Y)(DOUBLE newVal)
    {
        point.Y = newVal;
        return S_OK;
    }
       
    STDMETHOD(get_Z)(DOUBLE* pVal)
    {
        *pVal = point.Z;
        return S_OK;
    }

    STDMETHOD(put_Z)(DOUBLE newVal)
    {
        point.Z = newVal;
        return S_OK;
    }

    STDMETHOD(get_M)(DOUBLE* pVal)
    {
        *pVal = point.M;
        return S_OK;
    }

    STDMETHOD(put_M)(DOUBLE newVal)
    {
        point.M = newVal;
        return S_OK;
    }
       
    STDMETHOD(get_ToShape)(IShape** ppVal)
    {
        return E_NOTIMPL;
    }
       
    STDMETHOD(Clone)(IPoint* src)
    {
        if (! src) {
            return E_POINTER;
        }
        memcpy(&point, &((_CPoint*) src)->point, sizeof(point));
        return S_OK;
    }

    STDMETHOD(Set)(DOUBLE x,DOUBLE y,DOUBLE z,DOUBLE m)
    {
        point.X = x;
        point.Y = y;
        point.Z = z;
        point.M = m;
        return S_OK;
    }

    STDMETHOD(Distance)(IPoint* to, VARIANT coordref, DOUBLE* val)
    {
        GDBAPI_RESULT res;
        if (! to) {
            return E_POINTER;
        }

        if ( VariantTypeIsNull(coordref) ) {
            res = GDBPointGetDistance((const GDBPointZ *) &point,
                (const GDBPointZ *) &((_CPoint*) to)->point,
                0,
                val);

            if (res == GDBAPI_SUCCESS) {
                return S_OK;
            } else {
                return E_FAIL;
            }
        } else if ( VariantTypeIsDispatch(coordref) ) {
            CComQIPtr<ICoordref> spCoordref(coordref.pdispVal);
            if (spCoordref) {
                //res = GDBPointGetDistance((const GDBPointZ *) &point,
                //    (const GDBPointZ *) &((_CPoint*) to)->point,
                //    ((CCoordref*) spCoordref.p)->hspatref,
                //    val);
                return E_NOTIMPL;
            } else {
                return E_INVALIDARG;
            }

            return E_NOTIMPL;
        } else if ( VariantTypeIsBSTR(coordref) ) {
            CComPtr<ICoordref> spCoordref;
            HRESULT hr = CCoordref::CreateInstance(&spCoordref);
            if (SUCCEEDED(hr)) {
                //hr = spCoordref->Set(coordref.bstrVal);
                //if (SUCCEEDED(hr)) {
                    //res = GDBPointGetDistance((const GDBPointZ *) &point,
                    //    (const GDBPointZ *) &((_CPoint*) to)->point,
                    //    ((CCoordref*) spCoordref.p)->hspatref,
                    //    val);
                    return E_NOTIMPL;
                //} else {
                //    return hr;
                //}
            } else {
                return hr;
            }
        }

        return E_INVALIDARG;
    }

    STDMETHOD(Azimuth)(IPoint* to, VARIANT coordref, DOUBLE* val)
    {
        GDBAPI_RESULT res;
        if (! to) {
            return E_POINTER;
        }

        if ( VariantTypeIsNull(coordref) ) {
            res = GDBPointGetAzimuth((const GDBPointZ *) &point,
                (const GDBPointZ *) &((_CPoint*) to)->point,
                0,
                val);

            if (res == GDBAPI_SUCCESS) {
                return S_OK;
            } else {
                return E_FAIL;
            }
        } else if ( VariantTypeIsDispatch(coordref) ) {
            CComQIPtr<ICoordref> spCoordref(coordref.pdispVal);
            if (spCoordref) {
                //res = GDBPointGetAzimuth((const GDBPointZ *) &point,
                //    (const GDBPointZ *) &((_CPoint*) to)->point,
                //    ((CCoordref*) spCoordref.p)->hspatref,
                //    val);
                return E_NOTIMPL;
            } else {
                return E_INVALIDARG;
            }

            return E_NOTIMPL;
        } else if ( VariantTypeIsBSTR(coordref) ) {
            CComPtr<ICoordref> spCoordref;
            HRESULT hr = CCoordref::CreateInstance(&spCoordref);
            if (SUCCEEDED(hr)) {
                //hr = spCoordref->Set(coordref.bstrVal);
                //if (SUCCEEDED(hr)) {
                    //res = GDBPointGetAzimuth((const GDBPointZ *) &point,
                    //    (const GDBPointZ *) &((_CPoint*) to)->point,
                    //    ((CCoordref*) spCoordref.p)->hspatref,
                    //    val);
                    return E_NOTIMPL;
                //} else {
                //    return hr;
                //}
            } else {
                return hr;
            }
        }

        return E_INVALIDARG;
    }

    STDMETHOD(Transform)(ICoordref *coordref)
    {
        return E_NOTIMPL;
    }
};

//DEL:OBJECT_ENTRY_AUTO(__uuidof(Point), _CPoint)

