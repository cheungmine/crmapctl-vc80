// Layer.cpp : CLayer 的实现
//
#include "Layer.h"


BOOL CLayer::IsDraw (CDrawInfo* di)
{
	// 是否显示
    if (! m_bVisible) {
		return FALSE;
    }
/*
	// 启用比例控制, 判断是否在控制之内
	if (m_bScaleEnabled){
		double fScale = cdi->pvw->getScale();			
		if (fScale < m_MinScale || fScale > m_MaxScale)
			return FALSE;
	}

	// 动态显示直接返回
	if (m_OpenMode == goOpenDynamic)
		return TRUE;
	
	// 瓦片方式，直接跳过
	if (m_pImageTiles)
		return TRUE;
	
	// 指定可见的数据范围与图层可视数据范围是否重叠
	if (! CG_rect_is_overlapped((const cgRect*)&((CBound*)m_spViewBound.p)->_box, (const cgRect*)&cdi->clipData))
		return FALSE;
*/
	return TRUE;
}


HRESULT CLayer::LoadFileSHP (LPCSTR filename)
{
    LOG_TRACE(filename);

    return S_OK;
}


HRESULT CLayer::LoadFileDWG (LPCSTR filename)
{
    LOG_TRACE(filename);

    return S_OK;
}


HRESULT CLayer::LoadFileDXF (LPCSTR filename)
{
    LOG_TRACE(filename);
    
    return S_OK;
}


HRESULT CLayer::LoadFilePNG (LPCSTR filename)
{
    LOG_TRACE(filename);
    
    return S_OK;
}


HRESULT CLayer::LoadFileBMP (LPCSTR filename)
{
    LOG_TRACE(filename);
    
    return S_OK;
}


HRESULT CLayer::LoadFileGIF (LPCSTR filename)
{
    LOG_TRACE(filename);
    
    return S_OK;
}


HRESULT CLayer::LoadFileJPG (LPCSTR filename)
{
    LOG_TRACE(filename);
    
    return S_OK;
}


HRESULT CLayer::LoadFileTIF (LPCSTR filename)
{
    LOG_TRACE(filename);
    
    return S_OK;
}

// CLayer
//

STDMETHODIMP CLayer::Open(VARIANT arg1, VARIANT arg2)
{
    std::wstring  pathfile;
    const wchar_t *ext = 0;

    if (VariantTypeIsBSTRWithMaxLen(arg1, MAX_PATH)) {
        // A file name specified
        ReplaceDevicePath(arg1.bstrVal, pathfile);

        LOG_TRACE("arg1=%s", CW2A(pathfile.c_str()));

        if (DirectoryExists(pathfile.c_str())) {
            LOG_TRACE("DirectoryExists()=TRUE");

            if (VariantTypeIsBSTRWithMaxLen(arg2, MAX_PATH)) {
                LOG_TRACE("arg2=%s", CW2A(arg2.bstrVal));

                if (! DirectoryExists(arg2.bstrVal)) {
                    // 不是目录, 是文件名, 组合目录与文件名
                    if (pathfile.at(pathfile.size()-1) != L'\\') {
                        pathfile.append(L"\\");
                    }
                    pathfile.append(arg2.bstrVal);

                    ext = pathfile_ext_w(pathfile.c_str());
                }
            }
        } else if (PathfileExists(pathfile.c_str())) {
            LOG_TRACE("PathfileExists()=TRUE");

            ext = pathfile_ext_w(pathfile.c_str());
        }
    }


    if (ext) {
        // 根据扩展名处理
        if (! _wcsicmp(ext, L".SHP")) {
            LoadFileSHP(CW2A(pathfile.c_str()));
        } else if (! _wcsicmp(ext, L".DWG")) {
            LoadFileDWG(CW2A(pathfile.c_str()));
        } else if (! _wcsicmp(ext, L".DXF")) {
            LoadFileDXF(CW2A(pathfile.c_str()));
        } else if (! _wcsicmp(ext, L".PNG")) {
            LoadFilePNG(CW2A(pathfile.c_str()));
        } else if (! _wcsicmp(ext, L".BMP")) {
            LoadFileBMP(CW2A(pathfile.c_str()));
        } else if (! _wcsicmp(ext, L".GIF")) {
            LoadFileGIF(CW2A(pathfile.c_str()));
        } else if (! _wcsicmp(ext, L".JPG") || ! _wcsicmp(ext, L".JPEG")) {
            LoadFileJPG(CW2A(pathfile.c_str()));
        } else if (! _wcsicmp(ext, L".TIF") || ! _wcsicmp(ext, L".TIFF")) {
            LoadFileTIF(CW2A(pathfile.c_str()));
        } else {
            return E_NOTIMPL;
        }
    }

    return E_NOTIMPL;
}
