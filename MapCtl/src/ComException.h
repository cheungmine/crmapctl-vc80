// ComException.h - COM 异常处理类
//
#ifndef _COM_EXCEPTION_H__
#define _COM_EXCEPTION_H__

/*
 * Com Exception handler - by Don Box
 *
 * Usage:
 * 	
    HRESULT  hres = S_OK;
    try {
        if (...) {
            throw COMException (E_FAIL, IID_Ixxx, 
                OLESTR("Error Source"), 
                OLESTR("Error Message"));
        }
    } catch (COMException& ce) {
        // Catch a com exception
        HRESULT hr = SetErrorInfo(0, ce.m_pei);
        ATLASSERT(SUCCEEDED(hr));
        ce.m_pei->Release();
        hres = ce.m_hres;
    } catch(...) {
        // Catch a C++ exception
        COMException ex(E_FAIL,
            IID_Ixxx,
            OLESTR("错误源"),
            OLESTR("错误信息"));

        HRESULT hr = SetErrorInfo(0, ex.m_pei);
        ATLASSERT(SUCCEEDED(hr));
        
        ex.m_pei->Release();
        hres = ex.m_hres;
    }
    return hres;
*
*/

struct COMException
{
    HRESULT		    m_hres;
    IErrorInfo*     m_pei;

    COMException (
        HRESULT hres,
        REFIID riid,
        const OLECHAR* pszSource,
        const OLECHAR* pszDesc,
        const OLECHAR* pszHelpFile=0,
        DWORD dwHelpContext=0
        ) : m_hres(S_OK), m_pei(0)
    {
        ICreateErrorInfo* pcei = 0;
        HRESULT hr = CreateErrorInfo(&pcei);
        ATLASSERT(SUCCEEDED(hr));
        
        hr = pcei->SetGUID(riid);
        ATLASSERT(SUCCEEDED(hr));

        if (pszSource) {
            hr = pcei->SetSource(const_cast<OLECHAR*> (pszSource));
            ATLASSERT(SUCCEEDED(hr));
        }

        if (pszDesc) {
            hr = pcei->SetDescription(const_cast<OLECHAR*> (pszDesc));
            ATLASSERT(SUCCEEDED(hr));
        }

        if (pszHelpFile) {
            hr = pcei->SetSource(const_cast<OLECHAR*> (pszHelpFile));
            ATLASSERT(SUCCEEDED(hr));
        }

        hr = pcei->SetHelpContext(dwHelpContext);
        ATLASSERT(SUCCEEDED(hr));
        
        m_hres = hres;
        
        hr = pcei->QueryInterface(IID_IErrorInfo, (void**) &m_pei);
        ATLASSERT(SUCCEEDED(hr));

        pcei->Release();		
    }
};

#endif  // _COM_EXCEPTION_H__
