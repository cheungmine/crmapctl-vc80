// Coldef.h : CColdef ������

#pragma once

#include "Config.h"

class CDataset;

// CColdef

class ATL_NO_VTABLE CColdef :
	public CComObjectRootEx<CComSingleThreadModel>,
    public CComCoClass<CColdef>,
	public ISupportErrorInfo,
	public IItemTypeDispatchImpl<IDataset, IColdef, &IID_IColdef, &LIBID_CRMapCtlLib, 1, 0>
{
public:
	CColdef()
	{
	}

DECLARE_NO_REGISTRY()


BEGIN_COM_MAP(CColdef)
	COM_INTERFACE_ENTRY(IColdef)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid)
    {
	    static const IID* arr[] = 
	    {
		    &IID_IColdef
	    };

	    for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	    {
		    if (InlineIsEqualGUID(*arr[i],riid))
			    return S_OK;
	    }
	    return S_FALSE;
    }



	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

};

//DEL:OBJECT_ENTRY_AUTO(__uuidof(Coldef), CColdef)
