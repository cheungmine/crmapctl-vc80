// LicenseManager.h
//
#ifndef _LICENSE_MANAGER_H__
#define _LICENSE_MANAGER_H__

#pragma warning (push)
#pragma warning (disable: 4996)

#include "./rc4/rc4.h"
#include "./rc4/md5.h"

#if defined(_WIN32_WCE) && !defined(SMARTPHONE2003_UI_MODEL) && \
    !defined(POCKETPC2003_UI_MODEL) && !defined(STANDARDSHELL_UI_MODEL)
    #define GET_DEVICE_UNIQUE_ID
#endif

#ifdef GET_DEVICE_UNIQUE_ID
    #include <GetDeviceUniqueId.h>
#endif

#ifndef GETDEVICEUNIQUEID_V1_OUTPUT
    #define GETDEVICEUNIQUEID_V1_OUTPUT  20
#endif


#if defined(SMARTPHONE2003_UI_MODEL) || defined(POCKETPC2003_UI_MODEL)
    #define MulDiv  MulDiv_Repl
#endif


static int MulDiv_Repl(int nNumber, int nNumerator, int nDenominator)
{
    return (int)(((LONGLONG)nNumber*(LONGLONG)nNumerator+nDenominator/2)/nDenominator);
}


static long get_seed(unsigned char *key, int nch)
{
    int  ch = 0;
    long seed = 0;

    for (ch=0; ch<nch; ch++) {
        seed += (long)key[ch];
    }

    return seed;
}


static int wstr2ansi(WCHAR *inWstr, char **outAnsi)
{
    *outAnsi = 0;
    int		cchNeeded = 0;

    if ( 0 == (cchNeeded = ::WideCharToMultiByte (0, 0, inWstr, -1, NULL, 0, NULL, NULL)) ) {
        return 0;
    }

    LPSTR	pstr = (LPSTR) malloc (cchNeeded) ;
    if ( !pstr ) {
        return 0;
    }
            
    if ( 0 == ::WideCharToMultiByte (0, 0, inWstr, -1, pstr, cchNeeded, NULL, NULL) ) {
        free(pstr);
        return 0;
    }

    *outAnsi = pstr;	
    return  cchNeeded;
}


static int ansi2wstr(const char *inAnsi, WCHAR **outWstr)
{
    *outWstr = 0;
    if (!inAnsi)
        return 0;

    DWORD cbSize = *(DWORD*)((char*)inAnsi-sizeof(DWORD)) + sizeof(char);
    LPSTR szStr = (char*)inAnsi;

    int   cchNeeded = ::MultiByteToWideChar(0, 0, szStr, -1, 0, 0);
    if (0 == cchNeeded)
        return 0;
    
    LPWSTR	pwstr = (LPWSTR) malloc (sizeof(WCHAR)*cchNeeded);
    if (!pwstr)
        return 0;

    if (0 == ::MultiByteToWideChar(0, 0, szStr, -1, pwstr, cchNeeded)){
        free(pwstr);
        return 0;
    }

    *outWstr = pwstr;
    return cchNeeded;
}


static HRESULT hex_encode (const BYTE value, LPTSTR pszBuffer, const DWORD cbBufferSize, DWORD *pcbAdded)
{
    static const TCHAR HexLookup[16] =  {
        _T('0'), _T('1'), _T('2'), _T('3'),
        _T('4'), _T('5'), _T('6'), _T('7'),
        _T('8'), _T('9'), _T('A'), _T('B'),
        _T('C'), _T('D'), _T('E'), _T('F')
    };

    HRESULT hr;

    if (cbBufferSize < 3) {
        hr = HRESULT_FROM_WIN32(ERROR_INSUFFICIENT_BUFFER);
    } else {
        *pszBuffer++ = HexLookup[(value >> 4) & 0x0F];
        *pszBuffer++ = HexLookup[value & 0x0F];
        *pszBuffer = _T('\0');

        *pcbAdded = 2;

        hr = S_OK;
    }

    return hr;
}


// 得到路径名，不包括文件
static BOOL GetModulePathW(HMODULE hInst, wchar_t  *wszPath, size_t wchSize)
{
    *wszPath = 0;

    if (0 == ::GetModuleFileNameW(hInst, wszPath, (DWORD) wchSize))
        return FALSE;
    wszPath[wchSize-1] = 0;

    wchar_t *pwch = wcsrchr(wszPath, L'\\');
    if (pwch){
        *++pwch = 0;
        return TRUE;
    }
    return FALSE;
}


// 向文件中添加 DeviceID
static BOOL  AppendDeviceID(const wchar_t *wszDeviceIDFile, char *szDeviceID, size_t chLen)
{
    // 首先检查是否已经存在设备标识号
    char   devId[255];
    FILE *fp = _wfopen(wszDeviceIDFile, L"r");

    if (fp) {
        while(!feof(fp)) {
            *devId = 0;
            fscanf(fp, "%s", devId);

            if (_strnicmp(szDeviceID, devId+1, chLen)==0){
                fclose(fp);
                return 1;
            }
        }
        fclose(fp);
    }

    HANDLE  hFile = ::CreateFileW(wszDeviceIDFile, GENERIC_WRITE, FILE_SHARE_WRITE, 0, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
    DWORD  dwWritten = 0;
    if (hFile==INVALID_HANDLE_VALUE){
        MessageBoxW(0, wszDeviceIDFile, L"GetDeviceID 无法打开指定文件", MB_ICONERROR|MB_OK);
        return 0;
    }

    // 移到文件末尾
    DWORD dwPtr = ::SetFilePointer(hFile, 0, 0, FILE_END);

    if (dwPtr == INVALID_SET_FILE_POINTER) {
        // Test for failure
        ::CloseHandle(hFile);
        MessageBoxW(0, wszDeviceIDFile, L"GetDeviceID 文件操作失败", MB_ICONERROR|MB_OK);
        return 0;
    }
        
    // 写消息
    char  hdr[] = "{";
    ::WriteFile(hFile, hdr, 1, &dwWritten, 0);

    if (!::WriteFile(hFile, szDeviceID, (DWORD) chLen, &dwWritten, 0) || chLen!=dwWritten){
        ::CloseHandle(hFile);
        MessageBoxW(0, wszDeviceIDFile, L"GetDeviceID 写文件异常", MB_ICONERROR|MB_OK);
        return 0;
    }

    char  end[] = "}\r\n";
    ::WriteFile(hFile, end, 3, &dwWritten, 0);

    ::CloseHandle(hFile);
    return 1;
}


// Generate printable version of device ID
static HRESULT DeviceID2String (
    const BYTE * const bDeviceID,
    const DWORD cbDeviceID,
    const LPTSTR pszIDAsString,
    const DWORD cbIDAsString)
{
    HRESULT   hr;
    LPTSTR    pszOutput           = pszIDAsString;
    DWORD     cbOutputRemaining   = cbIDAsString;
    DWORD     i;
    DWORD     cbAdded;

    for (i = 0; i < cbDeviceID; ++i) {
        hr = hex_encode (bDeviceID[i], pszOutput, cbOutputRemaining, &cbAdded);

        if (FAILED (hr)) {
            break;
        }

        cbOutputRemaining -= cbAdded;
        pszOutput += cbAdded;
    }

    return hr;
}


#ifdef GET_DEVICE_UNIQUE_ID

static HRESULT GetDeviceId(char devId[33])
{
    HRESULT        hr;
    
    DWORD          cbDeviceID = GETDEVICEUNIQUEID_V1_OUTPUT;
    BYTE           byDeviceID[GETDEVICEUNIQUEID_V1_OUTPUT];

    *devId = 0;

    // CRMapCtlLib UUID = {C3E5700C-B71C-408e-AE89-7780DFD98EE5}
    static const GUID bApplicationData  = {
        0xC3E5700C, 0xB71C, 0x408e, { 0xAE, 0x89, 0x77, 0x80, 0xDF, 0xD9, 0x8E, 0xE5 }
    };

    const DWORD    cbApplicationData = sizeof (bApplicationData);

    WCHAR          szDeviceID[(GETDEVICEUNIQUEID_V1_OUTPUT * 2) + 1];

    hr = ::GetDeviceUniqueID (
        reinterpret_cast<LPBYTE>(const_cast<LPGUID>(&bApplicationData)),
        cbApplicationData,
        GETDEVICEUNIQUEID_V1, 
        byDeviceID, 
        &cbDeviceID);

    if (FAILED(hr)) {
        return hr;
    }

    RC4_encrypt_string((char*)byDeviceID, cbDeviceID,
        (char*) &byDeviceID[4], GETDEVICEUNIQUEID_V1_OUTPUT-4);

    long lSeed = get_seed(byDeviceID, cbDeviceID);

    hr = DeviceID2String (byDeviceID, cbDeviceID,
        szDeviceID, sizeof(szDeviceID)/sizeof(szDeviceID[0]));

    if (FAILED(hr)) {
        return hr;
    }

    szDeviceID[GETDEVICEUNIQUEID_V1_OUTPUT * 2] = 0;

    char  *pbData = 0;
    int    cbData = wstr2ansi(szDeviceID, &pbData);

    MD5_hash_string((unsigned char*)pbData, cbData, lSeed, devId);

    free(pbData);

    devId[32] = 0;

    return S_OK;
}

#endif


static BOOL CheckLicValid(WCHAR *wlic, const char *devId)
{
    char  *lic = 0;
    int    nlic = 0;

    if (wlic && (nlic=wstr2ansi(wlic, &lic))==83) {
        char  key[20];
        char  hash[33];
        char  dev[33];

        if (lic[0]=='{' && lic[81]=='}') {
            strncpy(key, lic+65, 16);
            _strlwr(key);

            // 生成密码
            MD5_hash_string((unsigned char*)key, 16, get_seed((unsigned char*)key, 16), hash);

            // 解码
            RC4_hex_decode(&lic[1], 64, dev, 33);
            
            // 解密
            RC4_encrypt_string(dev, 32, hash, 16);
            dev[32] = 0;

            free(lic);

            return !strcmp(dev, devId);
        }
    }
    
    if (lic) {
        free(lic);
    }

    return FALSE;
}


#pragma warning (pop)

#endif // _LICENSE_MANAGER_H__
