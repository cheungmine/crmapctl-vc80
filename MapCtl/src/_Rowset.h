// Rowset.h : _CRowset ������

#pragma once

#include "Config.h"


// _CRowset

class ATL_NO_VTABLE _CRowset :
	public CComObjectRootEx<CComSingleThreadModel>,
    public CComCoClass<_CRowset>,
	public ISupportErrorInfo,
	public IDispatchImpl<IRowset, &IID_IRowset, &LIBID_CRMapCtlLib, 1, 0>
{
public:
	_CRowset()
	{
	}

DECLARE_NO_REGISTRY()


BEGIN_COM_MAP(_CRowset)
	COM_INTERFACE_ENTRY(IRowset)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid)
    {
	    static const IID* arr[] = 
	    {
		    &IID_IRowset
	    };

	    for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	    {
		    if (InlineIsEqualGUID(*arr[i],riid))
			    return S_OK;
	    }
	    return S_FALSE;
    }


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

};

//DEL:OBJECT_ENTRY_AUTO(__uuidof(Rowset), _CRowset)
