// _Color.h : _CColor ������
//
#pragma once

#include "Config.h"
#include "rgbacolor.h"

// _CColor

class ATL_NO_VTABLE _CColor :
    public CComObjectRootEx<CComSingleThreadModel>,
    public CComCoClass<_CColor>,
    public ISupportErrorInfo,
    public IDispatchImpl<IColor, &IID_IColor, &LIBID_CRMapCtlLib, 1, 0>
{
public:
    _CColor()
    {
        rgba.red = 0;
        rgba.green = 0;
        rgba.blue = 0;
        rgba.alpha = COLOR_ALPHA_OPAQUE;
    }

DECLARE_NO_REGISTRY()

DECLARE_NOT_AGGREGATABLE(_CColor)

BEGIN_COM_MAP(_CColor)
    COM_INTERFACE_ENTRY(IColor)
    COM_INTERFACE_ENTRY(IDispatch)
    COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
    STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);


    DECLARE_PROTECT_FINAL_CONSTRUCT()

    HRESULT FinalConstruct()
    {
        return S_OK;
    }

    void FinalRelease()
    {
    }

public:
    STDMETHOD(get_Alpha)(BYTE* pVal);
    STDMETHOD(put_Alpha)(BYTE newVal);
    STDMETHOD(get_Red)(BYTE* pVal);
    STDMETHOD(put_Red)(BYTE newVal);
    STDMETHOD(get_Green)(BYTE* pVal);
    STDMETHOD(put_Green)(BYTE newVal);
    STDMETHOD(get_Blue)(BYTE* pVal);
    STDMETHOD(put_Blue)(BYTE newVal);
    STDMETHOD(get_Hue)(BYTE* pVal);
    STDMETHOD(put_Hue)(BYTE newVal);
    STDMETHOD(get_Sat)(BYTE* pVal);
    STDMETHOD(put_Sat)(BYTE newVal);
    STDMETHOD(get_Lum)(BYTE* pVal);
    STDMETHOD(put_Lum)(BYTE newVal);
    STDMETHOD(get_OleColor)(OLE_COLOR* pVal);	
    STDMETHOD(put_OleColor)(OLE_COLOR newVal);
    STDMETHOD(get_WebColor)(BSTR* pVal);
    STDMETHOD(put_WebColor)(VARIANT newVal);
    STDMETHOD(get_RgbaColor)(BSTR* pVal);
	STDMETHOD(put_RgbaColor)(BSTR newVal);

    STDMETHOD(Neg)(VARIANT_BOOL hasAlpha = VARIANT_FALSE);
    STDMETHOD(Tune)(BYTE bVal);
    STDMETHOD(Clone)(IColor* source);    

private:
    rgba_color_t  rgba;
};


static const WCHAR * X11_COLOR_ENTRIES[] =
{
    L"aliceblue",               // RGB(240, 248, 255)    #f0f8ff  ����˹��
    L"antiquewhite",            // RGB(250, 235, 215)    #faebd7  �Ŷ���
    L"aqua",                    // RGB(  0, 255, 255)    #00ffff  ǳ��ɫ
    L"aquamarine",              // RGB(127, 255, 212)    #7fffd4  ����ɫ
    L"azure",                   // RGB(240, 255, 255)    #f0ffff  ����ɫ
    L"beige",                   // RGB(245, 245, 220)    #f5f5dc  ��ɫ
    L"bisque",                  // RGB(255, 228, 196)    #ffe4c4  �ۻ�ɫ
    L"black",                   // RGB(  0,   0,   0)    #000000  ��ɫ
    L"blanchedalmond",          // RGB(255, 235, 205)    #ffebcd  ����ɫ
    L"blue",                    // RGB(  0,   0, 255)    #0000ff  ��ɫ
    L"blueviolet",              // RGB(138,  43, 226)    #8a2be2  ������ɫ
    L"brown",                   // RGB(165,  42,  42)    #a52a2a  ��ɫ
    L"burlywood",               // RGB(222, 184, 135)    #deb887  ʵľɫ
    L"cadetblue",               // RGB( 95, 158, 160)    #5f9ea0  ����ɫ
    L"chartreuse",              // RGB(127, 255,   0)    #7fff00  ����ɫ
    L"chocolate",               // RGB(210, 105,  30)    #d2691e  �ɿ���ɫ
    L"coral",                   // RGB(255, 127,  80)    #ff7f50  ɺ��ɫ
    L"cornflowerblue",          // RGB(100, 149, 237)    #6495ed  ����ɫ
    L"cornsilk",                // RGB(255, 248, 220)    #fff8dc  �׳�ɫ
    L"crimson",                 // RGB(220,  20,  60)    #dc143c  �����ɫ
    L"cyan",                    // RGB(  0, 255, 255)    #00ffff  ��ɫ
    L"darkblue",                // RGB(  0,   0, 139)    #00008b  ����ɫ
    L"darkcyan",                // RGB(  0, 139, 139)    #008b8b  ����ɫ
    L"darkgoldenrod",           // RGB(184, 134,  11)    #b8860b  �����ɫ
    L"darkgray",                // RGB(169, 169, 169)    #a9a9a9  ����ɫ
    L"darkgreen",               // RGB(  0, 100,   0)    #006400  ����ɫ
    L"darkgrey",                // RGB(169, 169, 169)    #a9a9a9  ����ɫ
    L"darkkhaki",               // RGB(189, 183, 107)    #bdb76b  ���ƺ�ɫ
    L"darkmagenta",             // RGB(139,   0, 139)    #8b008b  �����
    L"darkolivegreen",          // RGB( 85, 107,  47)    #556b2f  �������
    L"darkorange",              // RGB(255, 140,   0)    #ff8c00  ���ۻ�ɫ
    L"darkorchid",              // RGB(153,  50, 204)    #9932cc  ����ɫ
    L"darkred",                 // RGB(139,   0,   0)    #8b0000  ����ɫ
    L"darksalmon",              // RGB(233, 150, 122)    #e9967a  ����ɫ
    L"darkseagreen",            // RGB(143, 188, 143)    #8fbc8f  ������ɫ
    L"darkslateblue",           // RGB( 72,  61, 139)    #483d8b  ������ɫ
    L"darkslategray",           // RGB( 47,  79,  79)    #2f4f4f  ���߻�ɫ
    L"darkslategrey",           // RGB( 47,  79,  79)    #2f4f4f  ���߻�ɫ
    L"darkturquoise",           // RGB(  0, 206, 209)    #00ced1  ����ʯ��
    L"darkviolet",              // RGB(148,   0, 211)    #9400d3  ��������ɫ
    L"deeppink",                // RGB(255,  20, 147)    #ff1493  ��ۺ�ɫ
    L"deepskyblue",             // RGB(  0, 191, 255)    #00bfff  ������ɫ
    L"dimgray",                 // RGB(105, 105, 105)    #696969  ����ɫ
    L"dimgrey",                 // RGB(105, 105, 105)    #696969  ����ɫ
    L"dodgerblue",              // RGB( 30, 144, 255)    #1e90ff  ����ɫ
    L"firebrick",               // RGB(178,  34,  34)    #b22222  ��שɫ
    L"floralwhite",             // RGB(255, 250, 240)    #fffaf0  ����ɫ
    L"forestgreen",             // RGB( 34, 139,  34)    #228b22  ɭ����
    L"fuchsia",                 // RGB(255,   0, 255)    #ff00ff  �Ϻ�ɫ
    L"gainsboro",               // RGB(220, 220, 220)    #dcdcdc  ����ɫ
    L"ghostwhite",              // RGB(248, 248, 255)    #f8f8ff  �����
    L"gold",                    // RGB(255, 215,   0)    #ffd700  ��ɫ
    L"goldenrod",               // RGB(218, 165,  32)    #daa520  ������ɫ
    L"gray",                    // RGB(128, 128, 128)    #808080  ��ɫ
    L"grey",                    // RGB(128, 128, 128)    #808080  ��ɫ
    L"green",                   // RGB(  0, 128,   0)    #008000  ��ɫ
    L"greenyellow",             // RGB(173, 255,  47)    #adff2f  ����ɫ
    L"honeydew",                // RGB(240, 255, 240)    #f0fff0  ��ɫ 
    L"hotpink",                 // RGB(255, 105, 180)    #ff69b4  �ȷۺ�ɫ
    L"indianred",               // RGB(205,  92,  92)    #cd5c5c  ӡ�ڰ���
    L"indigo",                  // RGB( 75,   0, 130)    #4b0082  ����ɫ
    L"ivory",                   // RGB(255, 255, 240)    #fffff0  ����ɫ
    L"khaki",                   // RGB(240, 230, 140)    #f0e68c  �ƺ�ɫ
    L"lavender",                // RGB(230, 230, 250)    #e6e6fa  ����ɫ
    L"lavenderblush",           // RGB(255, 240, 245)    #fff0f5  ���Ϻ�
    L"lawngreen",               // RGB(124, 252,   0)    #7cfc00  ����ɫ
    L"lemonchiffon",            // RGB(255, 250, 205)    #fffacd  ���ʳ�ɫ
    L"lightblue",               // RGB(173, 216, 230)    #add8e6  ����ɫ
    L"lightcoral",              // RGB(240, 128, 128)    #f08080  ��ɺ��ɫ
    L"lightcyan",               // RGB(224, 255, 255)    #e0ffff  ����ɫ
    L"lightgoldenrodyellow",    // RGB(250, 250, 210)    #fafad2  �����ɫ
    L"lightgray",               // RGB(211, 211, 211)    #d3d3d3  ����ɫ
    L"lightgreen",              // RGB(144, 238, 144)    #90ee90  ����ɫ 
    L"lightgrey",               // RGB(211, 211, 211)    #d3d3d3  ����ɫ
    L"lightpink",               // RGB(255, 182, 193)    #ffb6c1  ���ۺ�ɫ
    L"lightsalmon",             // RGB(255, 160, 122)    #ffa07a  ����ɫ
    L"lightseagreen",           // RGB( 32, 178, 170)    #20b2aa  ������ɫ
    L"lightskyblue",            // RGB(135, 206, 250)    #87cefa  ������ɫ
    L"lightslategray",          // RGB(119, 136, 153)    #778899  ������
    L"lightslategrey",          // RGB(119, 136, 153)    #778899  ������
    L"lightsteelblue",          // RGB(176, 196, 222)    #b0c4de  ������ɫ
    L"lightyellow",             // RGB(255, 255, 224)    #ffffe0  ����ɫ
    L"lime",                    // RGB(  0, 255,   0)    #00ff00  ���ɫ
    L"limegreen",               // RGB( 50, 205,  50)    #32cd32  ����ɫ
    L"linen",                   // RGB(250, 240, 230)    #faf0e6  ����ɫ
    L"magenta",                 // RGB(255,   0, 255)    #ff00ff  ����ɫ
    L"maroon",                  // RGB(128,   0,   0)    #800000  ��ɫ 
    L"mediumaquamarine",        // RGB(102, 205, 170)    #66cdaa  ����ɫ
    L"mediumblue",              // RGB(  0,   0, 205)    #0000cd  ����ɫ
    L"mediumorchid",            // RGB(186,  85, 211)    #ba55d3  �з���ɫ
    L"mediumpurple",            // RGB(147, 112, 219)    #9370db  ����ɫ
    L"mediumseagreen",          // RGB( 60, 179, 113)    #3cb371  �к���
    L"mediumslateblue",         // RGB(123, 104, 238)    #7b68ee  �а���ɫ
    L"mediumspringgreen",       // RGB(  0, 250, 154)    #00fa9a  �д���ɫ
    L"mediumturquoise",         // RGB( 72, 209, 204)    #48d1cc  ���̱�ʯ
    L"mediumvioletred",         // RGB(199,  21, 133)    #c71585  ��������ɫ
    L"midnightblue",            // RGB( 25,  25, 112)    #191970  �л���ɫ
    L"mintcream",               // RGB(245, 255, 250)    #f5fffa  ����ɫ
    L"mistyrose",               // RGB(255, 228, 225)    #ffe4e1  ǳõ��ɫ
    L"moccasin",                // RGB(255, 228, 181)    #ffe4b5  ¹Ƥɫ
    L"navajowhite",             // RGB(255, 222, 173)    #ffdead  ���߰�
    L"navy",                    // RGB(  0,   0, 128)    #000080  ����ɫ
    L"oldlace",                 // RGB(253, 245, 230)    #fdf5e6  �ϻ�ɫ
    L"olive",                   // RGB(128, 128,   0)    #808000  ���ɫ
    L"olivedrab",               // RGB(107, 142,  35)    #6b8e23  ���̺�ɫ
    L"orange",                  // RGB(255, 165,   0)    #ffa500  ��ɫ
    L"orangered",               // RGB(255,  69,   0)    #ff4500  ���ɫ
    L"orchid",                  // RGB(218, 112, 214)    #da70d6  ����ɫ
    L"palegoldenrod",           // RGB(238, 232, 170)    #eee8aa  ������ɫ
    L"palegreen",               // RGB(152, 251, 152)    #98fb98  ����ɫ
    L"paleturquoise",           // RGB(175, 238, 238)    #afeeee  �Ա�ʯ��
    L"palevioletred",           // RGB(219, 112, 147)    #db7093  ��������ɫ
    L"papayawhip",              // RGB(255, 239, 213)    #ffefd5  ��ľɫ
    L"peachpuff",               // RGB(255, 218, 185)    #ffdab9  ��ɫ
    L"peru",                    // RGB(205, 133,  63)    #cd853f  ��³ɫ
    L"pink",                    // RGB(255, 192, 203)    #ffc0cb  �ۺ�ɫ
    L"plum",                    // RGB(221, 160, 221)    #dda0dd  ����ɫ
    L"powderblue",              // RGB(176, 224, 230)    #b0e0e6  ����ɫ
    L"purple",                  // RGB(128,   0, 128)    #800080  ��ɫ
    L"red",                     // RGB(255,   0,   0)    #ff0000  ��ɫ
    L"rosybrown",               // RGB(188, 143, 143)    #bc8f8f  ��õ���
    L"royalblue",               // RGB( 65, 105, 225)    #4169e1  �ʼ���
    L"saddlebrown",             // RGB(139,  69,  19)    #8b4513  �غ�ɫ
    L"salmon",                  // RGB(250, 128, 114)    #fa8072  ����ɫ
    L"sandybrown",              // RGB(244, 164,  96)    #f4a460  ɳ��ɫ
    L"seagreen",                // RGB( 46, 139,  87)    #2e8b57  ����ɫ
    L"seashell",                // RGB(255, 245, 238)    #fff5ee  ����ɫ
    L"sienna",                  // RGB(160,  82,  45)    #a0522d  ��ɫ 
    L"silver",                  // RGB(192, 192, 192)    #c0c0c0  ��ɫ
    L"skyblue",                 // RGB(135, 206, 235)    #87ceeb  ����ɫ
    L"slateblue",               // RGB(106,  90, 205)    #6a5acd  ʯ��ɫ
    L"slategray",               // RGB(112, 128, 144)    #708090  ��ʯɫ
    L"slategrey",               // RGB(112, 128, 144)    #708090  ��ʯɫ
    L"snow",                    // RGB(255, 250, 250)    #fffafa  ѩ��ɫ
    L"springgreen",             // RGB(  0, 255, 127)    #00ff7f  ����ɫ
    L"steelblue",               // RGB( 70, 130, 180)    #4682b4  ����ɫ
    L"tan",                     // RGB(210, 180, 140)    #d2b48c  ��ɫ
    L"teal",                    // RGB(  0, 128, 128)    #008080  ˮѼɫ
    L"thistle",                 // RGB(216, 191, 216)    #d8bfd8  ��ɫ
    L"tomato",                  // RGB(255,  99,  71)    #ff6347  ������ɫ
    L"turquoise",               // RGB( 64, 224, 208)    #40e0d0  ����ɫ
    L"violet",                  // RGB(238, 130, 238)    #ee82ee  ������ɫ
    L"wheat",                   // RGB(245, 222, 179)    #f5deb3  ǳ��ɫ
    L"white",                   // RGB(255, 255, 255)    #ffffff  ��ɫ
    L"whitesmoke",              // RGB(245, 245, 245)    #f5f5f5  �̰�ɫ
    L"yellow",                  // RGB(255, 255,   0)    #ffff00  ��ɫ
    L"yellowgreen",             // RGB(154, 205,  50)    #9acd32  ����ɫ
    0
};


static const COLORREF X11_COLOR_PALETTE[] =
{
    RGB(240, 248, 255),    // #f0f8ff  ����˹��
    RGB(250, 235, 215),    // #faebd7  �Ŷ���
    RGB(  0, 255, 255),    // #00ffff  ǳ��ɫ
    RGB(127, 255, 212),    // #7fffd4  ����ɫ 
    RGB(240, 255, 255),    // #f0ffff  ����ɫ 
    RGB(245, 245, 220),    // #f5f5dc  ��ɫ  
    RGB(255, 228, 196),    // #ffe4c4  �ۻ�ɫ 
    RGB(  0,   0,   0),    // #000000  ��ɫ  
    RGB(255, 235, 205),    // #ffebcd  ����ɫ 
    RGB(  0,   0, 255),    // #0000ff  ��ɫ  
    RGB(138,  43, 226),    // #8a2be2  ������ɫ
    RGB(165,  42,  42),    // #a52a2a  ��ɫ  
    RGB(222, 184, 135),    // #deb887  ʵľɫ 
    RGB( 95, 158, 160),    // #5f9ea0  ����ɫ 
    RGB(127, 255,   0),    // #7fff00  ����ɫ 
    RGB(210, 105,  30),    // #d2691e  �ɿ���ɫ    
    RGB(255, 127,  80),    // #ff7f50  ɺ��ɫ 
    RGB(100, 149, 237),    // #6495ed  ����ɫ 
    RGB(255, 248, 220),    // #fff8dc  �׳�ɫ 
    RGB(220,  20,  60),    // #dc143c  �����ɫ    
    RGB(  0, 255, 255),    // #00ffff  ��ɫ  
    RGB(  0,   0, 139),    // #00008b  ����ɫ 
    RGB(  0, 139, 139),    // #008b8b  ����ɫ
    RGB(184, 134,  11),    // #b8860b  �����ɫ
    RGB(169, 169, 169),    // #a9a9a9  ����ɫ 
    RGB(  0, 100,   0),    // #006400  ����ɫ 
    RGB(169, 169, 169),    // #a9a9a9  ����ɫ
    RGB(189, 183, 107),    // #bdb76b  ���ƺ�ɫ
    RGB(139,   0, 139),    // #8b008b  ����� 
    RGB( 85, 107,  47),    // #556b2f  �������    
    RGB(255, 140,   0),    // #ff8c00  ���ۻ�ɫ    
    RGB(153,  50, 204),    // #9932cc  ����ɫ 
    RGB(139,   0,   0),    // #8b0000  ����ɫ 
    RGB(233, 150, 122),    // #e9967a  ����ɫ 
    RGB(143, 188, 143),    // #8fbc8f  ������ɫ    
    RGB( 72,  61, 139),    // #483d8b  ������ɫ    
    RGB( 47,  79,  79),    // #2f4f4f  ���߻�ɫ
    RGB( 47,  79,  79),    // #2f4f4f  ���߻�ɫ
    RGB(  0, 206, 209),    // #00ced1  ����ʯ��
    RGB(148,   0, 211),    // #9400d3  ��������ɫ   
    RGB(255,  20, 147),    // #ff1493  ��ۺ�ɫ
    RGB(  0, 191, 255),    // #00bfff  ������ɫ
    RGB(105, 105, 105),    // #696969  ����ɫ 
    RGB(105, 105, 105),    // #696969  ����ɫ 
    RGB( 30, 144, 255),    // #1e90ff  ����ɫ
    RGB(178,  34,  34),    // #b22222  ��שɫ 
    RGB(255, 250, 240),    // #fffaf0  ����ɫ 
    RGB( 34, 139,  34),    // #228b22  ɭ���� 
    RGB(255,   0, 255),    // #ff00ff  �Ϻ�ɫ 
    RGB(220, 220, 220),    // #dcdcdc  ����ɫ 
    RGB(248, 248, 255),    // #f8f8ff  ����� 
    RGB(255, 215,   0),    // #ffd700  ��ɫ  
    RGB(218, 165,  32),    // #daa520  ������ɫ    
    RGB(128, 128, 128),    // #808080  ��ɫ  
    RGB(128, 128, 128),    // #808080  ��ɫ  
    RGB(  0, 128,   0),    // #008000  ��ɫ  
    RGB(173, 255,  47),    // #adff2f  ����ɫ 
    RGB(240, 255, 240),    // #f0fff0  ��ɫ  
    RGB(255, 105, 180),    // #ff69b4  �ȷۺ�ɫ    
    RGB(205,  92,  92),    // #cd5c5c  ӡ�ڰ���    
    RGB( 75,   0, 130),    // #4b0082  ����ɫ 
    RGB(255, 255, 240),    // #fffff0  ����ɫ 
    RGB(240, 230, 140),    // #f0e68c  �ƺ�ɫ 
    RGB(230, 230, 250),    // #e6e6fa  ����ɫ 
    RGB(255, 240, 245),    // #fff0f5  ���Ϻ� 
    RGB(124, 252,   0),    // #7cfc00  ����ɫ 
    RGB(255, 250, 205),    // #fffacd  ���ʳ�ɫ
    RGB(173, 216, 230),    // #add8e6  ����ɫ 
    RGB(240, 128, 128),    // #f08080  ��ɺ��ɫ    
    RGB(224, 255, 255),    // #e0ffff  ����ɫ 
    RGB(250, 250, 210),    // #fafad2  �����ɫ
    RGB(211, 211, 211),    // #d3d3d3  ����ɫ
    RGB(144, 238, 144),    // #90ee90  ����ɫ 
    RGB(211, 211, 211),    // #d3d3d3  ����ɫ
    RGB(255, 182, 193),    // #ffb6c1  ���ۺ�ɫ    
    RGB(255, 160, 122),    // #ffa07a  ����ɫ 
    RGB( 32, 178, 170),    // #20b2aa  ������ɫ    
    RGB(135, 206, 250),    // #87cefa  ������ɫ
    RGB(119, 136, 153),    // #778899  ������ 
    RGB(119, 136, 153),    // #778899  ������ 
    RGB(176, 196, 222),    // #b0c4de  ������ɫ
    RGB(255, 255, 224),    // #ffffe0  ����ɫ 
    RGB(  0, 255,   0),    // #00ff00  ���ɫ 
    RGB( 50, 205,  50),    // #32cd32  ����ɫ 
    RGB(250, 240, 230),    // #faf0e6  ����ɫ 
    RGB(255,   0, 255),    // #ff00ff  ����ɫ 
    RGB(128,   0,   0),    // #800000  ��ɫ  
    RGB(102, 205, 170),    // #66cdaa  ����ɫ 
    RGB(  0,   0, 205),    // #0000cd  ����ɫ 
    RGB(186,  85, 211),    // #ba55d3  �з���ɫ
    RGB(147, 112, 219),    // #9370db  ����ɫ 
    RGB( 60, 179, 113),    // #3cb371  �к��� 
    RGB(123, 104, 238),    // #7b68ee  �а���ɫ
    RGB(  0, 250, 154),    // #00fa9a  �д���ɫ
    RGB( 72, 209, 204),    // #48d1cc  ���̱�ʯ
    RGB(199,  21, 133),    // #c71585  ��������ɫ   
    RGB( 25,  25, 112),    // #191970  �л���ɫ
    RGB(245, 255, 250),    // #f5fffa  ����ɫ 
    RGB(255, 228, 225),    // #ffe4e1  ǳõ��ɫ    
    RGB(255, 228, 181),    // #ffe4b5  ¹Ƥɫ 
    RGB(255, 222, 173),    // #ffdead  ���߰� 
    RGB(  0,   0, 128),    // #000080  ����ɫ 
    RGB(253, 245, 230),    // #fdf5e6  �ϻ�ɫ
    RGB(128, 128,   0),    // #808000  ���ɫ 
    RGB(107, 142,  35),    // #6b8e23  ���̺�ɫ
    RGB(255, 165,   0),    // #ffa500  ��ɫ  
    RGB(255,  69,   0),    // #ff4500  ���ɫ 
    RGB(218, 112, 214),    // #da70d6  ����ɫ 
    RGB(238, 232, 170),    // #eee8aa  ������ɫ
    RGB(152, 251, 152),    // #98fb98  ����ɫ
    RGB(175, 238, 238),    // #afeeee  �Ա�ʯ��    
    RGB(219, 112, 147),    // #db7093  ��������ɫ
    RGB(255, 239, 213),    // #ffefd5  ��ľɫ 
    RGB(255, 218, 185),    // #ffdab9  ��ɫ  
    RGB(205, 133,  63),    // #cd853f  ��³ɫ 
    RGB(255, 192, 203),    // #ffc0cb  �ۺ�ɫ 
    RGB(221, 160, 221),    // #dda0dd  ����ɫ 
    RGB(176, 224, 230),    // #b0e0e6  ����ɫ
    RGB(128,   0, 128),    // #800080  ��ɫ
    RGB(255,   0,   0),    // #ff0000  ��ɫ  
    RGB(188, 143, 143),    // #bc8f8f  ��õ���    
    RGB( 65, 105, 225),    // #4169e1  �ʼ��� 
    RGB(139,  69,  19),    // #8b4513  �غ�ɫ 
    RGB(250, 128, 114),    // #fa8072  ����ɫ 
    RGB(244, 164,  96),    // #f4a460  ɳ��ɫ 
    RGB( 46, 139,  87),    // #2e8b57  ����ɫ 
    RGB(255, 245, 238),    // #fff5ee  ����ɫ 
    RGB(160,  82,  45),    // #a0522d  ��ɫ  
    RGB(192, 192, 192),    // #c0c0c0  ��ɫ  
    RGB(135, 206, 235),    // #87ceeb  ����ɫ 
    RGB(106,  90, 205),    // #6a5acd  ʯ��ɫ
    RGB(112, 128, 144),    // #708090  ��ʯɫ 
    RGB(112, 128, 144),    // #708090  ��ʯɫ 
    RGB(255, 250, 250),    // #fffafa  ѩ��ɫ 
    RGB(  0, 255, 127),    // #00ff7f  ����ɫ 
    RGB( 70, 130, 180),    // #4682b4  ����ɫ 
    RGB(210, 180, 140),    // #d2b48c  ��ɫ  
    RGB(  0, 128, 128),    // #008080  ˮѼɫ 
    RGB(216, 191, 216),    // #d8bfd8  ��ɫ  
    RGB(255,  99,  71),    // #ff6347  ������ɫ
    RGB( 64, 224, 208),    // #40e0d0  ����ɫ
    RGB(238, 130, 238),    // #ee82ee  ������ɫ
    RGB(245, 222, 179),    // #f5deb3  ǳ��ɫ 
    RGB(255, 255, 255),    // #ffffff  ��ɫ  
    RGB(245, 245, 245),    // #f5f5f5  �̰�ɫ 
    RGB(255, 255,   0),    // #ffff00  ��ɫ  
    RGB(154, 205,  50)     // #9acd32  ����ɫ
};

#pragma warning (push)
#pragma warning (disable: 4996)

static VARIANT_BOOL X11ColorFind(const WCHAR *entry, COLORREF *rgb)
{
    int i = 0;

    while ( X11_COLOR_ENTRIES[i] && wcsicmp(X11_COLOR_ENTRIES[i], entry) ) {
        i++;
    }

    if (i < sizeof(X11_COLOR_PALETTE)/sizeof(X11_COLOR_PALETTE[0])) {
        *rgb = X11_COLOR_PALETTE[i];
        return VARIANT_TRUE;
    }

    return VARIANT_FALSE;
}

#pragma warning (pop)

//DEL:OBJECT_ENTRY_AUTO(__uuidof(Color), _CColor)
