// Rasters.h : CRasters ������

#pragma once

#include "Config.h"
#include "CollType.h"
#include "Raster.h"


typedef CComEnumOnSTL<IEnumVARIANT, &IID_IEnumVARIANT, VARIANT, 
    _CopyVariantFromAdaptItf<IRaster>,
    deque< CAdapt< CComPtr<IRaster> > > >
CComEnumVariantOnDequeOfRasters;

typedef ICollectionOnSTLArray<IDispatchImpl<IRasters, &IID_IRasters>,
    deque< CAdapt< CComPtr<IRaster> > >,
    IRaster*,
    _CopyItfFromAdaptItf<IRaster>,
    CComEnumVariantOnDequeOfRasters>
IRasterCollImpl;

typedef CAdapt< CComPtr<IRaster> >	    RASTER_ITEM;
typedef deque< RASTER_ITEM >::iterator  RASTER_ITER;

// CRasters

class ATL_NO_VTABLE CRasters :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CRasters>,
	public ISupportErrorInfo,
    public IRasterCollImpl
{
public:
    VARIANT_BOOL        m_bVisible;

	CRasters() : m_bVisible (VARIANT_TRUE)
	{
	}

DECLARE_NO_REGISTRY()


BEGIN_COM_MAP(CRasters)
	COM_INTERFACE_ENTRY(IRasters)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

    void Draw (CDrawInfo *di);

public:

    STDMETHOD(get_Visible)(VARIANT_BOOL* pVal)
    {
        *pVal = m_bVisible;
        return S_OK;
    }

    STDMETHOD(put_Visible)(VARIANT_BOOL newVal)
    {
        m_bVisible = newVal;
        return S_OK;
    }
};

//DEL:OBJECT_ENTRY_AUTO(__uuidof(Rasters), CRasters)
