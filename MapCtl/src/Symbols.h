// Symbols.h : CSymbols 的声明

#pragma once

#include "Config.h"
#include "CollType.h"
#include "Symbol.h"


typedef list< CAdapt< CComPtr<ISymbol> > > SymbolCollType;

typedef CComEnumOnSTL<IEnumVARIANT, &IID_IEnumVARIANT, VARIANT, 
    _CopyVariantFromAdaptItf<ISymbol>,
    SymbolCollType >
CComEnumVariantOnListOfSymbols;

typedef ICollOnSTLListWithMapImpl<IDispatchImpl<ISymbols, &IID_ISymbols>,
    SymbolCollType,
    ISymbol*,
    _CopyItfFromAdaptItf<ISymbol>,
    CComEnumVariantOnListOfSymbols,
    std::wstring,
    CSymbol,
    ISymbol >
ISymbolCollImpl;


typedef  COLL_ITER(vector, ISymbol)  SYMBOL_ITER;
typedef  COLL_RITER(vector, ISymbol) SYMBOL_RITER;

// 指定索引的指针
#define  SYMBOL_ITER_AT(it,index)  SYMBOL_ITER (##it) = m_coll.begin()+index


// CSymbols

class ATL_NO_VTABLE CSymbols :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CSymbols>,
	public ISupportErrorInfo,
    public ISymbolCollImpl
{
public:
	CSymbols()
	{
	}

DECLARE_NO_REGISTRY()

DECLARE_NOT_AGGREGATABLE(CSymbols)

BEGIN_COM_MAP(CSymbols)
	COM_INTERFACE_ENTRY(ISymbols)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

};

//DEL:OBJECT_ENTRY_AUTO(__uuidof(Symbols), CSymbols)
