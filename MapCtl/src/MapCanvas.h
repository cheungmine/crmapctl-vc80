// MapCanvas.h : CMapCanvas 的声明
//
#pragma once

#ifndef _WIN32_WCE
    #include "../CRMapCtl/resource.h"       // 主符号
#endif

#include <atlctl.h>                      // IObjectSafetyImpl

#include "../CRMapCtl/CRMapCtl.h"

#include "_IMapCanvasEvents_CP.h"

#include "IdlType.h"

#include "Bound.h"
#include "Layers.h"
#include "Layout.h"
#include "Connections.h"
#include "ImageButtons.h"
#include "GpsDevice.h"
#include "TrackingShape.h"

#include "Config.h"


#ifndef _WIN32_WCE

class CMapCanvasLic
{
protected:
   static BOOL VerifyLicenseKey(BSTR bstr)
   {
      return !lstrcmpW(bstr, L"MapCanvas 许可");
   }


   static BOOL GetLicenseKey(DWORD dwReserved, BSTR* pBstr)
   {
      if( pBstr == NULL )
        return FALSE;
      *pBstr = SysAllocString(L"MapCanvas 许可");
      return TRUE;
   }


   static BOOL IsLicenseValid()
   {
       return TRUE;
   }
};

#endif


// CMapCanvas
class ATL_NO_VTABLE CMapCanvas :
    public CComObjectRootEx<CComSingleThreadModel>,
    public CStockPropImpl<CMapCanvas, IMapCanvas>,
    public IPersistStreamInitImpl<CMapCanvas>,
    public IOleControlImpl<CMapCanvas>,
    public IOleObjectImpl<CMapCanvas>,
    public IOleInPlaceActiveObjectImpl<CMapCanvas>,
    public IViewObjectExImpl<CMapCanvas>,
    public IOleInPlaceObjectWindowlessImpl<CMapCanvas>,
    public ISupportErrorInfo,
    public IConnectionPointContainerImpl<CMapCanvas>,
    public CProxy_IMapCanvasEvents<CMapCanvas>,
    public IObjectWithSiteImpl<CMapCanvas>,
    public IServiceProviderImpl<CMapCanvas>,
    public IPersistStorageImpl<CMapCanvas>,
    public ISpecifyPropertyPagesImpl<CMapCanvas>,
    public IQuickActivateImpl<CMapCanvas>,

#ifndef _WIN32_WCE
    public IDataObjectImpl<CMapCanvas>,
#endif

    public IProvideClassInfo2Impl<&CLSID_MapCanvas, &__uuidof(_IMapCanvasEvents), &LIBID_CRMapCtlLib>,
    public IPropertyNotifySinkCP<CMapCanvas>,

    // 要在 Windows CE 上正确加载该控件，要求 IObjectSafety
    public IObjectSafetyImpl<CMapCanvas, INTERFACESAFE_FOR_UNTRUSTED_CALLER|INTERFACESAFE_FOR_UNTRUSTED_DATA>,
    public CComCoClass<CMapCanvas, &CLSID_MapCanvas>,
    public CComControl<CMapCanvas>
{
public:

    CComPtr<ILayers>           m_spLayers;
    CComPtr<ILayout>           m_spLayout;
    CComPtr<IConnections>      m_spConnections;
    CComPtr<IImageButtons>     m_spImageButtons;
    CComPtr<IGpsDevice>        m_spGpsDevice;
    CComPtr<ILayer>            m_spTrackingLayer;
    CComPtr<ITrackingShape>    m_spTrackingShape;

    CDrawInfo       _drawinfo;

    CMapCanvas()
    {
        // 必须创建自己的窗口
        m_bWindowOnly =  TRUE;

        m_clrBackColor = 0xFFFFFF;
    }


#ifndef _WIN32_WCE
    DECLARE_CLASSFACTORY2(CMapCanvasLic)
#endif

DECLARE_OLEMISC_STATUS(OLEMISC_RECOMPOSEONRESIZE |
    OLEMISC_CANTLINKINSIDE |
    OLEMISC_INSIDEOUT |
    OLEMISC_ACTIVATEWHENVISIBLE |
    OLEMISC_SETCLIENTSITEFIRST
)

//#ifndef _CE_DCOM
DECLARE_REGISTRY_RESOURCEID(IDR_MAPCANVAS)
//#endif


BEGIN_COM_MAP(CMapCanvas)
    COM_INTERFACE_ENTRY(IMapCanvas)
    COM_INTERFACE_ENTRY(IDispatch)
    COM_INTERFACE_ENTRY(IViewObjectEx)
    COM_INTERFACE_ENTRY(IViewObject2)
    COM_INTERFACE_ENTRY(IViewObject)
    COM_INTERFACE_ENTRY(IOleInPlaceObjectWindowless)
    COM_INTERFACE_ENTRY(IOleInPlaceObject)
    COM_INTERFACE_ENTRY2(IOleWindow, IOleInPlaceObjectWindowless)
    COM_INTERFACE_ENTRY(IOleInPlaceActiveObject)
    COM_INTERFACE_ENTRY(IOleControl)
    COM_INTERFACE_ENTRY(IOleObject)
    COM_INTERFACE_ENTRY(IPersistStreamInit)
    COM_INTERFACE_ENTRY2(IPersist, IPersistStreamInit)
    COM_INTERFACE_ENTRY(ISupportErrorInfo)
    COM_INTERFACE_ENTRY(IConnectionPointContainer)
    COM_INTERFACE_ENTRY(ISpecifyPropertyPages)
    COM_INTERFACE_ENTRY(IQuickActivate)
    COM_INTERFACE_ENTRY(IPersistStorage)
#ifndef _WIN32_WCE
    COM_INTERFACE_ENTRY(IDataObject)
#endif
    COM_INTERFACE_ENTRY(IProvideClassInfo)
    COM_INTERFACE_ENTRY(IProvideClassInfo2)
    COM_INTERFACE_ENTRY(IObjectWithSite)
    COM_INTERFACE_ENTRY(IServiceProvider)

    // 要在 Windows CE 上正确加载该控件，要求 IObjectSafety
    COM_INTERFACE_ENTRY_IID(IID_IObjectSafety, IObjectSafety)
END_COM_MAP()


BEGIN_PROP_MAP(CMapCanvas)
    PROP_DATA_ENTRY("_cx", m_sizeExtent.cx, VT_UI4)
    PROP_DATA_ENTRY("_cy", m_sizeExtent.cy, VT_UI4)
    PROP_ENTRY("Appearance", DISPID_APPEARANCE, CLSID_NULL)
    PROP_ENTRY("BackColor", DISPID_BACKCOLOR, CLSID_NULL)
    PROP_ENTRY("Enabled", DISPID_ENABLED, CLSID_NULL)
    PROP_ENTRY("WindowOnly", DISPID_WINDOWONLY, CLSID_NULL)
    // 示例项
    // PROP_ENTRY("Property Description", dispid, clsid)
    // PROP_PAGE(CLSID_StockColorPage)
END_PROP_MAP()


BEGIN_CONNECTION_POINT_MAP(CMapCanvas)
    CONNECTION_POINT_ENTRY(IID_IPropertyNotifySink)
    CONNECTION_POINT_ENTRY(__uuidof(_IMapCanvasEvents))
END_CONNECTION_POINT_MAP()


#ifndef _WIN32_WCE
// 加入到CATID_SafeForScripting 和 CATID_SafeForInitializing COM分组
BEGIN_CATEGORY_MAP(CMapCanvas)
    IMPLEMENTED_CATEGORY(CATID_SafeForScripting)
    IMPLEMENTED_CATEGORY(CATID_SafeForInitializing)
    IMPLEMENTED_CATEGORY(CATID_Control)
    IMPLEMENTED_CATEGORY(CATID_Programmable)
	IMPLEMENTED_CATEGORY(CATID_PersistsToStreamInit)
	IMPLEMENTED_CATEGORY(CATID_PersistsToStorage)
	IMPLEMENTED_CATEGORY(CATID_Insertable)
END_CATEGORY_MAP()
#endif


BEGIN_MSG_MAP(CMapCanvas)
    MESSAGE_HANDLER(WM_CREATE, OnCreate)
	MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_CONTEXTMENU, OnContextMenu)
    MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBkgnd)

	MESSAGE_HANDLER(WM_LBUTTONDOWN, OnLButtonDown)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnLButtonUp)
	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)

    CHAIN_MSG_MAP(CComControl<CMapCanvas>)
    DEFAULT_REFLECTION_HANDLER()
END_MSG_MAP()


// 处理程序原型:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

    LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnContextMenu(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnEraseBkgnd(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnLButtonDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnLButtonUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);


    //
    // IObjectSafetyImpl
    //
    STDMETHODIMP GetInterfaceSafetyOptions(REFIID riid, DWORD *pdwSupportedOptions, DWORD *pdwEnabledOptions)
    {
        LOG_TRACE0();
        
        if (!pdwSupportedOptions || !pdwEnabledOptions) {
            return E_FAIL;
        }

        LPUNKNOWN pUnk;

        if (_InternalQueryInterface (riid, (void**)&pUnk) == E_NOINTERFACE) {
            // Our object doesn't even support this interface.
            return E_NOINTERFACE;
        } else {
            // Cleanup after ourselves.
            pUnk->Release();
            pUnk = NULL;
        }

        if (riid == IID_IDispatch) {
            // IDispatch is an interface used for scripting. If your control supports 
            // other IDispatch or Dual interfaces, you may decide to add them here as well. 
            // Client wants to know if object is safe for scripting. Only indicate safe 
            // for scripting when the interface is safe.
            *pdwSupportedOptions = INTERFACESAFE_FOR_UNTRUSTED_CALLER;
            *pdwEnabledOptions = m_dwCurrentSafety & INTERFACESAFE_FOR_UNTRUSTED_CALLER;
            return S_OK;
        } else if ((riid == IID_IPersistStreamInit) || (riid == IID_IPersistStorage)) {
            // IID_IPersistStreamInit and IID_IPersistStorage are interfaces used for Initialization. 
            // If your control supports other Persistence interfaces, you may decide to add them here 
            // as well. Client wants to know if object is safe for initializing. Only indicate safe 
            // for initializing when the interface is safe.
            *pdwSupportedOptions = INTERFACESAFE_FOR_UNTRUSTED_DATA;
            *pdwEnabledOptions = m_dwCurrentSafety & INTERFACESAFE_FOR_UNTRUSTED_DATA;
            return S_OK;
        } else {
            // We are saying that no other interfaces in this control are safe for initializing or scripting.
            *pdwSupportedOptions = 0;
            *pdwEnabledOptions = 0;
            return E_FAIL;
        }
    }

    STDMETHODIMP SetInterfaceSafetyOptions(REFIID riid, DWORD dwOptionSetMask, DWORD dwEnabledOptions)
    {
        LOG_TRACE0();
        
        if (!dwOptionSetMask && !dwEnabledOptions) {
            return E_FAIL;
        }

        LPUNKNOWN pUnk;

        if (_InternalQueryInterface (riid, (void**)&pUnk) == E_NOINTERFACE) {
            // Our object doesn't even support this interface.
            return E_NOINTERFACE;
        } else {
            // Cleanup after ourselves.
            pUnk->Release();
            pUnk = NULL;
        }

        // Store our current safety level to return in GetInterfaceSafetyOptions
        m_dwCurrentSafety |= dwEnabledOptions & dwOptionSetMask;

        if ((riid == IID_IDispatch) && (m_dwCurrentSafety & INTERFACESAFE_FOR_UNTRUSTED_CALLER)) {
            // Client wants us to disable any functionality that would make the control 
            // unsafe for scripting. The same applies to any other IDispatch or Dual 
            // interfaces your control may support. Because our control is safe for 
            // scripting by default we just return S_OK.
            return S_OK;
        } else if (((riid == IID_IPersistStreamInit) || (riid == IID_IPersistStorage)) &&
            (m_dwCurrentSafety & INTERFACESAFE_FOR_UNTRUSTED_DATA)) {
            // Client wants us to make the control safe for initializing
            // from persistent data. For these interfaces, this control
            // is safe so we return S_OK. For Any interfaces that are not
            // safe, we would return E_FAIL.
            return S_OK;
        } else {
            // This control doesn't allow Initialization or Scripting from 
            // any other interfaces so return E_FAIL.
            return E_FAIL;
        }
    }


    STDMETHOD(SetObjectRects)(LPCRECT prcPos, LPCRECT prcClip)
    {
        HRESULT hr = IOleInPlaceObjectWindowlessImpl<CMapCanvas>::SetObjectRects(prcPos, prcClip);
        
//		if (m_bWndLess)
//			ResizeCanvas((short)(prcPos->right-prcPos->left), (short)(prcPos->bottom-prcPos->top));

        return hr;
    }


    STDMETHOD(InPlaceDeactivate)(void)
    {
        LOG_TRACE0();

        HRESULT hr = IOleInPlaceObjectWindowlessImpl<CMapCanvas>::InPlaceDeactivate();
        
		Fire_OnMapFinal();

        return hr;
    }


    STDMETHOD(OnPostVerbInPlaceActivate)()
    {
        LOG_TRACE0();

//#ifndef _WIN32_WCE
        // WinCE 无法在此处通知事件
        Fire_OnMapReady();
//#endif

        return S_OK;
    }


    STDMETHOD(GetControlInfo)(LPCONTROLINFO pCI)
    {
        if (!pCI) {
            return E_POINTER;
        }
        
        pCI->hAccel = 0;
        pCI->cAccel = 0;
        pCI->dwFlags = 0;

        return S_OK;
    }


    // ISupportsErrorInfo
    STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid)
    {
        static const IID* arr[] = {
            &IID_IMapCanvas,
        };

        for (int i=0; i<sizeof(arr)/sizeof(arr[0]); i++) {
            if (InlineIsEqualGUID(*arr[i], riid)) {
                return S_OK;
            }
        }

        return S_FALSE;
    }


    // IViewObjectEx
    DECLARE_VIEW_STATUS(VIEWSTATUS_SOLIDBKGND | VIEWSTATUS_OPAQUE)


// IMapCanvas
public:
    HRESULT OnDraw(ATL_DRAWINFO& di)
    {
        RECT& rc = *(RECT*)di.prcBounds;

        // 将剪辑区域设置为 di.prcBounds 指定的矩形
        HRGN hRgnOld = NULL;
        if (GetClipRgn(di.hdcDraw, hRgnOld) != 1) {
            hRgnOld = NULL;
        }
        bool bSelectOldRgn = false;

        HRGN hRgnNew = CreateRectRgn(rc.left, rc.top, rc.right, rc.bottom);
        if (hRgnNew != NULL) {
            bSelectOldRgn = (SelectClipRgn(di.hdcDraw, hRgnNew) != ERROR);
        }

        Rectangle(di.hdcDraw, rc.left, rc.top, rc.right, rc.bottom);
        SetTextAlign(di.hdcDraw, TA_CENTER|TA_BASELINE);
        LPCTSTR pszText = _T("ATL 8.0 : MapCanvas");
#ifndef _WIN32_WCE
        TextOut(di.hdcDraw,
            (rc.left + rc.right) / 2,
            (rc.top + rc.bottom) / 2,
            pszText,
            lstrlen(pszText));
#else
        ExtTextOut(di.hdcDraw,
            (rc.left + rc.right) / 2,
            (rc.top + rc.bottom) / 2,
            ETO_OPAQUE,
            NULL,
            pszText,
            ATL::lstrlen(pszText),
            NULL);
#endif

        if (bSelectOldRgn) {
            SelectClipRgn(di.hdcDraw, hRgnOld);
        }
        if (hRgnNew) {
            DeleteObject(hRgnNew);
        }

        return S_OK;
    }


    SHORT m_nAppearance;
    void OnAppearanceChanged()
    {
        LOG_TRACE0();
    }


    OLE_COLOR m_clrBackColor;
	STDMETHOD(put_BackColor)(OLE_COLOR clr)
	{
		m_clrBackColor = clr;
		return S_OK;
	}
	STDMETHOD(get_BackColor)(OLE_COLOR *clr)
	{
		*clr = m_clrBackColor;
		return S_OK;
	}


    BOOL m_bEnabled;
    void OnEnabledChanged()
    {
        LOG_TRACE0();
    }


  	STDMETHOD(put_WindowOnly)(VARIANT_BOOL vbool)
	{
		m_bWindowOnly = TRUE;
		return S_OK;
	}

    STDMETHOD(get_WindowOnly)(VARIANT_BOOL* pbool)
	{
		*pbool = VARIANT_TRUE;
		return S_OK;
	}


    STDMETHOD(_InternalQueryService)(REFGUID guidService, REFIID riid, void** ppvObject)
    {
        return E_NOTIMPL;
    }


    DECLARE_PROTECT_FINAL_CONSTRUCT()


    HRESULT FinalConstruct()
    {
        LOG_TRACE0();

        HRESULT hr;

        hr = CLayers::CreateInstance(&m_spLayers);

        hr = CLayout::CreateInstance(&m_spLayout);

        hr = CConnections::CreateInstance(&m_spConnections);

        hr = CImageButtons::CreateInstance(&m_spImageButtons);

        hr = CGpsDevice::CreateInstance(&m_spGpsDevice);

        hr = CLayer::CreateInstance(&m_spTrackingLayer);

        hr = CTrackingShape::CreateInstance(&m_spTrackingShape);

        return hr;
    }


    void FinalRelease()
    {
        LOG_TRACE0();
    }


public:

    STDMETHOD(get_Layers)(ILayers** ppVal)
    {
        return m_spLayers.CopyTo(ppVal);
    }

    STDMETHOD(get_Layout)(ILayout** ppVal)
    {
        return m_spLayout.CopyTo(ppVal);
    }

    STDMETHOD(get_Connections)(IConnections** ppVal)
    {
        return m_spConnections.CopyTo(ppVal);
    }

    STDMETHOD(get_GpsDevice)(IGpsDevice** ppVal)
    {
        return m_spGpsDevice.CopyTo(ppVal);
    }

    STDMETHOD(get_ImageButtons)(IImageButtons** ppVal)
    {
        return m_spImageButtons.CopyTo(ppVal);
    }

    STDMETHOD(get_TrackingLayer)(ILayer** ppVal)
    {
        return m_spTrackingLayer.CopyTo(ppVal);
    }

    STDMETHOD(get_TrackingShape)(ITrackingShape** ppVal)
    {
        return m_spTrackingShape.CopyTo(ppVal);
    }

    STDMETHOD(CreateObject)(VARIANT coClassName, IDispatch** ppObject);

};

OBJECT_ENTRY_AUTO(__uuidof(MapCanvas), CMapCanvas)
