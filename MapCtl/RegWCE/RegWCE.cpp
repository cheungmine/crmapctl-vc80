// RegWCE.cpp
//

#include "../src/RegsvrCE.h"

#define WCE_DLL      L"CRMapCtl.dll"
#define REG_WCE      L"RegWCE CRMapCtl.dll"


int _tmain(int argc, _TCHAR* argv[])
{
    WCHAR ERR_MSG[128];
    WCHAR INFO_MSG[256];

    LOG_INIT();

    int ret = RegsvrWCE (WCE_DLL, ERR_MSG);

    if (ret == REGCE_ERROR_LOADLIBRARY) {
        wsprintfW(INFO_MSG, L"Error on LoadLibrary '%s': %s", WCE_DLL, ERR_MSG);

        ::MessageBoxW(0, INFO_MSG, REG_WCE, MB_OK|MB_ICONSTOP);
    } else if (ret == REGCE_ERROR_GET_DLLREG_PROC) {
        wsprintfW(INFO_MSG, L"Error on GetProcAddress '%s': %s", DLLREG_PROC, ERR_MSG);

        ::MessageBoxW(0, INFO_MSG, REG_WCE, MB_OK|MB_ICONSTOP);
    } else if (ret == REGCE_ERROR_CALL_DLLREG_PROC) {
        wsprintfW(INFO_MSG, L"Error on %s: %s", DLLREG_PROC, ERR_MSG);

        ::MessageBoxW(0, INFO_MSG, REG_WCE, MB_OK|MB_ICONSTOP);
    } else if (ret == REGCE_SUCCESS) {
        wsprintfW(INFO_MSG, L"Success on %s: %s", DLLREG_PROC, WCE_DLL);

        ::MessageBoxW(0, INFO_MSG, REG_WCE, MB_OK|MB_ICONINFORMATION);
    } else {
        wsprintfW(INFO_MSG, L"Error on %s: %s", DLLREG_PROC, WCE_DLL);

        ::MessageBoxW(0, INFO_MSG, REG_WCE, MB_OK|MB_ICONSTOP);
    }

    LOG_FINI();
	return 0;
}
