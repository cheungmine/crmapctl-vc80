; CRMapCtlWinn32Setup.iss
; -- CRMapCtl Inno Setup Script for Windows --
;
[Setup]
AppName=CRMapCtl
AppVerName=CRMapCtl
AppCopyright=Copyright (C) 2014,2015 cheungmine
DefaultDirName={pf}\DigitalCloudMap\CRMapCtl
DefaultGroupName=CRMapCtl
UninstallDisplayIcon={app}\about.htm
Compression=lzma
SolidCompression=yes
OutputDir=Bin
OutputBaseFilename=CRMapCtl-1.0-Setup

VersionInfoCompany=cheungmine
VersionInfoCopyright=(C) cheungmine, 2014-?, All rights Reserved.
VersionInfoDescription=CRMapCtl
VersionInfoProductName=CRMapCtl
VersionInfoProductVersion=1.0
VersionInfoTextVersion=1.0
VersionInfoVersion=1.0
AppPublisher=
AppPublisherURL=
AppVersion=1.0

EnableDirDoesntExistWarning=yes
DirExistsWarning=yes

LicenseFile="..\..\doc\LICENCE.lic"
SetupLogging=yes

;WizardImageFile=crmapctl-wz.bmp
;WizardSmallImageFile=crmapctl-small.bmp
;SetupIconFile=crmapctl.ico


[LangOptions]
;LanguageName=English
;LanguageID=$0409
LanguageName=Chinese
LanguageID=0x0C04
LanguageCodePage=0
DialogFontName=Arial
DialogFontSize=9
WelcomeFontName=Verdana
WelcomeFontSize=12
TitleFontName=Verdana
TitleFontSize=29
CopyrightFontName=Arial
CopyrightFontSize=9
RightToLeft=no

[Files]
Source: "..\..\libproj4\Release\libproj4.dll"; DestDir: "{app}"
Source: "..\..\libgdbapi\Release\libgdbapi.dll"; DestDir: "{app}"
Source: "..\..\log4c-embed-win\release\liblog4c.dll"; DestDir: "{app}"
Source: "..\..\CRMapCtl\Release\CRMapCtl.dll"; DestDir: "{app}"; Flags: regserver
Source: "..\..\doc\LICENCE.pdf"; DestDir: "{app}"
Source: "..\..\src\MapCanvas.htm"; DestDir: "{app}"

[Icons]
Name: "{group}\About CRMapCtl"; Filename: "{app}\About.htm"
Name: "{group}\CRMapCtl IE Test"; Filename: "{app}\MapCanvas.htm"
Name: "{group}\Uninstall CRMapCtl"; Filename: "{uninstallexe}"
