// GetDeviceID.cpp : Defines the entry point for the application.
//
// 使用说明
// 1) 将本程序复制到WinCE设备然后运行, 生成 DeviceID.txt,
// 2) 将 DeviceID.txt 传回CRMapCtl软件开发商, 生成 DeviceID.LIC
// 3) CRMapCtl软件开发商根据 DeviceID.txt 生成 DeviceID.LIC
// 4) DeviceID.LIC 必须与 CRMapCtl 控件安装设备在同一目录下
// 5) 运行 RegWCE.exe程序以注册 CRMapCtl 控件
//
#include "stdafx.h"

#ifdef POCKETPC2003_UI_MODEL
#include "resourceppc.h"
#endif

#ifdef SMARTPHONE2003_UI_MODEL
#include "resourcesp.h"
#endif

#include <windows.h>
#include <commctrl.h>

#include "Macros.h"

#include "../src/LicenseManager.h"

#define MAX_LOADSTRING 100

#define DEVICEID_TXT   L"DeviceID.txt"
//
// DEVICEID_TXT 文件格式说明:
//
// {设备号} 时间
// {设备号} 时间
//

// Global Variables:
HINSTANCE           g_hInst;            // current instance
HWND                g_hWndMenuBar;      // menu bar handle


// Buffers to hold the two device IDs we are going to generate
BYTE                g_bDeviceID1[GETDEVICEUNIQUEID_V1_OUTPUT];
long                g_lSeed;

// Lengths of the returned device IDs
DWORD               g_cbDeviceID1;

// DeviceID.txt File
WCHAR               g_DeviceIDFile[MAX_PATH];

// Forward declarations of functions included in this code module:
ATOM            MyRegisterClass(HINSTANCE, LPTSTR);
BOOL            InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
#ifndef WIN32_PLATFORM_WFSP
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
#endif // !WIN32_PLATFORM_WFSP


/**
 *    Call the GetDeviceUniqueID function twice using different
 *    Application Specific Data to demonstrate that different
 *    values are returned. This allows different applications to
 *    have a device ID that can be used to identify a device. If 
 *    two (or more) applications need to use the same device ID then
 *    they need to use the same Application Specific Data when calling
 *    GetDeviceUniqueID.
 */
#ifdef GET_DEVICE_UNIQUE_ID

static HRESULT CreateDeviceID ()
{
    HRESULT            hr;

    // CRMapCtlLib UUID = {C3E5700C-B71C-408e-AE89-7780DFD98EE5}
    static const GUID bApplicationData1  = {
        0xC3E5700C, 0xB71C, 0x408e, { 0xAE, 0x89, 0x77, 0x80, 0xDF, 0xD9, 0x8E, 0xE5 }
    };
    const DWORD    cbApplicationData1 = sizeof (bApplicationData1);

    g_cbDeviceID1 = GETDEVICEUNIQUEID_V1_OUTPUT;
    hr = GetDeviceUniqueID (reinterpret_cast<LPBYTE>(const_cast<LPGUID>(&bApplicationData1)), 
                             cbApplicationData1, 
                             GETDEVICEUNIQUEID_V1, 
                             g_bDeviceID1, 
                             &g_cbDeviceID1);

    RC4_encrypt_string((char*)g_bDeviceID1, g_cbDeviceID1, (char*)&g_bDeviceID1[4], 16);
    g_lSeed = get_seed(g_bDeviceID1, g_cbDeviceID1);

    return hr;
}
#else
    static HRESULT CreateDeviceID ()
    {
        return E_NOTIMPL;
    }
#endif


int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPTSTR    lpCmdLine,
                   int       nCmdShow)
{
    MSG msg;

    // 取得文件全路径名
    GetModulePathW(hInstance, g_DeviceIDFile, MAX_PATH);
    wcscat(g_DeviceIDFile, DEVICEID_TXT);

    // Perform application initialization:
    if (!InitInstance(hInstance, nCmdShow)) 
    {
        return FALSE;
    }

#ifndef WIN32_PLATFORM_WFSP
    HACCEL hAccelTable;
    hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_GETDEVICEID));
#endif // !WIN32_PLATFORM_WFSP

    // Main message loop:
    while (GetMessage(&msg, NULL, 0, 0)) 
    {
#ifndef WIN32_PLATFORM_WFSP
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) 
#endif // !WIN32_PLATFORM_WFSP
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
ATOM MyRegisterClass(HINSTANCE hInstance, LPTSTR szWindowClass)
{
    WNDCLASS wc;

    wc.style         = CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc   = WndProc;
    wc.cbClsExtra    = 0;
    wc.cbWndExtra    = 0;
    wc.hInstance     = hInstance;
    wc.hIcon         = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_GETDEVICEID));
    wc.hCursor       = 0;
    wc.hbrBackground = (HBRUSH) GetStockObject(WHITE_BRUSH);
    wc.lpszMenuName  = 0;
    wc.lpszClassName = szWindowClass;

    return RegisterClass(&wc);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
    HWND hWnd;
    TCHAR szTitle[MAX_LOADSTRING];        // title bar text
    TCHAR szWindowClass[MAX_LOADSTRING];    // main window class name

    g_hInst = hInstance; // Store instance handle in our global variable

#ifdef WIN32_PLATFORM_PSPC
    // SHInitExtraControls should be called once during your application's initialization to initialize any
    // of the Pocket PC special controls such as CAPEDIT and SIPPREF.
    SHInitExtraControls();
#endif // WIN32_PLATFORM_PSPC

    LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING); 
    LoadString(hInstance, IDC_GETDEVICEID, szWindowClass, MAX_LOADSTRING);

#if defined(WIN32_PLATFORM_PSPC) || defined(WIN32_PLATFORM_WFSP)
    //If it is already running, then focus on the window, and exit
    hWnd = FindWindow(szWindowClass, szTitle);    
    if (hWnd) 
    {
        // set focus to foremost child window
        // The "| 0x00000001" is used to bring any owned windows to the foreground and
        // activate them.
        SetForegroundWindow((HWND)((ULONG) hWnd | 0x00000001));
        return 0;
    } 
#endif // WIN32_PLATFORM_PSPC || WIN32_PLATFORM_WFSP

    if (!MyRegisterClass(hInstance, szWindowClass))
    {
        return FALSE;
    }

    hWnd = CreateWindow(szWindowClass, szTitle, WS_VISIBLE,
        CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, hInstance, NULL);

    if (!hWnd)
    {
        return FALSE;
    }

#ifdef WIN32_PLATFORM_PSPC
    // When the main window is created using CW_USEDEFAULT the height of the menubar (if one
    // is created is not taken into account). So we resize the window after creating it
    // if a menubar is present
    if (g_hWndMenuBar)
    {
        RECT rc;
        RECT rcMenuBar;

        GetWindowRect(hWnd, &rc);
        GetWindowRect(g_hWndMenuBar, &rcMenuBar);
        rc.bottom -= (rcMenuBar.bottom - rcMenuBar.top);
        
        MoveWindow(hWnd, rc.left, rc.top, rc.right-rc.left, rc.bottom-rc.top, FALSE);
    }
#endif // WIN32_PLATFORM_PSPC

    // Get the application specific device IDs (to display later)
    if (FAILED (CreateDeviceID())) {
        return FALSE;
    }

    ShowWindow(hWnd, nCmdShow);
    UpdateWindow(hWnd);

    return TRUE;
}


static void OnPaint (HWND hWnd, HFONT hFont)
{
    HDC            hdc;
    PAINTSTRUCT    ps;
    RECT           rt;
    TEXTMETRIC     tm;
    TCHAR          szDeviceID[(GETDEVICEUNIQUEID_V1_OUTPUT * 2) + 1];
    TCHAR          szHeader[MAX_LOADSTRING];

    hdc = BeginPaint(hWnd, &ps);
    
    if (hFont)
    {
        SelectObject (hdc, hFont);
    }
    GetClientRect(hWnd, &rt);
    GetTextMetrics (hdc, &tm);

    // Display the first ID
    if (SUCCEEDED (DeviceID2String (g_bDeviceID1, g_cbDeviceID1,
        szDeviceID, ARRAYSIZE (szDeviceID))))
    {
        LoadString(g_hInst, IDS_DEVICEID, szHeader, MAX_LOADSTRING);

        char  *pbData = 0;
        int    cbData = wstr2ansi(szDeviceID, &pbData);
        char   hashDevId[33];

        MD5_hash_string((unsigned char*)pbData, cbData, g_lSeed, hashDevId);
        free(pbData);

        hashDevId[32] = 0;
        wchar_t  *pwData = 0;
        ansi2wstr(hashDevId, &pwData);

        rt.bottom = tm.tmHeight + tm.tmExternalLeading;
        DrawText (hdc, szHeader, -1, &rt, DT_CENTER);

        rt.top = rt.bottom;
        rt.bottom += tm.tmHeight + tm.tmExternalLeading;
        DrawText (hdc, pwData, 8, &rt, DT_CENTER);
            
        rt.top = rt.bottom;
        rt.bottom += tm.tmHeight + tm.tmExternalLeading;
        DrawText (hdc, &pwData[8], 8, &rt, DT_CENTER);

        rt.top = rt.bottom;
        rt.bottom += tm.tmHeight + tm.tmExternalLeading;
        DrawText (hdc, &pwData[16], 8, &rt, DT_CENTER);

        rt.top = rt.bottom;
        rt.bottom += tm.tmHeight + tm.tmExternalLeading;
        DrawText (hdc, &pwData[24], 8, &rt, DT_CENTER);

        free(pwData);

        // 写设备号到文件中
        AppendDeviceID(g_DeviceIDFile, hashDevId, 32);

        rt.top = rt.bottom;
        rt.bottom += tm.tmHeight + tm.tmExternalLeading;
        DrawText (hdc, L"-------------------", -1, &rt, DT_CENTER);

        rt.top = rt.bottom;
        rt.bottom += tm.tmHeight + tm.tmExternalLeading;
        DrawText (hdc, L"Copy files:", -1, &rt, DT_CENTER);

        rt.top = rt.bottom;
        rt.bottom += tm.tmHeight + tm.tmExternalLeading;
        DrawText (hdc, L"\"GetDeviceID.exe\"", -1, &rt, DT_CENTER);

        rt.top = rt.bottom;
        rt.bottom += tm.tmHeight + tm.tmExternalLeading;
        DrawText (hdc, L"\"DeviceID.txt\"", -1, &rt, DT_CENTER);

        rt.top = rt.bottom;
        rt.bottom += tm.tmHeight + tm.tmExternalLeading;
        DrawText (hdc, L"to a new CE device and", -1, &rt, DT_CENTER);

        rt.top = rt.bottom;
        rt.bottom += tm.tmHeight + tm.tmExternalLeading;
        DrawText (hdc, L"get the DeviceID.", -1, &rt, DT_CENTER);
    }

    EndPaint(hWnd, &ps);
}


//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    int                wmId, wmEvent;
    static HFONT    hFont;

#if defined(SHELL_AYGSHELL) && !defined(WIN32_PLATFORM_WFSP)
    static SHACTIVATEINFO s_sai;
#endif // SHELL_AYGSHELL && !WIN32_PLATFORM_WFSP
    
    switch (message) 
    {
        case WM_COMMAND:
            wmId    = LOWORD(wParam); 
            wmEvent = HIWORD(wParam); 
            // Parse the menu selections:
            switch (wmId)
            {
#ifndef WIN32_PLATFORM_WFSP
                case IDM_HELP_ABOUT:
                    DialogBox(g_hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, About);
                    break;
#endif // !WIN32_PLATFORM_WFSP
#ifdef WIN32_PLATFORM_WFSP
                case IDM_OK:
                    DestroyWindow(hWnd);
                    break;
#endif // WIN32_PLATFORM_WFSP
#ifndef WIN32_PLATFORM_WFSP
                case IDM_OK:
                    SendMessage (hWnd, WM_CLOSE, 0, 0);                
                    break;
#endif // !WIN32_PLATFORM_WFSP
                default:
                    return DefWindowProc(hWnd, message, wParam, lParam);
            }
            break;
        case WM_CREATE:
#ifdef SHELL_AYGSHELL
            SHMENUBARINFO mbi;

            memset(&mbi, 0, sizeof(SHMENUBARINFO));
            mbi.cbSize     = sizeof(SHMENUBARINFO);
            mbi.hwndParent = hWnd;
            mbi.nToolBarId = IDR_MENU;
            mbi.hInstRes   = g_hInst;

            if (!SHCreateMenuBar(&mbi)) 
            {
                g_hWndMenuBar = NULL;
            }
            else
            {
                g_hWndMenuBar = mbi.hwndMB;
            }

#ifndef WIN32_PLATFORM_WFSP
            // Initialize the shell activate info structure
            memset(&s_sai, 0, sizeof (s_sai));
            s_sai.cbSize = sizeof (s_sai);
#endif // !WIN32_PLATFORM_WFSP
#endif // SHELL_AYGSHELL

            // Create a fixed pitch font to display results
            LOGFONT lf;
            HDC        hdc;
            memset (&lf, 0, sizeof (lf));

            hdc = GetDC (hWnd);

            lf.lfHeight = -MulDiv (14, GetDeviceCaps (hdc, LOGPIXELSY), 72);
            lf.lfCharSet = DEFAULT_CHARSET;
            lf.lfWeight = FW_NORMAL;
            lf.lfPitchAndFamily = FIXED_PITCH || FF_DONTCARE;
            hFont = CreateFontIndirect (&lf);
            ReleaseDC (hWnd, hdc);

            break;
        case WM_PAINT:
            OnPaint (hWnd, hFont);
            break;
        case WM_DESTROY:
#ifdef SHELL_AYGSHELL
            CommandBar_Destroy(g_hWndMenuBar);
#endif // SHELL_AYGSHELL
            PostQuitMessage(0);
            break;

#if defined(SHELL_AYGSHELL) && !defined(WIN32_PLATFORM_WFSP)
        case WM_ACTIVATE:
            // Notify shell of our activate message
            SHHandleWMActivate(hWnd, wParam, lParam, &s_sai, FALSE);
            break;
        case WM_SETTINGCHANGE:
            SHHandleWMSettingChange(hWnd, wParam, lParam, &s_sai);
            break;
#endif // SHELL_AYGSHELL && !WIN32_PLATFORM_WFSP

        default:
            return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

#ifndef WIN32_PLATFORM_WFSP
// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
        case WM_INITDIALOG:
#ifdef SHELL_AYGSHELL
            {
                // Create a Done button and size it.  
                SHINITDLGINFO shidi;
                shidi.dwMask = SHIDIM_FLAGS;
                shidi.dwFlags = SHIDIF_DONEBUTTON | SHIDIF_SIPDOWN | SHIDIF_SIZEDLGFULLSCREEN | SHIDIF_EMPTYMENU;
                shidi.hDlg = hDlg;
                SHInitDialog(&shidi);
            }
#endif // SHELL_AYGSHELL

            return (INT_PTR)TRUE;

        case WM_COMMAND:
#ifdef SHELL_AYGSHELL
            if (LOWORD(wParam) == IDOK)
#endif
            {
                EndDialog(hDlg, LOWORD(wParam));
                return (INT_PTR)TRUE;
            }
            break;

        case WM_CLOSE:
            EndDialog(hDlg, message);
            return (INT_PTR)TRUE;

#ifdef _DEVICE_RESOLUTION_AWARE
        case WM_SIZE:
            {
                DRA::RelayoutDialog(
                    g_hInst, 
                    hDlg, 
                    DRA::GetDisplayMode() != DRA::Portrait ?
                        MAKEINTRESOURCE(IDD_ABOUTBOX_WIDE) : MAKEINTRESOURCE(IDD_ABOUTBOX));
            }
            break;
#endif
    }
    return (INT_PTR)FALSE;
}
#endif // !WIN32_PLATFORM_WFSP
