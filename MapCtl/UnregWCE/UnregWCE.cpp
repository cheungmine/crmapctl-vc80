// UnregWCE.cpp
//

#include "../src/RegsvrCE.h"

#define WCE_DLL      L"CRMapCtl.dll"
#define UNREG_WCE    L"UnregWCE CRMapCtl.dll"


int _tmain(int argc, _TCHAR* argv[])
{
    WCHAR ERR_MSG[128];
    WCHAR INFO_MSG[256];

    int ret = UnregsvrWCE (WCE_DLL, ERR_MSG);

    if (ret == REGCE_ERROR_LOADLIBRARY) {
        wsprintfW(INFO_MSG, L"Error on LoadLibrary '%s': %s", WCE_DLL, ERR_MSG);

        ::MessageBoxW(0, INFO_MSG, UNREG_WCE, MB_OK|MB_ICONSTOP);
    } else if (ret == REGCE_ERROR_GET_DLLUNREG_PROC) {
        wsprintfW(INFO_MSG, L"Error on GetProcAddress '%s': %s", DLLUNREG_PROC, ERR_MSG);

        ::MessageBoxW(0, INFO_MSG, UNREG_WCE, MB_OK|MB_ICONSTOP);
    } else if (ret == REGCE_ERROR_CALL_DLLUNREG_PROC) {
        wsprintfW(INFO_MSG, L"Error on %s: %s", DLLUNREG_PROC, ERR_MSG);

        ::MessageBoxW(0, INFO_MSG, UNREG_WCE, MB_OK|MB_ICONSTOP);
    } else if (ret == REGCE_SUCCESS) {
        wsprintfW(INFO_MSG, L"Success on %s: %s", DLLUNREG_PROC, WCE_DLL);

        ::MessageBoxW(0, INFO_MSG, UNREG_WCE, MB_OK|MB_ICONINFORMATION);
    } else {
        wsprintfW(INFO_MSG, L"Error on %s: %s", DLLUNREG_PROC, WCE_DLL);

        ::MessageBoxW(0, INFO_MSG, UNREG_WCE, MB_OK|MB_ICONSTOP);
    }

	return 0;
}
