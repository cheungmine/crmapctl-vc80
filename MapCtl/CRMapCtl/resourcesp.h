//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by CRMapCtlsp.rc
//
#define IDS_PROJNAME                    100
#define IDR_CRMAPCTL                    101
#define IDR_MAPCANVAS                   102
#define IDR_LAYERS                      103
#define IDR_LAYER                       104
#define IDR_SHAPES                      105
#define IDR_SHAPE                       106
#define IDR_COLOR2                      107
#define IDR_CONNECTIONS                 108
#define IDR_CONNECTION                  109
#define IDR_RENDERS                     110
#define IDR_RENDER                      111
#define IDR_SYMBOLS                     112
#define IDR_SYMBOL                      113
#define IDR_RASTERS                     114
#define IDR_RASTER                      115
#define IDR_BOUND                       116
#define IDR_DATASET                     117

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        201
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           118
#endif
#endif
