// CRMapCtl.cpp : DLL 导出的实现。
//

#include "stdafx.h"

#ifdef POCKETPC2003_UI_MODEL
#include "resourceppc.h"
#endif

#ifdef SMARTPHONE2003_UI_MODEL
#include "resourcesp.h"
#endif

#ifdef STANDARDSHELL_UI_MODEL
#include "resource.h"
#endif

#include "CRMapCtl.h"

#include "../src/LicenseManager.h"
#include "../src/Config.h"

class CCRMapCtlModule : public CAtlDllModuleT< CCRMapCtlModule >
{
public :
	DECLARE_LIBID(LIBID_CRMapCtlLib)
#ifndef _CE_DCOM
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_CRMAPCTL, "C3E5700C-B71C-f000-AE89-7780DFD98EE5")
#endif

    CCRMapCtlModule ()
    {
        LOG_INIT();
        LOG_TRACE0();
    }

    ~CCRMapCtlModule ()
    {
        LOG_TRACE0();
        LOG_FINI();
    }
};

CCRMapCtlModule _AtlModule;


// 版权验证, 此处有异常将导致CRMapCtl.dll注册失败
//
static BOOL ValidateLicense ()
{
    return TRUE;
}


// DLL 入口点
extern "C" BOOL WINAPI DllMain(HANDLE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	hInstance;
    return _AtlModule.DllMain(dwReason, lpReserved); 
}


// 用于确定 DLL 是否可由 OLE 卸载
STDAPI DllCanUnloadNow(void)
{
    return _AtlModule.DllCanUnloadNow();
}


// 返回一个类工厂以创建所请求类型的对象
STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID* ppv)
{
    return _AtlModule.DllGetClassObject(rclsid, riid, ppv);
}


// DllRegisterServer - 将项添加到系统注册表
STDAPI DllRegisterServer(void)
{
    // 在此加入 Licence 检验的代码
    if (! ValidateLicense()) {
        return E_ACCESSDENIED;
    }

    // 注册对象、类型库和类型库中的所有接口
    HRESULT hr = _AtlModule.DllRegisterServer();
	return hr;
}


// DllUnregisterServer - 将项从系统注册表中移除
STDAPI DllUnregisterServer(void)
{
	HRESULT hr = _AtlModule.DllUnregisterServer(FALSE);
	return hr;
}

